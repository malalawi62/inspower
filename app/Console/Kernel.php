<?php

namespace App\Console;

use App\Events\sendEmailToSubscriberEvent;
use App\Mail\WelcomeMail;
use App\subscribers;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Mail;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            $subscribers = subscribers::all();
            for($i=0; $i<sizeof($subscribers); $i++) {
                Mail::to($subscribers[$i]->email)->send(new WelcomeMail($subscribers[$i]->email));
            }
        })->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
