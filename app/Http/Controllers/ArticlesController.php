<?php

namespace App\Http\Controllers;

use App\articles;
use App\categories;
use App\Mail\WelcomeMail;
use App\memberships;
use App\subscribers;
use App\writers;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class ArticlesController extends Controller
{
    public function index()
    {
        $articleView = "articles";

        if (Auth::user()){
            $membership = true;
            $membership_details = memberships::where('user_id',auth()->user()->id)->value('type');
        }
        else {
            $membership = false;
            $membership_details = null;
        }


        if (App::isLocale('en')) {
            $articles = articles::whereLanguage('Eng')->orderByDesc('id')->paginate(20);
            $lang = 'en';
        } else {
            $articles = articles::whereLanguage('Ar')->orderByDesc('id')->paginate(20);
            $lang = 'ar';
        }
        $categories = categories::all();
        $writers = writers::all();
        $data = [
            'articles' => $articles,
            'categories' => $categories,
            'writers' => $writers,
            'lang' => $lang
        ];
        return view('articles.index', compact('data' , 'membership','membership_details', 'articleView')); //"compact" is to pass database rows to the view
    }

    public function create()
    {
        if (App::isLocale('en')) {
            $lang = 'en';
        } else
            $lang = 'ar';
        $articles = new articles();
        $categories = categories::all();
        $writers = writers::all();
        $data = [
            'articles' => $articles,
            'categories' => $categories,
            'writers' => $writers,
            'lang' => $lang
        ];

        return view('articles.create', compact('data'));
    }

    public function store()
    {
        $article = articles::create($this->validateData());
        $this->storeImage($article);
        $subscriber = subscribers::all();
        for ($i = 0; $i < sizeof($subscriber); $i++)
            Mail::to($subscriber[$i]->email)->send(new WelcomeMail());
        return redirect('articles/' . $article->id);
    }

    public function show(articles $article)
    {
        if (App::isLocale('en')) {
            $lang = 'en';
        } else
            $lang = 'ar';
        $writers = writers::all();
        $categories = categories::all();
        $data = [
            'writers' => $writers,
            'categories' => $categories,
            'article' => $article,
            'lang' => $lang
        ];
        $title = $article->title;

        return view('articles.show', compact('data', 'title'));
    }

    public function edit(articles $article)
    {
        if (App::isLocale('en')) {
            $lang = 'en';
        } else
            $lang = 'ar';
        $categories = categories::all();
        $writers = writers::all();
        $data = [
            'article' => $article,
            'categories' => $categories,
            'writers' => $writers,
            'lang' => $lang
        ];
        return view('articles.edit', compact('data'));
    }

    public function update(articles $article)
    {
        $article->update($this->validateData());
        $this->storeImage($article);
        return redirect('articles');
    }

    public function destroy(articles $article)
    {
        $article->delete();
        return redirect('articles');
    }

    //validate inputs
    protected function validateData()
    {
        return tap(request()->validate([
            'title' => 'required',
            'date' => 'required',
            'body' => 'required',
            'language' => 'required',
            'category_id' => 'required',
            'writerName' => 'required_without:writer_id', //one of the must be filled
            'writer_id' => 'required_without:writerName',
            'public' => ''
        ]), function () {
            if (request()->hasFile('image')) {
                //we are not validating the image with the required fields at thr top because it is not
                //required in the edit form (same if you don't want it to be required always)
                request()->validate([
                    'image' => 'file|image|max:10000'
                ]);
            }
        });
    }

    private function storeImage($article)
    {
        if (request()->has('image')) {
            $article->update([
                'image' => request()->image->store('uploads', 'public')
            ]);
        }
    }
}
