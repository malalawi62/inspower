<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\memberships;
use App\membershipTypes;
use App\Providers\RouteServiceProvider;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/storeMembership';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users', 'confirmed'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'DOB' => [],
            'gender' => [],
        ]);
    }

    public function exclusive(){
        return view('auth.registerExclusive');
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $date = Carbon::parse($data['DOB'])->toDateString();
        $gender = $data['gender'];
        $type = $data['type'];
        $startDate = \Carbon\Carbon::now();
        $monthlyAmount = membershipTypes::where('type', 'Monthly')->first();
        $yearlyAmount = membershipTypes::where('type', 'Yearly')->first();

         if ($type == 'month'){
             $endDate = $startDate->modify('+1 month');
             $amount = $monthlyAmount->fees;
         }

         else if ($type == 'year'){
             $endDate = $startDate->modify('+1 year');
             $amount = $yearlyAmount->fees;
         }

         else {
             $endDate = null;
             $amount = 0;
         }

        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'DOB' => $date,
            'gender' => $gender,
            'password' => Hash::make($data['password']),
        ]);

        if ($type != 'free'){
            memberships::create([
                'user_id' => $user->id,
                'type' => $type,
                "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
                'startDate' => \Carbon\Carbon::now(),
                'endDate' => $endDate
            ]);
        }

        return $user;

    }
}
