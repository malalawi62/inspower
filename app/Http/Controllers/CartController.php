<?php

namespace App\Http\Controllers;

use App\cart;
use App\course;
use App\program;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use phpDocumentor\Reflection\Types\Array_;

class CartController extends Controller
{

    public function addToCart()
    {
        //
    }

    public function store()
    {
        if (request()->post('course_id')){
            if(cart::where('user_id',auth()->user()->id)->where('course_id', request()->post('course_id'))->count() == 0){
                cart::create($this->validateData());
                $notification = array(
                    'message' => 'Course added to your cart',
                    'messageAr' => 'تمت إضافة الدورة التدربية لسلة مشترياتك',
                    'alert-type' => 'success'
                );
                return Redirect::back()->with($notification);
            }
            else{
                $notification = array(
                    'message' => 'This course already in your cart',
                    'messageAr' => 'هذه الدورة مُضافة لديك مسبقًا',
                    'alert-type' => 'error'
                );
                return Redirect::back()->with($notification);
            }
        }
        if (request()->post('program_id')){
            if(cart::where('user_id',auth()->user()->id)->where('program_id', request()->post('program_id'))->count() == 0){
                cart::create($this->validateData());
                $notification = array(
                    'message' => 'Program added to your cart',
                    'messageAr' => 'تم إضافة البرنامج التدربي لسلة مشترياتك',
                    'alert-type' => 'success'
                );
                return Redirect::back()->with($notification);
            }
            else{
                $notification = array(
                    'message' => 'This program already in your cart',
                    'messageAr' => 'هذا البرنامج مُضاف لديك مسبقًا',
                    'alert-type' => 'error'
                );
                return Redirect::back()->with($notification);
            }
        }
    }

    public function viewCart()
    {
        $details = $this->cartDetailsCode();
        $courses = $details['courses'];
        $programs = $details['programs'];
        $counter = $details['counter'];

        return view('cart.cart', compact('courses', 'programs', 'counter'));
    }

    public function storeOrder(){
        $courses = null;
        $programs = null;
        $counter = 0;
        $allCourses[] = null;
        $allCourses2[] = null;
        $courseIds = null;
        $courseTitles = null;
        $allPrograms[] = null;
        $allPrograms2[] = null;
        $programIds = null;
        $programTitles = null;

        $cartData = cart::where('user_id', auth()->user()->id)->get();

        for($i=0; $i<count($cartData); $i++){
            if($cartData[$i]->course_id != null){
                $course = course::where('id', $cartData[$i]->course_id)->first();
                $courses[] = $course;
                $counter+=$course->price;
            }
            else{
                $program = program::where('id', $cartData[$i]->program_id)->first();
                $programs[] = $program;
                $counter+=$program->price;
            }
        }

        if ($courses != null){
            for ($i=0; $i<count($courses); $i++){
                $allCourses[] = $courses[$i]->id;
                $allCourses2[] = $courses[$i]->title;
            }
            $courseIds = implode(",",$allCourses);
            $courseTitles = implode(", ",$allCourses2);
        }

        if ($programs != null){
            for ($i=0; $i<count($programs); $i++){
                $allPrograms[] = $programs[$i]->id;
                $allPrograms2[] = $programs[$i]->title;
            }
            $programIds = implode(",",$allPrograms);
            $programTitles = implode(", ",$allPrograms2);
        }

        $order_id = DB::table('orders')->insertGetId([
            'courses' => $courseIds,
            'programs' => $programIds,
            'user_id' => auth()->user()->id,
            'amount' => $counter,
            "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
            "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
        ]);

        return redirect('/confirm/'.$order_id);
    }

    public function confirm($order_id)
    {

        $courses = null;
        $programs = null;
        $counter = 0;
        $allCourses[] = null;
        $allCourses2[] = null;
        $courseIds = null;
        $courseTitles = null;
        $allPrograms[] = null;
        $allPrograms2[] = null;
        $programIds = null;
        $programTitles = null;

        $cartData = cart::where('user_id', auth()->user()->id)->get();

        for($i=0; $i<count($cartData); $i++){
            if($cartData[$i]->course_id != null){
                $course = course::where('id', $cartData[$i]->course_id)->first();
                $courses[] = $course;
                $counter+=$course->price;
            }
            else{
                $program = program::where('id', $cartData[$i]->program_id)->first();
                $programs[] = $program;
                $counter+=$program->price;
            }
        }

        if ($courses != null){
            for ($i=0; $i<count($courses); $i++){
                $allCourses[] = $courses[$i]->id;
                $allCourses2[] = $courses[$i]->title;
            }
            $courseIds = implode(",",$allCourses);
            $courseTitles = implode(", ",$allCourses2);
        }

        if ($programs != null){
            for ($i=0; $i<count($programs); $i++){
                $allPrograms[] = $programs[$i]->id;
                $allPrograms2[] = $programs[$i]->title;
            }
            $programIds = implode(",",$allPrograms);
            $programTitles = implode(", ",$allPrograms2);
        }

        /*$order_id = DB::table('orders')->insertGetId([
            'courses' => $courseIds,
            'programs' => $programIds,
            'user_id' => auth()->user()->id,
            'amount' => $counter,
            "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
            "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
        ]);*/

        return view('cart.confirmation', compact('courses', 'programs', 'counter', 'order_id', 'courseTitles', 'programTitles'));
    }

    public function cartDetailsCode(){
        $details = array (
            'courses' => null,
            'programs' => null,
            'counter' => 0
        );

        $cartData = cart::where('user_id', auth()->user()->id)->get();

        for($i=0; $i<count($cartData); $i++){
            if($cartData[$i]->course_id != null){
                $course = course::where('id', $cartData[$i]->course_id)->first();
                $details['courses'][] = $course;
                $details['counter']+=$course->price;
            }
            else{
                $program = program::where('id', $cartData[$i]->program_id)->first();
                $details['programs'][] = $program;
                $details['counter']+=$program->price;
            }
        }

        return $details;
    }

    public function removeProgramFromCart($program_id)
    {
        $program = cart::where('user_id',auth()->user()->id)->where('program_id', $program_id)->first();
        $program->delete();
        return redirect('/viewCart');
    }

    public function removeCourseFromCart($course_id)
    {
        $course = cart::where('user_id',auth()->user()->id)->where('course_id', $course_id)->first();
        $course->delete();
        return redirect('/viewCart');
    }

    //validate inputs
    protected function validateData(){
        return request()->validate([
            'user_id' => '',
            'program_id' => '',
            'course_id' => '',
        ]);
    }

    //validate inputs
    protected function validateOrderData(){
        return request()->validate([
            'user_id' => '',
            'programs' => '',
            'courses' => '',
        ]);
    }
}
