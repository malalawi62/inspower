<?php

namespace App\Http\Controllers;

use App\categories;
use App\Mail\WelcomeMail;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;

class CategoriesController extends Controller
{
    public function index()
    {
        if(App::isLocale('en'))
            $lang='en';
        else
            $lang='ar';
        $categories = categories::all();
        $data=[
            'categories'=>$categories,
            'lang'=> $lang,
        ];
        return view('categories.index', compact('data')); //"compact" is to pass database rows to the view
    }

    public function create()
    {
        $categories = new categories();
        return view('categories.create', compact('categories'));
    }

    public function store()
    {
        categories::create($this->validateData());
        return redirect('categories');
    }

    public function edit(categories $category)
    {
        return view('categories.edit', compact('category'));
    }

    public function update(categories $category)
    {
        $category->update($this->validateData());
        return redirect('categories');
    }

    public function destroy(categories $category)
    {
        $category->delete();
        return redirect('categories');
    }

    //validate inputs
    protected function validateData(){
        return request()->validate([
            'englishName' => 'required',
            'arabicName' => 'required',
        ]);
    }
}
