<?php

namespace App\Http\Controllers;

use App\cart;
use App\course;
use App\courseModule;
use App\courseModuleVideo;
use App\Purchase;
use App\User;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class CourseController extends Controller
{
    public function index()
    {
        $orders = null;
        if(App::isLocale('en')){
            $lang='en';
            $courses = course::where('language','Eng')->orderByDesc('id')->get();
        }
        else{
            $lang='ar';
            $courses = course::where('language','Ar')->orderByDesc('id')->get();
        }

        $cart = new cart();
        if (Auth::user()) {
            $orders = Purchase::where('user_id',auth()->user()->id)->where('course_id', '!=', null)->orderByDesc('id')->get();
        }

        return view('courses.index', compact('courses', 'lang','cart' , 'orders')); //"compact" is to pass database rows to the view
    }

    public function create()
    {
        $course = new course();
        if (App::isLocale('en')) {
            $lang = 'en';
        } else
            $lang = 'ar';
        return view('courses.create', compact('course', 'lang'));
    }

    public function store()
    {
        $course = course::create($this->validateData());

        //to store image course
        $this->storeImage($course, null);

        //to take only the video ID without the full link
        $videoUrl = \request()->post('videoUrl');
        if ($videoUrl != null){
            $url = explode('https://vimeo.com/', $videoUrl);
            $course->update([
                'videoUrl' => $url[1]
            ]);
        }

        $this->storeModuleCode($course->id);

        $admins = User::where('type', 'admin')->get();
        foreach ($admins as $admin){
            DB::table('purchases')->insert([
                'course_id' => $course->id,
                'user_id' => $admin->id,
                "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
            ]);
        }

        return redirect('course/' . $course->id);
    }

    public function createModule()
    {
        $course_id = \request()->get('course_id');
        return view('courses.createModule', compact('course_id'));
    }

    public function storeModule(){
        $course = \request()->post('course_id');
        $this->storeModuleCode($course);

        return redirect()->back()->with('alert', "Module Has Been Created Successfully");
    }

    public function createLesson()
    {
        $module_id = \request()->get('module_id');
        return view('courses.createLesson', compact('module_id'));
    }

    public function storeLesson(){
        $admins = User::where('type', 'admin')->get();
        $module = \request()->post('module_id');
        $m_videoUrl = \request()->post('m_videoUrl');
        $v_description = \request()->post('v_description');
        $pdf = \request()->file('pdf');

        $this->storeLessonCode($m_videoUrl, $admins, $module, $v_description, $pdf);

        return redirect()->back()->with('alert', "Lesson Has Been Created Successfully");
    }

    public function show(course $course)
    {
        $orders = null;
        $purchased=false;
        $status = null;
        if (Auth::user()) {
            $purchased = (count(Purchase::where('user_id', auth()->user()->id)->where('course_id', $course->id)->get()) > 0);
            if ($purchased){
                $status = DB::table('course_module_video_status')->where('user_id', auth()->user()->id)->get();
            }
            if (auth()->user()->type == 'admin'){
                $purchased = true;
            }
        }

        if (App::isLocale('en')) {
            $lang = 'en';
        } else
            $lang = 'ar';
        if (Auth::user()) {
            $orders = Purchase::where('user_id',auth()->user()->id)->where('course_id', '!=', null)->orderByDesc('id')->get();
        }

        $lessons = [];
        $modules = courseModule::where('course_id', $course->id)->get();

        for ($i=0; $i<count($modules); $i++){
            $module_lessons = courseModuleVideo::where('course_module_id', $modules[$i]->id)->get();
            foreach ($module_lessons as $ml){
                $lessons[] = $ml;
            }
        }

        if ($course->status == 'inactive' && $purchased == false){
            return redirect('courses');
        }

        elseif ($course->status == 'inactive' && auth()->user()->type == 'admin'){
            return view('courses.show', compact('course', 'lang' ,'purchased', 'orders', 'modules', 'lessons', 'status'));
        }

        elseif ($course->status == 'inactive' && $purchased == true){
            return view('courses.show', compact('course', 'lang' ,'purchased', 'orders', 'modules', 'lessons', 'status'));
        }

        elseif ($course->status == 'active'){
            return view('courses.show', compact('course', 'lang' ,'purchased', 'orders', 'modules', 'lessons', 'status'));
        }
    }

    public function edit(course $course)
    {
        if (App::isLocale('en')) {
            $lang = 'en';
        }
        else
            $lang = 'ar';

        $lessons = [];
        $modules = courseModule::where('course_id', $course->id)->get();

        for ($i=0; $i<count($modules); $i++){
            $module_lessons = courseModuleVideo::where('course_module_id', $modules[$i]->id)->get();
            foreach ($module_lessons as $ml){
                $lessons[] = $ml;
            }
        }

        return view('courses.edit', compact('course', 'lang', 'modules', 'lessons'));
    }

    public function update(course $course)
    {
        $image_path = $course->image;
        $course->update($this->validateUpdateData());
        $this->storeImage($course, $image_path);
        $videoUrl = \request()->post('videoUrl');
        if ($videoUrl != null){
            $url = explode('https://vimeo.com/', $videoUrl);
            $course->update([
                'videoUrl' => $url[1]
            ]);
        }

        /*$countModules = count(\request()->post('m_title'));
        $module_titles = \request()->post('m_title');
        $module_descriptions = \request()->post('m_description');
        $courseModuleVideos = \request()->post('m_videoUrl');
        $moduleId = \request()->post('module_id');
        $videoId = \request()->post('video_id');

        $all_modules = DB::where('course_id', $course->id);

        for ($i=0; $i<$countModules; $i++){
            if ($moduleId[$i] == null){
                $courseModule = DB::table('course_modules')->insertGetId([
                    'm_title' => $module_titles[$i],
                    'm_description' => null,
                    'course_id' => $course->id,
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
                ]);

                if ($module_descriptions[$i] != null){
                    DB::table('course_modules')->where('id', $courseModule)->update([
                        'm_description'=> $module_descriptions[$i],
                    ]);
                }

                for ($j=0; $j<count($courseModuleVideos[$i+1]); $j++){
                    $video_id = explode('https://vimeo.com/', $courseModuleVideos[$i+1][$j]);
                    if ($videoId[$i+1][$j] == null){
                        DB::table('course_module_videos')->insert([
                            'm_videoUrl' => $video_id[1],
                            'course_module_id' => $courseModule,
                            "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                            "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
                        ]);
                    }
                    else {
                        DB::table('course_module_videos')->where('id', $videoId[$i+1][$j])->update([
                            'm_videoUrl' => $video_id[1],
                            'course_module_id' => $courseModule,
                            "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
                        ]);
                    }
                }
            }
            else{
                DB::table('course_modules')->where('id', $moduleId[$i])->update([
                    'm_title' => $module_titles[$i],
                    'm_description' => null,
                    'course_id' => $course->id,
                    "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
                ]);

                if ($module_descriptions[$i] != null){
                    DB::table('course_modules')->where('id', $moduleId[$i])->update([
                        'm_description'=> $module_descriptions[$i],
                    ]);
                }

                for ($j=0; $j<count($courseModuleVideos[$i+1]); $j++){
                    $video_id = explode('https://vimeo.com/', $courseModuleVideos[$i+1][$j]);
                    if ($videoId[$i+1][$j] == null){
                        DB::table('course_module_videos')->insert([
                            'm_videoUrl' => $video_id[1],
                            'course_module_id' => $moduleId[$i],
                            "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                            "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
                        ]);
                    }
                    else {
                        DB::table('course_module_videos')->where('id', $videoId[$i+1][$j])->update([
                            'm_videoUrl' => $video_id[1],
                            'course_module_id' => $moduleId[$i],
                            "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
                        ]);
                    }
                }
            }
        }*/

        return redirect()->back()->with('alert', "Main Course Details Updated Successfully");
    }

    public function editModule($module)
    {
        if (App::isLocale('en')) {
            $lang = 'en';
        }
        else
            $lang = 'ar';

        $lessons = [];
        $modules = courseModule::where('id', $module)->first();

        $module_lessons = courseModuleVideo::where('course_module_id', $modules->id)->get();
        foreach ($module_lessons as $ml){
            $lessons[] = $ml;
        }

        return view('courses.editModule', compact('modules', 'lang', 'lessons'));
    }

    public function updateModule(courseModule $module)
    {
        $module->update($this->validateUpdateModuleData());

        return redirect()->back()->with('alert', "Module Details Updated Successfully");
    }

    public function editLesson($lesson)
    {
        if (App::isLocale('en')) {
            $lang = 'en';
        }
        else
            $lang = 'ar';

        $lesson2 = courseModuleVideo::where('id', $lesson)->first();

        return view('courses.editLesson', compact('lesson2', 'lang'));
    }

    public function updateLesson(courseModuleVideo $lesson)
    {
        $pdf_path = $lesson->pdf;
        $lesson->update($this->validateUpdateLessonData());

        $videoUrl = \request()->post('m_videoUrl');
        if ($videoUrl != null){
            $url = explode('https://vimeo.com/', $videoUrl);
            $lesson->update([
                'm_videoUrl' => $url[1]
            ]);
        }

        $pdf = request()->file('pdf');

        $this->storePdf($lesson, $pdf, $pdf_path);

        return redirect()->back()->with('alert', "Lesson Details Updated Successfully");
    }

    public function updateStatus($time, $status_id, $fullTime){
        if ($fullTime == $time){
            DB::table('course_module_video_status')->where('id', $status_id)->update([
                'currentSecond' => $time,
                'status' => 'complete'
            ]);
            return response()->json(['response'=> 'complete']);
        }
        else{
            DB::table('course_module_video_status')->where('id', $status_id)->update([
                'currentSecond' => $time,
            ]);
            $check = DB::table('course_module_video_status')->where('id', $status_id)->first();
            if ($check->status == "complete"){
                return response()->json(['response'=> 'complete']);
            }
            else{
                return response()->json(['response'=> 'success']);
            }
        }
    }

    public function destroy(course $course)
    {
        $course->delete();
        return redirect('courses');
    }

    public function destroyModule($module){
        DB::table('course_module_video_status')->where('course_module_id', $module)->delete();
        $lesson = courseModuleVideo::where('course_module_id', $module)->first();
        Storage::delete('/pdfUploads/' . $lesson->pdf);
        DB::table('course_module_videos')->where('course_module_id', $module)->delete();
        DB::table('course_modules')->where('id', $module)->delete();

        return redirect()->back()->with('alert', "Module Has Been Deleted Successfully");
    }

    public function destroyLesson($lesson){
        DB::table('course_module_video_status')->where('course_module_video_id', $lesson)->delete();
        $lesson2 = courseModuleVideo::where('id', $lesson)->first();
        Storage::delete('/pdfUploads/' . $lesson2->pdf);
        DB::table('course_module_videos')->where('id', $lesson)->delete();

        return redirect()->back()->with('alert', "Lesson Has Been Deleted Successfully");
    }

    public function active(course $course){
        DB::table('courses')->where('id', $course->id)->update([
            'status' => 'active',
        ]);
        return redirect('course/' . $course->id);
    }

    public function inactive(course $course){
        DB::table('courses')->where('id', $course->id)->update([
            'status' => 'inactive',
        ]);
        return redirect('course/' . $course->id);
    }

    //validate inputs
    protected function validateData(){
        return tap(request()->validate([
            'title' => 'required',
            'overview' => 'required',
            'price' => 'required | numeric',
            'description' => '',
            'videoUrl' => '',
            'language' => 'required',
            'image' => 'required'
        ]), function () {
            if (request()->hasFile('image')) {
                //we are not validating the image with the required fields at thr top because it is not
                //required in the edit form (same if you don't want it to be required always)
                request()->validate([
                    'image' => 'file|image|max:10000'
                ]);
            }
        });
    }

    protected function validateUpdateData(){
        return tap(request()->validate([
            'title' => 'required',
            'overview' => 'required',
            'price' => 'required | numeric',
            'description' => '',
            'videoUrl' => '',
            'language' => '',
            'image' => ''
        ]), function () {
            if (request()->hasFile('image')) {
                //we are not validating the image with the required fields at thr top because it is not
                //required in the edit form (same if you don't want it to be required always)
                request()->validate([
                    'image' => 'file|image|max:10000'
                ]);
            }
        });
    }

    protected function validateUpdateModuleData(){
        return request()->validate([
            'm_title' => 'required',
            'm_description' => '',
        ]);
    }

    protected function validateUpdateLessonData(){
        return tap(request()->validate([
            'm_videoUrl' => 'required',
            'v_description' => '',
            'pdf' => ''
        ]), function () {
            if (request()->hasFile('pdf')) {
                request()->validate([
                    "pdf" => "mimes:pdf|max:10000"
                ]);
            }
        });
    }

    private function storeImage($course, $image_path)
    {
        if (request()->has('image')) {
            $path = 'storage/'.$image_path;
            \File::delete($path);
            $course->update([
                'image' => request()->image->store('uploads', 'public')
            ]);
        }
    }

    private function storePdf($lesson, $pdf, $pdf_path)
    {
        if (request()->has('pdf')) {
            Storage::delete('/pdfUploads/' . $pdf_path);
            $path = "pdfUploads";
            $file = $pdf;
            $name = Storage::put($path, $file);
            $file_name = explode('/', $name);
            $lesson->update([
                'pdf' => $file_name[1],
                "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
            ]);
        }

    }

    private function storeModuleCode($course){
        $admins = User::where('type', 'admin')->get();

        $countModules = count(\request()->post('m_title'));
        $module_titles = \request()->post('m_title');
        $module_descriptions = \request()->post('m_description');
        $courseModuleVideos = \request()->post('m_videoUrl');
        $courseModuleVideoDescription = \request()->post('v_description');
        $pdf = request()->file('pdf');

        for ($i=0; $i<$countModules; $i++){
            $courseModule = DB::table('course_modules')->insertGetId([
                'm_title' => $module_titles[$i],
                'm_description' => null,
                'course_id' => $course,
                "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
            ]);

            if ($module_descriptions[$i] != null){
                DB::table('course_modules')->where('id', $courseModule)->update([
                    'm_description'=> $module_descriptions[$i],
                ]);
            }

            $count = $i+1;

            while(!isset($courseModuleVideos[$count])){
                $count++;
            }
            for ($j=0; $j<count($courseModuleVideos[$count]); $j++){
                $this->storeLessonCode($courseModuleVideos[$count][$j], $admins, $courseModule, $courseModuleVideoDescription[$count][$j], $pdf[$count][$j]);
            }
        }
    }

    private function storeLessonCode($videoId, $admins, $courseModule, $lessonDescription, $pdf)
    {
        $video_id = explode('https://vimeo.com/', $videoId);
        $lessonId = DB::table('course_module_videos')->insertGetId([
            'm_videoUrl' => $video_id[1],
            'course_module_id' => $courseModule,
            'v_description' => $lessonDescription,
            "created_at" => \Carbon\Carbon::now(), # new \Datetime()
            "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
        ]);

        $lesson = courseModuleVideo::where('id', $lessonId)->first();

        $this->storePdf($lesson, $pdf, null);

        foreach ($admins as $admin) {
            DB::table('course_module_video_status')->insert([
                'course_module_id' => $courseModule,
                'course_module_video_id' => $lessonId,
                'user_id' => $admin->id,
                "created_at" => \Carbon\Carbon::now(), # new \Datetime()
                "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
            ]);
        }
    }

    public function showPdfLesson($lesson){
        if (Auth::user()){
            $lessonDetails = courseModuleVideo::where('pdf', $lesson)->first();
            $module = courseModule::where('id',$lessonDetails->course_module_id)->first();
            if($lessonDetails != null){
                $order = Purchase::where('course_id', $module->course_id)->where('user_id', auth()->user()->id)->first();
                if($order != null){
                    $file = storage_path('app/pdfUploads/').$lesson;

                    if (file_exists($file)) {
                        $headers = [
                            'Content-Type' => 'application/pdf'
                        ];
                        return response()->download($file, 'Test File', $headers, 'inline');
                    }
                    else {
                        abort(404, 'File not found!');
                    }
                }
                else{
                    abort(404, 'Sorry, You Are not authorized to view this page ..');
                }
            }
        }
    }
}
