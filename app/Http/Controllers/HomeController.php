<?php

namespace App\Http\Controllers;

use App\subscribers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(){
        return redirect('/');
    }

    public function languageAr(){
        App::setLocale('ar');
        return view('welcome');
    }

    public function languageEn(){
        App::setLocale('en');
        return view('welcome');
    }
}
