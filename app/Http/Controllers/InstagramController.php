<?php

namespace App\Http\Controllers;

use App\categories;
use App\instagram;
use Illuminate\Support\Facades\App;

class InstagramController extends Controller
{
    public function create()
    {
        if (App::isLocale('en')) {
            $lang = 'en';
        }
        else
            $lang = 'ar';
        $categories = categories::all();
        $data = [
            'categories' => $categories,
            'lang' => $lang
        ];

        return view('videos.createInsta', compact('data'));
    }

    public function store()
    {
        instagram::create($this->validateData());
        return redirect('videos');
    }

    public function edit(instagram $instagram)
    {
        if (App::isLocale('en')) {
            $lang = 'en';
        }
        else
            $lang = 'ar';
        $categories = categories::all();
        $data = [
            'instagram' => $instagram,
            'categories' => $categories,
            'lang' => $lang
        ];
        return view('videos.editInsta', compact('data'));
    }

    public function update(instagram $instagram)
    {
        $instagram->update($this->validateData());
        return redirect('videos');
    }

    public function destroy(instagram $instagram)
    {
        $instagram->delete();
        return redirect('videos');
    }

    //validate inputs
    protected function validateData()
    {
        return request()->validate([
            'title' => 'required',
            'titleAr' => 'required',
            'url' => 'required',
            'category_id' => 'required',
            'description' => '',
            'descriptionAr' => ''
        ]);
    }
}
