<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\membershipTypes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class MembershipTypesController extends Controller
{
    public function index()
    {
        if(App::isLocale('en'))
            $lang='en';
        else
            $lang='ar';
        $membershipTypes = membershipTypes::all();
        return view('membershipTypes.index', compact('lang', 'membershipTypes')); //"compact" is to pass database rows to the view
    }

    public function edit(membershipTypes $membershipType)
    {
        return view('membershipTypes.edit', compact('membershipType'));
    }

    public function update(membershipTypes $membershipType)
    {
        $membershipType->update($this->validateDataUpdate());
        return redirect('membershipType');
    }

    public function active($membershipTypes){
        DB::table('membership_types')->where('id', $membershipTypes)->update([
            'status' => 'active',
            "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
        ]);
        return redirect('membershipType');
    }

    public function inactive($membershipTypes){
        DB::table('membership_types')->where('id', $membershipTypes)->update([
            'status' => 'inactive',
            "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
        ]);
        return redirect('membershipType');
    }

    protected function validateData(){
        return request()->validate([
            'type' => 'required',
            'fees' => 'required',
        ]);
    }

    protected function validateDataUpdate(){
        return request()->validate([
            'fees' => 'required',
        ]);
    }
}
