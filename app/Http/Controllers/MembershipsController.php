<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\memberships;
use App\membershipTypes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MembershipsController extends Controller
{
    public function upgrade(){
        return view('auth.registerUpgrade');
    }

    public function storeOrderMembership(){
        $checkMembership = memberships::where('user_id', auth()->user()->id)->first();
        if ($checkMembership == null){
            return redirect('/');
        }

        else{
            if($checkMembership->type == 'month'){
                $membershipType = membershipTypes::where('type', 'Monthly')->first();
            }
            else{
                $membershipType = membershipTypes::where('type', 'Yearly')->first();
            }

            $amount = $membershipType->fees;
            $type = $membershipType->type;

            $order_id = DB::table('orders')->insertGetId([
                'user_id' => auth()->user()->id,
                'amount' => $amount,
                'membership' => $type,
                'status' => "pending",
                "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
            ]);

            DB::table('memberships')->update([
                'order_id' => $order_id,
                "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
            ]);

            return view('users.confirmMembershipOrder', compact('amount', 'type', 'order_id'));
        }
    }

    public function confirm($order_id){

        return view('cart.confirmation', compact('courses', 'programs', 'counter', 'order_id', 'courseTitles', 'programTitles'));
    }
}
