<?php

namespace App\Http\Controllers;
use ZipArchive;
use App\benefit\iPayBenefitPipe;
use App\cart;
use App\course;
use App\program;
use App\orders;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use phpDocumentor\Reflection\Types\Array_;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\App;

class PaymentController extends Controller
{
    public function benefitRedirect($order_id, $counter)
    {
        try {

            // require_once('iPayBenefitPipe.php');

    	    $myObj =new iPayBenefitPipe();

        	// Do NOT change the values of the following parameters at all.
        	$myObj->setAction("1");
        	$myObj->setCurrency("048");
        	$myObj->setLanguage("USA");
        	$myObj->setType("D");

        	// modify the following to reflect your "Alias Name", "resource.cgn" file path, "keystore.pooh" file path.
        	// is this the alias? yes
        	$myObj->setAlias("test710939");
        	$myObj->setResourcePath( app_path() . "/benefit/"); //only the path that contains the file; do not write the file name
        	$myObj->setKeystorePath( app_path() . "/benefit/"); //only the path that contains the file; do not write the file name

        	// modify the following to reflect your pages URLs
        	$myObj->setResponseURL("https://www.test.inspower-fitnesscoaching.com/benefit-response");
        	$myObj->setErrorURL("https://www.test.inspower-fitnesscoaching.com/benefit-response-err");

        	// set a unique track ID for each transaction so you can use it later to match transaction response and identify transactions in your system and “BENEFIT Payment Gateway” portal.
        	$myObj->setTrackId($order_id);

        	// set transaction amount
        	$myObj->setAmt($counter);

        	// The following user-defined fields (UDF1, UDF2, UDF3, UDF4, UDF5) are optional fields.
        	// However, we recommend setting theses optional fields with invoice/product/customer identification information as they will be reflected in “BENEFIT Payment Gateway” portal where you will be able to link transactions to respective customers. This is helpful for dispute cases.
        	$myObj->setEmail("inspowerpt@gmail.com");
        	$myObj->setUdf4("set value 4");
        	$myObj->setUdf5("set value 5");

    	    if(trim($myObj->performPaymentInitializationHTTP())!=0){
    		    echo("ERROR OCCURED! SEE CONSOLE FOR MORE DETAILS");
    		    return;
    	    }
    	    else{
    		    $url=$myObj->getwebAddress();
    		    echo "<meta http-equiv='refresh' content='0;url=$url'>";
    	    }
        }
        catch (exception $e) {
            $myObj =new iPayBenefitPipe();

        	// Do NOT change the values of the following parameters at all.
        	$myObj->setAction("1");
        	$myObj->setCurrency("048");
        	$myObj->setLanguage("USA");
        	$myObj->setType("D");

    	    // modify the following to reflect your "Alias Name", "resource.cgn" file path, "keystore.pooh" file path.
        	$myObj->setAlias("test710939");
        	$myObj->setResourcePath(app_path() ."benefit/"); //only the path that contains the file; do not write the file name
        	$myObj->setKeystorePath(app_path() ."benefit/"); //only the path that contains the file; do not write the file name

        	// modify the following to reflect your pages URLs
        	$myObj->setResponseURL("https://www.test.inspower-fitnesscoaching.com/benefit-response");
        	$myObj->setErrorURL("https://www.test.inspower-fitnesscoaching.com/benefit-response-err");

        	// set a unique track ID for each transaction so you can use it later to match transaction response and identify transactions in your system and “BENEFIT Payment Gateway” portal.
        	$myObj->setTrackId($order_id);

        	// set transaction amount
        	$myObj->setAmt($counter);

        	// The following user-defined fields (UDF1, UDF2, UDF3, UDF4, UDF5) are optional fields.
        	// However, we recommend setting theses optional fields with invoice/product/customer identification information as they will be reflected in “BENEFIT Payment Gateway” portal where you will be able to link transactions to respective customers. This is helpful for dispute cases.
        	$myObj->setEmail("inspowerpt@gmail.com");
        	$myObj->setUdf4("set value 4");
        	$myObj->setUdf5("set value 5");

        	if(trim($myObj->performPaymentInitializationHTTP())!=0){
        		echo("ERROR OCCURED! SEE CONSOLE FOR MORE DETAILS");
        		return;
        	}
        	else{
        		$url=$myObj->getwebAddress();
        		echo "<meta http-equiv='refresh' content='0;url=$url'>";
        	}
        }
    }

    public function benefitResponse(){

    	//require('iPayBenefitPipe.php');

    	$myObj =new iPayBenefitPipe();

    	// modify the following to reflect your "Alias Name", "resource.cgn" file path, "keystore.pooh" file path.
	    $myObj->setAlias("test710939");
    	$myObj->setResourcePath( app_path() . "/benefit/"); //only the path that contains the file; do not write the file name
    	$myObj->setKeystorePath( app_path() . "/benefit/"); //only the path that contains the file; do not write the file name

    	$trandata = "";
    	$paymentID = "";
    	$result = "";
    	$responseCode = "";
    	$response = "";
    	$transactionID = "";
    	$referenceID = "";
    	$trackID = "";
    	$amount = "";
    	$UDF1 = "";
    	$webAddress = "";
    	$Email = "";
    	$UDF4 = "";
    	$UDF5 = "";
    	$authCode = "";
    	$postDate = "";
    	$errorCode = "";
    	$errorText = "";

	    $trandata = isset($_POST["trandata"]) ? $_POST["trandata"] : "";

    	if ($trandata != ""){
    		$returnValue = $myObj->parseEncryptedRequest($trandata);
    		if ($returnValue == 0){
    			$paymentID = $myObj->getPaymentId();
    			$result = $myObj->getRef();
    			$responseCode = $myObj->getAuthRespCode();
    			$transactionID = $myObj->getTransId();
    			$referenceID = $myObj->getRef();
    			$trackID = $myObj->getTrackId();
    			$amount = $myObj->getAmt();
    			$UDF1 = $myObj->getUdf1();
    			$webAddress = $myObj->getwebAddress();
    			$Email = $myObj->getEmail();
    			$UDF4 = $myObj->getUdf4();
    			$UDF5 = $myObj->getUdf5();
    			$authCode = $myObj->getAuth();
    			$postDate = $myObj->getDate();
    			$errorCode = $myObj->getError();
    			$errorText = $myObj->getError_text();
    		}
		    else{
			    $errorText = $myObj->getError_text();
		    }
    	}
    	else if (isset($_POST["ErrorText"])){
            $paymentID = $_POST["paymentid"];
            $trackID = $_POST["trackid"];
            $amount = $_POST["amt"];
            $UDF1 = $_POST["udf1"];
            $webAddress = $_POST["webAddress"];
            $Email = $_POST["Email"];
            $UDF4 = $_POST["udf4"];
            $UDF5 = $_POST["udf5"];
            $errorText = $_POST["ErrorText"];
        }
        else{
            $errorText = "Unknown Exception";
        }


    	// Remove any HTML/CSS/javascrip from the page. Also, you MUST NOT write anything on the page EXCEPT the word "REDIRECT=" (in upper-case only) followed by a URL.
    	// If anything else is written on the page then you will not be able to complete the process.
    	if ($myObj->getResult() == "CAPTURED"){
    		echo("REDIRECT=https://www.test.inspower-fitnesscoaching.com/benefitApproved");
    	}

    	else if ($myObj->getResult() == "NOT CAPTURED" || $myObj->getResult() == "CANCELED" || $myObj->getResult() == "DENIED BY RISK" || $myObj->getResult() == "HOST TIMEOUT"){
    		if ($myObj->getResult() == "NOT CAPTURED"){
    			switch ($myObj->getAuthRespCode()){
    				case "05":
    					$response = "Please contact issuer";
    					break;
    				case "14":
    					$response = "Invalid card number";
    					break;
    				case "33":
    					$response = "Expired card";
    					break;
    				case "36":
    					$response = "Restricted card";
    					break;
    				case "38":
    					$response = "Allowable PIN tries exceeded";
    					break;
    				case "51":
    					$response = "Insufficient funds";
    					break;
    				case "54":
    					$response = "Expired card";
    					break;
    				case "55":
    					$response = "Incorrect PIN";
    					break;
    				case "61":
    					$response = "Exceeds withdrawal amount limit";
    					break;
    				case "62":
    					$response = "Restricted Card";
    					break;
    				case "65":
    					$response = "Exceeds withdrawal frequency limit";
    					break;
    				case "75":
    					$response = "Allowable number PIN tries exceeded";
    					break;
    				case "76":
    					$response = "Ineligible account";
    					break;
    				case "78":
    					$response = "Refer to Issuer";
    					break;
    				case "91":
    					$response = "Issuer is inoperative";
    					break;
    				default:
    					// for unlisted values, please generate a proper user-friendly message
    					$response = "Unable to process transaction temporarily. Try again later or try using another card.";
    					break;
    			}
    		}

    		else if ($myObj->getResult() == "CANCELED"){
    			$response = "Transaction was canceled by user.";
    		}

    		else if ($myObj->getResult() == "DENIED BY RISK"){
    			$response = "Maximum number of transactions has exceeded the daily limit.";
    		}

    		else if ($myObj->getResult() == "HOST TIMEOUT"){
    			$response = "Unable to process transaction temporarily. Try again later.";
    		}

    		echo "REDIRECT=https://www.test.inspower-fitnesscoaching.com/benefitDeclined";
    	}

    	else{
    		//Unable to process transaction temporarily. Try again later or try using another card.
    		echo "REDIRECT=https://www.test.inspower-fitnesscoaching.com/benefit-response-err";
    	}
    }

    public function benefitResponseErr(){
        echo "<b>From Resposne Page</b>" . "<br /><br />";
    	echo "Payment ID: " . $_POST["paymentid"] . "<br />";
    	echo "Track ID: " . $_POST["trackid"] . "<br />";
    	echo "Amount: " . $_POST["amt"] . "<br />";
    	echo "Error Text: " . $_POST["ErrorText"] . "<br />";
    	//dd($_REQUEST);

    }

    public function benefitApproved(){
        //require('iPayBenefitPipe.php');

    	$myObj =new iPayBenefitPipe();

    	// modify the following to reflect your "Alias Name", "resource.cgn" file path, "keystore.pooh" file path.
    	$myObj->setAlias("test710939");
    	$myObj->setResourcePath( app_path() . "/benefit/"); //only the path that contains the file; do not write the file name
    	$myObj->setKeystorePath( app_path() . "/benefit/"); //only the path that contains the file; do not write the file name

    	$trandata = "";
    	$paymentID = "";
    	$result = "";
    	$responseCode = "";
    	$transactionID = "";
    	$referenceID = "";
    	$trackID = "";
    	$amount = "";
    	$UDF1 = "";
    	$webAddress = "";
    	$Email = "";
    	$UDF4 = "";
    	$UDF5 = "";
    	$authCode = "";
    	$postDate = "";
    	$errorCode = "";
    	$errorText = "";
    	$courses = null;
    	$programs = null;

    	$trandata = isset($_POST["trandata"]) ? $_POST["trandata"] : "";

    	if ($trandata != ""){
    		$returnValue = $myObj->parseEncryptedRequest($trandata);
    		if ($returnValue == 0){
    			$paymentID = $myObj->getPaymentId();
    			$result = $myObj->getRef();
    			$responseCode = $myObj->getAuthRespCode();
    			$transactionID = $myObj->getTransId();
    			$referenceID = $myObj->getRef();
    			$trackID = $myObj->getTrackId();
    			$amount = $myObj->getAmt();
    			$UDF1 = $myObj->getUdf1();
    			$webAddress = $myObj->getwebAddress();
    			$Email = $myObj->getEmail();
    			$UDF4 = $myObj->getUdf4();
    			$UDF5 = $myObj->getUdf5();
    			$authCode = $myObj->getAuth();
    			$postDate = $myObj->getDate();
    			$errorCode = $myObj->getError();
    			$errorText = $myObj->getError_text();
    		}

    		else{
    			$errorText = $myObj->getError_text();
    		}
    	}

    	else if (isset($_POST["ErrorText"])){
            $paymentID = $_POST["paymentid"];
            $trackID = $_POST["Trackid"];
            $amount = $_POST["amt"];
            $UDF1 =  $_POST["UDF1"];
            $webAddress =  $_POST["webAddress"];
            $Email =  $_POST["Email"];
            $UDF4 =  $_POST["UDF4"];
            $UDF5 = $_POST["UDF5"];
            $errorText = $_POST["ErrorText"];
        }

        else{
            $errorText = "Unknown Exception";
        }

    	/*echo $paymentID;
    	echo $result;
    	echo $responseCode;
    	echo $transactionID;
    	echo $referenceID;
    	echo $trackID; //orderID
    	echo $amount;
    	echo $UDF1;
    	echo $webadress;
    	echo $Email;
    	echo $UDF4;
    	echo $UDF5;
    	echo $authCode;
    	echo $postDate;
    	echo $errorCode;
    	echo $errorText;
    	//dd($_REQUEST);*/

    	$orderData = orders::where('id', $trackID)->get();
    	$date = $orderData[0]->created_at;

        for($i=0; $i<count($orderData); $i++){
            $user_id = $orderData[$i]->user_id;
            $order_id = $orderData[$i]->id;
            if($orderData[$i]->courses != null){
                $courseIds = explode(',', $orderData[$i]->courses);
                for($j=1; $j<count($courseIds); $j++){
                    $course = course::where('id', $courseIds[$j])->first();
                    DB::table('purchases')->insert([
                            'user_id' => $user_id,
                            'course_id' => $course->id,
                            'order_id' => $order_id,
                            "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                            "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
                        ]);
                    $courses[] = $course;
                }
            }
            if($orderData[$i]->programs != null){
                $programIds = explode(',', $orderData[$i]->programs);
                for($j=1; $j<count($programIds); $j++){
                    $program = program::where('id', $programIds[$j])->first();
                    DB::table('purchases')->insert([
                            'user_id' => $user_id,
                            'program_id' => $program->id,
                            'order_id' => $order_id,
                            "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                            "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
                        ]);
                    $programs[] = $program;
                }
            }
        }

        DB::table('orders')->where('id', $trackID)->update([
            'transactionId' => $transactionID,
            'referenceId' => $referenceID,
            'paymentId' => $paymentID,
            'authorizationCode' => $authCode,
            'paymentMethod' => 'Benefit',
            'status' => 'Approved'
            ]);

        DB::table('carts')->where('user_id', auth()->user()->id)->delete();


    	    return view('payment.benefitApprove', compact('date', 'courses', 'programs', 'trackID','amount'));
    }

    public function benefitDeclined(){
        //require('iPayBenefitPipe.php');

    	$myObj =new iPayBenefitPipe();

    	// modify the following to reflect your "Alias Name", "resource.cgn" file path, "keystore.pooh" file path.
    	$myObj->setAlias("test710939");
    	$myObj->setResourcePath( app_path() . "/benefit/"); //only the path that contains the file; do not write the file name
    	$myObj->setKeystorePath( app_path() . "/benefit/"); //only the path that contains the file; do not write the file name

    	$trandata = "";
    	$paymentID = "";
    	$result = "";
    	$responseCode = "";
    	$transactionID = "";
    	$referenceID = "";
    	$trackID = "";
    	$amount = "";
    	$UDF1 = "";
    	$webAddress = "";
    	$Email = "";
    	$UDF4 = "";
    	$UDF5 = "";
    	$authCode = "";
    	$postDate = "";
    	$errorCode = "";
    	$errorText = "";

	    $trandata = isset($_POST["trandata"]) ? $_POST["trandata"] : "";

    	if ($trandata != ""){
    		$returnValue = $myObj->parseEncryptedRequest($trandata);

    		if ($returnValue == 0){
    			$paymentID = $myObj->getPaymentId();
    			$result = $myObj->getRef();
    			$responseCode = $myObj->getAuthRespCode();
    			$transactionID = $myObj->getTransId();
    			$referenceID = $myObj->getRef();
    			$trackID = $myObj->getTrackId();
    			$amount = $myObj->getAmt();
    			$UDF1 = $myObj->getUdf1();
    			$webAddress = $myObj->getwebAddress();
    			$Email = $myObj->getEmail();
    			$UDF4 = $myObj->getUdf4();
    			$UDF5 = $myObj->getUdf5();
    			$authCode = $myObj->getAuth();
    			$postDate = $myObj->getDate();
    			$errorCode = $myObj->getError();
    			$errorText = $myObj->getError_text();
    		}

    		else{
    			$errorText = $myObj->getError_text();
    		}
    	}
    	else if (isset($_POST["ErrorText"])){
            $paymentID = $_POST["paymentid"];
            $trackID = $_POST["Trackid"];
            $amount = $_POST["amt"];
            $UDF1 =  $_POST["UDF1"];
            $webAddress =  $_POST["webAdress"];
            $Email =  $_POST["Email"];
            $UDF4 =  $_POST["UDF4"];
            $UDF5 = $_POST["UDF5"];
            $errorText = $_POST["ErrorText"];
        }
        else{
            $errorText = "Unknown Exception";
        }

    	echo $paymentID;
    	echo $result;
    	echo $responseCode;
    	echo $transactionID;
    	echo $referenceID;
    	echo $trackID;
    	echo $amount;
    	echo $UDF1;
    	echo $UDF4;
    	echo $UDF5;
    	echo $authCode;
    	echo $postDate;
    	echo $errorCode;
    	echo $errorText;
    }

    public function invoice(){
        $order_id = $_GET['order_id'];
        $orderData = orders::where('id', $order_id)->get();
    	$date = $orderData[0]->created_at;
    	$amount = $orderData[0]->amount;

        for($i=0; $i<count($orderData); $i++){
            $user_id = $orderData[$i]->user_id;
            $order_id = $orderData[$i]->id;
            if($orderData[$i]->courses != null){
                $courseIds = explode(',', $orderData[$i]->courses);
                for($j=1; $j<count($courseIds); $j++){
                    $course = course::where('id', $courseIds[$j])->first();
                    $courses[] = $course;
                }
            }
            if($orderData[$i]->programs != null){
                $programIds = explode(',', $orderData[$i]->programs);
                for($j=1; $j<count($programIds); $j++){
                    $program = program::where('id', $programIds[$j])->first();
                    $programs[] = $program;
                }
            }
        }

        return view('payment.benefitInvoice', compact('date', 'courses', 'programs', 'order_id', 'amount'));

    }

    public function mastercardApproved(){
        $ch = curl_init();
        $merchant_id = "TEST100067231";
        $PWD = "8d4ec52a5782a2e4fbbf32625236e873";
        $currency_code = "BHD";
        $merchant_name = "INSPOWER FITNESS COACHING";
        $logo = "https://inspower-fitnesscoaching.com/images/payment-logo.png";
        $order_id = $_GET['order_id'];
        $description = $_GET['description'];

        $post_data = array("apiOperation" => "RETRIEVE_ORDER",
            "order" => array("id" => $order_id,"currency"=>$currency_code),
            'interaction' => array('operation' => "AUTHORIZE"));

        $data_string = json_encode($post_data);
        curl_setopt($ch, CURLOPT_USERPWD, "merchant.$merchant_id:$PWD");
        curl_setopt($ch, CURLOPT_URL,"https://afs.gateway.mastercard.com/api/rest/version/57/merchant/$merchant_id/order/$order_id");
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);

        $content = curl_exec($ch);
        $output = json_decode($content, true);

        //dd($output['sourceOfFunds']['provided']['card']['brand']);

    	$orderData = orders::where('id', $order_id)->get();
    	$date = $orderData[0]->created_at;
    	$amount = $orderData[0]->amount;

        for($i=0; $i<count($orderData); $i++){
            $user_id = $orderData[$i]->user_id;
            $order_id = $orderData[$i]->id;
            if($orderData[$i]->courses != null){
                $courseIds = explode(',', $orderData[$i]->courses);
                for($j=1; $j<count($courseIds); $j++){
                    $course = course::where('id', $courseIds[$j])->first();
                    DB::table('purchases')->insert([
                            'user_id' => $user_id,
                            'course_id' => $course->id,
                            'order_id' => $order_id,
                            "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                            "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
                        ]);
                    $courses[] = $course;
                }
            }
            if($orderData[$i]->programs != null){
                $programIds = explode(',', $orderData[$i]->programs);
                for($j=1; $j<count($programIds); $j++){
                    $program = program::where('id', $programIds[$j])->first();
                    DB::table('purchases')->insert([
                            'user_id' => $user_id,
                            'program_id' => $program->id,
                            'order_id' => $order_id,
                            "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                            "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
                        ]);
                    $programs[] = $program;
                }
            }
        }

        DB::table('orders')->where('id', $order_id)->update([
            'transactionId' => $output['transaction'][0]['transaction']['acquirer']['transactionId'],
            'authorizationCode' => $output['transaction'][0]['transaction']['authorizationCode'],
            'paymentMethod' => $output['sourceOfFunds']['provided']['card']['brand'],
            'JSONResponse' => $output,
            'status' => 'Approved'
            ]);

        DB::table('carts')->where('user_id', auth()->user()->id)->delete();
    	    return view('payment.mastercardApproved', compact('date', 'courses', 'programs', 'order_id', 'amount', 'description'));
    }
}
