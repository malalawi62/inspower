<?php

namespace App\Http\Controllers;

use App\cart;
use App\program;
use App\program_phase;
use App\program_phase_section;
use App\Purchase;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;


class ProgramController extends Controller
{
    public function index()
    {
        $orders = null;
        if (App::isLocale('en')) {
            $lang = 'en';
            $programs = program::where('language', 'Eng')->orderByDesc('id')->get();
        } else {
            $lang = 'ar';
            $programs = program::where('language', 'Ar')->orderByDesc('id')->get();
        }

        $cart = new cart();

        if (Auth::user()) {
            $orders = Purchase::where('user_id', auth()->user()->id)->where('program_id', '!=', null)->orderByDesc('id')->get();
        }

        return view('programs.index', compact('programs', 'lang', 'cart', 'orders')); //"compact" is to pass database rows to the view
    }

    public function create()
    {
        $program = new program();
        if (App::isLocale('en')) {
            $lang = 'en';
        } else
            $lang = 'ar';
        return view('programs.create', compact('program', 'lang'));
    }

    public function store()
    {
        $program = program::create($this->validateData());
        $this->storePdf($program, null);
        $this->storeImage($program, null);
        $videoUrl = \request()->post('videoUrl');
        if($videoUrl != null){
            $url = explode('https://vimeo.com/', $videoUrl);
            $program->update([
                'videoUrl' => $url[1],
                "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
            ]);
        }

        $countPhases=count(\request()->post('p_title'));
        $phase_titles = \request()->post('p_title');
        $phase_descriptions = \request()->post('p_description'); //nullable
        $phase_overviews= \request()->post('p_overview');
        $phase_video = \request()->post('p_videoUrl'); //nullable
        $phase_pdf = request()->file('phasePdf');
        $phase_section_descriptions = \request()->post('ps_description'); //video description
        $program_section_videos = \request()->post('ps_videoUrl');

        for ($i=0; $i<$countPhases; $i++){

            $programPhase = DB::table('program_phases')->insertGetId([
                'title' => $phase_titles[$i],
                'description' => null,
                'overview'=> $phase_overviews[$i],
                'videoUrl'=> null,
                'pdf' => null,
                'program_id' => $program->id,
                "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
            ]);

            if ($phase_video[$i] != null){
                $v_id = explode('https://vimeo.com/', $phase_video[$i]);
                DB::table('program_phases')->where('id', $programPhase)->update([
                    'videoUrl'=> $v_id[1],
                    "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
                ]);
            }

            if ($phase_descriptions[$i] != null){
                DB::table('program_phases')->where('id', $programPhase)->update([
                    'description'=> $phase_descriptions[$i],
                    "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
                ]);
            }

            $phase = program_phase::where('id', $programPhase)->first();

            if (isset($phase_pdf[$i])){
                $this->storePhasePdf($phase, null);
                $path = "pdfUploads";
                $name = Storage::put($path, $phase_pdf[$i]);
                $file_name = explode('/', $name);
                $phase->update([
                    'pdf' => $file_name[1],
                    "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
                ]);
            }
            $count = $i+1;

            while(!isset($program_section_videos[$count])){
                $count++;
            }
            for ($j=0; $j<count($program_section_videos[$count]); $j++){
                $video_id = explode('https://vimeo.com/', $program_section_videos[$count][$j]);
                $section_id = DB::table('program_phase_sections')->insertGetId([
                    'videoUrl' => $video_id[1],
                    'description' => null,
                    'program_phase_id' => $programPhase,
                    "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
                ]);

                if ($phase_section_descriptions[$count][$j] != null){
                    DB::table('program_phase_sections')->where('id', $section_id)->update([
                        'description' => $phase_section_descriptions[$count][$j],
                        "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
                    ]);
                }
            }
        }

        $admins = User::where('type', 'admin')->get();
        foreach ($admins as $admin){
            DB::table('purchases')->insert([
                'program_id' => $program->id,
                'user_id' => $admin->id,
                "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
            ]);
        }
        return redirect('program/' . $program->id);
    }

    public function createPhase()
    {
        $program_id = \request()->get('program_id');
        return view('programs.createPhase', compact('program_id'));
    }

    public function storePhase(){
        $program = \request()->post('program_id');
        $this->storePhaseCode($program);

        return redirect()->back()->with('alert', "Phase Has Been Created Successfully");
    }

    public function createSection()
    {
        $phase_id = \request()->get('phase_id');
        return view('programs.createSection', compact('phase_id'));
    }

    public function storeSection(){
        $phase = \request()->post('phase_id');
        $videoUrl = \request()->post('videoUrl');
        $description = \request()->post('description');

        $this->storeSectionCode($videoUrl, $phase, $description);

        return redirect()->back()->with('alert', "Section Has Been Created Successfully");
    }

    public function show(program $program)
    {
        $orders = null;
        $purchased=false;
        if (Auth::user()) {
            $purchased = (count(Purchase::where('user_id', auth()->user()->id)->where('program_id', $program->id)->get()) > 0);
            if (auth()->user()->type == 'admin'){
                $purchased = true;
            }
        }

        if (App::isLocale('en')) {
            $lang = 'en';
        } else
            $lang = 'ar';

        if (Auth::user()) {
            $orders = Purchase::where('user_id', auth()->user()->id)->where('program_id', '!=', null)->orderByDesc('id')->get();
        }

        $sections = [];
        $phases = program_phase::where('program_id', $program->id)->get();

        for ($i=0; $i<count($phases); $i++){
            $phase_sections = program_phase_section::where('program_phase_id', $phases[$i]->id)->get();
            foreach ($phase_sections as $ps){
                $sections[] = $ps;
            }
        }

        if ($program->status == 'inactive' && $purchased == false){
            return redirect('programs');
        }

        elseif ($program->status == 'inactive' && auth()->user()->type == 'admin'){
            return view('programs.show', compact('program', 'lang', 'purchased', 'orders', 'phases', 'sections'));
        }

        elseif ($program->status == 'inactive' && $purchased == true){
            return view('programs.show', compact('program', 'lang', 'purchased', 'orders', 'phases', 'sections'));
        }

        elseif ($program->status == 'active'){
            return view('programs.show', compact('program', 'lang', 'purchased', 'orders', 'phases', 'sections'));
        }
    }

    public function edit(program $program)
    {
        if (App::isLocale('en')) {
            $lang = 'en';
        } else
            $lang = 'ar';

        $sections = [];
        $phases = program_phase::where('program_id', $program->id)->get();

        for ($i=0; $i<count($phases); $i++){
            $phase_sections = program_phase_section::where('program_phase_id', $phases[$i]->id)->get();
            foreach ($phase_sections as $ps){
                $sections[] = $ps;
            }
        }

        return view('programs.edit', compact('program', 'lang', 'phases', 'sections'));
    }

    public function update(program $program)
    {
        $pdf_path = $program->pdf;
        $image_path = $program->image;
        $program->update($this->validateDataUpdate());
        $this->storePdf($program, $pdf_path);
        $this->storeImage($program, $image_path);
        $videoUrl = \request()->post('videoUrl');
        if($videoUrl != null){
            $url = explode('https://vimeo.com/', $videoUrl);
            $program->update([
                'videoUrl' => $url[1],
                "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
            ]);
        }

        return redirect()->back()->with('alert', "Main Program Details Updated Successfully");
    }

    public function editPhase($phase)
    {
        if (App::isLocale('en')) {
            $lang = 'en';
        }
        else
            $lang = 'ar';

        $sections = [];
        $phases = program_phase::where('id', $phase)->first();

        $phase_sections = program_phase_section::where('program_phase_id', $phases->id)->get();
        foreach ($phase_sections as $ps){
            $sections[] = $ps;
        }
        return view('programs.editPhase', compact('sections', 'lang', 'phases'));
    }

    public function updatePhase(program_phase $phase)
    {
        $pdf_path= $phase->pdf;
        $phase->update($this->validateUpdatePhaseData());
        $videoUrl = \request()->post('videoUrl');
        if ($videoUrl != null){
            $url = explode('https://vimeo.com/', $videoUrl);
            $phase->update([
                'videoUrl' => $url[1],
                "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
            ]);
        }
        $this->storePhasePdf($phase , $pdf_path);

        return redirect()->back()->with('alert', "Phase Details Updated Successfully");
    }

    public function editSection($section)
    {
        if (App::isLocale('en')) {
            $lang = 'en';
        }
        else
            $lang = 'ar';

        $section2 = program_phase_section::where('id', $section)->first();
        return view('programs.editSection', compact('section2', 'lang'));
    }

    public function updateSection(program_phase_section $section)
    {
        $section->update($this->validateUpdateSectionData());

        $videoUrl = \request()->post('videoUrl');
        if ($videoUrl != null){
            $url = explode('https://vimeo.com/', $videoUrl);
            $section->update([
                'videoUrl' => $url[1],
                "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
            ]);
        }

        return redirect()->back()->with('alert', "Section Details Updated Successfully");
    }

    public function destroy(program $program)
    {
        \File::delete('storage/'.$program->pdf);
        $program->delete();
        return redirect('programs');
    }

    public function destroyPhase($phase){
        DB::table('program_phase_sections')->where('program_phase_id', $phase)->delete();
        $phase2 = program_phase::where('id', $phase)->first();
        Storage::delete('/pdfUploads/' . $phase2->pdf);
        DB::table('program_phases')->where('id', $phase)->delete();

        return redirect()->back()->with('alert', "Phase Has Been Deleted Successfully");
    }

    public function destroySection($section){
        DB::table('program_phase_sections')->where('id', $section)->delete();

        return redirect()->back()->with('alert', "Section Has Been Deleted Successfully");
    }

    public function active(program $program){
        DB::table('programs')->where('id', $program->id)->update([
            'status' => 'active',
            "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
        ]);
        return redirect('program/' . $program->id);
    }

    public function inactive(program $program){
        DB::table('programs')->where('id', $program->id)->update([
            'status' => 'inactive',
            "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
        ]);
        return redirect('program/' . $program->id);
    }

    //validate inputs
    protected function validateData()
    {
        return tap(request()->validate([
            'title' => 'required',
            'description' => '',
            'videoUrl' => '',
            'overview' => 'required',
            'price' => 'required | numeric',
            'language' => 'required',
            'pdf' => 'required',
            'image' => 'required'
        ]), function () {
            if (request()->hasFile('pdf')) {
                request()->validate([
                    "pdf" => "mimes:pdf|max:10000"
                ]);
            }
            if (request()->hasFile('image')) {
                request()->validate([
                    'image' => 'file|image|max:10000'
                ]);
            }
        });
    }

    protected function validateDataUpdate()
    {
        return tap(request()->validate([
            'title' => 'required',
            'description' => '',
            'videoUrl' => '',
            'overview' => 'required',
            'price' => 'required | numeric',
            'pdf' => '',
            'image' => ''
        ]), function () {
            if (request()->hasFile('pdf')) {
                request()->validate([
                    "pdf" => "mimes:pdf|max:10000"
                ]);
            }
            if (request()->hasFile('image')) {
                request()->validate([
                    'image' => 'file|image|max:10000'
                ]);
            }
        });
    }

    private function validateUpdatePhaseData(){
        return tap(request()->validate([
            'title' => 'required',
            'description' => '',
            'videoUrl' => '',
            'overview' => 'required',
            'pdf' => ''
        ]), function () {
            if (request()->hasFile('pdf')) {
                request()->validate([
                    "pdf" => "mimes:pdf|max:10000"
                ]);
            }
        });
    }

    protected function validateUpdateSectionData(){
        return request()->validate([
            'videoUrl' => 'required',
            'description' => '',
        ]);
    }

    private function storePdf(program $program, $pdf_path)
    {
        if (request()->hasFile('pdf')) {
            Storage::delete('/pdfUploads/' . $pdf_path);
            $path = "pdfUploads";
            $file = request()->file('pdf');
            $name = Storage::put($path, $file);
            $file_name = explode('/', $name);
            $program->update([
                'pdf' => $file_name[1],
                "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
            ]);
        }
    }

    private function storePhasePdf(program_phase $phase , $pdf_path)
    {
        if (request()->hasFile('pdf')) {
            Storage::delete('/pdfUploads/' . $pdf_path);
            $path = "pdfUploads";
            $file = request()->file('pdf');
            $name = Storage::put($path, $file);
            $file_name = explode('/', $name);
            $phase->update([
                'pdf' => $file_name[1],
                "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
            ]);
        }
    }

    private function storeImage($program, $image_path)
    {
        if (request()->has('image')) {
            $path = 'storage/'.$image_path;
            \File::delete($path);
            $program->update([
                'image' => request()->image->store('uploads', 'public'),
                "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
            ]);
        }
    }

    public function showPdf($program){
        if (Auth::user()){
            $programDetails = program::where('pdf', $program)->first();
            if($programDetails != null){
                $order = Purchase::where('program_id', $programDetails->id)->where('user_id', auth()->user()->id)->first();
                if($order != null){
                    $file = storage_path('app/pdfUploads/').$program;

                    if (file_exists($file)) {
                        $headers = [
                            'Content-Type' => 'application/pdf'
                        ];
                        return response()->download($file, 'Test File', $headers, 'inline');
                    }
                    else {
                        abort(404, 'File not found!');
                    }
                }
                else{
                    abort(404, 'Sorry, You Are not authorized to view this page ..');
                }
            }
        }
    }

    public function showPhasePdf($phase){
        if (Auth::user()){
            $phaseDetails = program_phase::where('pdf', $phase)->first();
            if($phaseDetails != null){
                $order = Purchase::where('program_id', $phaseDetails->program_id)->where('user_id', auth()->user()->id)->first();
                if($order != null){
                    $file = storage_path('app/pdfUploads/').$phase;

                    if (file_exists($file)) {
                        $headers = [
                            'Content-Type' => 'application/pdf'
                        ];
                        return response()->download($file, 'Test File', $headers, 'inline');
                    }
                    else {
                        abort(404, 'File not found!');
                    }
                }
                else{
                    abort(404, 'Sorry, You Are not authorized to view this page ..');
                }
            }
        }
    }

    private function storePhaseCode($program)
    {
        $title = \request()->post('title');
        $overview = \request()->post('overview');
        $description = \request()->post('description');
        $videoUrl = \request()->post('videoUrl');
        $pdf = request()->file('pdf');
        $programPhaseVideos = \request()->post('section_videoUrl');
        $programPhaseDescriptions = \request()->post('section_description');

        $programPhase = DB::table('program_phases')->insertGetId([
            'title' => $title,
            'description' => null,
            'overview'=> $overview,
            'program_id' => $program,
            "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
            "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
        ]);

        if ($description != null){
            DB::table('program_phases')->where('id', $programPhase)->update([
                'description'=> $description,
                "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
            ]);
        }
        if ($videoUrl != null){
            DB::table('program_phases')->where('id', $programPhase)->update([
                'videoUrl'=> $videoUrl,
                "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
            ]);
        }

        $phase = program_phase::where('id', $programPhase)->first();
        $this->storePhasePdf($phase ,null);

        //To store Phase Sections
        for ($j=0; $j<count($programPhaseVideos); $j++){
            $this->storeSectionCode($programPhaseVideos[$j], $programPhase, $programPhaseDescriptions[$j]);
        }
    }

    private function storeSectionCode($videoUrl, $phase, $description)
    {
        $videoId = explode('https://vimeo.com/', $videoUrl);
        $sectionId = DB::table('program_phase_sections')->insertGetId([
            'videoUrl' => $videoId[1],
            'program_phase_id' => $phase,
            'description' => $description,
            "created_at" => \Carbon\Carbon::now(), # new \Datetime()
            "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
        ]);

        $section = program_phase_section::where('id', $sectionId)->first();
    }
}
