<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\slideShow;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class SlideShowController extends Controller
{
    public function index()
    {
        if(App::isLocale('en'))
            $lang='en';
        else
            $lang='ar';
        $images = slideShow::all();
        $data=[
            'images'=>$images,
            'lang'=> $lang,
        ];
        return view('slideShow.index', compact('data')); //"compact" is to pass database rows to the view
    }

    public function create()
    {
        $images = new slideShow();
        return view('slideShow.create', compact('images'));
    }

    public function store()
    {
        $image = slideShow::create($this->validateData());
        $this->storeImage($image, null, null);
        return redirect('slideShow');
    }

    public function active(slideShow $image)
    {
        DB::table('slide_shows')->where('id', $image->id)->update([
            'show' => 'Yes',
        ]);
        return redirect('slideShow');
    }

    public function inactive(slideShow $image)
    {
        DB::table('slide_shows')->where('id', $image->id)->update([
            'show' => 'No',
        ]);
        return redirect('slideShow');
    }

    public function destroy(slideShow $image)
    {
        Storage::delete('/uploads/' . $image->imageEng);
        Storage::delete('/uploads/' . $image->imageAr);
        $image->delete();
        return redirect('slideShow');
    }

    //validate inputs
    protected function validateData()
    {
        return tap(request()->validate([
            'show' => 'required',
        ]), function () {
            if (request()->hasFile('imageEng') && request()->hasFile('imageAr')) {
                //we are not validating the image with the required fields at thr top because it is not
                //required in the edit form (same if you don't want it to be required always)
                request()->validate([
                    'imageEng' => 'file|image|max:10000',
                    'imageAr' => 'file|image|max:10000'
                ]);
            }
        });
    }

    private function storeImage($slideShow, $imageEngPath, $imageArPath)
    {
        if (request()->has('imageEng') && request()->has('imageAr')) {
            $EngPath = 'storage/'.$imageEngPath;
            $ArPath = 'storage/'.$imageArPath;
            \File::delete($EngPath);
            \File::delete($ArPath);
            $slideShow->update([
                'imageEng' => request()->imageEng->store('uploads', 'public'),
                'imageAr' => request()->imageAr->store('uploads', 'public')
            ]);
        }
    }
}
