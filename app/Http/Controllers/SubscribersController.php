<?php

namespace App\Http\Controllers;

use App\articles;
use App\course;
use App\Events\sendEmailConfirmationEvent;
use App\Events\sendEmailToSubscriberEvent;
use App\Events\sendNewArticlesEvent;
use App\Events\sendNewCourseEvent;
use App\Events\sendNewProgramEvent;
use App\program;
use App\subscribers;
use App\youtube;
use Illuminate\Http\Request;
use App\Mail\WelcomeMail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class SubscribersController extends Controller
{
    public function store()
    {
        $subscriber_email = subscribers::where('email', \request('email'))->first();
        if ($subscriber_email != null){
            if ($subscriber_email->status == "pending"){
                event(new sendEmailToSubscriberEvent($subscriber_email));
                return redirect('/')->with('alert', "Thanks for your subscription, You will receive a conformation email to: ".$subscriber_email->email."NOTE: CHECK YOUR JUNK OR SPAM MAIL");
            }
            else{
                return redirect('/')->with('alert', "You are already subscribed. :)");
            }
        }
        else{
            $subscriber = subscribers::create($this->validateData());
            event(new sendEmailToSubscriberEvent($subscriber));
            return redirect('/')->with('alert', "Thanks for your subscription, You will receive a conformation email to: ".$subscriber->email."NOTE: CHECK YOUR JUNK OR SPAM MAIL");
        }
    }

    public function confirm($email)
    {
        $subscriber = subscribers::where('email',$email)->first();
        $subscriber->update(['status'=>'active']);
        return redirect('/welcome')->with('alert', "Your subscription is received.. Welcome to INSPOWER.. You will receive our latest activities on your email : ".$subscriber->email." NOTE: CHECK YOUR JUNK OR SPAM MAIL");
    }

    public function unsubscribe($email)
    {
        $subscriber = subscribers::where('email',$email)->first();
        $subscriber->update(['status'=>'pending']);
        return redirect('/welcome')->with('alert', "Your are unsubscribed..");
    }

    public function sendVideo($videoId)
    {
        $videoInfo = youtube::where('id',$videoId)->first();
        event(new sendEmailConfirmationEvent($videoInfo));
        return redirect('/welcome')->with('alert', "Video: ".$videoInfo->title." sent to subscribers");
    }

    public function sendArticle($articleId)
    {
        $articleInfo = articles::where('id',$articleId)->first();
        event(new sendNewArticlesEvent($articleInfo));
        return redirect('/welcome')->with('alert', "Article: ".$articleInfo->title." sent to subscribers");
    }

    public function sendCourse($courseId)
    {
        $courseInfo = course::where('id',$courseId)->first();
        event(new sendNewCourseEvent($courseInfo));
        return redirect('/welcome')->with('alert', "Course: ".$courseInfo->title." sent to subscribers");
    }

    public function sendProgram($programId)
    {
        $programInfo = program::where('id',$programId)->first();
        event(new sendNewProgramEvent($programInfo));
        return redirect('/welcome')->with('alert', "Program: ".$programInfo->title." sent to subscribers");
    }

    public function subscribersList()
    {
        $subscribers = subscribers::where('status', 'active')->get();
        $count = count($subscribers);
        return view('subscribersList', compact('subscribers', 'count'));
    }

    //validate inputs
    protected function validateData(){
        return request()->validate([
            'email' => 'required',
            'name' => 'required',
            'ar' => '',
            'eng' => ''
        ]);
    }
}
