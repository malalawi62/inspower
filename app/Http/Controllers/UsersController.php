<?php

namespace App\Http\Controllers;

use App\memberships;
use App\Purchase;
use App\Users;
use App\User;
use Brick\Math\BigInteger;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{
    public function users (){
        $users = User::where('type', 'regular')->where('email_verified_at', '!=', null)->get();
        $females = User::where('gender', 'f')->where('type', 'regular')->where('email_verified_at', '!=', null)->count();
        $males = User::where('gender', 'm')->where('type', 'regular')->where('email_verified_at', '!=', null)->count();

        $f_percentage = ($females/($females+$males))*100;
        $m_percentage = ($males/($females+$males))*100;

        $lessThanTwenties = 0;
        $twenties = 0;
        $thirties = 0;
        $forties = 0;
        $greaterThanForties = 0;


        for($i=0; $i<count($users); $i++){
            if ((int)date('Y') - (int)explode('-', $users[$i]->DOB)[0] < 20)
                $lessThanTwenties ++;
            elseif ((int)date('Y') - (int)explode('-', $users[$i]->DOB)[0] >= 20 && (int)date('Y') - (int)explode('-', $users[$i]->DOB)[0] < 30)
                $twenties ++;
            elseif ((int)date('Y') - (int)explode('-', $users[$i]->DOB)[0] >= 30 && (int)date('Y') - (int)explode('-', $users[$i]->DOB)[0] < 40)
                $thirties ++;
            elseif ((int)date('Y') - (int)explode('-', $users[$i]->DOB)[0] >= 40 && (int)date('Y') - (int)explode('-', $users[$i]->DOB)[0] < 50)
                $forties ++;
            else
                $greaterThanForties ++;
        }

        $pl = ($lessThanTwenties/count($users))*100;
        $pt = ($twenties/count($users))*100;
        $pth = ($thirties/count($users))*100;
        $pf = ($forties/count($users))*100;
        $pg = ($greaterThanForties/count($users))*100;

        return view('users.users', compact('users', 'females', 'males', 'f_percentage', 'm_percentage', 'lessThanTwenties', 'pl', 'twenties', 'pt', 'thirties', 'pth', 'forties', 'pf', 'greaterThanForties', 'pg'));
    }

    //validate inputs
    protected function validateData($user_id){
        return request()->validate([
            'name' => 'required',
            'email' => 'required | unique:users,email,'.$user_id.'', //unique: table_name, column_name_to_be_checked, id
            'DOB' => 'required'
        ]);
    }

    public function profile(Users $user){
        if($user->id != auth()->id()){
            $this->authorize('edit', auth()->user());
        }
        else{
            $courses = Purchase::where('course_id', '!=', null)->where('user_id',auth()->user()->id)->get();
            $programs = Purchase::where('program_id', '!=', null)->where('user_id',auth()->user()->id)->get();
            $orders = Purchase::where('user_id',auth()->user()->id)->orderByDesc('id')->get();
            $membership = memberships::where('user_id',auth()->user()->id)->value('type');
            return view('users.profile', compact('user','courses', 'programs', 'orders', 'membership')); //"compact" is to pass database rows to the view
        }
    }

    public function edit(Users $user)
    {
        if($user->id != auth()->id()){
            $this->authorize('edit', auth()->user());
        }
        else {
            $membership = memberships::where('user_id',auth()->user()->id)->value('type');
            return view('users.editProfile', compact('user', 'membership'));
        }
    }

    public function update(Users $user)
    {
        if($user->id != auth()->id()){
            $this->authorize('edit', auth()->user());
        }
        else {
            $user->update($this->validateData($user->id));
            return redirect('profile/'.$user->id);
        }
    }
}
