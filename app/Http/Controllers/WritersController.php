<?php

namespace App\Http\Controllers;

use App\writers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class WritersController extends Controller
{
    public function index()
    {
        if(App::isLocale('en'))
            $lang='en';
        else
            $lang='ar';
        $writers = writers::all();
        $data=[
            'writers'=>$writers,
            'lang'=> $lang,
        ];
        return view('writers.index', compact('data')); //"compact" is to pass database rows to the view
    }

    public function create()
    {
        $writers = new writers();
        return view('writers.create', compact('writers'));
    }

    public function store()
    {
        writers::create($this->validateData());
        return redirect('writers');
    }

    public function edit(writers $writer)
    {
        return view('writers.edit', compact('writer'));
    }

    public function update(writers $writer)
    {
        $writer->update($this->validateData());
        return redirect('writers');
    }

    public function destroy(writers $writer)
    {
        $writer->delete();
        return redirect('writers');
    }

    //validate inputs
    protected function validateData(){
        return request()->validate([
            'englishName' => 'required',
            'arabicName' => 'required',
        ]);
    }
}
