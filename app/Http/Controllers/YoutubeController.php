<?php

namespace App\Http\Controllers;

use App\categories;
use App\instagram;
use App\memberships;
use App\youtube;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class YoutubeController extends Controller
{
    public function index()
    {
        $videoView = 'videos';

        if (Auth::user()){
            $membership = true;
            $membership_details = memberships::where('user_id',auth()->user()->id)->value('type');
        }
        else{
            $membership = false;
            $membership_details = null;
        }


        if (App::isLocale('en')) {
            $lang = 'en';
            if(\request()->has('category_id')){
                    $videos = youtube::where('language','Eng')->where('category_id', \request('category_id'))
                        ->orderByDesc('id')->paginate(20)->appends('category_id', \request('category_id'));

                $cat_id = \request('category_id');
            }
            else{
                    $videos = youtube::where('language','Eng')->orderByDesc('id')->paginate(20);
                $cat_id = null;
            }
        }
        else {
            $lang = 'ar';
            if(\request()->has('category_id')){
                    $videos = youtube::where('language','Ar')->where('category_id', \request('category_id'))
                        ->orderByDesc('id')->paginate(20)->appends('category_id', \request('category_id'));

                $cat_id = \request('category_id');
            }
            else{
                    $videos = youtube::where('language','Ar')->orderByDesc('id')->paginate(20);

                $cat_id = null;
            }
        }
        $categories = categories::all();
        $data = [
            'categories' => $categories,
            'lang' => $lang,
            'videos' => $videos,
            'cat_id' => $cat_id
        ];
        return view('videos.index', compact('data', 'membership_details', 'membership', 'videoView'));
    }

    public function create()
    {
        if (App::isLocale('en')) {
            $lang = 'en';
        }
        else
            $lang = 'ar';
        $categories = categories::all();
        $data = [
            'categories' => $categories,
            'lang' => $lang
        ];

        return view('videos.create', compact('data'));
    }

    public function createVimeo()
    {
        if (App::isLocale('en')) {
            $lang = 'en';
        }
        else
            $lang = 'ar';
        $categories = categories::all();
        $data = [
            'categories' => $categories,
            'lang' => $lang
        ];

        return view('videos.createVimeo', compact('data'));
    }

    public function store()
    {
        $videoUrl = \request()->post('videoId');
        $subUrl = explode('v=', $videoUrl);
        $url = explode('&s=', $subUrl[1]);
        $video = youtube::create($this->validateData());
        $video->update([
            'videoId' => $url[0]
        ]);
        return redirect('videos');
    }

    public function storeVimeo()
    {
        $videoUrl = \request()->post('videoId');
        $url = explode('https://vimeo.com/', $videoUrl);
        $video = youtube::create($this->validateData());
        $video->update([
            'videoId' => $url[1]
        ]);
        return redirect('videos');
    }

    public function show(youtube $video)
    {
        if (App::isLocale('en')) {
            $lang = 'en';
        } else
            $lang = 'ar';
        $categories = categories::all();
        $data = [
            'categories' => $categories,
            'video' => $video,
            'lang' => $lang
        ];
        return view('videos.show', compact('data'));
    }

    public function edit(youtube $video)
    {
        if (App::isLocale('en')) {
            $lang = 'en';
        }
        else
            $lang = 'ar';
        $categories = categories::all();
        $data = [
            'video' => $video,
            'categories' => $categories,
            'lang' => $lang
        ];
        return view('videos.edit', compact('data'));
    }

    public function update(youtube $video)
    {
        $video->update($this->validateData());
        return redirect('videos');
    }

    public function destroy(youtube $video)
    {
        $video->delete();
        return redirect('videos');
    }

    //validate inputs
    protected function validateData()
    {
        return request()->validate([
            'title' => 'required',
            'videoId' => 'required',
            'language' => 'required',
            'type' => 'required',
            'category_id' => 'required',
            'description' => '',
            'public' => ''
        ]);
    }
}
