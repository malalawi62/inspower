<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '/benefit-check-out/{order_id}/{counter}',
        '/benefit-response',
        '/benefit-response-err',
        '/benefitApproved',
        '/benefitDeclined'
    ];
}
