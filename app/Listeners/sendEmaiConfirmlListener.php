<?php

namespace App\Listeners;

use App\Events\sendEmailConfirmationEvent;
use App\Mail\NewVideo;
use App\subscribers;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class sendEmaiConfirmlListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  sendEmailConfirmationEvent  $event
     * @return void
     */
    public function handle(sendEmailConfirmationEvent $event)
    {
        if($event->videoInfo->language == "Eng")
            $subscribers = subscribers::where('status','active')->where('eng','yes')->get();
        elseif($event->videoInfo->language == "Ar")
            $subscribers = subscribers::where('status','active')->where('ar','yes')->get();

        if(count($subscribers) > 0){
            for($i=0; $i < count($subscribers); $i++){
                Mail::to($subscribers[$i]->email)->send(new NewVideo($event->videoInfo, $subscribers[$i]->name, $subscribers[$i]->email));
            }
        }
    }
}
