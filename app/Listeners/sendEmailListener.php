<?php

namespace App\Listeners;

use App\Events\sendEmailToSubscriberEvent;
use App\Mail\WelcomeMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Mail;

class sendEmailListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    /**
     * Handle the event.
     *
     * @param  sendEmailToSubscriberEvent  $event
     * @return void
     */
    public function handle(sendEmailToSubscriberEvent $event)
    {
        Mail::to($event->subscriber->email)->send(new WelcomeMail($event->subscriber->email, $event->subscriber->name));
    }
}
