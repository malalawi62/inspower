<?php

namespace App\Listeners;

use App\Events\sendNewArticlesEvent;
use App\Mail\WelcomeMail;
use App\subscribers;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class sendEmailNotifyListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  sendNewArticlesEvent  $event
     * @return void
     */
    public function handle(sendNewArticlesEvent $event)
    {

    }
}
