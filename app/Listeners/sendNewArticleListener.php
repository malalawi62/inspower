<?php

namespace App\Listeners;

use App\Events\sendNewArticlesEvent;
use App\Mail\NewArticle;
use App\Mail\NewVideo;
use App\subscribers;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class sendNewArticleListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  sendNewArticlesEvent  $event
     * @return void
     */
    public function handle(sendNewArticlesEvent $event)
    {
        if($event->articleInfo->language == "Eng")
            $subscribers = subscribers::where('status','active')->where('eng','yes')->get();
        elseif($event->articleInfo->language == "Ar")
            $subscribers = subscribers::where('status','active')->where('ar','yes')->get();

        if(count($subscribers) > 0){
            for($i=0; $i < count($subscribers); $i++){
                Mail::to($subscribers[$i]->email)->send(new NewArticle($event->articleInfo, $subscribers[$i]->name, $subscribers[$i]->email));
            }
        }
    }
}
