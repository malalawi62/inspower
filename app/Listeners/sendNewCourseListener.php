<?php

namespace App\Listeners;

use App\Events\sendNewCourseEvent;
use App\Mail\NewCourse;
use App\subscribers;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class sendNewCourseListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  sendNewCourseEvent  $event
     * @return void
     */
    public function handle(sendNewCourseEvent $event)
    {
        if($event->courseInfo->language == "Eng"){
            $subscribers = subscribers::where('status','active')->where('eng','yes')->get();
        }
        elseif($event->courseInfo->language == "Ar")
            $subscribers = subscribers::where('status','active')->where('ar','yes')->get();

        if(count($subscribers) > 0){
            for($i=0; $i < count($subscribers); $i++){
                Mail::to($subscribers[$i]->email)->send(new NewCourse($event->courseInfo, $subscribers[$i]->name, $subscribers[$i]->email));
            }
        }
    }
}
