<?php

namespace App\Listeners;

use App\Events\sendNewProgramEvent;
use App\Mail\NewProgram;
use App\subscribers;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class sendNewProgramListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  sendNewProgramEvent  $event
     * @return void
     */
    public function handle(sendNewProgramEvent $event)
    {if($event->programInfo->language == "Eng"){
        $subscribers = subscribers::where('status','active')->where('eng','yes')->get();
    }
    elseif($event->programInfo->language == "Ar")
        $subscribers = subscribers::where('status','active')->where('ar','yes')->get();

        if(count($subscribers) > 0){
            for($i=0; $i < count($subscribers); $i++){
                Mail::to($subscribers[$i]->email)->send(new NewProgram($event->programInfo, $subscribers[$i]->name, $subscribers[$i]->email));
            }
        }
    }
}
