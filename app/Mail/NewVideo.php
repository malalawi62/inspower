<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewVideo extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $VideoInfo, $subscriberName, $mail;
    public function __construct($VideoInfo, $subscriberName, $mail)
    {
        $this->VideoInfo = $VideoInfo;
        $this->subscriberName = $subscriberName;
        $this->mail = $mail;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.NewVideo');
    }
}
