<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class WelcomeMail extends Mailable
{
    use Queueable, SerializesModels;

    public $mail, $name;

    public function __construct($mail, $name)
    {
        $this->mail = $mail;
        $this->name = $name;
    }

    public function build()
    {
        return $this->markdown('emails.welcome');
    }
}
