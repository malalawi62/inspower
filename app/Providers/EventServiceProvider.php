<?php

namespace App\Providers;

use App\Events\sendEmailConfirmationEvent;
use App\Events\sendEmailToSubscriberEvent;
use App\Events\sendNewArticlesEvent;
use App\Events\sendNewCourseEvent;
use App\Events\sendNewProgramEvent;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Queue\Listener;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        sendEmailToSubscriberEvent::class => [
            \App\Listeners\sendEmailListener::class,
        ],
        sendEmailConfirmationEvent::class => [
            \App\Listeners\sendEmaiConfirmlListener::class,
        ],
        sendNewArticlesEvent::class => [
            \App\Listeners\sendNewArticleListener::class,
        ],
        sendNewCourseEvent::class => [
            \App\Listeners\sendNewCourseListener::class,
        ],
        sendNewProgramEvent::class => [
            \App\Listeners\sendNewProgramListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
