<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    public function course()
    {
        return $this->belongsTo('App\course');
    }

    public function program()
    {
        return $this->belongsTo('App\program');
    }
}
