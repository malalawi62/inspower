<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    protected $guarded = []; //insert into the database

    public function membership()
    {
        return $this->belongsTo('App\memberships');
    }
}
