<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class articles extends Model
{
    public function writer()
    {
        return $this->belongsTo('App\writers');
    }

    public function category()
    {
        return $this->belongsTo('App\categories');
    }

    protected $guarded = []; //insert into the database
}
