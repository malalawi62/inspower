<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class categories extends Model
{
    public function article()
    {
        return $this->hasMany('App\articles');
    }

    public function youtube()
    {
        return $this->hasMany('App\youtube');
    }

    public function instagram()
    {
        return $this->hasMany('App\instagram');
    }

    protected $guarded = []; //insert into the database
}
