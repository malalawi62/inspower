<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class course extends Model
{
    public function course()
    {
        return $this->hasMany('App\Purchase');
    }

    protected $guarded = []; //insert into the database
}
