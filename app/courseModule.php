<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class courseModule extends Model
{
    public function course()
    {
        return $this->belongsTo('App\course');
    }

    public function video()
    {
        return $this->hasMany('App\courseModuleVideo');
    }

    protected $guarded = []; //insert into the database
}
