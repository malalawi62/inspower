<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class courseModuleVideo extends Model
{
    public function courseModule()
    {
        return $this->belongsTo('App\courseModule');
    }

    protected $guarded = []; //insert into the database
}
