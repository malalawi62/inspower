<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class memberships extends Model
{
    public function membership()
    {
        return $this->hasMany('App\Users');
    }

    protected $guarded = []; //insert into the database
}
