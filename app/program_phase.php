<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class program_phase extends Model
{
    public function program()
    {
        return $this->belongsTo('App\program');
    }

    public function section()
    {
        return $this->hasMany('App\program_phase_section');
    }

    protected $guarded = []; //insert into the database
}
