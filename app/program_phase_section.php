<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class program_phase_section extends Model
{
    public function programPhase()
    {
        return $this->belongsTo('App\programPhase');
    }

    protected $guarded = []; //insert into the database
}
