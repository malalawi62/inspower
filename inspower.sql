-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 24, 2021 at 09:09 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inspower`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `writerName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `language` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `writer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `category_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `title`, `date`, `writerName`, `image`, `body`, `language`, `created_at`, `updated_at`, `writer_id`, `category_id`) VALUES
(2, 'BLOOD FLOW RESTRICTION TRAINING MADE SIMPLE', '2020-11-12', NULL, 'uploads/AWbxZJQF8IrzPd6LKdECmb7bVpDURqOdJ7R8BmYH.jpeg', 'How the Hell Does BFR Work in the First Place?\r\nIn general terms, BFR uses bands or straps to partially reduce arterial blood flow while completely shutting off venous blood flow. This causes the blood in the limb that\'s wrapped to pool, which of course limits oxygen delivery and subsequently reduces the time it takes to reach muscular failure.\r\n\r\nYou can use lighter loads and they\'ll presumably work just as well as heavier loads. The usual rules determining hypertrophy still apply, though. Growth is still mediated by mTOR, but mTOR and its effects kick in earlier and to a greater degree when BFR is employed. Beyond that, BFR seems to employ two distinct underlying mechanisms to facilitate growth:\r\n\r\n1. Metabolite-Induced Fatigue\r\nBFR training, like conventional training, causes metabolites like lactate hydrogen ions, ATP, and inorganic phosphates to be formed, but the band(s) trap all that metabolic gunk in the limb. These lingering metabolites cause type 2 muscle fibers to be recruited earlier than they might with free-form exercise.\r\n\r\nAs these metabolites and fatigue increase, muscle activation increases and anabolic processes kick into gear.\r\n\r\n2. Cellular Swelling\r\nThis process, like it sounds, involves the accumulation of fluid in the blood-flow restricted limb, thereby stressing the cellular matrix and activating \"intracellular signaling pathways,\" which leads to growth.\r\n\r\nAside from those mechanisms, there\'s also evidence that BFR training enhances the satellite cell response to training. Satellite cells are little baby muscle cells that hang around the basal lamina of muscle cells, waiting to be called in after a muscle-damaging bout of training. At that point, they fuse with mature muscle cells and donate their nuclei and help with repair and growth.', 'Eng', '2020-11-10 19:36:14', '2020-11-10 19:36:14', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `course_id` bigint(20) UNSIGNED DEFAULT NULL,
  `program_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `carts`
--

INSERT INTO `carts` (`id`, `created_at`, `updated_at`, `user_id`, `course_id`, `program_id`) VALUES
(4, '2020-10-31 15:37:35', '2020-10-31 15:37:35', NULL, NULL, 23),
(5, '2020-10-31 15:37:40', '2020-10-31 15:37:40', NULL, NULL, 23),
(6, '2020-10-31 15:37:44', '2020-10-31 15:37:44', NULL, NULL, 23),
(8, '2020-10-31 15:38:47', '2020-10-31 15:38:47', 1, NULL, 23),
(12, '2020-11-25 06:38:05', '2020-11-25 06:38:05', 1, NULL, 25),
(14, '2021-01-24 18:26:10', '2021-01-24 18:26:10', 1, 43, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `englishName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `arabicName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `englishName`, `arabicName`, `created_at`, `updated_at`) VALUES
(1, 'sport', 'رياضة', '2020-09-21 11:40:41', '2020-09-21 11:40:41'),
(2, 'food', 'تغذية', '2020-09-21 11:41:07', '2020-09-21 11:41:07'),
(3, 'Training', 'التمرين', '2020-11-10 15:37:51', '2020-11-10 15:37:51');

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `videoUrl` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `overview` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `language` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `title`, `description`, `videoUrl`, `overview`, `price`, `created_at`, `updated_at`, `language`, `image`, `status`) VALUES
(35, 'Testing Add course from main menu', 'Your courses include:\r\n\r\nManaging Personal Health\r\nHealthy Eating Habits\r\nExercise Science Terminology\r\nFitness Anatomy and Physiology\r\nFlexibility Training and Injury Prevention\r\nStress Management Techniques\r\n...and much more.\r\nAnd you’ll learn it all at home — no classroom needed!\r\n\r\nWhy take a distance learning program in Fitness and Nutrition? With the right credentials, you can:\r\n\r\nBecome a fitness trainer\r\nBe an important part of a fitness and nutrition staff\r\nWork at a health club or gym\r\nDemand for Fitness and Nutrition professionals will rise nearly 27% by 2016.* As people spend more time and money on leisure and fitness services.\r\n\r\nPenn Foster Career School\'s Fitness and Nutrition program includes:\r\n\r\nAll the books and learning aids you need\r\nToll-free instructional support\r\nAccess to student services by website, phone, and mail\r\nGet Started Today!\r\nStart today and in as little as six months from enrollment you can earn your certificate.', NULL, 'Your courses include:\r\n\r\nManaging Personal Health\r\nHealthy Eating Habits\r\nExercise Science Terminology\r\nFitness Anatomy and Physiology\r\nFlexibility Training and Injury Prevention\r\nStress Management Techniques\r\n...and much more.\r\nAnd you’ll learn it all at home — no classroom needed!\r\n\r\nWhy take a distance learning program in Fitness and Nutrition? With the right credentials, you can:\r\n\r\nBecome a fitness trainer\r\nBe an important part of a fitness and nutrition staff\r\nWork at a health club or gym\r\nDemand for Fitness and Nutrition professionals will rise nearly 27% by 2016.* As people spend more time and money on leisure and fitness services.\r\n\r\nPenn Foster Career School\'s Fitness and Nutrition program includes:\r\n\r\nAll the books and learning aids you need\r\nToll-free instructional support\r\nAccess to student services by website, phone, and mail\r\nGet Started Today!\r\nStart today and in as little as six months from enrollment you can earn your certificate.', '150', '2020-12-22 17:54:12', '2021-01-02 08:20:39', 'Eng', 'uploads/U82V5T7C3VMQyLKcbJZ27FI7TTLmalpmx6kCoA3C.jpeg', 'active'),
(37, 'دورة جديدة محدثة', 'معلومات إضافية محدثة', '467136406', 'وصف الدورة الجديدة محدثة', '130', '2021-01-02 08:54:38', '2021-01-02 08:56:20', 'Ar', 'uploads/Fu0XMijKM7wK2BNgKjHlkZv02SQbDRrQh8nDzhHv.jpeg', 'inactive'),
(40, 'NEW COURSE 6 JAN', NULL, NULL, '6 JAN OVERVIEW', '150', '2021-01-06 17:35:02', '2021-01-06 17:35:02', 'Eng', 'uploads/JHFiwhxJxHHO2K65VXS188xNikRdHuDbchUIU8V2.jpeg', 'active'),
(41, 'TEST 2 COURSE', NULL, '467136407', 'TEST 2 COURSE OVERVIEW', '200', '2021-01-06 17:39:00', '2021-01-06 17:39:00', 'Eng', 'uploads/rzTKjCNQmWYKX7RdpQlsQKPuUhjA71H7em36sQKC.jpeg', 'active'),
(42, 'TESTING COURSE', NULL, NULL, 'TESTING COURSE OVERVIEW', '100', '2021-01-06 17:47:45', '2021-01-06 17:47:45', 'Eng', 'uploads/sqXwBudUyNlfYmsrdGK53Couwfi3e1gE0cF4opCs.jpeg', 'active'),
(43, 'Course with ERROR', NULL, NULL, 'Course with ERROR', '260', '2021-01-06 17:57:52', '2021-01-06 17:57:52', 'Eng', 'uploads/WwyDbRD6CTIyLIsbYcvUQWjeeJH2mG6ZX5SJqphm.jpeg', 'active'),
(44, 'Course with ERROR', NULL, NULL, 'Course with ERROR', '260', '2021-01-06 18:10:07', '2021-01-06 18:10:07', 'Eng', 'uploads/HMvX3IqDRiRAmMSqmZE1WPUysXtucfojdkgLNMwS.jpeg', 'active'),
(45, 'course', NULL, NULL, 'course', '222', '2021-01-06 18:12:23', '2021-01-06 18:12:23', 'Eng', 'uploads/jLgkV6oppgyne7G1j2lRsRwpuSne5KM4OGXlnBgy.jpeg', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `course_modules`
--

CREATE TABLE `course_modules` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `m_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `m_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `course_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `course_modules`
--

INSERT INTO `course_modules` (`id`, `m_title`, `m_description`, `created_at`, `updated_at`, `course_id`) VALUES
(36, 'Module New', 'Your courses include:\r\n\r\nManaging Personal Health\r\nHealthy Eating Habits\r\nExercise Science Terminology\r\nFitness Anatomy and Physiology\r\nFlexibility Training and Injury Prevention\r\nStress Management Techniques\r\n...and much more.\r\nAnd you’ll learn it all at home — no classroom needed!\r\n\r\nWhy take a distance learning program in Fitness and Nutrition? With the right credentials, you can:\r\n\r\nBecome a fitness trainer\r\nBe an important part of a fitness and nutrition staff\r\nWork at a health club or gym\r\nDemand for Fitness and Nutrition professionals will rise nearly 27% by 2016.* As people spend more time and money on leisure and fitness services.\r\n\r\nPenn Foster Career School\'s Fitness and Nutrition program includes:\r\n\r\nAll the books and learning aids you need\r\nToll-free instructional support\r\nAccess to student services by website, phone, and mail\r\nGet Started Today!\r\nStart today and in as little as six months from enrollment you can earn your certificate.', '2020-12-28 04:56:36', '2020-12-28 04:56:36', 35),
(39, 'الجزء الثاني الجديد', 'وصف للجزء الثاني الجديد', '2021-01-02 08:58:10', '2021-01-02 08:58:10', 37),
(44, '6 JAN MODULE 1', 'MODULE 1 DESCRIPTION', '2021-01-06 17:35:02', '2021-01-06 17:35:02', 40),
(45, '6 JAN MODULE 2', 'MODULE 2 DESCRIPTION', '2021-01-06 17:35:02', '2021-01-06 17:35:02', 40),
(46, '6 JAN MODULE 3', 'MODULE 3 DESCRIPTION', '2021-01-06 17:35:02', '2021-01-06 17:35:02', 40),
(47, 'TEST 2 MODULE 1', 'MODULE 1 DESCRIPTION', '2021-01-06 17:39:00', '2021-01-06 17:39:00', 41),
(48, 'TESTING MODULE 1', 'MODULE 1 DESCRIPTION', '2021-01-06 17:47:45', '2021-01-06 17:47:45', 42),
(49, 'TESTING MODULE 2', 'MODULE 2 DESCRIPTION', '2021-01-06 17:47:45', '2021-01-06 17:47:45', 42),
(50, 'TESTING MODULE 3', 'MODULE 3 DESCRIPTION', '2021-01-06 17:47:45', '2021-01-06 17:47:45', 42),
(51, 'Module 1 - ERROR', 'Course with ERROR', '2021-01-06 17:57:52', '2021-01-06 17:57:52', 43),
(52, 'Module 3 - ERROR', 'Course with ERROR', '2021-01-06 17:57:52', '2021-01-06 17:57:52', 43),
(53, 'Module 1 - ERROR', 'Course with ERROR', '2021-01-06 18:10:07', '2021-01-06 18:10:07', 44),
(54, 'Module 3 - ERROR', 'Course with ERROR', '2021-01-06 18:10:07', '2021-01-06 18:10:07', 44),
(55, 'module 1', 'module 1', '2021-01-06 18:12:23', '2021-01-06 18:12:23', 45),
(56, 'module 2', 'module 2', '2021-01-06 18:12:23', '2021-01-06 18:12:23', 45),
(57, 'module 4', 'module 4', '2021-01-06 18:12:23', '2021-01-06 18:12:23', 45),
(58, 'module 6', 'module 6', '2021-01-06 18:12:23', '2021-01-06 18:12:23', 45);

-- --------------------------------------------------------

--
-- Table structure for table `course_module_videos`
--

CREATE TABLE `course_module_videos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `m_videoUrl` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `course_module_id` bigint(20) UNSIGNED DEFAULT NULL,
  `v_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pdf` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `course_module_videos`
--

INSERT INTO `course_module_videos` (`id`, `m_videoUrl`, `created_at`, `updated_at`, `course_module_id`, `v_description`, `pdf`) VALUES
(47, '467136407', '2021-01-02 08:46:43', '2021-01-02 08:46:43', 36, 'description', 'PE2yHXm9Npx1nQy9bGdERN6bor9MmKfoISY9HcL8.pdf'),
(49, '467136407', '2021-01-02 08:58:10', '2021-01-02 08:58:10', 39, NULL, 'TdersaVIbEi5hZF2y3O9ilUbLKL23sCf3FnWJGiH.pdf'),
(53, '467136407', '2021-01-06 17:35:02', '2021-01-06 17:35:02', 44, 'MODULE 1 LESSON 1 DESCRIPTION', 'QfRi2MJIzL539a9fmr7ByOfesS28yaP3qvJrpYJk.pdf'),
(54, '467136407', '2021-01-06 17:35:02', '2021-01-06 17:35:02', 45, 'MODULE 2 LESSON 1 DESCRIPTION', 'gjk2T24RARshprYkcQuUyTdJNgvPWOZkUWvKcwAA.pdf'),
(55, '467136407', '2021-01-06 17:35:02', '2021-01-06 17:35:02', 46, 'MODULE 3 LESSON 1 DESCRIPTION', 'cJNdRATENFcNABweerjEuzjsJQZDd1GfRMiSWnfH.pdf'),
(56, '467136407', '2021-01-06 17:39:00', '2021-01-06 17:39:00', 47, 'LESSON 1 DESCRIPTION', 'hniMjORY2AUptoXSCQq5IHtyB4sVZZPfqljPI6x1.pdf'),
(57, '467136407', '2021-01-06 17:39:00', '2021-01-06 17:39:00', 47, 'LESSON 2 DESCRIPTION', 'ZRL4Zzhp8aFhJjirivpbQX4uDBs22u7FCTl5xIFN.pdf'),
(58, '467136407', '2021-01-06 17:39:00', '2021-01-06 17:39:00', 47, 'LESSON 3 DESCRIPTION', '62hjQ2FEQlcUEaou9IJbg20EbkW9ZpCG5qIN6tMr.pdf'),
(59, '467136407', '2021-01-06 17:47:45', '2021-01-06 17:47:45', 48, 'MODULE 1 LESSON 1 DESCRIPTION', 'NsMc5DGdq456fqMheGbqaHfhXdqV3O0OIyYFUEMM.pdf'),
(60, '467136407', '2021-01-06 17:47:45', '2021-01-06 17:47:45', 48, 'MODULE 1 LESSON 2 DESCRIPTION', 'yOqM5jE5gpXgdJgFueoIuLPDuXlDlTrpzbNgdlGI.pdf'),
(61, '467136407', '2021-01-06 17:47:45', '2021-01-06 17:47:45', 49, 'MODULE 2 LESSON 1 DESCRIPTION', 'nYBFQl7lfv8y31bfs5hz7C6R5TnkdpvR8hlOLtcN.pdf'),
(62, '467136407', '2021-01-06 17:47:45', '2021-01-06 17:47:45', 49, 'MODULE 2 LESSON 2 DESCRIPTION', 'TixnoQ2tCHNGF4CDj0u00NItgrNYtyE1UfP2KMpy.pdf'),
(63, '467136407', '2021-01-06 17:47:45', '2021-01-06 17:47:45', 49, 'MODULE 2 LESSON 3 DESCRIPTION', 'eEmcVWg39wbt23PHs25f9l5N26xzoAcZ3ypjte9a.pdf'),
(64, '467136407', '2021-01-06 17:47:45', '2021-01-06 17:47:45', 50, 'MODULE 3 LESSON 1 DESCRIPTION', '0EWWm5UVpSU5rlEirMorgOBMuwVwgpoIfxDGyuki.pdf'),
(65, '467136407', '2021-01-06 17:57:52', '2021-01-06 17:57:52', 51, 'description', 'Z6qKL06XI68w0C1GtICJ94Hj54euIHkcSWAIOK7I.pdf'),
(66, '467136407', '2021-01-06 18:10:07', '2021-01-06 18:10:07', 53, 'description', 'ZzS17vRptAYnieahJozQzmp620X2MG8qgqkLmjsh.pdf'),
(67, '467136407', '2021-01-06 18:10:07', '2021-01-06 18:10:07', 54, 'Course with ERROR', 'd2AQDHMAuapUQ2ZrBIHZ2l7LVe90dKikQwDRyPde.pdf'),
(68, '467136407', '2021-01-06 18:12:23', '2021-01-06 18:12:23', 55, 'description', 'xw2BStNRDNhDYMCRxlFHwy5TPLKt68GgDPLSvEud.pdf'),
(69, '467136407', '2021-01-06 18:12:23', '2021-01-06 18:12:23', 56, 'desc', 'B9xyQr3uQaNJzEJIBtfx5YdTkYDxF8Y4v8j9J03E.pdf'),
(70, '467136407', '2021-01-06 18:12:23', '2021-01-06 18:12:23', 57, 'description', 'xesVfAB0IPdd6iipuPQ44BgxwgZifktgshbD8MmF.pdf'),
(71, '467136407', '2021-01-06 18:12:23', '2021-01-06 18:12:23', 58, 'description', 'xesVfAB0IPdd6iipuPQ44BgxwgZifktgshbD8MmF.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `course_module_video_status`
--

CREATE TABLE `course_module_video_status` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `currentSecond` float NOT NULL DEFAULT 0,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'incomplete',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `course_module_video_id` bigint(20) UNSIGNED DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `course_module_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `course_module_video_status`
--

INSERT INTO `course_module_video_status` (`id`, `currentSecond`, `status`, `created_at`, `updated_at`, `course_module_video_id`, `user_id`, `course_module_id`) VALUES
(46, 0, 'incomplete', '2021-01-02 08:46:43', '2021-01-02 08:46:43', 47, 1, 36),
(47, 0, 'incomplete', NULL, NULL, 47, 2, 36),
(49, 0, 'incomplete', '2021-01-02 08:58:10', '2021-01-02 08:58:10', 49, 1, 39),
(53, 0, 'incomplete', '2021-01-06 17:35:02', '2021-01-06 17:35:02', 53, 1, 44),
(54, 0, 'incomplete', '2021-01-06 17:35:02', '2021-01-06 17:35:02', 54, 1, 45),
(55, 0, 'incomplete', '2021-01-06 17:35:02', '2021-01-06 17:35:02', 55, 1, 46),
(56, 0, 'incomplete', '2021-01-06 17:39:00', '2021-01-06 17:39:00', 56, 1, 47),
(57, 0, 'incomplete', '2021-01-06 17:39:00', '2021-01-06 17:39:00', 57, 1, 47),
(58, 0, 'incomplete', '2021-01-06 17:39:00', '2021-01-06 17:39:00', 58, 1, 47),
(59, 0, 'incomplete', '2021-01-06 17:47:45', '2021-01-06 17:47:45', 59, 1, 48),
(60, 0, 'incomplete', '2021-01-06 17:47:45', '2021-01-06 17:47:45', 60, 1, 48),
(61, 0, 'incomplete', '2021-01-06 17:47:45', '2021-01-06 17:47:45', 61, 1, 49),
(62, 0, 'incomplete', '2021-01-06 17:47:45', '2021-01-06 17:47:45', 62, 1, 49),
(63, 0, 'incomplete', '2021-01-06 17:47:45', '2021-01-06 17:47:45', 63, 1, 49),
(64, 0, 'incomplete', '2021-01-06 17:47:45', '2021-01-06 17:47:45', 64, 1, 50),
(65, 0, 'incomplete', '2021-01-06 17:57:52', '2021-01-06 17:57:52', 65, 1, 51),
(66, 0, 'incomplete', '2021-01-06 18:10:07', '2021-01-06 18:10:07', 66, 1, 53),
(67, 0, 'incomplete', '2021-01-06 18:10:07', '2021-01-06 18:10:07', 67, 1, 54),
(68, 0, 'incomplete', '2021-01-06 18:12:23', '2021-01-06 18:12:23', 68, 1, 55),
(69, 0, 'incomplete', '2021-01-06 18:12:23', '2021-01-06 18:12:23', 69, 1, 56),
(70, 0, 'incomplete', '2021-01-06 18:12:23', '2021-01-06 18:12:23', 70, 1, 57),
(71, 0, 'incomplete', '2021-01-06 18:12:23', '2021-01-06 18:12:23', 71, 1, 58);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `instagrams`
--

CREATE TABLE `instagrams` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `titleAr` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `descriptionAr` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `category_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_05_28_082112_create_categories_table', 1),
(5, '2020_05_28_082157_create_articles_table', 1),
(6, '2020_05_28_082313_create_writers_table', 1),
(7, '2020_05_28_102346_update_articles_table', 1),
(8, '2020_05_31_203351_create_subscribers_table', 1),
(9, '2020_06_07_151910_create_youtubes_table', 1),
(10, '2020_06_07_162043_update_youtube_table', 1),
(11, '2020_06_19_180106_create_instagrams_table', 1),
(12, '2020_06_20_013044_update_instagrams_table', 1),
(13, '2020_06_21_195805_create_jobs_table', 1),
(14, '2020_09_02_190252_create_programs_table', 1),
(15, '2020_09_02_190653_create_courses_table', 1),
(16, '2020_09_02_190959_create_course_modules_table', 1),
(17, '2020_09_02_191121_create_course_module_videos_table', 1),
(18, '2020_09_29_121519_create_carts_table', 2),
(19, '2020_10_06_100818_update_carts_table', 2),
(20, '2020_10_11_162210_create_purchases_table', 2),
(21, '2020_10_11_162535_update_purchases_table', 2),
(22, '2020_10_26_025526_update_course_modules_table', 3),
(23, '2020_10_26_025546_update_course_module_videos_table', 3),
(24, '2020_10_26_171016_create_program_phases_table', 4),
(25, '2020_10_26_173351_create_program_phase_sections_table', 4),
(26, '2020_10_26_192212_update_program_phases_table', 5),
(27, '2020_10_26_192230_update_program_phase_sections_table', 5),
(28, '2020_11_11_010919_update_courses_table', 6),
(29, '2020_11_11_010939_update_programs_table', 6),
(30, '2020_11_24_165853_create_course_module_video_status_table', 7),
(31, '2020_11_24_165950_update_course_module_video_status_table', 7),
(32, '2020_11_28_124333_update2_course_module_videos_table', 8),
(33, '2020_12_12_164149_update2_course_module_video_status_table', 9);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `programs`
--

CREATE TABLE `programs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `videoUrl` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `overview` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pdf` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `language` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `programs`
--

INSERT INTO `programs` (`id`, `title`, `description`, `videoUrl`, `overview`, `price`, `pdf`, `created_at`, `updated_at`, `language`, `image`, `status`) VALUES
(23, 'Fitness and Nutrition Certificate UPDATED', 'Your courses include:\r\n\r\nManaging Personal Health\r\nHealthy Eating Habits\r\nExercise Science Terminology\r\nFitness Anatomy and Physiology\r\nFlexibility Training and Injury Prevention\r\nStress Management Techniques\r\n...and much more.\r\nAnd you’ll learn it all at home — no classroom needed!\r\n\r\nWhy take a distance learning program in Fitness and Nutrition? With the right credentials, you can:\r\n\r\nBecome a fitness trainer\r\nBe an important part of a fitness and nutrition staff\r\nWork at a health club or gym\r\nDemand for Fitness and Nutrition professionals will rise nearly 27% by 2016.* As people spend more time and money on leisure and fitness services.\r\n\r\nPenn Foster Career School\'s Fitness and Nutrition program includes:\r\n\r\nAll the books and learning aids you need\r\nToll-free instructional support\r\nAccess to student services by website, phone, and mail\r\nGet Started Today!\r\nStart today and in as little as six months from enrollment you can earn your certificate.', '437552856', 'Learn the skills you need to become a Fitness and Nutrition expert — at home, at your own pace, with Penn Foster Career School International.\r\nThe Penn Foster Career School Fitness and Nutrition Program can help you achieve your goals of a more productive career and the satisfaction that comes with doing a job you love. Earn your certificate quickly and conveniently through online learning.', '140', 'pv9pa8itMTdjaSuB0T3ooKPpsIu1bDAe6h3CBsL0.pdf', '2020-10-31 15:10:40', '2020-12-19 10:25:21', 'Eng', 'uploads/ioiF5MjFztQOSt1uOxsXcb0Sq9ZBBA0VWSKthwpq.jpeg', 'active'),
(24, 'الدبلوم التدريبي لإعداد المدرب الشخصي', 'الفئات المستهدفة للدراسة في الدبلوم التدريبي\r\nطلاب وخريجي كليات التربية الرياضية\r\nالمهتمين بالعمل في مجال الأندية الصحية Gym \r\nالمهتمين بالعمل في مجال التدريب الرياضي واللياقة البدنية', '437552856', 'برنامج تدريبي أونلاين يهدف الى تأهيل المدرب الشخصي ومدربي اللياقة البدنية بالأندية الصحية - الجيم، حيث يكتسب المشاركون المهارات الادارية والفنية اللازمة للنجاح في هذا المجال، كمفاهيم التدريب الرياضي وتخطيط الاحمال والتحليل الحركي في المجال الرياضي، وأيضا التغذية للرياضيين والاعداد النفسي واصابات الملاعب وكيفية التعامل معها.\r\n\r\nدبلوم تدريبي اونلاين يهدف الى تأهيل المدرب الشخصي ومدربي اللياقة البدنية بالاندية الصحية - الجيم، حيث يكتسب المشاركون في هذ البرنامج التدريبي جميع المهارات الادارية والفنية اللازمة للنجاح في هذا المجال مثل مفاهيم التدريب الرياضي وتخطيط الأحمال والتحليل الحركي في المجال الرياضي وكذلك مواضيع مثل التغذية للرياضيين والاعداد النفسي للرياضيين واصابات الملاعب وكيفية التعامل معها، تتم الدراسة اونلاين من خلال الانترنت عبر محاضرات تفاعلية من خلال نظام الفصول الافتراضية.', '120', 'Kl4YJxxOreNDEKQzjGdAg7u3J6nnyOMq07nvY3cW.pdf', '2020-11-04 14:35:46', '2020-12-19 10:52:34', 'Ar', 'uploads/YMjuNeRDpSI99k3RuqQbFsgYflubw9KIwxebm5GL.jpeg', 'active'),
(25, 'الدبلوم التدريبي لإعداد المدرب الشخصي', 'دبلوم تدريبي اونلاين يهدف الى تأهيل المدرب الشخصي ومدربي اللياقة البدنية بالاندية الصحية - الجيم، حيث يكتسب المشاركون في هذ البرنامج التدريبي جميع المهارات الادارية والفنية اللازمة للنجاح في هذا المجال مثل مفاهيم التدريب الرياضي وتخطيط الأحمال والتحليل الحركي في المجال الرياضي وكذلك مواضيع مثل التغذية للرياضيين والاعداد النفسي للرياضيين واصابات الملاعب وكيفية التعامل معها، تتم الدراسة اونلاين من خلال الانترنت عبر محاضرات تفاعلية من خلال نظام الفصول الافتراضية.', '437552856', 'برنامج تدريبي أونلاين يهدف الى تأهيل المدرب الشخصي ومدربي اللياقة البدنية بالأندية الصحية - الجيم، حيث يكتسب المشاركون المهارات الادارية والفنية اللازمة للنجاح في هذا المجال، كمفاهيم التدريب الرياضي وتخطيط الاحمال والتحليل الحركي في المجال الرياضي، وأيضا التغذية للرياضيين والاعداد النفسي واصابات الملاعب وكيفية التعامل معها.', '150', 'fUAZ8knQnIEWbi4JceE5GkSHsuMF8pkczjcUN2xq.pdf', '2020-11-04 14:46:59', '2020-11-04 14:46:59', 'Ar', 'uploads/AMBbYRfS2ovihxYwe2qZcC1DA3l5L3GpaHGUORQm.jpeg', 'active'),
(26, 'الدبلوم التدريبي لإعداد المدرب الشخصي تحديث', 'دبلوم تدريبي اونلاين يهدف الى تأهيل المدرب الشخصي ومدربي اللياقة البدنية بالاندية الصحية - الجيم، حيث يكتسب المشاركون في هذ البرنامج التدريبي جميع المهارات الادارية والفنية اللازمة للنجاح في هذا المجال مثل مفاهيم التدريب الرياضي وتخطيط الأحمال والتحليل الحركي في المجال الرياضي وكذلك مواضيع مثل التغذية للرياضيين والاعداد النفسي للرياضيين واصابات الملاعب وكيفية التعامل معها، تتم الدراسة اونلاين من خلال الانترنت عبر محاضرات تفاعلية من خلال نظام الفصول الافتراضية.', '437552856', 'برنامج تدريبي أونلاين يهدف الى تأهيل المدرب الشخصي ومدربي اللياقة البدنية بالأندية الصحية - الجيم، حيث يكتسب المشاركون المهارات الادارية والفنية اللازمة للنجاح في هذا المجال، كمفاهيم التدريب الرياضي وتخطيط الاحمال والتحليل الحركي في المجال الرياضي، وأيضا التغذية للرياضيين والاعداد النفسي واصابات الملاعب وكيفية التعامل معها.', '150', 'pv9pa8itMTdjaSuB0T3ooKPpsIu1bDAe6h3CBsL0.pdf', '2020-11-04 14:46:59', '2020-12-22 12:54:11', 'Ar', 'uploads/GJbmOBtRXQcWTtcnFIu3ueobKaDOBm3mjG4eUa3F.jpeg', 'active'),
(27, 'NEW PROGRAM TESTING', NULL, NULL, 'NEW PROGRAM TESTING OVERVIEW', '150', 'D1YsGvBdpMRwsUE3k47ePAt0K1HMTS3pzwP3xBwE.pdf', '2021-01-02 09:55:04', '2021-01-02 09:55:04', 'Eng', 'uploads/NeczZwwXoSABeXnb5NWx479fDY0zvOAUBZWI2R1h.jpeg', 'active'),
(28, 'program testignc UPDATE', NULL, '', 'overview testing UPDATE', '160', 'vMeoZofWnpZzBM2EoEFhQ4CplcFM1myaDOxgY2V5.pdf', '2021-01-02 09:57:53', '2021-01-02 10:31:01', 'Eng', 'uploads/fp3MD6ZzX8AfJgooJDkH3QxdzwXYXFaLy5GveAXg.jpeg', 'inactive'),
(31, 'Program MULTI Phases ONE Section', NULL, NULL, 'Program MULTI Phases ONE Section Overview', '260', 'aue9ysydMsvIf1V1ZXTwE98JwoYcbz9H2UZR5AHc.pdf', '2021-01-03 13:57:14', '2021-01-03 13:57:14', 'Eng', 'uploads/Fjshu1JJWTnT3hf01AEW0UKpgqtc0WkLqkcHpvKj.jpeg', 'active'),
(32, 'Program MULTI Phases ONE Section', NULL, NULL, 'Program MULTI Phases ONE Section Overview', '260', 'lICDxvDt2fxunZvbaqjJ6637BTpZ8NdQn4ve4OJ1.pdf', '2021-01-03 14:46:12', '2021-01-03 14:46:12', 'Eng', 'uploads/xSWvBjLzQM91kiJc9m21bB9L2KnSaOlPCLtIDp3m.jpeg', 'active'),
(33, 'https://vimeo.com/467136407', NULL, NULL, 'https://vimeo.com/467136407', '250', 'ZCQPpcPjnePnAh357RCPpXCyXTN94qIwMT2057Rt.pdf', '2021-01-03 14:49:46', '2021-01-03 14:49:46', 'Eng', 'uploads/rxI3FeoacnCRg96kcZN9nGgRmgD5TgDez5NjwFOj.jpeg', 'active'),
(34, 'Fitness and Nutrition Certificate', NULL, NULL, 'Fitness and Nutrition Certificate', '249', 'iO9G1z4er2jy1lytD5akpEKVT4RXjlxC5WXL8EH4.pdf', '2021-01-03 15:14:21', '2021-01-03 15:14:21', 'Eng', 'uploads/Ur43hpIKpuJ589beSDQlyBnyxlbeokOR1btsrptP.jpeg', 'active'),
(35, 'program with multi sections', NULL, NULL, 'program with multi sections', '200', 'e8zNCAUfnWzyQuZ4R1IfeWjRMwHUVnuSr4HGevbw.pdf', '2021-01-04 17:50:26', '2021-01-04 17:50:26', 'Eng', 'uploads/WAgIulMfiIE7M0uj3Wv1h5i09ajeoyU4Br9kYmFY.jpeg', 'active'),
(36, 'multi multi', NULL, NULL, 'multi multi', '299', '64zPNvFFUyIqfy7XP6lTIqp39YBYQJiJhzvW9IHe.pdf', '2021-01-04 17:53:52', '2021-01-04 17:53:52', 'Eng', 'uploads/aW1nEeRJBgjIa9M7ziBtJRJIDy6H0M6pK8qGA7cG.jpeg', 'active'),
(37, 'program', NULL, NULL, 'program', '250', 'FZjpbAnMfpqBpsLtMJ1lhxbZWkHwWoYSDQMKjpy3.pdf', '2021-01-06 18:15:10', '2021-01-06 18:15:10', 'Eng', 'uploads/F4TDqUv6Z4T31oIvWX5A82Ps1Z518ZUbvd30jzr4.jpeg', 'active'),
(38, 'program', NULL, NULL, 'program', '250', 'pK7y77fBkr3XB2qbkKisHXOYJ0WzhESzTbtOzH4I.pdf', '2021-01-06 18:16:17', '2021-01-06 18:16:17', 'Eng', 'uploads/FeeinsElRYIT7bS8MoSTj4rQN7GniqSHfRR23eFZ.jpeg', 'active'),
(39, 'تست فيز بي دي اف', NULL, NULL, 'تست فيز بي دي اف', '150', '9bu1YJ20u80USqAfSGT41yTcgpd4ywGfAElnKF5i.pdf', '2021-01-19 17:05:22', '2021-01-19 17:12:46', 'Ar', 'uploads/tEz775IfRf9LKBZX3GSolKx21xqWtV3ek37bNisc.jpeg', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `program_phases`
--

CREATE TABLE `program_phases` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `videoUrl` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `overview` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `pdf` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `program_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `program_phases`
--

INSERT INTO `program_phases` (`id`, `title`, `description`, `videoUrl`, `overview`, `pdf`, `created_at`, `updated_at`, `program_id`) VALUES
(7, 'phase -1-', 'Become a fitness trainer\r\nBe an important part of a fitness and nutrition staff\r\nWork at a health club or gym\r\nDemand for Fitness and Nutrition professionals will rise nearly 27% by 2016.* As people spend more time and money on leisure and fitness services.', '437552856', 'Become a fitness trainer\r\nBe an important part of a fitness and nutrition staff\r\nWork at a health club or gym\r\nDemand for Fitness and Nutrition professionals will rise nearly 27% by 2016.* As people spend more time and money on leisure and fitness services.', NULL, '2020-10-31 15:10:40', '2020-10-31 15:10:40', 23),
(8, 'المرحلة الأولى', 'مفهوم الحركات الرياضية\r\nتعريف الحركة الرياضية\r\nمميزات الحركات الرياضية\r\nأنواع الحركات الرياضية\r\nخصائص الحركات الرياضية\r\nقواعد تقويم الحركات الرياضية', '437552856', 'مفهوم التدريب الرياضي\r\nتعريف التدريب الرياضي\r\nالتدريب الرياضي الحديث\r\nأهداف التدريب الرياضي\r\nخصائص التدريب الرياضي\r\nواجبات التدريب الرياضي.\r\nاحتياجات الرياضي الفردية.\r\nمبادئ التدريب الرياضي.', NULL, '2020-11-04 14:35:46', '2020-11-04 14:35:46', 24),
(9, 'المرحلة الثانية UPDATED', 'مفهوم الحركات الرياضية\r\nتعريف الحركة الرياضية\r\nمميزات الحركات الرياضية\r\nأنواع الحركات الرياضية\r\nخصائص الحركات الرياضية\r\nقواعد تقويم الحركات الرياضية', 'https://vimeo.com/https://vimeo.com/437552856', 'مفهوم التدريب الرياضي\r\nتعريف التدريب الرياضي\r\nالتدريب الرياضي الحديث\r\nأهداف التدريب الرياضي\r\nخصائص التدريب الرياضي\r\nواجبات التدريب الرياضي.\r\nاحتياجات الرياضي الفردية.\r\nمبادئ التدريب الرياضي.', '0CRb7D8z5vfmpbfAQ9bYF7Dwj0WLd40GjcsCwBSc.pdf', '2020-11-04 14:35:46', '2020-12-19 12:42:33', 24),
(10, 'المرحلة الأولى', 'الفئات المستهدفة للدراسة في الدبلوم التدريبي\r\nطلاب وخريجي كليات التربية الرياضية\r\nالمهتمين بالعمل في مجال الأندية الصحية Gym\r\nالمهتمين بالعمل في مجال التدريب الرياضي واللياقة البدنية', '437552856', 'الفئات المستهدفة للدراسة في الدبلوم التدريبي\r\nطلاب وخريجي كليات التربية الرياضية\r\nالمهتمين بالعمل في مجال الأندية الصحية Gym\r\nالمهتمين بالعمل في مجال التدريب الرياضي واللياقة البدنية', NULL, '2020-11-04 14:46:59', '2020-11-04 14:46:59', 25),
(11, 'المرحلة الثانية', 'الفئات المستهدفة للدراسة في الدبلوم التدريبي\r\nطلاب وخريجي كليات التربية الرياضية\r\nالمهتمين بالعمل في مجال الأندية الصحية Gym\r\nالمهتمين بالعمل في مجال التدريب الرياضي واللياقة البدنية', '437552856', 'الفئات المستهدفة للدراسة في الدبلوم التدريبي\r\nطلاب وخريجي كليات التربية الرياضية\r\nالمهتمين بالعمل في مجال الأندية الصحية Gym\r\nالمهتمين بالعمل في مجال التدريب الرياضي واللياقة البدنية', NULL, '2020-11-04 14:46:59', '2020-11-04 14:46:59', 25),
(13, 'المرحلة الثانية', 'الفئات المستهدفة للدراسة في الدبلوم التدريبي\r\nطلاب وخريجي كليات التربية الرياضية\r\nالمهتمين بالعمل في مجال الأندية الصحية Gym\r\nالمهتمين بالعمل في مجال التدريب الرياضي واللياقة البدنية', '437552856', 'الفئات المستهدفة للدراسة في الدبلوم التدريبي\r\nطلاب وخريجي كليات التربية الرياضية\r\nالمهتمين بالعمل في مجال الأندية الصحية Gym\r\nالمهتمين بالعمل في مجال التدريب الرياضي واللياقة البدنية', NULL, '2020-11-04 14:46:59', '2020-11-04 14:46:59', 26),
(14, 'New phase testing title', NULL, NULL, 'New phase testing overview', 'Vj3USZwGtdxqTyM9aovyiRs2pgCpYe0xM7wtbafb.pdf', '2020-12-22 13:29:00', '2020-12-22 13:29:01', 26),
(15, 'NEW PHASE', NULL, NULL, 'NEW PHASE OVERVIEW', NULL, '2021-01-02 09:55:04', '2021-01-02 09:55:04', 27),
(21, 'Phase 1 MULTI Phases ONE section', NULL, NULL, 'Phase 1 MULTI Phases ONE section overview', NULL, '2021-01-03 13:57:14', '2021-01-03 13:57:14', 31),
(22, 'Phase 1 MULTI Phases ONE section', NULL, NULL, 'Phase 1 MULTI Phases ONE section overview', NULL, '2021-01-03 14:46:12', '2021-01-03 14:46:12', 32),
(23, 'Phase 2 MULTI Phases ONE section', NULL, NULL, 'Phase 2 MULTI Phases ONE section overview', NULL, '2021-01-03 14:46:12', '2021-01-03 14:46:12', 32),
(24, 'Phase 3 MULTI Phases ONE section', NULL, NULL, 'Phase 3 MULTI Phases ONE section overview', NULL, '2021-01-03 14:46:12', '2021-01-03 14:46:12', 32),
(25, 'https://vimeo.com/467136407', NULL, NULL, 'https://vimeo.com/467136407', NULL, '2021-01-03 14:49:46', '2021-01-03 14:49:46', 33),
(26, 'Fitness and Nutrition Certificate', NULL, NULL, 'Fitness and Nutrition Certificate', 'GPb4LoJodynlfrENeYiLQyjsDu4d1S7M5Ikzh4dX.pdf', '2021-01-03 15:14:21', '2021-01-03 15:14:21', 34),
(27, 'Fitness and Nutrition Certificate', NULL, NULL, 'Fitness and Nutrition Certificate', 'Z5magIzGENqg10BR9Cld01pVzJLUNNb396l1tlb8.pdf', '2021-01-03 15:14:21', '2021-01-03 15:14:21', 34),
(28, 'phase', NULL, NULL, 'phase', 'P3C9ZsIA8uPlbVVOev8tb0vpR8L1Ql9pTeWcFHmu.pdf', '2021-01-04 17:50:26', '2021-01-04 17:50:26', 35),
(29, 'multi multi phase 1', NULL, NULL, 'multi multi', 'uTbyin08Rhyi8RUSL5pZUooJm2EEMY0E1x6Gcf5Z.pdf', '2021-01-04 17:53:52', '2021-01-04 17:53:52', 36),
(30, 'multi multi phase 1', NULL, NULL, 'multi multi', 'EfTVyQj35j5ww3CXfiKtqorH8eHEQZnwl8QYB0Nf.pdf', '2021-01-04 17:53:52', '2021-01-04 17:53:52', 36),
(31, 'multi multi phase 3', NULL, NULL, 'multi multi phase 3', 'w6wJJRT6UsAreP9q4KjbFuniSWP4UZByIZOktZym.pdf', '2021-01-04 17:53:52', '2021-01-04 17:53:52', 36),
(32, 'phase 1', NULL, NULL, 'phase 1', 'WgC8YJkIPwyzsDenTKNpbT3zyxpq1KcqgMl4gnM0.pdf', '2021-01-06 18:15:10', '2021-01-06 18:15:10', 37),
(33, 'phase 3', NULL, NULL, 'phase 3', '0RdXApoaCb6alwKPFQxmKuBXAZB1ISYQ7h7Bl0Np.pdf', '2021-01-06 18:15:10', '2021-01-06 18:15:10', 37),
(34, 'phase 1', NULL, NULL, 'phase 1', 'Txu85VmgK7QRQFWiHPTLnKPzozG4cR3dlsRf7CuF.pdf', '2021-01-06 18:16:17', '2021-01-06 18:16:17', 38),
(35, 'phase 3', NULL, NULL, 'phase 3', 'AWG7rTRGLmk3JcHWvdCMB8YFWzQUF7TklcfYzJZW.pdf', '2021-01-06 18:16:17', '2021-01-06 18:16:17', 38),
(36, 'test phase', NULL, NULL, 'test phase', NULL, '2021-01-06 18:42:06', '2021-01-06 18:42:06', 38),
(37, 'مرحلة 1', NULL, NULL, 'وصف', 'd4qMqjs4iY1l0Ie47w8g6hvXunOfhB6pY0HfYqx8.pdf', '2021-01-19 17:05:22', '2021-01-19 17:11:27', 39),
(38, 'مرحلة بي دي اف', NULL, NULL, 'ننن', '3IGdnhvB6Z8iqBLqj3iF1m5q5bG9FXXsEfyPKodK.pdf', '2021-01-19 17:06:23', '2021-01-19 17:06:23', 39);

-- --------------------------------------------------------

--
-- Table structure for table `program_phase_sections`
--

CREATE TABLE `program_phase_sections` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `program_phase_id` bigint(20) UNSIGNED DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `videoUrl` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `program_phase_sections`
--

INSERT INTO `program_phase_sections` (`id`, `created_at`, `updated_at`, `program_phase_id`, `description`, `videoUrl`) VALUES
(7, '2020-10-31 15:10:40', '2020-10-31 15:10:40', 7, 'Become a fitness trainer\r\nBe an important part of a fitness and nutrition staff\r\nWork at a health club or gym', '437552856'),
(8, '2020-11-04 14:35:46', '2020-11-04 14:35:46', 8, 'تعريف حمل التدريب الرياضي\r\nأهمية حمل التدريب الرياضي\r\nأسس حمل التدريب الرياضي\r\nأنواع حمل التدريب الرياضي', '437552856'),
(9, '2020-11-04 14:35:46', '2020-12-19 12:58:46', 9, 'تعريف حمل التدريب الرياضي\r\nأهمية حمل التدريب الرياضي\r\nأسس حمل التدريب الرياضي', '437552856'),
(10, '2020-11-04 14:35:46', '2020-11-04 14:35:46', 9, 'تعريف حمل التدريب الرياضي\r\nأهمية حمل التدريب الرياضي\r\nأسس حمل التدريب الرياضي\r\nأنواع حمل التدريب الرياضي', '437552856'),
(11, '2020-11-04 14:46:59', '2020-11-04 14:46:59', 10, 'الفئات المستهدفة للدراسة في الدبلوم التدريبي\r\nطلاب وخريجي كليات التربية الرياضية\r\nالمهتمين بالعمل في مجال الأندية الصحية Gym\r\nالمهتمين بالعمل في مجال التدريب الرياضي واللياقة البدنية', '437552856'),
(12, '2020-11-04 14:46:59', '2020-11-04 14:46:59', 10, 'الفئات المستهدفة للدراسة في الدبلوم التدريبي\r\nطلاب وخريجي كليات التربية الرياضية\r\nالمهتمين بالعمل في مجال الأندية الصحية Gym\r\nالمهتمين بالعمل في مجال التدريب الرياضي واللياقة البدنية', '437552856'),
(13, '2020-11-04 14:46:59', '2020-11-04 14:46:59', 11, 'الفئات المستهدفة للدراسة في الدبلوم التدريبي\r\nطلاب وخريجي كليات التربية الرياضية\r\nالمهتمين بالعمل في مجال الأندية الصحية Gym\r\nالمهتمين بالعمل في مجال التدريب الرياضي واللياقة البدنية', '437552856'),
(16, '2020-11-04 14:46:59', '2020-11-04 14:46:59', 13, 'الفئات المستهدفة للدراسة في الدبلوم التدريبي\r\nطلاب وخريجي كليات التربية الرياضية\r\nالمهتمين بالعمل في مجال الأندية الصحية Gym\r\nالمهتمين بالعمل في مجال التدريب الرياضي واللياقة البدنية', '437552856'),
(17, '2020-12-22 13:07:50', '2020-12-22 13:07:50', 13, 'New Section Video Description Testing', '467136407'),
(18, '2020-12-22 13:29:01', '2020-12-22 13:29:01', 14, NULL, '467136407'),
(19, '2021-01-02 09:55:04', '2021-01-02 09:55:04', 15, NULL, '467136407'),
(26, '2021-01-03 14:46:12', '2021-01-03 14:46:12', 22, NULL, '467136407'),
(27, '2021-01-03 14:46:12', '2021-01-03 14:46:12', 23, NULL, '467136407'),
(28, '2021-01-03 14:46:12', '2021-01-03 14:46:12', 24, NULL, '467136407'),
(29, '2021-01-03 14:49:46', '2021-01-03 14:49:46', 25, NULL, '467136407'),
(30, '2021-01-03 15:14:21', '2021-01-03 15:14:21', 26, NULL, '467136407'),
(31, '2021-01-03 15:14:21', '2021-01-03 15:14:21', 27, NULL, '467136407'),
(32, '2021-01-04 17:50:26', '2021-01-04 17:50:26', 28, 'section 1', '467136407'),
(33, '2021-01-04 17:50:26', '2021-01-04 17:50:26', 28, 'section 2', '467136407'),
(34, '2021-01-04 17:50:26', '2021-01-04 17:50:26', 28, 'section 3', '467136407'),
(35, '2021-01-04 17:53:52', '2021-01-04 17:53:52', 29, 'phase 1 section 1', '467136407'),
(36, '2021-01-04 17:53:52', '2021-01-04 17:53:52', 29, 'phase 1 section 2', '467136407'),
(37, '2021-01-04 17:53:52', '2021-01-04 17:53:52', 30, 'phase 2 section 1', '467136407'),
(38, '2021-01-04 17:53:52', '2021-01-04 17:53:52', 31, 'phase 3 section 1', '467136407'),
(39, '2021-01-04 17:53:52', '2021-01-04 17:53:52', 31, 'phase 3 section 2', '467136407'),
(40, '2021-01-06 18:15:10', '2021-01-06 18:15:10', 32, NULL, '467136407'),
(41, '2021-01-06 18:16:17', '2021-01-06 18:16:17', 34, NULL, '467136407'),
(42, '2021-01-06 18:16:17', '2021-01-06 18:16:17', 35, NULL, '467136407'),
(43, '2021-01-06 18:42:06', '2021-01-06 18:42:06', 36, NULL, '467136407'),
(44, '2021-01-06 18:42:06', '2021-01-06 18:42:06', 36, NULL, '467136407'),
(45, '2021-01-06 18:42:06', '2021-01-06 18:42:06', 36, NULL, '467136407'),
(46, '2021-01-19 17:05:22', '2021-01-19 17:05:22', 37, NULL, '467136407'),
(47, '2021-01-19 17:06:23', '2021-01-19 17:06:23', 38, NULL, '467136407');

-- --------------------------------------------------------

--
-- Table structure for table `purchases`
--

CREATE TABLE `purchases` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `course_id` bigint(20) UNSIGNED DEFAULT NULL,
  `program_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `purchases`
--

INSERT INTO `purchases` (`id`, `created_at`, `updated_at`, `user_id`, `course_id`, `program_id`) VALUES
(2, '2020-10-21 16:27:43', NULL, 1, NULL, 26),
(6, '2020-10-21 16:27:43', NULL, 1, NULL, 23),
(13, '2020-12-22 17:54:12', '2020-12-22 17:54:12', 1, 35, NULL),
(14, NULL, NULL, 2, 35, NULL),
(15, NULL, NULL, 2, 35, NULL),
(16, '2021-01-02 08:54:38', '2021-01-02 08:54:38', 1, 37, NULL),
(17, '2021-01-02 09:57:53', '2021-01-02 09:57:53', 1, NULL, 28),
(18, NULL, NULL, 2, NULL, 28),
(20, '2021-01-03 14:46:12', '2021-01-03 14:46:12', 1, NULL, 32),
(21, '2021-01-03 14:49:46', '2021-01-03 14:49:46', 1, NULL, 33),
(22, '2021-01-03 15:14:21', '2021-01-03 15:14:21', 1, NULL, 34),
(23, '2021-01-04 17:50:26', '2021-01-04 17:50:26', 1, NULL, 35),
(24, '2021-01-04 17:53:52', '2021-01-04 17:53:52', 1, NULL, 36),
(25, '2021-01-06 17:35:02', '2021-01-06 17:35:02', 1, 40, NULL),
(26, '2021-01-06 17:39:00', '2021-01-06 17:39:00', 1, 41, NULL),
(27, '2021-01-06 17:47:45', '2021-01-06 17:47:45', 1, 42, NULL),
(28, '2021-01-06 18:10:07', '2021-01-06 18:10:07', 1, 44, NULL),
(29, '2021-01-06 18:12:23', '2021-01-06 18:12:23', 1, 45, NULL),
(30, '2021-01-06 18:16:17', '2021-01-06 18:16:17', 1, NULL, 38),
(31, '2021-01-19 17:05:22', '2021-01-19 17:05:22', 1, NULL, 39);

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

CREATE TABLE `subscribers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'no',
  `eng` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'no',
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subscribers`
--

INSERT INTO `subscribers` (`id`, `email`, `name`, `ar`, `eng`, `status`, `created_at`, `updated_at`) VALUES
(1, 'malalawi62@gmail.com', 'Maryam', 'yes', 'yes', 'active', '2020-11-10 22:03:33', '2020-11-10 22:03:33');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `DOB` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` char(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'regular',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `DOB`, `gender`, `type`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Maryam Al-Alawi', 'malalawi62@gmail.com', '2020-09-15 16:40:30', '2020-09-09', 'm', 'admin', '$2y$10$ZnfLSRUqp2UpE3JwCTfcWO1KV5a3PYoBNlB6nQy6Irxage1F7HhKu', 'XWZ1VvtDOWbZiiftkb2ROqWQq8N6tsJfWZxJFKZc7M8wP5Vt76YVBPLzH6SR', '2020-09-13 23:53:28', '2020-09-19 11:32:00'),
(2, 'زينب الموت', 'zainab.almoot66@gmail.com', NULL, '2020-09-09', 'f', 'regular', '$2y$10$ns0NtTfFswxKiCIB/XrVGO.wzy8noHIZNbToMwLZnwlcJQa2LucCK', NULL, '2020-09-15 15:47:05', '2020-09-15 15:47:05'),
(3, 'مريم العلوي', 'lera@gmail.com', NULL, '2020-09-11', 'f', 'regular', '$2y$10$KHLi183l1/z97aV5zPgXoe4bkyY7SQxSF/6fBD0eM2duAXx0zGeVi', NULL, '2020-09-15 15:53:08', '2020-09-15 15:53:08'),
(4, 'مريم العلوي', 'malalawi62@outlook.com', NULL, '2020-09-12', 'f', 'regular', '$2y$10$HPJICNmhjlqYdIdFh0iuUeIYXidRKW18SkBt.GqWJ5iMEffr3XmDW', NULL, '2020-09-15 15:54:07', '2020-09-15 15:54:07'),
(5, 'زينب الموت', 'ketabat_alhawza@gmail.com', NULL, '2020-09-04', 'f', 'regular', '$2y$10$kVeBZZdJMMzAYl5UUO1yCew9qw1PdkqSUA43DcQMWkg0hcni9Qtwu', NULL, '2020-09-15 15:55:39', '2020-09-15 15:55:39'),
(6, 'مريم العلوي', 'm.mtx@hotmail.com', NULL, '2020-09-12', 'f', 'regular', '$2y$10$uJ907GsE8/SHS2a7UAPBlO/k1tpE/Wm31lXErYjeIxZsFnYcnKVMW', NULL, '2020-09-15 16:10:05', '2020-09-15 16:10:05'),
(7, 'مريم العلوي', 'malalawi@gmail.com', NULL, '2020-09-03', 'f', 'regular', '$2y$10$gZ8JJDBwP9g4BDjYN2Ijgu/ptB/G/SJ13FyQDaOOS72gLXcx8ksre', NULL, '2020-09-15 16:18:30', '2020-09-15 16:18:30'),
(8, 'test', 'test@gmail.com', '2020-09-15 16:24:35', '2020-09-19', 'f', 'regular', '$2y$10$o5V6cH4.U0rJ75Ha8rZmau63l8Xl5/e5pPG4F368eqFGzcZakqJLK', NULL, '2020-09-15 16:24:15', '2020-09-15 16:24:35'),
(9, 'جاسم حسين عباس', 'jassim@gmail.com', '2020-11-11 00:37:37', '2020-11-01', 'm', 'regular', '$2y$10$.xZXQvSs27T4V32FiEVtpOEIKquzdFu0Vfv2.68IyoQxESGarSXmy', NULL, '2020-11-11 00:24:25', '2020-11-11 00:37:37');

-- --------------------------------------------------------

--
-- Table structure for table `writers`
--

CREATE TABLE `writers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `englishName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `arabicName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `writers`
--

INSERT INTO `writers` (`id`, `englishName`, `arabicName`, `created_at`, `updated_at`) VALUES
(1, 'Maryam Al-Alawi', 'مريم العلوي', '2020-11-10 15:38:11', '2020-11-10 15:38:11');

-- --------------------------------------------------------

--
-- Table structure for table `youtubes`
--

CREATE TABLE `youtubes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `videoId` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `language` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `category_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `youtubes`
--

INSERT INTO `youtubes` (`id`, `videoId`, `title`, `description`, `type`, `language`, `created_at`, `updated_at`, `category_id`) VALUES
(1, '437552856', 'video', NULL, 'sport', 'Eng', NULL, NULL, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `articles_writer_id_foreign` (`writer_id`),
  ADD KEY `articles_category_id_foreign` (`category_id`);

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `carts_user_id_foreign` (`user_id`),
  ADD KEY `carts_course_id_foreign` (`course_id`),
  ADD KEY `carts_program_id_foreign` (`program_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course_modules`
--
ALTER TABLE `course_modules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `course_modules_course_id_foreign` (`course_id`);

--
-- Indexes for table `course_module_videos`
--
ALTER TABLE `course_module_videos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `course_module_videos_course_module_id_foreign` (`course_module_id`);

--
-- Indexes for table `course_module_video_status`
--
ALTER TABLE `course_module_video_status`
  ADD PRIMARY KEY (`id`),
  ADD KEY `course_module_video_status_course_module_video_id_foreign` (`course_module_video_id`),
  ADD KEY `course_module_video_status_user_id_foreign` (`user_id`),
  ADD KEY `course_module_video_status_course_module_id_foreign` (`course_module_id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `instagrams`
--
ALTER TABLE `instagrams`
  ADD PRIMARY KEY (`id`),
  ADD KEY `instagrams_category_id_foreign` (`category_id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `programs`
--
ALTER TABLE `programs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `program_phases`
--
ALTER TABLE `program_phases`
  ADD PRIMARY KEY (`id`),
  ADD KEY `program_phases_program_id_foreign` (`program_id`);

--
-- Indexes for table `program_phase_sections`
--
ALTER TABLE `program_phase_sections`
  ADD PRIMARY KEY (`id`),
  ADD KEY `program_phase_sections_program_phase_id_foreign` (`program_phase_id`);

--
-- Indexes for table `purchases`
--
ALTER TABLE `purchases`
  ADD PRIMARY KEY (`id`),
  ADD KEY `purchases_user_id_foreign` (`user_id`),
  ADD KEY `purchases_course_id_foreign` (`course_id`),
  ADD KEY `purchases_program_id_foreign` (`program_id`);

--
-- Indexes for table `subscribers`
--
ALTER TABLE `subscribers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `writers`
--
ALTER TABLE `writers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `youtubes`
--
ALTER TABLE `youtubes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `youtubes_category_id_foreign` (`category_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `course_modules`
--
ALTER TABLE `course_modules`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `course_module_videos`
--
ALTER TABLE `course_module_videos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `course_module_video_status`
--
ALTER TABLE `course_module_video_status`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `instagrams`
--
ALTER TABLE `instagrams`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `programs`
--
ALTER TABLE `programs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `program_phases`
--
ALTER TABLE `program_phases`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `program_phase_sections`
--
ALTER TABLE `program_phase_sections`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `purchases`
--
ALTER TABLE `purchases`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `subscribers`
--
ALTER TABLE `subscribers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `writers`
--
ALTER TABLE `writers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `youtubes`
--
ALTER TABLE `youtubes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `articles`
--
ALTER TABLE `articles`
  ADD CONSTRAINT `articles_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
  ADD CONSTRAINT `articles_writer_id_foreign` FOREIGN KEY (`writer_id`) REFERENCES `writers` (`id`);

--
-- Constraints for table `carts`
--
ALTER TABLE `carts`
  ADD CONSTRAINT `carts_course_id_foreign` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`),
  ADD CONSTRAINT `carts_program_id_foreign` FOREIGN KEY (`program_id`) REFERENCES `programs` (`id`),
  ADD CONSTRAINT `carts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `course_modules`
--
ALTER TABLE `course_modules`
  ADD CONSTRAINT `course_modules_course_id_foreign` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`);

--
-- Constraints for table `course_module_videos`
--
ALTER TABLE `course_module_videos`
  ADD CONSTRAINT `course_module_videos_course_module_id_foreign` FOREIGN KEY (`course_module_id`) REFERENCES `course_modules` (`id`);

--
-- Constraints for table `course_module_video_status`
--
ALTER TABLE `course_module_video_status`
  ADD CONSTRAINT `course_module_video_status_course_module_id_foreign` FOREIGN KEY (`course_module_id`) REFERENCES `course_modules` (`id`),
  ADD CONSTRAINT `course_module_video_status_course_module_video_id_foreign` FOREIGN KEY (`course_module_video_id`) REFERENCES `course_module_videos` (`id`),
  ADD CONSTRAINT `course_module_video_status_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `instagrams`
--
ALTER TABLE `instagrams`
  ADD CONSTRAINT `instagrams_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);

--
-- Constraints for table `program_phases`
--
ALTER TABLE `program_phases`
  ADD CONSTRAINT `program_phases_program_id_foreign` FOREIGN KEY (`program_id`) REFERENCES `programs` (`id`);

--
-- Constraints for table `program_phase_sections`
--
ALTER TABLE `program_phase_sections`
  ADD CONSTRAINT `program_phase_sections_program_phase_id_foreign` FOREIGN KEY (`program_phase_id`) REFERENCES `program_phases` (`id`);

--
-- Constraints for table `purchases`
--
ALTER TABLE `purchases`
  ADD CONSTRAINT `purchases_course_id_foreign` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`),
  ADD CONSTRAINT `purchases_program_id_foreign` FOREIGN KEY (`program_id`) REFERENCES `programs` (`id`),
  ADD CONSTRAINT `purchases_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `youtubes`
--
ALTER TABLE `youtubes`
  ADD CONSTRAINT `youtubes_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
