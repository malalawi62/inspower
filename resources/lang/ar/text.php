<?php

return[
    //html lang
    'lang' => 'ar',
    'dir' => 'rtl',
    'or' => 'أو',

    //NavBar
    'home' => 'الرئيسية',
    'products'=>'المنتجات',
    'courses'=> 'الدورات التدريبية',
    'programs'=> 'البرامج التدريبية',
    'adminPanel' => 'الإدارة',
    'addSlideShow' => 'إضافة صورة عرض جديدة',
    'editDeleteSlideShow' => 'إدارة صور العرض',
    'addArticle' => 'إضافة مقال جديد',
    'editDeleteArticle' => 'تحديث أو حذف مقال',
    'addCategory' => 'إضافة فئة جديدة',
    'editDeleteCategory' => 'تحديث أو حذف فئة',
    'addWriter' => 'إضافة كاتب جديد',
    'editDeleteWriter' => 'تحديث أو حذف كاتب',
    'addVideo' => 'إضافة فيديو يوتيوب جديد',
    'addVideoInstagram' => 'إضافة فيديو انستقرام جديد',
    'editDeleteVideo' => 'تحديث أو حذف فيديو',
    'articles' => 'المقالات',
    'videos' => 'المقاطع المرئية',
    'about' => 'عنَّا',
    'aboutUs' => 'معلومات',
    'ourTeam' => 'فريق التدريب',
    'services' => 'الخدمات المقدمة',
    'contact' => 'تواصل معنا',
    'logout' => 'تسجيل الخروج',
    'language' => 'اللغة',
    'arabic' => 'العربية',
    'english' => 'English',
    'account' => 'حسابي',
    'myProfile' => 'معلومات الحساب',
    'profile' => 'حسابي',
    'myCourses' => 'الدورات التدريبية',
    'myPrograms' => 'البرامج التدريبية',
    'myOrders' => 'كل الطلبات',
    'subscribers' => 'قائمة المشتركين',
    'subscribersNum'=>'مشترك',
    'addCourses'=> 'إضافة دورة جديدة',
    'editDeleteCourses'=> 'تحديث أو حذف دورة',
    'addPrograms'=> 'إضافة برنامج جديد',
    'editDeletePrograms'=> 'تحديث أو حذف برنامج',

    //Banner
    'bannerImg' => "images/bannerAr.jpg",

    //Slide Show
    'addImageTitle' => "إضافة صورة جديدة للعرض",
    'slideShowEng' => "الصورة باللغة الإنجليزية",
    'slideShowAr' => "الصورة باللغة العربية",
    'showImage' => "إظهار الصورة في الصفحة الرئيسية",
    'hideImage' => "إخفاء الصورة من الصفحة الرئيسية",
    'addImage' => "إضافة الصورة",

    //Public And Private Content
    'publicContent' => 'محتوى مجاني للجميع',
    'privateContent' => 'محتوى خاص للمشتركين',

    //About Section
    'aboutTitle' => "معلومات",
    'aboutText' => "تعود بداية حسن في التدريب إلى كروسفت، ومن ثم توجه إلى التدريب الشخصي منذ 7 أعوام حتى إنضم إلى إنسباور في 2017.
كما أنه يختص في الإستشفاء وزيادة نطاق الحركة وتفادي الإصابات الرياضية من خلال طرق خاصة في التدليك والتخلص من العقد العضلية عند الرياضيين التي طالما تكون عائقًا عن الوصول لأفضل المستويات من خلال آيدلوجية تخصص بها بالجمع بين أكثر من طريقة إستشفاء في جلساته العلاجية.",

    //Team Section
    'teamTitle' => 'فريق التدريب',
    'talalName' => 'طلال نبيل',
    'talalInfo' => 'طلال نبيل هو مؤسس إنسباور للتدريب الرياضي، وهو المدير الفني للمدربين. بالإضافة إلى خبرة طلال في الرياضة على المستوى الفردي، تعود خبرته في التدريب الشخصي كمهنة إلى 7 أعوام، حيث عمل على تدريب رياضيين ذوي أهداف خاصة متعلقة برياضتهم من خلال تطوير القوة، القوة الإنفجارية والسرعة مما يحسن من أداءهم الرياضي، وتدريب العامة من الناس (من يحملوا أهداف بدنية عامة).',
    'monaName' => 'منى فَرَجْ',
    'monaInfo' => 'منى فَرَجْ بدأت في مجال التدريب الشخصي عام 2017، حيث تتخصص في تدريب النساء اللاتي يتطلعن إلى الحصول على جسم مثالي من حيث الجمالية من خلال؛ زيادة القوة، بناء عضل وخسارة الدهون وتحسين مستوى اللياقة البدنية.',
    'hasanName' => 'حسن العرادي',
    'hasanInfo' => 'تعود بداية حسن في التدريب إلى كروسفت، ومن ثم توجه إلى التدريب الشخصي منذ 7 أعوام حتى إنضم إلى إنسباور في 2017.
كما أنه يختص في الإستشفاء وزيادة نطاق الحركة وتفادي الإصابات الرياضية من خلال طرق خاصة في التدليك والتخلص من العقد العضلية عند الرياضيين التي طالما تكون عائقًا عن الوصول لأفضل المستويات من خلال آيدلوجية تخصص بها بالجمع بين أكثر من طريقة إستشفاء في جلساته العلاجية.',

    //Services Section
    'servicesTitle' => 'خدمات',
    'personalTitle' => 'أنواع التدريب الشخصي التي نقدمها',
    'personal1' => 'التدريب الشخصي الفردي',
    'personal2' => 'التدريب الشخصي مع مجموعة من الأفراد',
    'personal3' => 'التدريب عن بعد (عبر الإنترنت)',
    'otherTitle' => 'خدمات أخرى',
    'other1' => 'دورات وورش عمل',
    'other2' => 'فعاليات لبناء أسس الفريق المتكاتف المتكامل',
    'other3' => 'دورات تعليمية اونلاين',

    //Contact Section
    'contactTitle' => 'اتصل بنا',
    'followTitle' => 'تابعنا',
    'locationTitle' => 'موقعنا',
    'contactMsgTitle' => 'تواصل معنا',
    'chooseTopic' => 'اختر موضوع رسالتك',
    'topic1' => 'الدورات عبر الإنترنت',
    'topic2' => 'التدريب في الصالة الرياضية',
    'topic3' => 'التدريب الشخصي',
    'topic4' => 'تدريب المجموعات',
    'topic5' => 'العضوية',
    'topic6' => 'الدفع',
    'otherTopic' => 'مواضيع أخرى',
    'subject' => 'العنوان',
    'enterMessage' => 'يمكنك كتابة رسالتك هنا ...',
    'sendMessage' => 'إرسال',

    //Subscribe Section
    'subscribeTitle' => 'الاشتراك',
    'inputPlaceholder' => 'ادخل بريدك الالكتروني هنا *',
    'inputNamePlaceholder' => 'ادخل اسمك هنا *',
    'button' => 'اشترك',
    'chooseLanguage' => 'يمكنك اختيار اللغتين',

    'filter' => 'فرز بحسب التصنيف:',
    'filterType' => 'فرز بحسب النوع',
    'showFilter' => 'عرض الكل',

    'addVimeoTitle' => 'إضافة فيديو فيميو جديد',
    'videoDescription' => 'وصف الفيديو',

    'search' => 'بحث',
    'searchResults' => 'نتائج البحث',
    'noResults' => 'لا توجد نتائج لبحثك',

    'someStatistics' => 'إحصائيات',
    'female' => 'عدد الإناث',
    'male' => 'عدد الذكور',
    'allUsers' => 'إحصائيات المستخدمين',
    'less20' => 'أقل من 20 عام',
    '20s' => 'في العشرينيات',
    '30s' => 'في الثلاثينيات',
    '40s' => 'في الأربعينيات',
    'greater40' => '50 عام أو أكبر',

    'registeredInfo' => 'قائمة المستخدمين',

    'price' => 'السعر : ',

    //register
    'exclusiveSentence' => 'هذا المقطح متاح حصريًا للمشتركين معنا، انضم إلينا الآن!',
    'exclusiveSentenceArticles' => 'هذا المقال متاح حصريًا للمشتركين معنا، انضم إلينا الآن!',
    'register' => 'إنشاء حساب جديد',
    'upgrade' => 'الحصول على العضوية',
    'registerName' => 'الاسم',
    'registerEmail' => 'البريد الإلكتروني',
    'registerConfirmEmail' => 'تأكيد البريد الإلكتروني',
    'registerDOB' => "تاريخ الميلاد",
    'registerGender' => 'الجنس',
    'registerGenderFemale' => 'أنثى',
    'registerGenderMale' => 'ذكر',
    'registerPassword' => 'كلمة المرور',
    'registerConfirmPassword' => 'تأكيد كلمة المرور',
    'dontHaveAccount' => 'ليس لديك حساب ؟',
    'join' => 'انضم إلينا !',
    'freePlan' => 'اشتراك مجاني !',
    'freePlanPricing' => 'لا توجد رسوم اشتراك',
    'monthlyPlan' => 'اشتراك شهري',
    'monthlyPlanPricing' => '10 د.ب شهريا',
    'yearlyPlan' => 'اشتراك سنوي',
    'yearlyPlanPricing' => '100 د.ب سنويا',
    'free1' => 'حفظ المقالات والمقاطع المرئية',
    'monthly1' => 'قراءة المزيد من المقالات',
    'monthly2' => 'مشاهدة المزيد من المقاطع المرئية',
    'monthly3' => 'شراء الدورات التدربيبية',
    'monthly4' => 'شراء البرامج التدريبية',


    //login
    'login' => 'تسجيل الدخول',
    'forgetPassword' => 'هل نسيت كلمة المرور ؟',
    'resetPassword' => 'إعادة تعيين كلمة المرور',

    //Profile
    'DOB' => 'تاريخ الميلاد',
    'editProfile' => 'تحديث المعلومات الشخصية',
    'personalInfo' => 'المعلومات الشخصية',
    'viewCourse' => 'تفاصيل الدورة',
    'viewProgram' => 'تفاصيل البرنامج',
    'viewOrder' => 'تفاصيل الطلب',
    'viewInvoice' => 'الفاتورة',
    'orderId' => 'رقم الطلب',
    'orderDate' => 'تاريخ الطلب',
    'orderPrice' => 'السعر',
    'view' => 'عرض الفاتورة',

    //verify
    'verifyTitle' => 'تحقق من عنوان بريدك الإلكتروني',
    'freshLink' => 'تم إرسال رابط تحقق جديد إلى عنوان بريدك الإلكتروني',
    'before' => 'قبل المتابعة، يرجى التحقق من بريدك الإلكتروني للحصول على رابط التحقق',
    'sure' => 'إذا لم تستلم بريد التحقق الالكتروني',
    'click' => 'انقر هنا لطلب آخر',

    //Articles Index
    'allArticlesTitle' => 'كل المقالات',

    //Articles Create
    'addArticleTitle' => 'إضافة مقال جديد',
    'inputArticleTitle' => 'عنوان المقال *',
    'chooseWriter' => 'الكاتب',
    'inputArticleWriter' => 'اسم الكاتب',
    'chooseCategory'=> 'الفئة *',
    'articleBody'=> 'المقال *',
    'articleImage' => 'صورة المقال *',
    'addArticleBtn'=> 'إضافة المقال',

    //Articles Edit
    'editArticleTitle'=> 'تحديث المقال',
    'updatedArticleBtn'=>'تحديث المقال',

    //Category Index
    'allCategoriesTitle' => 'كل الفئات',

    //Category Create
    'addCategoryTitle' => 'إضافة فئة جديدة',
    'addCategoryBtn' => 'إضافة الفئة',

    //Category Edit
    'EditCategoryTitle' => 'تحديث فئة',
    'EditCategoryBtn' => 'تحديث الفئة',

    //Category Index
    'allWritersTitle' => 'كل الكتَّاب',

    //Writers Create
    'addWriterTitle' => 'إضافة كاتب جديد',
    'addWriterBtn' => 'إضافة الكاتب',

    //Writers Edit
    'EditWriterTitle' => 'تحديث كاتب',
    'EditWriterBtn' => 'تحديث الكاتب',

    //Youtube Create
    'addYoutubeTitle' => 'إضافة فيديو يوتيوب جديد',
    'inputYoutubeUrl' => 'ادخل رابط الفيديو هنا',
    'addYoutubeBtn' => 'إضافة الفيديو',
    'showVideoInfo' => 'رؤية المعلومات',
    'editYoutubeTitle' => 'تحديث الفيديو',
    'editYoutubeBtn' => 'تحديث الفيديو',

    //Instagram Create
    'addInstagramTitle' => 'إضافة فيديو انستقرام جديد',
    'editInstagramTitle' => 'تحديث فيدية انستقرام',
    'editInstagramBtn' => 'تحديث الفيديو',

    //Youtube Index
    'allVideosTitle' => 'كل المقاطع المرئية',

    //Course Create
    'addCourseTitle' => 'إضافة دورة جديدة',
    'inputCourseTitle' => 'عنوان الدورة *',
    'inputCourseOverview' => 'وصف الدورة *',
    'inputCoursePrice' => 'سعر الدورة *',
    'inputCourseDescription' => 'معلومات إضافية عن الدورة (اختياري)',
    'inputCourseVideo' => 'رابط الفيديو *',
    'inputImage' => 'صورة الدورة *',
    'addCourseBtn' => 'إضافة الدورة',
    'courseDetails' => 'المعلومات الأساسية للدورة',
    'courseModuleDetails' => 'المعلومات الأساسية للجزء',
    'lessonNew'=> 'درس جديد',
    'inputLessonPdf'=>'ملف الدورة',
    'lessonDescription'=>'وصف للدرس (اختياري)',
    'addNewLesson'=>'إضافة درس جديد',
    'removeLesson'=>'حذف هذا الدرس',
    'addLessonBtn'=>'إضافة درس جديد',
    'editLessonTitle' => 'تحديث معلومات الدرس',

    //Course Edit
    'editCourseTitle' => 'تحديث معلومات الدورة',
    'editCourseBtn' => 'تحديث الدورة',

    //Course Index
    'allCoursesTitle'=> 'كل الدورات',
    'noCourseMessage'=> 'لا توجد دورات متاحة',

    //Course Show
    'coursePrice' => 'سعر الدورة التدريبية',
    'courseOverview' => 'وصف الدورة التدريبية',
    'courseDescription' => 'معلومات إضافية عن الدورة التدريبية',
    'courseModules' => 'أجزاء الدورة التدريبية',
    'moduleDescriptionShow' => 'وصف الجزء',
    'moduleLessons' => 'الدروس',
    'lesson' => 'الدرس',
    'incomplete' => 'غير مكتمل',
    'complete' => 'مكتمل',
    'module' => 'الجزء',

    //Program Create
    'addProgramTitle' => 'إضافة برنامج جديد',
    'inputProgramTitle' => 'عنوان البرنامج *',
    'inputProgramOverview' => 'وصف البرنامج *',
    'inputProgramPrice' => 'سعر البرنامج *',
    'inputProgramDescription' => 'معلومات إضافية عن البرنامج (اختياري)',
    'inputProgramVideo' => 'رابط الفيديو (اختياري)',
    'inputProgramPdf'=> 'ملف البرنامج *',
    'inputProgramImage' => 'صورة البرنامج *',
    'addProgramBtn' => 'إضافة البرنامج',

    //Program Edit
    'editProgramTitle' => 'تحديث معلومات البرنامج',

    //Program Index
    'allProgramsTitle'=> 'كل البرامج',
    'noProgramMessage'=> 'لا توجد برامج متاحة',

    //Program Show
    'programPrice'=>'سعر البرنامج',
    'programOverview' => 'معلومات البرنامج',
    'programDescription' => 'وصف البرنامج',
    'programPhases' => 'مراحل البرنامج',
    'phaseOverviewShow' => 'معلومات المرحلة',
    'phaseDescriptionShow' => 'وصف المرحلة',
    'phaseSections' => 'أقسام المرحلة',
    'watchVideo' => 'اضغط هنا لمشاهدة الفيديو',
    'phase' => 'المرحلة',

    //Program Show
    'readBtn'=> 'اضغط هنا للقراءة',
    'viewPdf'=> 'اضغط هنا لقراءة ملف الـ Pdf',

    //Module
    'moduleNew'=>'جزء جديد',
    'moduleTitle'=>'عنوان الجزء *',
    'moduleTitle2'=>'عنوان الجزء',
    'moduleDescription'=>'وصف الجزء *',
    'moduleVideoURL'=>'رابط الفيديو *',
    'addModuleBtn'=>'إضافة جزء جديد',
    'editModuleTitle' => 'تحديث معلومات الجزء',
    'editModuleBtn'=>'تحديث الجزء',

    //Phase
    'phaseNew'=>'مرحلة جديدة',
    'phaseTitle'=>'عنوان المرحلة *',
    'phaseTitle2'=>'عنوان المرحلة',
    'phaseOverview'=>'وصف المرحلة *',
    'phaseDescription'=>'معلومات إضافية عن المرحلة (اختياري)',
    'phaseVideoURL'=>'رابط الفيديو للمرحلة (اختياري)',
    'sectionVideoURL'=>'رابط الفيديو لقسم المرحلة *',
    'phaseBriefDescription'=>'وصف مقطع الفيديو (اختياري)',
    'phasePdf'=>'ملف المرحلة *',
    'addNewSection' => 'إضافة قسم جديد',
    'removeSection' => 'حذف هذا القسم',
    'editProgramBtn'=> 'تحديث البرنامج',
    'addPhaseBtn'=>'إضافة مرحلة جديد',
    'editPhaseTitle' => 'تحديث معلومات المرحلة',
    'editPhaseBtn'=>'تحديث المرحلة',

    //Section
    'sectionTitle'=>'قسـم',
    'editSectionTitle' => 'تحديث معلومات القسم',

    //Membership Type
    'editMembershipType'=>'تحديث العضوية',
    'allMembershipTypesTitle' => 'كل أنواع العضويات',
    'membershipTypes'=>'العضويات',
    'premiumBecome' => 'كٌن من المشتركين في موقعنا الآن!',
    'premium' => 'أنت من الأعضاء المميزين في موقعنا!',
    'premiumUser'=> 'عضوية مميزة',
    'freeUser'=> 'عضوية مجانية',
    'membershipConfirm'=>'نوع العضوية',

    //Cart Show
    'cartTitle'=> 'سلة المشتريات',
    'checkoutBtn'=>'المتابعة للدفع',
    'cartRowTitle'=>'عنوان البرنامج / الدورة',
    'cartPrice'=>'السعر',
    'cartTotal'=>'السعر الكلي',
    'BD' => 'د.ب',
    'viewCourses' => 'الدورات التدريبية',
    'viewPrograms' => 'البرامج التدريبية',
    'empty' => 'لم تتم إضافة أي منتج لسلة مشترياتك، إلق نظرة على منتجاتنا ..',
    'addToCartTitle' => 'أضف إلى سلة المشتريات',

    //Order Confirmation
    'paymentBtn'=>'المتابعة للدفع',
    'confirmTitle' => 'تأكيد الطلب',
    'addMoreCourses' => 'إضافة المزيد من الدورات',
    'addMorePrograms' => 'إضافة المزيد من البرامج',
    'detailsTitle' => 'معلومات الطلب',

    //Some  Styling
    'textAlign' => 'text-align:right; direction:rtl',
    'floating' => 'float: right',
    'opFloating' => 'float: left',

    //Some Buttons
    'learnMoreBtn'=>'اقرأ المزيد',
    'addToCartBtn'=> 'أضف إلى السلة',

    'inactiveProgram' => 'البرنامج غير فعَّال',
    'inactiveCourse' => 'الدورة غير فعَّالة'
    ];

