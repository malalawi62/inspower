<?php

return [
    //html lang
    'lang' => 'en',
    'dir' => 'ltr',
    'or' => 'OR',

    //NavBar
    'home' => 'Home',
    'products'=>'Products',
    'courses'=> 'Courses',
    'programs'=> 'Programs',
    'adminPanel' => 'Admin Panel',
    'addSlideShow' => 'Add A New Slide Image',
    'editDeleteSlideShow' => 'Manage Slide Image',
    'addArticle' => 'Add A New Article',
    'editDeleteArticle' => 'Edit OR Delete Article',
    'addCategory' => 'Add A New Category',
    'editDeleteCategory' => 'Edit OR Delete Category',
    'addWriter' => 'Add A New Writer',
    'editDeleteWriter' => 'Edit OR Delete Writer',
    'addVideo' => 'Add A New Youtube Video',
    'editDeleteVideo' => 'Edit OR Delete Video',
    'addVideoInstagram' => 'Add A New Instagram Video',
    'articles' => 'Articles',
    'videos' => 'videos',
    'about' => 'About',
    'aboutUs' => 'About Us',
    'ourTeam' => 'Our Team',
    'services' => 'Services',
    'contact' => 'Contact',
    'logout' => 'Log-Out',
    'language' => 'Language',
    'arabic' => 'العربية',
    'english' => 'English',
    'account' => 'My Account',
    'myProfile' => 'Personal Info',
    'profile' => 'My Profile',
    'myCourses' => 'My Courses',
    'myPrograms' => 'My Programs',
    'myOrders' => 'All Orders',
    'subscribers' => 'List of Subscribers',
    'subscribersNum'=>'Subscribers',
    'addCourses'=> 'Add A New Course',
    'editDeleteCourses'=> 'Edit OR Delete Course',
    'addPrograms'=> 'Add A New Program',
    'editDeletePrograms'=> 'Edit OR Delete Program',

    //Banner
    'bannerImg' => "images/banner.jpg",

    //Slide Show
    'addImageTitle' => "Add A New Slide Show Image",
    'slideShowEng' => "English Image",
    'slideShowAr' => "Arabic Image",
    'showImage' => "Show Image In Home Page",
    'hideImage' => "Hide Image From Home Page",
    'addImage' => "ADD IMAGE",

    //Public And Private Content
    'publicContent' => 'Public Content',
    'privateContent' => 'Membership Content',

    //About Section
    'aboutTitle' => "ABOUT US",
    'aboutText' => "INSPOWER Fitness Coaching is situated in Bahrain, it was established by TALAL NABEEL in 2018. INSPOWER Fitness Coaching is in cooperation with MONA FARAJ and HASAN AL ARADI.
        Our main purpose is Achieving physical goals through enhancing performance.
        Because each one of you is unique as an individual, our services are restricted to personal training. This comes from our understanding to how different each individual is from the other which makes the need of personalizing the training programs at least to a certain level. Different; Goals, Fitness Level, Posture and Personality (one of the biggest factors to work around in programming that impact results physically and psychologically).",

    //Team Section
    'teamTitle' => 'OUR TEAM',
    'talalName' => 'TALAL NABEEL',
    'talalInfo' => 'Talal is the founder, owner and head coach of INSPOWER Fitness Coaching.
        In addition to the experience Talal holds in sports on the personal level, his experience in Personal Training goes back to 6 years ago, where he works with athletes which are aiming for specific performance goals related to their sports and performance on the field, and clients from the general public which are aiming for body composition and general health goals.',
    'monaName' => 'MONA FARAJ',
    'monaInfo' => 'Mona started her journey with Personal Training in 2017. She expertise in coaching women that aims for getting a functional aesthetic body through Strength, Fat Loss and Building Muscles.',
    'hasanName' => 'HASAN AL ARADI',
    'hasanInfo' => 'Hasan expertise in massage therapy. With his various certificates in more than one type of massage, Hasan developed a unique approach by himself. He is responsible for getting you pain free from muscle knots, increasing recovery levels, increasing range of motion and prevents injuries.',

    //Services Section
    'servicesTitle' => 'OUR SERVICES',
    'personalTitle' => 'Personal Training Types That We Provide',
    'personal1' => 'Individual Personal Training',
    'personal2' => 'Group Personal Training',
    'personal3' => 'Online Personal Training',
    'otherTitle' => 'Other Services',
    'other1' => 'Seminars and Workshops',
    'other2' => 'Online courses',
    'other3' => 'Team building events',

    //Contact Section
    'contactTitle' => 'CONTACT',
    'followTitle' => 'FOLLOW US',
    'locationTitle' => 'LOCATION',
    'contactMsgTitle' => 'LEAVE A MESSAGE',
    'chooseTopic' => 'Choose Your Message Topic',
    'topic1' => 'Online Courses',
    'topic2' => 'Studio Classes',
    'topic3' => 'Personal Training',
    'topic4' => 'Group Training',
    'topic5' => 'Membership',
    'topic6' => 'Payment',
    'otherTopic' => 'Others',
    'subject' => 'Subject',
    'enterMessage' => 'Enter your message here ...',
    'sendMessage' => 'Send Message',

    //Subscribe Section
    'subscribeTitle' => 'Subscription',
    'inputPlaceholder' => 'Enter Your Email *',
    'inputNamePlaceholder' => 'Enter Your Name *',
    'button' => 'Subscribe',
    'chooseLanguage' => 'You can choose both languages',

    'filter' => 'Filter By Category:',
    'filterType' => 'Filter By Type:',
    'showFilter' => 'Show all',

    'addVimeoTitle' => 'ADD A NEW VIMEO VIDEO',
    'videoDescription' => 'Video Description',

    'search' => 'Search',
    'searchResults' => 'SEARCH RESULTS',
    'noResults' => 'No Results Found',

    'someStatistics' => 'Statistics',
    'female' => 'Females',
    'male' => 'Males',
    'allUsers' => 'Users Statistics',
    'less20' => 'Under 20 years',
    '20s' => '20\'s',
    '30s' => '30\'s',
    '40s' => '40\'s',
    'greater40' => '50 years and older',

    'registeredInfo' => 'List of Users',

    'price' => 'Price : ',

    //register
    'exclusiveSentence' => 'This Video is Exclusive for PREMIUM members, JOIN US NOW!',
    'exclusiveSentenceArticles' => 'This Article is Exclusive for PREMIUM members, JOIN US NOW!',
    'register' => 'Register A New Account',
    'upgrade' => 'Become a Premium Member',
    'registerName' => 'Name',
    'registerEmail' => 'Email',
    'registerConfirmEmail' => 'Confirm Email',
    'registerDOB' => "Date Of Birth",
    'registerGender' => 'Gender',
    'registerGenderFemale' => 'Female',
    'registerGenderMale' => 'Male',
    'registerPassword' => 'Password',
    'registerConfirmPassword' => 'Confirm Password',
    'dontHaveAccount' => 'Don\'t Have An Account ?',
    'join' => 'Join Us !',
    'freePlan' => 'Free Subscription !',
    'freePlanPricing' => 'No fees for free Plan',
    'monthlyPlan' => 'Monthly Subscription',
    'monthlyPlanPricing' => '10 BD per Month',
    'yearlyPlan' => 'Yearly Subscription',
    'yearlyPlanPricing' => '100 BD per Year',
    'free1' => 'Saving Articles & Videos',
    'monthly1' => 'View More Articles',
    'monthly2' => 'View More Videos',
    'monthly3' => 'Can By Online Courses',
    'monthly4' => 'Can By Online Programs',

    //login
    'login' => 'Log-In',
    'forgetPassword' => 'Forget Your Password ?',
    'resetPassword' => 'Reset Pasword',

    //Profile
    'DOB' => 'Date Of Birth',
    'editProfile' => 'Edit Profile Info',
    'personalInfo' => 'Personal Information',
    'viewCourse' => 'View Course',
    'viewProgram' => 'View Program',
    'viewOrder' => 'View Order Details',
    'viewInvoice' => 'View Invoice Details',
    'orderId' => 'Order #ID',
    'orderDate' => 'Order Date',
    'orderPrice' => 'Price',
    'view' => 'View Receipt',

    //verify
    'verifyTitle' => 'Verify Your Email Address',
    'freshLink' => 'A fresh verification link has been sent to your email address',
    'before' => 'Before proceeding, please check your email for a verification link',
    'sure' => 'If you did not receive the email',
    'click' => 'click here to request another',

    //Articles Index
    'allArticlesTitle' => 'ALL ARTICLES',

    //Articles Create
    'addArticleTitle' => 'ADD A NEW ARTICLE',
    'inputArticleTitle' => 'Article Title *',
    'chooseWriter' => 'Choose Writer',
    'inputArticleWriter' => 'Writer Name',
    'chooseCategory'=> 'Choose Category *',
    'articleBody'=> 'Article Body *',
    'articleImage' => 'Article Image',
    'addArticleBtn'=> 'Add Article',

    //Articles Edit
    'editArticleTitle'=> 'Edit Article',
    'updatedArticleBtn'=>'Update Article',

    //Category Index
    'allCategoriesTitle' => 'All Categories',

    //Category Create
    'addCategoryTitle' => 'ADD A NEW CATEGORY',
    'addCategoryBtn' => 'Add Category',

    //Category Edit
    'EditCategoryTitle' => 'EDIT CATEGORY',
    'EditCategoryBtn' => 'Update Category',

    //Category Index
    'allWritersTitle' => 'All Writers',

    //Writers Create
    'addWriterTitle' => 'ADD A NEW WRITER',
    'addWriterBtn' => 'Add Writer',

    //Writers Edit
    'EditWriterTitle' => 'EDIT WRITER',
    'EditWriterBtn' => 'Update Writer',

    //Youtube Create
    'addYoutubeTitle' => 'ADD A NEW YOUTUBE VIDEO',
    'inputYoutubeUrl' => 'Enter Video URL here *',
    'addYoutubeBtn' => 'Add Video',
    'showVideoInfo' => 'Show Information',
    'editYoutubeTitle' => 'EDIT YOUTUBE VIDEO',
    'editYoutubeBtn' => 'Edit Video',

    //Instagram Create
    'addInstagramTitle' => 'ADD A NEW INSTAGRAM VIDEO',
    'editInstagramTitle' => 'EDIT INSTAGRAM VIDEO',
    'editInstagramBtn' => 'Edit Video',

    //Youtube Index
    'allVideosTitle' => 'All Videos',

    //Course Create
    'addCourseTitle' => 'ADD A NEW COURSE',
    'inputCourseTitle' => 'Course Title *',
    'inputCourseOverview' => 'Course Overview *',
    'inputCoursePrice' => 'Course Price *',
    'inputCourseDescription' => 'Course Description (Optional)',
    'inputCourseVideo' => 'Video Url (Optional)',
    'inputImage' => 'Course Image *',
    'addCourseBtn' => 'Add Course',
    'courseDetails' => 'Main Course Details',
    'courseModuleDetails' => 'Modules Details',
    'lessonNew'=> 'New Lesson',
    'inputLessonPdf'=>'Lesson PDF',
    'lessonDescription'=>'Lesson Description',
    'addNewLesson'=>'ADD NEW LESSON',
    'removeLesson'=>'REMOVE THIS LESSON',
    'addLessonBtn'=>'ADD NEW LESSON',
    'editLessonBtn'=>'EDIT LESSON',
    'editLessonTitle' => 'EDIT LESSON DETAILS',

    //Course Edit
    'editCourseTitle' => 'EDIT COURSE DETAILS',
    'editCourseBtn' => 'EDIT COURSE',

    //Course Index
    'allCoursesTitle'=> 'All COURSES',
    'noCourseMessage'=> 'No Available Courses',

    //Course Show
    'coursePrice' => 'COURSE PRICE',
    'courseOverview' => 'COURSE OVERVIEW',
    'courseDescription' => 'COURSE DESCRIPTION',
    'courseModules' => 'COURSE MODULES',
    'moduleDescriptionShow' => 'MODULE DESCRIPTION',
    'moduleLessons' => 'MODULE LESSONS',
    'lesson' => 'Lesson',
    'incomplete' => 'INCOMPLETE',
    'complete' => 'COMPLETE',
    'module' => 'Module',

    //Program Create
    'addProgramTitle' => 'ADD A NEW PROGRAM',
    'inputProgramTitle' => 'Program Title *',
    'inputProgramOverview' => 'Program Overview *',
    'inputProgramPrice' => 'Program Price *',
    'inputProgramDescription' => 'Program Description (Optional)',
    'inputProgramVideo' => 'Video Url (Optional)',
    'inputProgramPdf'=> 'Program PDF *',
    'inputProgramImage' => 'Program Image *',
    'addProgramBtn' => 'ADD PROGRAM',

    //Program Edit
    'editProgramTitle' => 'EDIT PROGRAM DETAILS',
    'editProgramBtn' => 'EDIT PROGRAM',

    //Program Index
    'allProgramsTitle'=> 'All PROGRAMS',
    'noProgramMessage'=> 'No Available Programs',

    //Program Show
    'programPrice'=>'PROGRAM PRICE',
    'programOverview' => 'PROGRAM OVERVIEW',
    'programDescription' => 'PROGRAM DESCRIPTION',
    'programPhases' => 'PROGRAM PHASES',
    'phaseOverviewShow' => 'PHASE OVERVIEW',
    'phaseDescriptionShow' => 'PHASE DESCRIPTION',
    'phaseSections' => 'PHASE SECTIONS',
    'watchVideo' => 'Click Here To Watch The Video',
    'phase' => 'Phase',

    //Program Show
    'readBtn'=> 'Click To Read',
    'viewPdf'=> 'Click to Read Program PDF',

    //Module
    'moduleNew'=>'New Module',
    'moduleTitle'=>'Module Title *',
    'moduleTitle2'=>'MODULE TITLE',
    'moduleDescription'=>'Module Description *',
    'moduleVideoURL'=>'Video URL *',
    'addModuleBtn'=>'ADD NEW MODULE',
    'editModuleTitle' => 'EDIT MODULE DETAILS',
    'editModuleBtn'=>'EDIT MODULE',

    //Phase
    'phaseNew'=>'New Phase',
    'phaseTitle'=>'Phase Title *',
    'phaseTitle2'=>'Phase Title',
    'phaseOverview'=>'Phase Overview *',
    'phaseDescription'=>'Phase Description (Optional)',
    'phaseVideoURL'=>'Phase Video URL (Optional)',
    'sectionVideoURL'=>'Section Video URL *',
    'phaseBriefDescription'=>'Video Description (Optional)',
    'phasePdf'=>'Phase PDF *',
    'addNewSection' => 'ADD NEW SECTION',
    'removeSection' => 'REMOVE THIS SECTION',
    'editSectionBtn'=> 'UPDATE SECTION',
    'addPhaseBtn'=>'ADD NEW PHASE',
    'editPhaseTitle' => 'EDIT PHASE DETAILS',
    'editPhaseBtn'=>'EDIT PHASE',

    //Section
    'sectionTitle'=>'SECTION',
    'editSectionTitle' => 'EDIT SECTION DETAILS',

    //Membership Type
    'editMembershipType'=>'EDIT MEMBERSHIP DETAILS',
    'allMembershipTypesTitle' => 'ALL MEMBERSHIP TYPES',
    'membershipTypes'=>'Memberships',
    'premiumBecome' => 'Become a PREMIUM member now!',
    'premium' => 'You are a PREMIUM member!',
    'premiumUser'=> 'Premium Membership',
    'freeUser'=> 'Free Membership',
    'membershipConfirm'=>'Membership Type',

    //Cart Show
    'cartTitle'=> 'Shopping Cart',
    'checkoutBtn'=>'CHECK-OUT',
    'cartRowTitle'=>'Course \ Program Title',
    'cartPrice'=>'Price',
    'cartTotal'=>'Total Amount',
    'BD' => 'BD',
    'viewCourses' => 'View Courses',
    'viewPrograms' => 'View Programs',
    'empty' => 'YOUR CART IS EMPTY, CHECK OUT OUR PRODUCTS ..',
    'addToCartTitle' => 'Add To Cart',

    //Order Confirmation
    'paymentBtn'=>'PROCEED TO PAY',
    'confirmTitle' => 'ORDER CONFIRMATION',
    'addMoreCourses' => 'ADD MORE COURSES',
    'addMorePrograms' => 'ADD MORE PROGRAMS',
    'detailsTitle' => 'ORDER DETAILS',

    //Some  Styling
    'textAlign' => 'text-align:left;',
    'floating' => 'float: left',
    'opFloating' => 'float: right',

    //Some Buttons
    'learnMoreBtn'=>'LEARN MORE',
    'addToCartBtn'=>'ADD TO CART',

    'inactiveProgram' => 'INACTIVE PROGRAM',
    'inactiveCourse' => 'INACTIVE COURSE'
    ];
