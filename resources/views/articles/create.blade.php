<!DOCTYPE html>
<html lang="{{ __('text.lang') }}" dir="{{ __('text.dir') }}">

@include('includes.head')

<body>

@include('layout.header')
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app" id="main">
        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe">
            <div class="container">
                <div class="cta-inner">
                    <div class="row">
                        <h1 style="text-align: center; float: none"> {{ __('text.addArticleTitle') }} </h1>
                    </div>
                    <div class="wow fadeInUp" data-wow-delay="0.3s">
                        <form class="forms-form center-form wow zoomIn" action="/articles" method="post"
                              enctype="multipart/form-data">

                            @error('title') <h5 style="color: red;">{{ $message }}</h5> @enderror
                            <input class="mail" type="text" name="title"
                                   placeholder="{{ __('text.inputArticleTitle') }}"
                                   autocomplete="off" id="title" value="{{ old('title') }}">

                            @error('writer_id') <h5 style="color: red">{{ $message }}</h5> @enderror
                            <select class="mail" name="writer_id" id="category">
                                <option value=""> {{ __('text.chooseWriter') }} </option>
                                @forelse($data['writers'] as $writer)
                                    <option value="{{ $writer->id }}">
                                        @if($data['lang'] == 'en')
                                            {{ $writer->englishName }}
                                        @else
                                            {{ $writer->arabicName }}
                                        @endif
                                    </option>
                                @empty
                                    <option>No Writers Found</option>
                                @endforelse
                            </select>

                            <h3 style="color: #2D322F; padding-bottom: 10px">{{__('text.or')}}</h3>
                            @error('writerName') <h5 style="color: red">{{ $message }}</h5> @enderror
                            <input class="mail" type="text" name="writerName"
                                   placeholder="{{ __('text.inputArticleWriter') }}" autocomplete="off"
                                   id="writer" value="{{ old('writer') }}">

                            @error('date') <h5 style="color: red;">{{ $message }}</h5> @enderror
                            <input class="mail" type="date" name="date" autocomplete="off" id="date"
                                   value="{{ old('date') }}">

                            @error('category_id') <h5 style="color: red">{{ $message }}</h5> @enderror
                            <select class="mail" name="category_id" id="categoryId">
                                <option hidden selected value="">{{__('text.chooseCategory')}}</option>
                                @foreach($data['categories'] as $category)
                                    <option value="{{ $category->id }}">
                                        @if($data['lang'] == 'en')
                                            {{ $category->englishName }}
                                        @else
                                            {{ $category->arabicName }}
                                        @endif
                                    </option>
                                @endforeach
                            </select>

                            @error('public') <h5 style="color: red">{{ $message }}</h5> @enderror
                            <select class="mail" name="public" id="public">
                                <option value="YES" selected>{{__('text.publicContent')}}</option>
                                <option value="NO">{{__('text.privateContent')}}</option>
                            </select>

                            @error('image') <h5 style="color: red">{{ $message }}</h5> @enderror
                            <h3 style="padding-bottom: 10px">{{__('text.articleImage')}}</h3>
                            <input class="mail" type="file" name="image" id="image"
                                   style="display: inline-block;padding-top: 10px">

                            @error('body') <p style="color: red">{{ $message }}</p> @enderror
                            <textarea class="mail" rows="20" name="body" placeholder="{{__('text.articleBody')}}"
                                      id="body"
                                      value="{{ old('body') }}" style="height: auto;"></textarea>
                            @if($data['lang'] == 'en')
                                <input type="hidden" name="language" id="language" value="Eng">
                            @else
                                <input type="hidden" name="language" id="language" value="Ar">
                            @endif

                            @csrf
                            <div>
                                <button class="submit-button" style="width: 60%;">
                                    {{__('text.addArticleBtn')}}
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        @include('layout.footer')
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
@include('includes.footerScripts')
</body>
</html>
