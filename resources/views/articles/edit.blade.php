<!DOCTYPE html>
<html lang="{{ __('text.lang') }}" dir="{{ __('text.dir') }}">

@include('includes.head')

<body>

@include('layout.header')
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app" id="main">
        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe">
            <div class="container">
                <div class="cta-inner">
                    <div class="row">
                        <h1 style="text-align: center; float: none">{{ __('text.editArticleTitle') }}</h1>
                    </div>
                    <div class="wow fadeInUp" data-wow-delay="0.3s">
                        <form class="forms-form center-form wow zoomIn" action="/articles/{{ $data['article']->id }}"
                              method="post" enctype="multipart/form-data">
                            @method('PATCH')
                            @error('title') <h5 style="color: red;">{{ $message }}</h5> @enderror
                            <input class="mail" type="text" name="title"
                                   autocomplete="off" id="title" value="{{ $data['article']->title }}">

                            @error('writer_id') <h5 style="color: red">{{ $message }}</h5> @enderror
                            <select class="mail" name="writer_id" id="category" style="color: #FFF">
                                <option value="">Choose Writer</option>
                                @forelse($data['writers'] as $writer)
                                    <option style="width: 100%"
                                        @if($data['article']->writer_id != null)
                                            @if($data['article']->writer_id == $writer->id)
                                                selected
                                            @endif
                                        @endif
                                            value="{{ $writer->id }}" style="background-color: #2D322F">
                                        @if($data['lang'] == 'en')
                                            {{ $writer->englishName }}
                                        @else
                                            {{ $writer->arabicName }}
                                        @endif
                                    </option>
                                @empty
                                    <option>No Writers Found</option>
                                @endforelse
                            </select>

                            <h3 style="color: #FFF; padding-bottom: 10px">OR</h3>
                            @error('writerName') <h5 style="color: red">{{ $message }}</h5> @enderror
                            <input class="mail" type="text" name="writerName" placeholder="{{__('text.inputArticleWriter')}}" autocomplete="off"
                                   id="writer"
                                   @if($data['article']->writerName != null) value="{{ $data['article']->writerName }}"@endif>
                            1
                            @error('date') <h5 style="color: red">{{ $message }}</h5> @enderror
                            <input class="mail" type="date" name="date" autocomplete="off" id="date"
                                   value="{{ $data['article']->date }}" style="color: #FFF">

                            @error('category_id') <h5 style="color: red">{{ $message }}</h5> @enderror
                            <select class="mail" name="category_id" id="categoryId" style="color: #FFF">
                                <option hidden>Choose Category</option>
                                @forelse($data['categories'] as $category)
                                    <option @if($category->id == $data['article']->category_id) selected @endif
                                    value="{{ $category->id }}" style="background-color: #2D322F">
                                        @if($data['lang'] == 'en')
                                            {{ $category->englishName }}
                                        @else
                                            {{ $category->arabicName }}
                                        @endif
                                    </option>
                                @empty
                                    <option>No Categories Found</option>
                                @endforelse
                            </select>

                            @error('public') <h5 style="color: red">{{ $message }}</h5> @enderror
                            <select class="mail" name="public" id="public" style="color: #FFF">
                                <option value="YES" @if($data['video']->public == "YES") selected @endif>{{__('text.publicContent')}}</option>
                                <option value="NO" style="background-color: #2D322F" @if($data['video']->public == "NO") selected @endif>{{__('text.privateContent')}}</option>
                            </select>


                            @error('image') <h5 style="color: red">{{ $message }}</h5> @enderror
                            <input class="mail" type="file" name="image" id="image"
                                   style="display: inline-block;padding-top: 10px">

                            @error('body') <p style="color: red">{{ $message }}</p> @enderror
                            <textarea class="mail" rows="20" name="body" placeholder="Article Body *" id="body"
                                      style="height: auto;">{{ $data['article']->body }}</textarea>

                            <input type="hidden" name="language" id="language" value="{{ $data['article']->language }}">
                            @csrf
                            <div>
                                <button class="btn btn-primary"
                                        style="background-color: #EC3642; border:0; padding: 10px 40px 10px 40px; width:60%;">
                                    {{__('text.updatedArticleBtn')}}
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        @include('layout.footer')
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
@include('includes.footerScripts')
</body>
</html>
