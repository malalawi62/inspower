<html lang="{{ __('text.lang') }}" dir="{{ __('text.dir') }}">

@include('includes.head')

<body>

@include('layout.header')
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app form" id="main">

        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe" style="min-height: 100%">
            <div class="container">
                <div class="cta-inner">
                    <div class="col-md-12">
                        <form action="/articles/{{ $data['article']->id }}" method="post">
                            @method('DELETE')
                            <div class="row banner-padding">
                                <h1 style="color: #ee2724;">{{ $data['article']->title }}</h1>
                                @guest
                                @else
                                    @if (auth()->user()->type == 'admin')
                                        <div class="banner-padding">
                                            <span class="editDeleteIcons">
                                                <a href="/articles/{{ $data['article']->id }}/edit" style="color: #364f87">
                                                    <i class="ion-edit"></i>
                                                </a>
                                                <a href="/sendArticle/{{ $data['article']->id }}" style="color: #364f87">
                                                    <i class="ion-android-send"></i>
                                                </a>
                                                <button onclick="return confirm('Are you sure you want to delete the article?')"
                                                        style="color: #364f87; background-color: transparent; border-width: 0; font-size: 24px">
                                                    <i class="ion-android-delete"></i>
                                                </button>
                                            </span>
                                        </div>
                                    @endif
                                @endguest
                            </div>
                            @csrf
                        </form>

                        <div class="row" style="padding: 10px 0 10px 0">
                            @if($data['lang'] == 'en')
                                <h3 style="color: #ee2724; {{ __('text.textAlign') }}"> Category: <a style="color: #364f87;" href="https://www.inspower-fitnesscoaching.com/articles?category_id={{ $data['article']->category->id }}"> {{ $data['article']->category->englishName }} </a> </h3>
                            @else
                                <h3 style="color: #ee2724; {{ __('text.textAlign') }}"> الفئة:<a style="color: #364f87;" href="https://www.inspower-fitnesscoaching.com/articles?category_id={{ $data['article']->category->id }}"> {{ $data['article']->category->arabicName }} </a> </h3>
                            @endif
                        </div>

                        <div class="row" style="padding: 10px 0 10px 0">
                            @if($data['article']->writerName != null)
                                <h3 style="color: #ee2724; {{ __('text.textAlign') }}">Written
                                    By: <span style="color: #364f87"> {{ $data['article']->writerName }}</span></h3>
                            @else
                                @if($data['lang'] == 'en')
                                    <h3 style="color: #ee2724; {{ __('text.textAlign') }}">Written
                                        By: <span style="color: #364f87"> {{ $data['article']->writer->englishName }} </span></h3>
                                @else
                                    <h3 style="color: #ee2724; {{ __('text.textAlign') }}">
                                        <span style="color: #364f87">الكاتب: {{ $data['article']->writer->arabicName }} </span></h3>
                                @endif
                            @endif
                        </div>

                        <div class="row" style="padding: 10px 0 10px 0">
                            @if($data['lang'] == 'en')
                                <h3 style="color: #ee2724; {{ __('text.textAlign') }}">
                                    Date: <span style="color: #364f87"> {{ $data['article']->date }} </span></h3>
                            @else
                                <h3 style="color: #ee2724; {{ __('text.textAlign') }}">
                                    <span style="color: #364f87">التاريخ: {{ $data['article']->date }} </span></h3>
                            @endif
                        </div>
                    </div>

                    <hr style="width: 100%; border-width:1px">

                    <div class="form wow fadeInUp" data-wow-delay="0.3s">
                            <img class="img-responsive wow fadeIn" src="{{ asset('storage/'.$data['article']->image) }}"
                                 alt="{{$data['article']->title}}" style="max-height: 500px; display: block"/>
                    </div>

                    <div class="line"></div>

                <p class="article-body" style="color: #2D322F"> {!! nl2br($data['article']->body) !!}
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        @include('layout.footer')
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
@include('includes.footerScripts')
</body>
</html>
