<html lang="{{ __('text.lang') }}" dir="{{ __('text.dir') }}">
@include('includes.head')

<body>

@include('layout.header')

<div class="wrapper">
    <!-- Main Section-->
    <div class="main app" id="main">
        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe" style="height: 90%">
            <div class="container">
                <div class="cta-inner">
                    <div class="row">
                        <h1 style="text-align: center; float: none"> {{ __('text.resetPassword') }} </h1>
                    </div>
                    <div class="wow fadeInUp" data-wow-delay="0.3s">
                        <form class="forms-form center-form wow zoomIn" method="POST" action="{{ route('password.email') }}">

                            @error('email') <h5 style="color: red;">{{ $message }}</h5> @enderror
                            <input class="mail" type="email" name="email" placeholder="{{ __('text.registerEmail') }}"
                                   required autocomplete="email" autofocus value="{{ old('email') }}">

                            @csrf
                            <div class="form-group row mb-0" style="padding-top: 20px">
                                <button type="submit" class="submit-button"
                                        style="width:60%;">
                                    {{ __('text.resetPassword') }}
                                </button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        @include('layout.footer')
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->


<!-- Jquery and Js Plugins -->
@include('includes.footerScripts')

</body>
</html>


