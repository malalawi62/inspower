<?php
$yearMTP = \Illuminate\Support\Facades\DB::table('membership_types')->where('type', 'Yearly')->value('fees');
$monthMTP = \Illuminate\Support\Facades\DB::table('membership_types')->where('type', 'Monthly')->value('fees');
?>

<html lang="{{ __('text.lang') }}" dir="{{ __('text.dir') }}">
@include('includes.head')

<body>

@include('layout.header')

<div class="wrapper">
    <!-- Main Section-->
    <div class="main app" id="main">
        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe" style="min-height: 100%">
            <div class="container">
                <div class="cta-inner">
                    <div class="row">
                    <h1 style="text-align: center; float: none"> {{ __('text.register') }} </h1>
                    </div>
                    <div class="wow fadeInUp" data-wow-delay="0.3s">
                        <form class="forms-form center-form wow zoomIn" method="POST" action="{{ route('register') }}">

                            @error('name') <h5 style="color: red;">{{ $message }}</h5> @enderror
                            <input class="mail" type="text" name="name" placeholder="{{ __('text.registerName') }}"
                                   required autocomplete="name" autofocus value="{{ old('name') }}">

                            @error('email') <h5 style="color: red;">{{ $message }}</h5> @enderror
                            <input id="email" class="mail" type="email" name="email" placeholder="{{ __('text.registerEmail') }}"
                                   required autocomplete="email" value="{{ old('email') }}">

                            <input id="email-confirm" class="mail" type="email" name="email_confirmation" placeholder="{{ __('text.registerConfirmEmail') }}"
                                   required autocomplete="off" value="{{ old('email') }}">

                            @error('DOB') <h5 style="color: red;">{{ $message }}</h5> @enderror
                            <div><label for="DOB">{{ __('text.registerDOB') }}</label></div>
                            <input class="mail" type="date" name="DOB" id="DOB" required value="{{ old('DOB') }}">

                            <div style="padding-bottom: 20px;">
                                @error('gender') <h5 style="color: red;">{{ $message }}</h5> @enderror
                                <span style="color: #2D322F"> {{ __('text.registerGender') }}: </span>
                                <input type="radio" name="gender" value="f"> <span style="color: #2D322F;"> {{ __('text.registerGenderFemale') }} </span>
                                <input type="radio" name="gender" value="m"> <span style="color: #2D322F;"> {{ __('text.registerGenderMale') }} </span>
                            </div>

                            @error('password') <h5 style="color: red;">{{ $message }}</h5> @enderror
                            <input id="password" class="mail" type="password" name="password" placeholder="{{ __('text.registerPassword') }}"
                                   autocomplete="new-password">

                            <input id="password-confirm" class="mail" type="password" name="password_confirmation" placeholder="{{ __('text.registerConfirmPassword') }}"
                                   required autocomplete="new-password">

                            <div class="col-md-12">
                                <div class="col-md-4" style="padding: 15px">
                                    <div class="pricing-col">
                                        <h4><input type="radio" name="type" value="free" checked style="height: 20px"> {!! nl2br(__('text.freePlan')) !!}</h4>
                                        <h2><i class="ion-checkmark" style="color: green"></i> {!! nl2br(__('text.free1')) !!}</h2>
                                        <h2><i class="ion-close" style="color: red"></i> {!! nl2br(__('text.monthly1')) !!}</h2>
                                        <h2><i class="ion-close" style="color: red"></i> {!! nl2br(__('text.monthly2')) !!}</h2>
                                        <h2><i class="ion-close" style="color: red"></i> {!! nl2br(__('text.monthly3')) !!}</h2>
                                        <h2><i class="ion-close" style="color: red"></i> {!! nl2br(__('text.monthly4')) !!}</h2>
                                        <h3><i class="ion-social-usd" style="color: #ee2724"></i> {!! nl2br(__('text.freePlanPricing')) !!}</h3>
                                    </div>
                                </div>

                                <div class="col-md-4" style="padding: 15px; filter: blur(4px); -webkit-filter: blur(4px);">
                                    <div class="pricing-col">
                                        <h4><input type="radio" name="type" value="month" disabled style="height: 20px"> {!! nl2br(__('text.monthlyPlan')) !!}</h4>
                                        <h2><i class="ion-checkmark" style="color: green"></i> {!! nl2br(__('text.free1')) !!}</h2>
                                        <h2><i class="ion-checkmark" style="color: green"></i> {!! nl2br(__('text.monthly1')) !!}</h2>
                                        <h2><i class="ion-checkmark" style="color: green"></i> {!! nl2br(__('text.monthly2')) !!}</h2>
                                        <h2><i class="ion-checkmark" style="color: green"></i> {!! nl2br(__('text.monthly3')) !!}</h2>
                                        <h2><i class="ion-checkmark" style="color: green"></i> {!! nl2br(__('text.monthly4')) !!}</h2>
                                        <h3><i class="ion-social-usd" style="color: #ee2724"></i> {{ $monthMTP }} {{__("text.BD")}} </h3>
                                    </div>
                                </div>

                                <div class="col-md-4" style="padding: 15px; filter: blur(4px); -webkit-filter: blur(4px);">
                                    <div class="pricing-col">
                                        <h4><input type="radio" name="type" value="year" disabled style="height: 20px"> {!! nl2br(__('text.yearlyPlan')) !!}</h4>
                                        <h2><i class="ion-checkmark" style="color: green"></i> {!! nl2br(__('text.free1')) !!}</h2>
                                        <h2><i class="ion-checkmark" style="color: green"></i> {!! nl2br(__('text.monthly1')) !!}</h2>
                                        <h2><i class="ion-checkmark" style="color: green"></i> {!! nl2br(__('text.monthly2')) !!}</h2>
                                        <h2><i class="ion-checkmark" style="color: green"></i> {!! nl2br(__('text.monthly3')) !!}</h2>
                                        <h2><i class="ion-checkmark" style="color: green"></i> {!! nl2br(__('text.monthly4')) !!}</h2>
                                        <h3><i class="ion-social-usd" style="color: #ee2724"></i> {{ $yearMTP }} {{__("text.BD")}} </h3>
                                    </div>
                                </div>
                            </div>

                            <div>
                                <button class="submit-button"
                                        style="width:60%; margin-top: 30px">
                                    {{ __('text.register') }}
                                </button>
                            </div>
                            @csrf
                        </form>

                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        @include('layout.footer')
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->


<!-- Jquery and Js Plugins -->
@include('includes.footerScripts')

</body>
</html>
