<html lang="{{ __('text.lang') }}" dir="{{ __('text.dir') }}">
@include('includes.head')

<body>


<div class="wrapper">
    <!-- Main Section-->
    <div class="main app" id="main">
        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe" style="min-height: 100%">
            <div class="container">
                <div class="cta-inner">
                    <div class="row">
                        <h1 style="text-align: center; float: none; margin-top: 10px"> {{ __('text.upgrade') }} </h1>
                    </div>
                    <div class="wow fadeInUp" data-wow-delay="0.3s">
                        <form class="forms-form center-form wow zoomIn" method="POST" action="{{ route('register') }}">
                            <div class="col-sm-12">
                                <div class="col-sm-6" style="padding: 15px">
                                    <div class="pricing-col">
                                        <h4><input type="radio" name="type" value="month" style="height: 20px"> {!! nl2br(__('text.monthlyPlan')) !!}</h4>
                                        <h2><i class="ion-checkmark" style="color: green"></i> {!! nl2br(__('text.free1')) !!}</h2>
                                        <h2><i class="ion-checkmark" style="color: green"></i> {!! nl2br(__('text.monthly1')) !!}</h2>
                                        <h2><i class="ion-checkmark" style="color: green"></i> {!! nl2br(__('text.monthly2')) !!}</h2>
                                        <h2><i class="ion-checkmark" style="color: green"></i> {!! nl2br(__('text.monthly3')) !!}</h2>
                                        <h2><i class="ion-checkmark" style="color: green"></i> {!! nl2br(__('text.monthly4')) !!}</h2>
                                        <h3><i class="ion-social-usd" style="color: #ee2724"></i> {!! nl2br(__('text.monthlyPlanPricing')) !!}</h3>
                                    </div>
                                </div>

                                <div class="col-sm-6" style="padding: 15px">
                                    <div class="pricing-col">
                                        <h4><input type="radio" name="type" value="year" style="height: 20px"> {!! nl2br(__('text.yearlyPlan')) !!}</h4>
                                        <h2><i class="ion-checkmark" style="color: green"></i> {!! nl2br(__('text.free1')) !!}</h2>
                                        <h2><i class="ion-checkmark" style="color: green"></i> {!! nl2br(__('text.monthly1')) !!}</h2>
                                        <h2><i class="ion-checkmark" style="color: green"></i> {!! nl2br(__('text.monthly2')) !!}</h2>
                                        <h2><i class="ion-checkmark" style="color: green"></i> {!! nl2br(__('text.monthly3')) !!}</h2>
                                        <h2><i class="ion-checkmark" style="color: green"></i> {!! nl2br(__('text.monthly4')) !!}</h2>
                                        <h3><i class="ion-social-usd" style="color: #ee2724"></i> {!! nl2br(__('text.yearlyPlanPricing')) !!}</h3>
                                    </div>
                                </div>
                            </div>

                            <div>
                                <button class="submit-button"
                                        style="width:60%; margin-top: 30px">
                                    {{ __('text.paymentBtn') }}
                                </button>
                            </div>
                            @csrf
                        </form>

                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->


<!-- Jquery and Js Plugins -->
@include('includes.footerScripts')

</body>
</html>
