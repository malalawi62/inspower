<html lang="{{ __('text.lang') }}" dir="{{ __('text.dir') }}">

@include('includes.head')

<body>

@include('layout.header')
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app form" id="main">

        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe" style="min-height: 95%">
            <div class="container">
                <div class="cta-inner">
                    <div class="form wow fadeInUp" data-wow-delay="0.3s">
                        <div class="row top-padding-center" style="padding-bottom: 20px">
                            <div class="row" style="padding-bottom: 20px">
                                <h1 style="color: #fdca00; font-weight: 700; margin-top: 15px; margin-bottom: 0; text-align: center; float: none;">
                                    {{ __('text.verifyTitle') }}</h1>
                            </div>

                            @if (session('resent'))
                                <div class="alert alert-success" role="alert">
                                    {{ __('text.freshLink') }}
                                </div>
                            @endif

                            <div class="row">
                                <h1 style="color: #F0E299; font-weight: 700; margin-top: 15px; margin-bottom: 0; text-align: center; float: none;"> {{ __('text.before') }} </h1>
                                <h1 style="color: #F0E299; font-weight: 700; margin-top: 15px; margin-bottom: 0; text-align: center; float: none;"> {{ __('text.sure') }} </h1>
                            <form class="d-inline" method="POST" style="padding-top: 20px" action="{{ route('verification.resend') }}">
                                @csrf
                                <button type="submit" class="btn btn-link p-0 m-0 align-baseline"> <span style="font-weight: 700; font-size: 28px;margin-top: 15px; margin-bottom: 0; text-align: center; float: none;color: #8c0000"> {{ __('text.click') }} </span></button>.
                            </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        @include('layout.footer')
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
@include('includes.footerScripts')
</body>
</html>

