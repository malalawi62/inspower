<html lang="{{ __('text.lang') }}" dir="{{ __('text.dir') }}">

@include('includes.head')

<body>

@include('layout.header')
<div class="wrapper" style="background-color: #2D322F">
    <!-- Main Section-->
    <div class="main app form" id="main">
        <div class="pricing-section text-center" style="min-height: 100%">
            <div class="container">
                <div class="col-md-8 col-md-offset-2 col-sm-12 nopadding text-center" style="float: left">
                    <div class="pricing-intro">
                        <h1 class="wow fadeInUp" data-wow-delay="0s"> {{ __('text.cartTitle') }} </h1>
                    </div>

                    @if($courses != null || $programs != null)
                        <div class="table-left wow fadeInUp" data-wow-delay="0.4s" style="overflow: auto;">
                            <div class="pricing-details">
                                <div style="display: block">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-8">
                                            <span class="video-title">
                                                <h1 style="color: #ee2724; font-size:20px"> {{__('text.cartRowTitle')}} </h1>
                                            </span>
                                            </div>

                                            <div class="col-md-3">
                                            <span class="video-title">
                                                <h1 style="color: #ee2724; font-size:20px;"> {{__('text.cartPrice')}} </h1>
                                            </span>
                                            </div>
                                        </div>

                                        <hr style="width: 100%; border-width:1px">

                                        @if($courses != null)
                                            @foreach($courses as $course)
                                                <div class="row">
                                                    <div class="col-md-8">
                                                    <span class="video-title">
                                                        <a style="color: #364f87; font-size:20px" href="/course/{{ $course->id }}_{{ str_replace(' ', '_', $course->title) }}">
                                                            {{ $course->title }}
                                                        </a>
                                                    </span>
                                                    </div>

                                                    <div class="col-md-3">
                                                    <span class="video-title">
                                                        <h1 style="color: #364f87; font-size:20px;"> {{ $course->price }} {{ __('text.BD') }} </h1>
                                                    </span>
                                                    </div>

                                                    <div class="col-md-1">
                                                    <span class="video-title">
                                                        <form action="/removeCourseFromCart/{{ $course->id }}" method="post">
                                                            @method('DELETE')
                                                            <button onclick="return confirm('Are you sure you want to remove this course from your cart?')"
                                                                    style="color: #ee2724; background-color: transparent; border-width: 0; font-size: 24px">
                                                                <i class="ion-android-delete"></i>
                                                            </button>
                                                            @csrf
                                                        </form>
                                                    </span>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif

                                        @if($programs != null)
                                            @foreach($programs as $program)
                                                <div class="row">
                                                    <div class="col-md-8">
                                                    <span class="video-title">
                                                        <a style="color: #364f87; font-size:20px" href="/program/{{ $program->id }}_{{ str_replace(' ', '_', $program->title) }}">
                                                        {{ $program->title }}
                                                        </a>
                                                    </span>
                                                    </div>

                                                    <div class="col-md-3">
                                                    <span class="video-title">
                                                        <h1 style="color: #364f87; font-size:20px;"> {{ $program ->price }} {{ __('text.BD') }} </h1>
                                                    </span>
                                                    </div>

                                                    <div class="col-md-1">
                                                    <span class="video-title">
                                                        <form action="/removeProgramFromCart/{{ $program->id }}" method="post">
                                                            @method('DELETE')
                                                            <button onclick="return confirm('Are you sure you want to remove this program from your cart?')"
                                                                    style="color: #ee2724; background-color: transparent; border-width: 0; font-size: 24px">
                                                                <i class="ion-android-delete"></i>
                                                            </button>
                                                            @csrf
                                                        </form>
                                                    </span>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                    <br>
                                </div>
                            </div>
                        </div>

                        <hr style="width: 100%; border-width:1px">

                        <div class="row">
                            <div class="col-md-9">
                            <span class="video-title">
                                <h1 style="color: #2D322F; font-size:24px; font-weight: 800;"> {{__('text.cartTotal')}}: <span style="color: red"> {{ $counter }} {{ __('text.BD') }} </span></h1>
                            </span>
                            </div>

                            <form action="/confirmOrder" method="GET">
                                <div class="col-md-3">
                                    <button class="cart-button" type="submit"
                                            style="{{ __('text.opFloating') }}; padding-left: 15px; padding-right: 15px">
                                        {{ __('text.checkoutBtn') }}
                                    </button>
                                </div>
                                @csrf
                            </form>

                        </div>
                    @else
                        <div class="row">
                            <div class="col-md-12">
                                <span class="h1">
                                    <h1 style="color: #ee2724; font-size:20px; text-align: center; float: none"> {{ __('text.empty') }} </h1>
                                </span>
                                <hr style="width: 100%; border-width:1px">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <form action="" method="get">
                                    <div class="col-md-6">
                                        <button class="checkoutBtn" formaction="/programs">
                                            {{ __('text.viewPrograms') }}
                                        </button>
                                    </div>

                                    <div class="col-md-6">
                                        <button class="checkoutBtn"  formaction="/courses">
                                            {{ __('text.viewCourses') }}
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <!-- Footer Section -->
        @include('layout.footer')
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
@include('includes.footerScripts')
</body>
</html>
