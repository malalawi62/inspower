<html lang="{{ __('text.lang') }}" dir="{{ __('text.dir') }}">

@include('includes.head')

<body>

@include('layout.header')
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app" id="main">

        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe" style="min-height: 90%">
            <div class="container">
                <div class="cta-inner">
                    <div class="row">
                        <h1 style="float: none; text-align: center"> {{ __('text.addCategoryTitle')  }} </h1>
                    </div>
                    <div class="wow fadeInUp" data-wow-delay="0.3s">
                        <form class="forms-form center-form wow zoomIn" action="/categories" method="post">

                            @error('englishName') <h5 style="color: red">{{ $message }}</h5> @enderror
                            <input class="mail" type="text" name="englishName" placeholder="Category English Name *"
                                   autocomplete="off" id="englishName" value="{{ old('englishName') }}">

                            @error('arabicName') <h5 style="color: red">{{ $message }}</h5> @enderror
                            <input dir="rtl" class="mail" type="text" name="arabicName"
                                   placeholder="اسم الفئة باللغة العربية *" autocomplete="off" id="arabicName"
                                   value="{{ old('arabicName') }}">

                            @csrf
                            <div>
                                <button class="submit-button" style="width: 60%;">
                                    {{__('text.addCategoryBtn')}}
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        @include('layout.footer')
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
@include('includes.footerScripts')
</body>
</html>
