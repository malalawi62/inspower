<!DOCTYPE html>
<html lang="{{ __('text.lang') }}" dir="{{ __('text.dir') }}">

@include('includes.head')

<body>
<script>
    var msg = '{{Session::get('alert')}}';
    var exist = '{{Session::has('alert')}}';
    if (exist) {
        alert(msg);
    }
</script>
@include('layout.header')
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app" id="main">
        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe">
            <div class="container">
                <div class="cta-inner">
                    <div class="row">
                        <h1 style="text-align: center; float: none"> {{ __('text.addModuleBtn') }} </h1>
                    </div>

                    <div class="wow fadeInUp" data-wow-delay="0.3s">
                        <form class="forms-form center-form wow zoomIn" action="/module" method="post"
                              enctype="multipart/form-data">
                            <input type="hidden" name="course_id" id="course_id" value="{{ $course_id }}">
                            <div class="modules">
                                <div class="module module_wrapper_1">
                                    <div class="row">
                                        <h1 style="text-align: center; float: none; padding-top: 0" id="module_title_1"> {{__('text.moduleNew')}}
                                            <input class="add_module_button" type="button" onclick="addModule(1)" value="+" style="color: #ee2724; background: transparent; border-color: transparent; border-width: 0">
                                        </h1>
                                    </div>

                                    @error('m_title') <h5 style="color: red;">{{ $message }}</h5> @enderror
                                    <input class="mail" type="text" name="m_title[]"
                                           placeholder="{{ __('text.moduleTitle') }}"
                                           autocomplete="m_title" value="{{ old('m_title') }}">

                                    <p>{{ __('text.moduleDescription') }}</p>
                                    <textarea class="mail" rows="10" name="m_description[]" style="height: auto;" value="{{ old('m_description') }}"></textarea>

                                    <div class="videos_1">
                                        <div class="video_wrapper video_1">
                                            <div class="row">
                                                <h1 style="text-align: center; float: none; padding-top: 0" id="lesson_title_1"> {{__('text.lessonNew')}}
                                                    <input class="add_video_course_button" type="button" onclick="addCourseVideo(1)" value="+" style="color: #ee2724; background: transparent; border-color: transparent; border-width: 0">
                                                </h1>
                                            </div>

                                            <div class="row">
                                                <input class="mail" type="text" name="m_videoUrl[1][]"
                                                       placeholder="{{ __('text.moduleVideoURL') }}"
                                                       autocomplete="off" value="{{ old('m_videoUrl') }}" />
                                                <label class="mail" style="border: 1.5px solid #babdc2; padding-top: 10px">
                                                    {{__('text.inputLessonPdf')}}
                                                    <input type="file" name="pdf[1][]" id="bodyPdf" style="display: inline-block"/>
                                                </label>
                                                <p>{{ __('text.lessonDescription') }}</p>
                                                <textarea class="mail" rows="10" name="v_description[1][]" style="height: auto;" value="{{ old('v_description') }}"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            @csrf
                            <div>
                                <div>
                                    <button class="submit-button" style="width: 60%;">
                                        {{__('text.addModuleBtn')}}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        @include('layout.footer')
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
@include('includes.footerScripts')
</body>
</html>
