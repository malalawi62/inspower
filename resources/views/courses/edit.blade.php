<!DOCTYPE html>
<html lang="{{ __('text.lang') }}" dir="{{ __('text.dir') }}">

@include('includes.head')

<body>
<script>
    var msg = '{{Session::get('alert')}}';
    var exist = '{{Session::has('alert')}}';
    if (exist) {
        alert(msg);
    }
</script>
@include('layout.header')
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app" id="main">
        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe">
            <div class="container">
                <div class="cta-inner">
                    <div class="row">
                        <h1 style="text-align: center; float: none"> {{ __('text.editCourseTitle') }} </h1>
                    </div>

                    <div class="wow fadeInUp" data-wow-delay="0.3s">
                        <form class="forms-form center-form wow zoomIn" action="/course/{{ $course->id }}" method="post"
                              enctype="multipart/form-data">
                            @method('PATCH')

                            <div class="row">
                                <h1 style="text-align: center; float: none"> {{ __('text.courseDetails') }} </h1>
                            </div>

                            @error('title') <h5 style="color: red;">{{ $message }}</h5> @enderror
                            <input class="mail" type="text" name="title"
                                   placeholder="{{ __('text.inputCourseTitle') }}"
                                   autocomplete="title" value="{{ $course->title }}">

                            @error('overview') <p style="color: red">{{ $message }}</p> @enderror
                            <p>{{ __('text.inputCourseOverview') }}</p>
                            <textarea class="mail" rows="10" name="overview" style="height: auto;">{{ $course->overview }}</textarea>

                            @error('image') <h5 style="color: red">{{ $message }}</h5> @enderror
                            <p>{{ __('text.inputImage') }}</p>
                            <input class="mail" type="file" name="image" id="image"
                                   style="display: inline-block;padding-top: 10px">

                            @error('price') <h5 style="color: red;">{{ $message }}</h5> @enderror
                            <input class="mail" type="number" name="price"
                                   placeholder="{{ __('text.inputCoursePrice') }}"
                                   value="{{ $course->price }}">

                            <input class="mail" type="text" name="videoUrl"
                                   placeholder="{{ __('text.inputCourseVideo') }}"
                                   autocomplete="off" value="@if($course->videoUrl != null) https://vimeo.com/{{ $course->videoUrl }} @endif">

                            <p>{{ __('text.inputCourseDescription') }}</p>
                            <textarea class="mail" rows="10" name="description" style="height: auto;">{{ $course->description }}</textarea>

                            @csrf
                            <div>
                                <button class="submit-button" style="width: 60%;">
                                    {{__('text.editCourseBtn')}}
                                </button>
                            </div>
                        </form>
                        <div class="line"></div>

                            <div class="modules">
                                <?php $counter = 0; ?>
                                @foreach ($modules as $module)
                                    <?php $counter ++; ?>
                                        <div class="row" style="padding-bottom: 30px">
                                            <div class="row">
                                                <form action="/module/{{ $module->id }}/delete" method="post">
                                                <h2 class="h2" style="text-transform: uppercase"> {{ __('text.moduleTitle2') }}: {{ $module->m_title }}
                                                    <a href="/module/{{ $module->id }}/edit" style="color: #364f87">
                                                        <i class="ion-edit"></i>
                                                    </a>
                                                    <button onclick="return confirm('Are you sure you want to DELETE this module?')"
                                                            style="color: #364f87; background-color: transparent; border-width: 0; font-size: 24px">
                                                        <i class="ion-android-delete"></i>
                                                    </button>
                                                </h2>
                                                    @csrf
                                                </form>
                                            </div>

                                            <div class="row">
                                                <h2 class="h2" style="{{ __('text.textAlign') }}"> {{ __('text.moduleDescriptionShow') }}: </h2>
                                                <p class="article-body">
                                                    {!! nl2br($module->m_description) !!}
                                                </p>
                                            </div>

                                            <div class="row">
                                                <h2 class="h2" style="{{ __('text.textAlign') }}"> {{ __('text.moduleLessons') }}: </h2>
                                            </div>
                                            @foreach($lessons as $lesson)
                                                @if($lesson->course_module_id == $module->id)
                                                    <div class="row">
                                                        <p class="article-body"> <a href="https://vimeo.com/{{ $lesson->m_videoUrl }}"> https://vimeo.com/{{ $lesson->m_videoUrl }} </a> </p>
                                                    </div>

                                                    <div class="row" style="width: 80%; margin-top: 20px">
                                                        <p class="article-body">
                                                            {!! nl2br($lesson->v_description) !!}
                                                        </p>
                                                    </div>

                                                    <div id="read" class="showPdf row" style="width: 83%; margin-top: 20px">
                                                        <a class="pdfBody" href="/lessonPdf/{{$lesson->pdf}}#toolbar=0" target="_blank" style="color: #fff;"> {{__('text.viewPdf')}} </a>
                                                    </div>

                                                    <div class="row">
                                                        <div class="line"></div>
                                                    </div>
                                                @endif
                                            @endforeach
                                        </div>
                                @endforeach

                                    <!-- this section is to add a new module -->
                                    <form action="/module/create" method="get">
                                        <input type="hidden" name="course_id" value="{{ $course->id }}">
                                        <div>
                                            <button class="submit-button" style="width: 60%;">
                                                {{__('text.addModuleBtn')}}
                                            </button>
                                        </div>
                                        @csrf
                                    </form>
                            </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        @include('layout.footer')
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
@include('includes.footerScripts')
</body>
</html>
