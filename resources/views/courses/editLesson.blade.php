<!DOCTYPE html>
<html lang="{{ __('text.lang') }}" dir="{{ __('text.dir') }}">

@include('includes.head')

<body>
<script>
    var msg = '{{Session::get('alert')}}';
    var exist = '{{Session::has('alert')}}';
    if (exist) {
        alert(msg);
    }
</script>
@include('layout.header')
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app" id="main">
        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe">
            <div class="container">
                <div class="cta-inner">
                    <div class="row">
                        <h1 style="text-align: center; float: none"> {{ __('text.editLessonTitle') }} </h1>
                    </div>

                    <div class="wow fadeInUp" data-wow-delay="0.3s">
                        <form class="forms-form center-form wow zoomIn" action="/lesson/{{ $lesson2->id }}" method="post"
                              enctype="multipart/form-data">
                            @method('PATCH')
                            <div class="modules">
                                <div class="module module_wrapper_1">
                                    <div class="row">
                                        <h1 style="text-align: center; float: none; padding-top: 0" id="module_title_1">  </h1>
                                    </div>

                                    @error('m_videoUrl') <h5 style="color: red;">{{ $message }}</h5> @enderror
                                    <input class="mail" type="text" name="m_videoUrl"
                                           value="https://vimeo.com/{{ $lesson2->m_videoUrl }}"
                                           autocomplete="m_videoUrl">

                                    <label class="mail" style="border: 1.5px solid #babdc2; padding-top: 10px">
                                        {{__('text.inputLessonPdf')}}
                                        <input type="file" name="pdf" style="display: inline-block"/>
                                    </label>

                                    <p>{{ __('text.lessonDescription') }}</p>
                                    <textarea class="mail" rows="10" name="v_description" style="height: auto;"> {{ $lesson2->v_description }} </textarea>
                                </div>
                            </div>
                            @csrf
                            <div>
                                <button class="submit-button" style="width: 60%;">
                                    {{__('text.editLessonBtn')}}
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        @include('layout.footer')
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
@include('includes.footerScripts')
</body>
</html>
