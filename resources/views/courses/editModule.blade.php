<!DOCTYPE html>
<html lang="{{ __('text.lang') }}" dir="{{ __('text.dir') }}">

@include('includes.head')

<body>
<script>
    var msg = '{{Session::get('alert')}}';
    var exist = '{{Session::has('alert')}}';
    if (exist) {
        alert(msg);
    }
</script>
@include('layout.header')
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app" id="main">
        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe">
            <div class="container">
                <div class="cta-inner">
                    <div class="row">
                        <h1 style="text-align: center; float: none"> {{ __('text.editModuleTitle') }} </h1>
                    </div>

                    <div class="wow fadeInUp" data-wow-delay="0.3s">
                        <form class="forms-form center-form wow zoomIn" action="/module/{{ $modules->id }}" method="post"
                              enctype="multipart/form-data">
                            @method('PATCH')
                            <div class="modules">
                                <div class="module module_wrapper_1">

                                    @error('m_title') <h5 style="color: red;">{{ $message }}</h5> @enderror
                                    <input class="mail" type="text" name="m_title"
                                           value="{{ $modules->m_title }}"
                                           autocomplete="m_title">

                                    <p>{{ __('text.moduleDescription') }}</p>
                                    <textarea class="mail" rows="10" name="m_description" style="height: auto;"> {{ $modules->m_description }} </textarea>
                                </div>
                            </div>
                            @csrf
                            <div>
                                <button class="submit-button" style="width: 60%;">
                                    {{__('text.editModuleBtn')}}
                                </button>
                            </div>
                        </form>
                                    <div class="line"></div>

                                    <?php $counter = 1; ?>
                                    @foreach($lessons as $lesson)
                                        <?php $counter ++; ?>
                                        <div class="">
                                            <div class="row" style="padding-bottom: 30px">
                                                <div class="row">
                                                    <form action="/lesson/{{ $lesson->id }}/delete" method="post">
                                                    <h2 class="h2" style="{{ __('text.textAlign') }} ; text-transform: uppercase"> {{__('text.lesson')}} {{ $counter }}
                                                        <a href="/lesson/{{ $lesson->id }}/edit" style="color: #364f87">
                                                            <i class="ion-edit"></i>
                                                        </a>
                                                        <button onclick="return confirm('Are you sure you want to DELETE this lesson?')"
                                                                style="color: #364f87; background-color: transparent; border-width: 0; font-size: 24px">
                                                            <i class="ion-android-delete"></i>
                                                        </button>
                                                    </h2>
                                                        @csrf
                                                    </form>
                                                </div>

                                                <div class="row">
                                                    <p class="article-body"> <a href="https://vimeo.com/{{ $lesson->m_videoUrl }}"> https://vimeo.com/{{ $lesson->m_videoUrl }} </a> </p>
                                                </div>
                                                @if ($lesson->v_description != null)
                                                    <div class="row"  style="margin-top: 20px">
                                                        <p class="article-body">
                                                            {!! nl2br($lesson->v_description) !!}
                                                        </p>
                                                    </div>
                                                @endif

                                                @if ($lesson->pdf != null)
                                                    <div class="row">
                                                        <div id="read" class="showPdf" style="margin-top: 20px">
                                                            <a class="pdfBody" href="/lessonPdf/{{$lesson->pdf}}#toolbar=0" target="_blank" style="color: #ffffff;"> {{__('text.viewPdf')}} </a>
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                            <div class="line"></div>

                                    @endforeach

                            <form action="/lesson/create" method="get">
                                <input type="hidden" name="module_id" value="{{ $modules->id }}">
                                <div>
                                    <button class="submit-button" style="width: 60%;">
                                        {{__('text.addLessonBtn')}}
                                    </button>
                                </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        @include('layout.footer')
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
@include('includes.footerScripts')
</body>
</html>
