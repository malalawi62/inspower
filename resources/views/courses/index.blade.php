<html lang="{{ __('text.lang') }}" dir="{{ __('text.dir') }}">

@include('includes.head')

<body>
<script>
    var msg = '{{Session::get('alert')}}';
    var exist = '{{Session::has('alert')}}';
    if (exist) {
        alert(msg);
    }
</script>

@include('layout.header')
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app form" id="main">
        <div class="pricing-section text-center" style="min-height: 100%">
            <div class="container">
                <div  style="width: 100%">
                    <div class="pricing-intro banner-padding">
                        <h1 class="wow fadeInUp" data-wow-delay="0s"> {{ __('text.allCoursesTitle') }} </h1>
                        @if((App::isLocale('en')))
                            <h2>For courses in Arabic language <a href="setlocale/ar">press here</a></h2>
                        @endif
                        @if((App::isLocale('ar')))
                            <h2>للدورات التدريبية باللغة الإنجليزية  <a href="setlocale/en">اضغط هنا</a></h2>
                        @endif
                    </div>
                    @foreach($courses as $course)
                        <?php $flag = false; ?>
                            @guest
                            @else
                        @if (auth()->user()->type == 'admin')
                            <?php $flag = true; ?>
                        @elseif($orders != null)
                            @for($i=0; $i<count($orders); $i++)
                                @if($orders[$i]->course_id == $course->id)
                                    <?php $flag = true; ?>
                                    @break
                                @endif
                            @endfor
                        @endif
                            @endguest
                            @if ($course->status == 'inactive')
                                @guest
                                @else
                                    @if (auth()->user()->type == 'admin' || $flag == true)
                                        <div class="table-left wow fadeInUp" data-wow-delay="0.4s" style="overflow: auto;">
                                            <div class="pricing-details">
                                                <div style="display: block">
                                                    <div class="col-md-4 articleImg">
                                                        <img class="show-img wow fadeIn"
                                                             src="{{ asset('storage/'.$course->image) }}" style="padding-bottom: 10px"/>
                                                    </div>
                                                    <br>
                                                    <div class="col-md-8">
                                                        <div class="row">
                                                            <span class="video-title">
                                                                <a style="color: #ee2724; font-size:20px" href="/course/{{ $course->id }}_{{ str_replace(' ', '_', $course->title) }}">
                                                                    {{ $course->title }} <span style="color: #2D322F; font-weight: 700;">*{{ __('text.inactiveCourse') }}*</span>
                                                                </a>
                                                            </span>
                                                            @guest
                                                            @else
                                                                @if (auth()->user()->type == 'admin')
                                                                    <form action="/course/{{ $course->id }}" method="post">
                                                                        <span class="editDeleteIcons">
                                                                            <a href="/course/{{ $course->id }}/edit"
                                                                                style="color: #364f87">
                                                                                <i class="ion-edit"></i>
                                                                            </a>
                                                                            <a href="/sendCourse/{{ $course->id }}"
                                                                               style="color: #364f87">
                                                                                <i class="ion-android-send"></i>
                                                                            </a>
                                                                            @if($course->status == 'active')
                                                                                <button onclick="return confirm('Are you sure you want to INACTIVE this course?')"
                                                                                        style="color: #364f87; background-color: transparent; border-width: 0; font-size: 24px">
                                                                                    <i class="fas fa-toggle-off"></i>
                                                                                </button>
                                                                            @else
                                                                                <a href="/courseActive/{{ $course->id }}"
                                                                                   style="color: #364f87">
                                                                                    <i class="fas fa-toggle-on"></i>
                                                                                </a>
                                                                            @endif
                                                                        </span>
                                                                        @csrf
                                                                    </form>
                                                                @endif
                                                            @endguest
                                                        </div>
                                                        <h2 class="articleBody" style="color: #2D322F">
                                                            {{__("text.price")}}
                                                            <span style="color: #ee2724">{{$course->price}} {{__("text.BD")}}</span>
                                                        </h2>
                                                        <h2 class="articleBody" style="color: #969696">
                                                            {!! nl2br($course->overview) !!}
                                                        </h2>
                                                        <form action="/cart" method="post">
                                                            <input type="hidden" name="course_id" value="{{ $course->id }}">
                                                            @guest
                                                            @else
                                                                <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
                                                            @endguest
                                                            <button class="submit-button"
                                                                    style="{{ __('text.floating') }}; padding-left: 15px; padding-right: 15px" formaction="/course/{{ $course->id }}" formmethod="get">
                                                                {{ __('text.learnMoreBtn') }}
                                                            </button>

                                                            @if($flag == false)
                                                                <button class="submit-button" type="submit"
                                                                        style="{{ __('text.opFloating') }}; padding-left: 15px; padding-right: 15px">
                                                                    {{ __('text.addToCartBtn') }}
                                                                </button>
                                                            @endif
                                                            @csrf
                                                        </form>
                                                    </div>

                                                    @csrf
                                                    <br>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endguest
                            @else
                                <div class="table-left wow fadeInUp" data-wow-delay="0.4s" style="overflow: auto;">
                                    <div class="pricing-details">
                                        <div style="display: block">
                                                <div class="col-md-4 articleImg">
                                                    <img class="show-img wow fadeIn"
                                                         src="{{ asset('storage/'.$course->image) }}" style="padding-bottom: 10px"/>
                                                </div>
                                                <br>
                                                <div class="col-md-8">
                                                    <div class="row">
                                                        <span class="video-title">
                                                            <a style="color: #ee2724; font-size:20px" href="/course/{{ $course->id }}_{{ str_replace(' ', '_', $course->title) }}">
                                                                {{ $course->title }}
                                                            </a>
                                                        </span>
                                                        @guest
                                                            @else
                                                            @if (auth()->user()->type == 'admin')
                                                                <form action="/course/{{ $course->id }}" method="post">
                                                                <span class="editDeleteIcons">
                                                                    <a href="/course/{{ $course->id }}/edit"
                                                                       style="color: #364f87">
                                                                        <i class="ion-edit"></i>
                                                                    </a>
                                                                    <a href="/sendCourse/{{ $course->id }}"
                                                                       style="color: #364f87">
                                                                        <i class="ion-android-send"></i>
                                                                    </a>
                                                                    <button onclick="return confirm('Are you sure you want to INACTIVE this course?')"
                                                                            style="color: #364f87; background-color: transparent; border-width: 0; font-size: 24px">
                                                                        <i class="fas fa-toggle-off"></i>
                                                                    </button>
                                                                </span>
                                                                    @csrf
                                                                </form>
                                                            @endif
                                                            @endguest
                                                    </div>
                                                    <h2 class="articleBody" style="color: #2D322F">
                                                        {{__("text.price")}}
                                                        <span style="color: #ee2724">{{$course->price}} {{__("text.BD")}}</span>
                                                    </h2>
                                                    <h2 class="articleBody" style="color: #969696">
                                                        {!! nl2br($course->overview) !!}
                                                    </h2>
                                                    <form action="/cart" method="post">
                                                        <input type="hidden" name="course_id" value="{{ $course->id }}">
                                                        @guest
                                                        @else
                                                            <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
                                                        @endguest
                                                        <button class="submit-button"
                                                            style="{{ __('text.floating') }}; padding-left: 15px; padding-right: 15px" formaction="/course/{{ $course->id }}" formmethod="get">
                                                            {{ __('text.learnMoreBtn') }}
                                                        </button>

                                                        @if($flag == false)
                                                            <button class="cart-button"
                                                                    style="{{ __('text.opFloating') }}; padding-left: 15px; padding-right: 15px">
                                                                {{ __('text.addToCartBtn') }}
                                                            </button>
                                                        @endif
                                                        @csrf
                                                    </form>
                                                </div>

                                                @csrf
                                            <br>
                                        </div>
                                    </div>
                                </div>
                            @endif
                    @endforeach
                </div>
            </div>
        </div>
        <!-- Footer Section -->
        @include('layout.footer')
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
@include('includes.footerScripts')

</body>
</html>

