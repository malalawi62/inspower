<html lang="{{ __('text.lang') }}" dir="{{ __('text.dir') }}">

@include('includes.head')

<body>

@include('layout.header')

<?php $flag = false; ?>
@if($orders != null)
    @for($i=0; $i<count($orders); $i++)
        @if($orders[$i]->course_id == $course->id)
            <?php $flag = true; ?>
            @break
        @endif
    @endfor
@endif
@guest
@else
@if(auth()->user()->type == 'admin')
    <?php $flag = true; ?>
@endif
@endguest
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app form" id="main">

        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe" style="min-height: 100%">
            <div class="container">
                <div class="cta-inner">
                    <div class="col-md-12">
                        <form class="subscribe-form" action="/cart" method="post">
                            <input type="hidden" name="course_id" value="{{ $course->id }}">
                            @guest
                            @else
                                <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
                            @endguest
                            <div class="row">
                                <span class="h1" style="color: #ee2724; padding: 5px; width: 100%">{{ $course->title }} @if($course->status == 'inactive') <span style="color: #2D322F; font-weight: 700;">*{{ __('text.inactiveCourse') }}*</span> @endif
                                    @if($flag == false)
                                        <button class="addToCartBtn" title="{{ __("text.addToCartTitle") }}">
                                            <i class="ion-android-cart"></i>
                                        </button>
                                    @endif
                                </span>
                            </div>
                            @csrf
                        </form>
                        <form action="/course/{{ $course->id }}" method="post">
                            @guest
                                @else
                                @if (auth()->user()->type == 'admin')
                                    <span class="editDeleteIcons">
                                        <a href="/course/{{ $course->id }}/edit" style="color: #364f87">
                                            <i class="ion-edit"></i>
                                        </a>
                                        <a href="/sendCourse/{{ $course->id }}" style="color: #364f87">
                                            <i class="ion-android-send"></i>
                                        </a>
                                        @if ($course->status == 'active')
                                            <button onclick="return confirm('Are you sure you want to INACTIVE this course?')"
                                                    style="color: #364f87; background-color: transparent; border-width: 0; font-size: 24px">
                                                <i class="fas fa-toggle-off"></i>
                                            </button>
                                        @else
                                            <a href="/courseActive/{{ $course->id }}"
                                               style="color: #364f87">
                                                <i class="fas fa-toggle-on"></i>
                                            </a>
                                        @endif
                                    </span>
                                @endif
                            @endguest
                            @csrf
                        </form>
                    </div>

                    <div class="form wow fadeInUp" data-wow-delay="0.3s">
                        <div class="row" style="padding-bottom: 20px">
                            <img class="img-responsive wow fadeIn" src="{{ asset('storage/'.$course->image) }}"
                                 alt="{{$course->title}}" style="max-height: 500px; display: block"/>
                        </div>
                    </div>

                    <div>
                        <form action="/cart" method="post">
                            <input type="hidden" name="course_id" value="{{ $course->id }}">
                            @guest
                            @else
                                <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
                            @endguest

                            @if($flag == false)
                                <button class="addToCartBtn2" type="submit">
                                    {{ __('text.addToCartBtn') }}
                                </button>
                            @endif
                            @csrf
                        </form>
                    </div>

                    <div class="line"> </div>

                    <div class="row">
                        <h2 class="h2"> {{ __('text.coursePrice') }} </h2>
                        <h2 class="h2" style="color: #ee2724; text-align: center">
                           {{$course->price}} {{__("text.BD")}}
                        </h2>
                    </div>

                    <div class="line"> </div>

                    <div class="row">
                        <h2 class="h2"> {{ __('text.courseOverview') }} </h2>
                        <p class="article-body" style="color: #2D322F">
                            {!! nl2br($course->overview) !!}
                        </p>
                    </div>


                    @if($course->videoUrl != null)
                        <div class="row">
                            <iframe class="iframe-courses"
                                    src="https://player.vimeo.com/video/{{ $course->videoUrl }}"
                                    frameborder="0" allowfullscreen></iframe>
                        </div>
                    @endif

                    @if($course->description != null)
                        <div class="line"> </div>

                        <div class="row">
                            <h2 class="h2"> {{ __('text.courseDescription') }} </h2>
                            <p class="article-body"  style="color: #2D322F">
                                {!! nl2br($course->description) !!}
                            </p>
                        </div>
                    @endif



                    <form action="/cart" method="post">
                        <input type="hidden" name="course_id" value="{{ $course->id }}">
                        @guest
                        @else
                            <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
                        @endguest

                        @if($flag == false)
                                <button class="addToCartBtn2" type="submit">
                                    {{ __('text.addToCartBtn') }}
                                </button>
                        @endif
                        @csrf
                    </form>
                @if($flag == true)
                        <div class="line"> </div>

                    <div class="row">
                        <h2 class="h2"> {{ __('text.courseModules') }} </h2>
                    </div>

                    <?php $counter = 0; ?>
                    @foreach($modules as $module)
                        <?php $counter ++; ?>
                        <button class="accordion">
                            {{ __('text.module') }} {{ $counter }}: {{$module->m_title}}
                        </button>
                        <div class="panel">
                            <h2 class="h2"> {{ __('text.moduleDescriptionShow') }}: </h2>
                            <p style="color: #2D322F; text-align: justify">{{ $module->m_description }}</p>

                            <h2 class="h2"> {{ __('text.moduleLessons') }}: </h2>

                            <?php $v_counter = 0; ?>
                            @foreach($lessons as $lesson)
                                @if($lesson->course_module_id == $module->id)
                                    <?php $v_counter ++; ?>
                                        @foreach($status as $state)
                                            @if($state->course_module_video_id == $lesson->id)
                                                <button class="accordion2" onclick="myfunction('iframe-{{ $counter }}-{{ $v_counter }}', '{{ $state->id }}', 'status-{{ $counter }}-{{ $v_counter }}')">
                                                    {{ __('text.lesson') }} {{ $v_counter }}
                                                @if($state->status == 'incomplete')
                                                    <span id="status-{{ $counter }}-{{ $v_counter }}"> <i class="fas fa-exclamation-circle incomplete" title="{{ __('text.incomplete') }}"></i> {{ __('text.incomplete') }}</span>
                                                @else
                                                    <span id="status-{{ $counter }}-{{ $v_counter }}"> <i class="fas fa-check-circle complete" title="{{ __('text.complete') }}"></i> {{ __('text.complete') }}</span>
                                                @endif
                                                </button>
                                                <div class="panel">
                                                    <iframe class="iframe-video" id="iframe-{{ $counter }}-{{ $v_counter }}"
                                                            src="https://player.vimeo.com/video/{{ $lesson->m_videoUrl }}?autoplay=1#t={{ $state->currentSecond }}s"
                                                            frameborder="0" allowfullscreen>
                                                    </iframe>
                                                    <p style="color: #2D322F; padding-bottom: 10px; text-align: justify">{{ $lesson->v_description }}</p>
                                                    <div id="read" class="showPdf">
                                                        <a class="pdfBody" href="/lessonPdf/{{$lesson->pdf}}#toolbar=0" target="_blank" style="color: #ffffff;"> {{__('text.viewPdf')}} </a>
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                    <br />
                                @endif
                            @endforeach
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
        </div>

        @include('layout.footer')
    </div>

        <!-- Footer Section -->

    </div>
    <!-- Main Section -->
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
@include('includes.footerScripts')
</body>
</html>
