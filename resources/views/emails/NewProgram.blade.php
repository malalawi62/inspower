<!DOCTYPE html>
<html>

<body style="background-color: #cccccc">

<div class="wrapper" style="background-color: #cccccc; padding: 20px;">
    <!-- Main Section-->
    <div class="main app form" id="main" style="background-color: #cccccc">
        <img style="display: block; margin-left: auto; margin-right: auto; width: 50%;"
             src="{{ asset('images/logoColored.png') }}" alt="inspower">

        <hr style="margin: 20px">

        <!-- About Section -->
        @if($ProgramInfo->language == "Eng")
            <div class="split-features" id="about" lang="en" dir="ltr">
                <div class="col-md-12 nopadding">
                    <div>
                        <h1 class="wow fadeInUp" style="text-align: center"> Dear {{ $subscriberName }}, </h1>
                        <h1 class="wow fadeInUp" style="text-align: center"> Check out our available program in INSPOWER website </h1>
                        <p class="wow fadeInUp" style="text-align: center">
                            <a href="https://www.inspower-fitnesscoaching.com/program/{{ $ProgramInfo->id }}"> {{ $ProgramInfo->title }} </a>
                        </p>
                        <p style="text-align: center">
                            {!! nl2br($ProgramInfo->overview) !!}
                        </p>
                    </div>
                </div>
            </div>
        @endif

        @if($ProgramInfo->language == "Ar")
            <div class="split-features" id="about" lang="ar" dir="rtl">
                <div class="col-md-12 nopadding">
                    <div>
                        <h1 class="wow fadeInUp" style="text-align: center"> أهلُا {{ $subscriberName }}, </h1>
                        <h1 class="wow fadeInUp" style="text-align: center"> يمكنك رؤية البرنامج التدريبي المتاح في موقعنا </h1>
                        <p class="wow fadeInUp" style="text-align: center">
                            <a href="https://www.inspower-fitnesscoaching.com/program/{{ $ProgramInfo->id }}"> {{ $ProgramInfo->title }} </a>
                        </p>
                        <p style="text-align: center">
                            {!! nl2br($ProgramInfo->overview) !!}
                        </p>
                    </div>
                </div>
            </div>
        @endif

        <hr style="margin: 20px">

        <div style="text-align: center">
            <a href="https://inspower-fitnesscoaching.com"><img src="{{ asset('images/domain.png') }}" style="padding: 10px; width: 50px; display:inline-block;"></a>
            <a href="https://www.instagram.com/inspower_pt/"><img src="{{ asset('images/instagram.png') }}" style="padding: 10px; width: 50px"></a>
            <a href="https://www.youtube.com/channel/UCkTu54pUXDLzMp8j0T1Ping"><img src="{{ asset('images/youtube.png') }}" style="padding: 10px; width: 50px"></a>
        </div>

        <div style="text-align: center">
            @if($ProgramInfo->language == "Eng")
                @component('mail::button', ['url'=>'https://www.inspower-fitnesscoaching.com/unsubscribe/'.$mail])
                    UnSubscribe
                @endcomponent
            @endif
            @if($ProgramInfo->language == "Ar")
                @component('mail::button', ['url'=>'https://www.inspower-fitnesscoaching.com/unsubscribe/'.$mail])
                    إلغاء الاشتراك
                @endcomponent
            @endif
        </div>

    </div>
</div>

</body>
</html>
