<!DOCTYPE html>
<html lang="en" dir="ltr">

<body style="background-color: #cccccc">

<div class="wrapper" style="background-color: #cccccc; padding: 20px;">
    <!-- Main Section-->
    <div class="main app form" id="main" style="background-color: #cccccc">
        <img style="display: block; margin-left: auto; margin-right: auto; width: 50%;"
             src="{{ asset('images/logoColored.png') }}" alt="inspower">

        <hr style="margin: 20px">

        <!-- About Section -->
        <div class="split-features" id="about">
            <div class="col-md-12 nopadding">
                <div>
                    <h1 class="wow fadeInUp" style="text-align: center"> Dear {{ $name }}, </h1>
                    <h1 class="wow fadeInUp" style="text-align: center"> Your subscription is received.. Welcome to INSPOWER </h1>
                    <h1 class="wow fadeInUp" style="text-align: center"> Please confirm your Subscription by clicking the following button</h1>

                    @component('mail::button', ['url'=>'https://inspower-fitnesscoaching.com/subscribe/'.$mail])
                        CONFIRM
                    @endcomponent

                    <p class="wow fadeInUp" style="text-align: center">
                        if you received this email by mistake, simply delete it. You won't be subscribed if you don't
                        click the confirmation button above.
                    </p>
                </div>
            </div>
        </div>

        <hr style="margin: 20px">

        <div style="text-align: center">
            <a href="https://inspower-fitnesscoaching.com"><img src="{{ asset('images/domain.png') }}" style="padding: 10px; width: 50px; display:inline-block;"></a>
            <a href="https://www.instagram.com/inspower_pt/"><img src="{{ asset('images/instagram.png') }}" style="padding: 10px; width: 50px"></a>
            <a href="https://www.youtube.com/channel/UCkTu54pUXDLzMp8j0T1Ping"><img src="{{ asset('images/youtube.png') }}" style="padding: 10px; width: 50px"></a>
        </div>

    </div>
</div>

</body>
</html>
