<div class="filterDiv {{ $video->category->englishName ?? $article->category->englishName }}">
    <div class="table-left wow fadeInUp" data-wow-delay="0.4s"
         style="overflow: auto;  @if ($membership_details == null && $public == "NO") background-color:#ccc; @endif">
        <div class="pricing-details">
            <div style="display: block">
                <form action="/{{ $videoView ?? $articleView }}/{{ $video->id ?? $article->id}}" method="post">
                    @method('DELETE')

                    <div class="col-md-4">
                        @if(isset($video))
                            @if($video->type == 'vimeo')
                                <iframe width="100%" height="250px" frameborder="0" allowfullscreen
                                        style="padding-bottom: 10px; @if ($membership_details == null && $public == "NO") pointer-events: none @endif"
                                        src="https://player.vimeo.com/video/{{ $video->videoId }}"></iframe>
                            @else
                                <iframe width="100%" height="250px" frameborder="0" allowfullscreen
                                        style="padding-bottom: 10px; display: block; @if ($membership_details == null && $public == "NO") pointer-events: none @endif"
                                        src="https://www.youtube.com/embed/{{ $video->videoId }}"></iframe>
                            @endif
                        @elseif(isset($article))
                            <div class="articleImg">
                                @if($article->image != null)
                                    <img class="show-img wow fadeIn" src="{{ asset('storage/'.$article->image) }}"
                                         style="padding-bottom: 10px"/>
                                @endif
                            </div>
                        @endif
                    </div>
                    <br>
                    <div class="col-md-8">
                        <div class="row">
                            @guest
                                @if ($public == "NO")
                                    <span class="editDeleteIcons">
                                        <a href="#test-popup-1" class="open-popup-link" style="color: #ee2724; font-size: 60px">
                                            <i class="ion-ios-locked"></i>
                                        </a>
                                    </span>
                                @endif
                                @if ($membership_details == null && $public == "NO") <!-- Guest or Free membership -->
                                    <div id="test-popup-1" class="white-popup mfp-hide">
                                        <h3 class="exclusive"> {{ __('text.exclusiveSentence') }} </h3>
                                        <iframe class="iframe-exclusive" src="/registerExclusive" frameborder="0" allowfullscreen></iframe>
                                    </div>
                                    <span class="video-title">
                                        <a href="#test-popup-1" class="open-popup-link"
                                           style="color: #737373; font-size:20px"> {{ $video->title ?? $article->title }} </a>
                                    </span>
                                @else
                                    <span class="video-title">
                                        <a style="color: #ee2724; font-size:20px"
                                           href="/{{$videoView ?? $articleView}}/{{ $video->id ?? $article->id }}_{{ str_replace(' ', '_', $video->title ?? $article->title) }}">
                                            {{ $video->title ?? $article->title }}
                                        </a>
                                    </span>
                                @endif
                            @else
                                @if (auth()->user()->type == 'admin')
                                    <span class="editDeleteIcons">
                                        <a href="/{{$videoView ?? $articleView}}/{{ $video->id ?? $article->id }}/edit" style="color: #364f87">
                                            <i class="ion-edit"></i>
                                        </a>
                                        <a href="/sendVideo/{{ $video->id ?? $article->id }}" style="color: #364f87">
                                            <i class="ion-android-send"></i>
                                        </a>
                                        <button onclick="return confirm('Are you sure you want to delete?')"
                                                style="color: #364f87; background-color: transparent; border-width: 0; font-size: 24px">
                                            <i class="ion-android-delete"></i>
                                        </button>
                                    </span>
                                    <span class="video-title">
                                        <a href="/{{$videoView ?? $articleView}}/{{ $video->id ?? $article->id }}_{{ str_replace(' ', '_', $video->title ?? $article->title) }}"
                                           style="color: #364f87; font-size:20px"> {{ $video->title ?? $article->title }} </a>
                                    </span>
                                    <!-- Signed in with Premium membership -->
                                @elseif (auth()->user()->type != 'admin' && $membership_details != null && $public == "NO")
                                    <span class="editDeleteIcons">
                                        <a title="{{ __('text.premium') }}" style="color: #009933; font-size: 60px">
                                            <i class="ion-ios-unlocked"></i>
                                        </a>
                                    </span>
                                    <span class="video-title">
                                        <a href="/{{$videoView ?? $articleView}}/{{ $video->id ?? $article->id }}_{{ str_replace(' ', '_', $video->title ?? $article->title) }}"
                                           style="color: #364f87; font-size:20px"> {{ $video->title ?? $article->title }} </a>
                                    </span>
                                    <!-- Signed in with free membership -->
                                @elseif (auth()->user()->type != 'admin' && $membership_details == null && $public == "NO")
                                    <span class="editDeleteIcons">
                                        <a href="#test-popup-2" class="open-popup-link" style="color: #ee2724; font-size: 60px">
                                            <i class="ion-ios-locked"></i>
                                        </a>
                                    </span>
                                    <div id="test-popup-2" class="white-popup mfp-hide">
                                        <h3 class="exclusive"> {{ __('text.exclusiveSentence') }} </h3>
                                        <iframe class="iframe-exclusive" src="/registerUpgrade" frameborder="0" allowfullscreen></iframe>
                                    </div>
                                    <span class="video-title">
                                        <a href="#test-popup-2" class="open-popup-link"
                                           style="color: #737373; font-size:20px"> {{ $video->title ?? $article->title }} </a>
                                    </span>
                                @endif
                            @endguest
                        </div>
                        <h5 class="articleCategory" @if ($membership_details == null && $public == "NO") style="color: #737373" @endif>
                            @if($data['lang'] == 'en')
                                Category:
                                <a @if ($membership_details == null && $public == "NO") style="color: #737373" @endif class="articleCategory"
                                   href="https://www.inspower-fitnesscoaching.com/videos?category_id={{ $video->category->id ?? $article->category->id }}">
                                    {{ $video->category->englishName ?? $article->category->englishName }}
                                </a>
                            @else
                                الفئة:
                                <a @if ($membership_details == null && $public == "NO") style="color: #737373" @endif class="articleCategory"
                                   href="https://www.inspower-fitnesscoaching.com/videos?category_id={{ $video->category->id ?? $article->category->id }}">
                                    {{ $video->category->arabicName ?? $article->category->arabicName }}
                                </a>
                            @endif
                        </h5>
                        @if(isset($check) && $check)
                            <h5 class="articleWriter"
                                style=" @if ($membership_details == null && $public == "NO") color: #737373 @else color: #364f87 @endif">
                                @if($article->writerName != null)
                                    @if($data['lang'] == 'en')
                                        Written By: {{ $article->writerName }}
                                    @else
                                        الكاتب: {{ $article->writerName }}
                                    @endif
                                @else
                                    @if($data['lang'] == 'en')
                                        Written By: {{ $article->writer->englishName }}
                                    @else
                                        الكاتب: {{ $article->writer->arabicName }}
                                    @endif
                                @endif
                                <span class="articleDate"
                                      style="@if ($membership_details == null) color: #737373 @else color: #364f87 @endif">{{ $article->date }}</span>
                            </h5>
                        @endif
                        <h2 class="articleBody"
                            @if ($membership_details == null && $public == "NO") style="color: #737373" @endif>
                            {!! nl2br($video->description ?? substr($article->body, 0, 330)." ...") !!}
                        </h2>
                    </div>
                    @csrf
                </form>
                <br>
            </div>
        </div>
    </div>
</div>