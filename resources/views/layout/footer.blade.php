<div lang="en" dir="ltr" class="footer fixed-bottom" id="footer">
    <div class="container footer-policies" >
        <div class="col-md-4">
            <p style="text-align: center"><a href="{{ url('/terms') }}" style="color: #FFF">Terms & Conditions</a></p>
        </div>
        <div class="col-md-4">
            <p style="text-align: center"><a href="{{ url('/privacy') }}" style="color: #FFF">Privacy Policy</a></p>
        </div>
        <div class="col-md-4">
            <p style="text-align: center"><a href="{{ url('/pricing') }}" style="color: #FFF">Pricing Policy</a></p>
        </div>
    </div>
    <div class="white-line" ></div>
    <div class="container">
        <p style="text-align: center">©2020 Inspower. All Rights Reserved</p>
        <p style="text-align: center">Designed and Developed by <a href="http://www.maryam-alalawi.com" style="color: #fcd403">Elle</a></p>
    </div>

</div>
