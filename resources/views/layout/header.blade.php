<style>
    @media (min-width: 1025px) {
        .navbar-nav.navbar-center {
            position: absolute;
            left: 50%;
            transform: translatex(-50%);
        }
    }
    @media (max-width: 1025px) {
        .background-language {
            background-color: rgba(178, 198, 188, 0.4);
            color: #2D322F;
        }
    }
</style>

<div class="container">
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <a href="/"><img src="{{ asset('images/logoColored.png') }}" class="navbar-brand" alt="INSPOWER" title="INSPOWER"/></a>
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false" aria-controls="navbar">
                    <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
                </button>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-center">
                    <li class="search-screen-size-small">
                        @include('layout.searchBar')
                    </li>
                    <!--<li><a href="/"> {{ __('text.home') }} </a>-->

                    @guest
                    @else
                        @if (auth()->user()->type == 'admin')
                        <li class="dropdown">
                            <a id="navbarDropdown" class="dropdown-toggle" href="#" role="button" data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ __('text.adminPanel') }} <span class="caret"></span>
                            </a>
                            <ul class="nav navbar-nav dropdown-menu" role="menu" style="text-align: center;">
                                <li><a href="{{ url('/membershipType') }}"> {{__('text.membershipTypes')}} </a></li>
                                <li><a href="{{ url('/subscribersList') }}"> {{__('text.subscribers')}} </a></li>
                                <li><a href="{{ url('/usersList') }}"> {{__('text.allUsers')}} </a></li>
                                <li class="nav-divider"></li>
                                <li class="nav-divider"></li>
                                <li><a href="/slideShow/create"> {{ __('text.addSlideShow') }} </a></li>
                                <li><a href="/slideShow"> {{ __('text.editDeleteSlideShow') }} </a></li>
                                <li class="nav-divider"></li>
                                <li class="nav-divider"></li>
                                <li><a href="/categories/create"> {{ __('text.addCategory') }} </a></li>
                                <li><a href="/categories"> {{ __('text.editDeleteCategory') }} </a></li>
                                <li class="nav-divider"></li>
                                <li class="nav-divider"></li>
                                <li><a href="/writers/create">{{ __('text.addWriter') }}</a></li>
                                <li><a href="/writers">{{ __('text.editDeleteWriter') }}</a></li>
                                <li class="nav-divider"></li>
                                <li class="nav-divider"></li>
                                <li><a href="/videos/createVimeo">{{ __('text.addVimeoTitle') }}</a></li>
                                <!--<li><a href="/videos">{{ __('text.editDeleteVideo') }}</a></li>-->
                                <li class="nav-divider"></li>
                                <li class="nav-divider"></li>
                                <li><a href="{{ url('/articles/create') }}"> {{ __('text.addArticle') }} </a></li>
                                <!--<li><a href="{{ url('/articles') }}"> {{ __('text.editDeleteArticle') }} </a></li>-->
                                <li class="nav-divider"></li>
                                <li class="nav-divider"></li>
                                <li><a href="/course/create">{{ __('text.addCourses') }}</a></li>
                                <!--<li><a href="/courses">{{ __('text.editDeleteCourses') }}</a></li>-->
                                <li class="nav-divider"></li>
                                <li class="nav-divider"></li>
                                <li><a href="/program/create">{{ __('text.addPrograms') }}</a></li>
                                <!--<li><a href="/programs">{{ __('text.editDeletePrograms') }}</a></li>-->
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"> {{ __('text.products') }} <span class="caret"></span></a>
                            <ul class="nav navbar-nav dropdown-menu" role="list" style="text-align: center;">
                                <li class="dropdown">
                                   <a href="{{ url('/courses/') }}">{{ __('text.courses') }}</a>
                                </li>
                                <li class="nav-divider"></li>
                                <li class="dropdown">
                                    <a href="{{ url('/programs/')}}">{{ __('text.programs') }}</a>
                                </li>
                            </ul>
                        </li>
                        @endif
                    @endguest

                    <li><a href="{{ url('/articles') }}"> {{ __('text.articles') }} </a></li>

                    <li><a href="/videos"> {{ __('text.videos') }} </a></li>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"> {{ __('text.about') }} <span class="caret"></span></a>
                        <ul class="nav navbar-nav dropdown-menu" role="menu" style="text-align: center;">
                            <li><a href="{{ url('/about') }}">{{ __('text.aboutUs') }}</a></li>
                            <li class="nav-divider"></li>
                            <li class="nav-divider"></li>
                            <li><a href="{{ url('/team') }}">{{ __('text.ourTeam') }}</a></li>
                            <li class="nav-divider"></li>
                            <li class="nav-divider"></li>
                            <li><a href="{{ url('/services') }}">{{ __('text.services') }}</a></li>
                            <li class="nav-divider"></li>
                            <li class="nav-divider"></li>
                            <li><a href="{{ url('/contact') }}">{{ __('text.contact') }}</a></li>
                        </ul>
                    </li>

                    @guest()
                        <li><a href="{{ url('/register') }}" style="color: #ee2724"> {{ __('text.join') }} </a></li>
                    @endguest
                </ul>

                <div class="form-inline my-2 my-lg-0 search-screen-size-big @if((App::isLocale('ar'))) navbar-left @endif @if((App::isLocale('en'))) navbar-right @endif">
                    @include('layout.searchBar')
                </div>

            </div>
        </div>
    </nav>
    <!-- /.navbar-collapse -->
</div>
