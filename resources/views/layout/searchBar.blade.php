<div class="search-bar text-center">

    <form action="/search" method="get" role="search" class="">
        {{ csrf_field() }}
        <div>
            <input type="text" class="mail" name="q"
                   placeholder="{{ __('text.search') }}">

            @guest
                <button title="{{ __("text.search") }}" type="submit" class="mail" style="width: 12%; font-size: 20px;">
                    <i class="ion-search"></i>
                </button>

                <button title="{{ __("text.login") }}" class="mail" style="width: 12%; color: #ee2724" formaction="{{ route('login') }}">
                    <i class="ion-person"></i>
                </button>

                @if((App::isLocale('ar')))
                    <button title="{{ __("text.english") }}" class="mail" style="width: 12%;" formaction="/setlocale/en">
                        <i class="ion-ios-world-outline"></i>
                    </button>
                @endif
                @if((App::isLocale('en')))
                    <button title="{{ __("text.arabic") }}" class="mail" style="width: 12%;" formaction="/setlocale/ar">
                        <i class="ion-ios-world-outline"></i>
                    </button>
                @endif
            @else
                <?php
                    $cartData = \App\cart::where('user_id', auth()->user()->id)->get();
                    $count = count($cartData);
                ?>
                <button title="{{ __("text.search") }}" type="submit" class="mail" style="width: 10%; font-size: 20px;">
                    <i class="ion-search"></i>
                </button>

               <button title="{{ __("text.account") }}" class="mail" style="width: 10%;" formaction="{{ url('/profile/'.auth()->user()->id) }}">
                    <i class="ion-person" ></i>
                </button>

{{--                <button title="{{ __("text.cartTitle") }}" class="mail notification" style="width: 10%; font-size: 20px;" formaction="{{ url('/viewCart')}}">--}}
{{--                    <i class="ion-android-cart"></i>--}}
{{--                    @if($count > 0)--}}
{{--                        <span class="badge">{{ $count }}</span>--}}
{{--                    @endif--}}
{{--                </button>--}}

                <button title="{{ __("text.logout") }}" class="mail" style="width: 10%; font-size: 20px" formaction="{{ route('logout') }}" formmethod="post">
                    <i class="ion-log-out" ></i>
                </button>

                <!--<div class="dropdown">
                    <button class="btn" formaction="{{ url('/profile/'.auth()->user()->id) }}" style="width: 10%; font-size: 20px">
                        <i class="ion-android-settings"></i>
                    </button>
                    <div class="dropdown-content">
                        <a href="{{ url('/profile/'.auth()->user()->id) }}"><i class="ion-person" ></i>{{ __("text.profile") }}</a>
                        <a href="{{ route('logout') }}"><i class="ion-log-out" ></i>{{ __("text.logout") }}</a>
                    </div>
                </div> -->

                @if((App::isLocale('ar')))
                    <button title="{{ __("text.english") }}" class="mail" style="width: 10%;" formaction="/setlocale/en">
                        <i class="ion-ios-world-outline"></i>
                    </button>
                @endif
                @if((App::isLocale('en')))
                    <button title="{{ __("text.arabic") }}" class="mail" style="width: 10%;" formaction="/setlocale/ar">
                        <i class="ion-ios-world-outline"></i>
                    </button>
                @endif
            @endguest
        </div>
    </form>
</div>
