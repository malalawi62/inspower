<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@extends('includes.head')
<body>
@extends('layout.header')
<div id="app" class="wrapper" style="padding-top: 50px">
    <div class="split-features" style="background-color: #FFF">
        <div class="split-content">
            @yield('content')
        </div>
    </div>
</div>
@extends('includes.footerScripts')
</body>
</html>
