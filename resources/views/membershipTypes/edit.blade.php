<html lang="{{ __('text.lang') }}" dir="{{ __('text.dir') }}">

@include('includes.head')

<body>

@include('layout.header')
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app form" id="main">

        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe" style="height: 90%">
            <div class="container">
                <div class="cta-inner">
                    <div class="row">
                        <h1 style="text-align: center; float: none"> {{ __('text.editMembershipType') }} </h1>
                    </div>
                    <div class="form wow fadeInUp" data-wow-delay="0.3s">
                        <form class="subscribe-form center-form wow zoomIn" action="/membershipType/{{ $membershipType->id }}" method="post">
                            @method('PATCH')

                            <input class="mail" disabled type="text" name="type" autocomplete="off" id="type" value="{{ $membershipType->type }}" style="margin-bottom: 10px">

                            <input dir="rtl" disabled class="mail" type="text" name="typeAr" autocomplete="off" id="typeAr" value="{{ $membershipType->typeAr }}" style="margin-bottom: 10px">

                            @error('fees') <h5 style="color: red">{{ $message }}</h5> @enderror
                            <input class="mail" type="text" name="fees" autocomplete="off" id="fees" value="{{ $membershipType->fees }}" style="margin-bottom: 10px">

                            @csrf
                            <div>
                                <button class="submit-button" style="width: 80%; margin-top: 30px; margin-bottom: 15px">
                                    {{ __('text.editMembershipType') }}
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        @include('layout.footer')
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
@include('includes.footerScripts')
</body>
</html>
