<html lang="{{ __('text.lang') }}" dir="{{ __('text.dir') }}">

@include('includes.head')

<body>

@include('layout.header')
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app form" id="main">
        <div class="pricing-section text-center" style="height: 90%">
            <div class="container">
                <div class="col-md-12 col-sm-12 nopadding">
                    <div class="pricing-intro banner-padding">
                        <h1 class="wow fadeInUp" data-wow-delay="0s"> {{ __('text.allMembershipTypesTitle') }} </h1>
                    </div>
                    <div class="col-sm-12">
                        <div class="table-left wow fadeInUp" data-wow-delay="0.4s">
                            <div class="pricing-details">
                                <div>
                                    @forelse($membershipTypes as $membershipType)
                                        <form action="/membershipType/{{ $membershipType->id }}" method="post">
                                            <div class="row">
                                                <span class="article-title">
                                                    <a style="color: #ee2724;">
                                                        @if($lang == 'en')
                                                            {{ $membershipType->type}}
                                                        @else
                                                            {{ $membershipType->typeAr }}
                                                        @endif
                                                    </a>
                                                </span>
                                                <span class="editDeleteIcons">
                                                    <a href="/membershipType/{{ $membershipType->id }}/edit" style="color: #364f87">
                                                        <i class="ion-edit"></i>
                                                    </a>
                                                    @if($membershipType->status == 'active')
                                                        <a href="/membershipType/{{ $membershipType->id }}" onclick="return confirm('Are you sure you want to INACTIVE this type of memberships?')" style="color: #364f87">
                                                            <i class="fas fa-toggle-on"></i>
                                                        </a>
                                                    @else
                                                        <a href="/membershipTypeActive/{{ $membershipType->id }}" style="color: #364f87">
                                                            <i class="fas fa-toggle-off"></i>
                                                        </a>
                                                    @endif
                                                </span>
                                            </div>
                                            @csrf
                                        </form>
                                    @empty
                                        <h2>No Membership Types Found</h2>
                                    @endforelse
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        @include('layout.footer')
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
@include('includes.footerScripts')
</body>
</html>
