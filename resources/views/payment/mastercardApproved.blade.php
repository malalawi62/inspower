<html lang="{{ __('text.lang') }}" dir="{{ __('text.dir') }}">

@include('includes.head')
<body>

@include('layout.header')
<div class="wrapper" style="background-color: #2D322F">
    <!-- Main Section-->
    <div class="main app form" id="main">
        <div class="pricing-section text-center" style="min-height: 100%">
            <div class="container">
                <div class="col-md-8 col-md-offset-2 col-sm-12 nopadding text-center" style="float: left">
                    <div class="pricing-intro">
                        <h1 class="wow fadeInUp" data-wow-delay="0s"> {{ __('text.invoice') }} - #{{ $order_id }} </h1>
                        <h1 class="wow fadeInUp" data-wow-delay="0s" style="font-size:16; {{ __('text.floating') }}; {{ __('text.textAlign') }}; padding-top: 10px"> {{ $date }} </h1>
                    </div>
                    

                    @if($courses != null || $programs != null)
                        <div class="table-left wow fadeInUp" data-wow-delay="0.4s" style="overflow: auto; background-color: rgba(255, 255, 255, 0.8)">
                            <div class="pricing-details">
                                <div style="display: block">
                                    <div class="col-md-12" style="margin-bottom: 15px">
                                            <h1 class="wow fadeInUp" data-wow-delay="0s" style="font-weight: 800; font-size: 24px; color: black"> {{ __('text.detailsTitle') }} </h1>
                                        @if($courses != null)
                                            <div class="row">
                                                <span class="video-title">
                                                    <h1 style="color: black; font-size:20px; font-weight: 800"> {{__('text.courses')}}: </h1>
                                                </span>
                                            </div>

                                            @foreach($courses as $course)
                                                <div class="row">
                                                    <div class="col-md-8">
                                                    <span class="video-title">
                                                        <h1 style="color: black; font-size:20px">
                                                            {{ $course->title }}
                                                        </h1>
                                                    </span>
                                                    </div>
                                                </div>
                                            @endforeach
                                            <hr style="width: 100%; border-width:0; border-color: rgba(0, 0, 0, 0.2)">
                                        @endif

                                        @if($programs != null)
                                                <div class="row">
                                                <span class="video-title">
                                                    <h1 style="color: black; font-size:20px; font-weight: 800"> {{__('text.programs')}}: </h1>
                                                </span>
                                                </div>
                                            @foreach($programs as $program)
                                                <div class="row">
                                                    <div class="col-md-8">
                                                    <span class="video-title">
                                                        <h1 style="color: black; font-size:20px">
                                                        {{ $program->title }}
                                                        </h1>
                                                    </span>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif

                                    </div>
                                </div>
                                <hr style="width: 100%; border-width:1px; border-color: black">

                                <div class="row">
                                    <div class="col-md-8">
                                            <span class="video-title">
                                                <h1 style="color: red; font-size:20px; font-weight: 800;"> {{__('text.cartTotal')}}</h1>
                                            </span>
                                    </div>

                                    <div class="col-md-3">
                                            <span class="video-title">
                                                <h1 style="color: red; font-size:20px; font-weight: 800;"> {{ $amount }} {{ __('text.BD') }} </h1>
                                            </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <!-- Footer Section -->
        @include('layout.footer')
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
@include('includes.footerScripts')
</body>
</html>

