<?php
    // init the resource
    $ch = curl_init();
    $merchant_id = "TEST100067231";
    $PWD = "8d4ec52a5782a2e4fbbf32625236e873";
    $currency_code = "BHD";
    $merchant_name = "INSPOWER FITNESS COACHING";
    $logo = "https://inspower-fitnesscoaching.com/images/payment-logo.png";
    $lang= __('text.lang');
    $courseTitles = $_GET['courseTitles'];
    $programTitles = $_GET['programTitles'];
    $order_id = $_GET['order_id'];
    $counter = $_GET['counter'];
    
    if ($courseTitles != null && $programTitles != null)
        $description = "Courses: " . $courseTitles . " || Programs: " . $programTitles;
    else if ($courseTitles != null && $programTitles == null)
        $description = "Courses: " . $courseTitles;
    else if ($courseTitles == null && $programTitles != null)
        $description = "Programs: " . $programTitles;
        
    $post_data = array("apiOperation" => "CREATE_CHECKOUT_SESSION",
        "order" => array("id" => $order_id,"currency"=>$currency_code),
        'interaction' => array('operation' => "PURCHASE"));

    $data_string = json_encode($post_data);
    curl_setopt($ch, CURLOPT_USERPWD, "merchant.$merchant_id:$PWD");
    curl_setopt($ch, CURLOPT_URL,"https://afs.gateway.mastercard.com/api/rest/version/57/merchant/$merchant_id/session");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_POSTFIELDS,$data_string);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
    );
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);

    $content = curl_exec($ch);
    $output = json_decode($content, true);
    
    if(isset($_GET['resultIndicator']))
        dd($_GET['resultIndicator']);
    else{
        
    }
        

    //dd($order_id);

?>
<html lang="{{ __('text.lang') }}" dir="{{ __('text.dir') }}">

@include('includes.head')
<head>
    <script src="https://afs.gateway.mastercard.com/checkout/version/57/checkout.js" 
        data-error="errorCallback" 
        data-cancel="cancelCallback"
        data-beforeRedirect="Checkout.saveFormFields"
        data-afterRedirect="Checkout.restoreFormFields"
        data-timeout="timeoutCallback"
        data-complete="completeCallback">
    </script>

    <script type="text/javascript">
        function errorCallback(error) {
            console.log(JSON.stringify(error));
        }
                
        function cancelCallback() {
            confirm('Are you sure you want to cancel?');
            console.log('Payment cancelled');
        }
                
        timeoutCallback = "https://www.test.inspower-fitnesscoaching.com/viewCart";
        completeCallback = "https://www.test.inspower-fitnesscoaching.com/mastercardApproved?order_id=<?php echo $order_id; ?>&description=<?php echo $description; ?>"
                

        Checkout.configure({
            merchant: '<?php echo $merchant_id; ?>',
            session: {
                id: "<?php echo $output['session']['id']; ?>"
            },
            order: {
                amount: '<?php echo $counter; ?>',
                currency: 'BHD',
                description: '<?php echo $description; ?>',
                id: "<?php echo $order_id; ?>",
            },
            interaction: {
                //returnUrl: "https://www.test.inspower-fitnesscoaching.com/paymentState?order_id=<?php echo $order_id; ?>&description=<?php echo $description; ?>&SI=<?php echo $output['successIndicator']; ?>",
                merchant: {
                    name: '<?php echo $merchant_name; ?>',
                    logo : '<?php echo $logo; ?>',
                    url: "https://inspower-fitnesscoaching.com",
                    email: "inspowerpt@gmail.com",
                    address: {
                        line1: "Building 1226, Block 351, Road 5124",
                        line2: "Manama, Kingdom of Bahrain"                    
                    }
                },
                displayControl: {
                    billingAddress  : 'HIDE',
                    orderSummary    : 'SHOW'
                }
            }
        });
        
        window.onload = Checkout.showPaymentPage();

    </script>
</head>
@if(!isset($_GET['']))
    <body onload="Checkout.showPaymentPage();">
    
    </body>
@endif
</html>
