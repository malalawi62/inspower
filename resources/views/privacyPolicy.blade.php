<html lang="{{ __('text.lang') }}" dir="{{ __('text.dir') }}">

@include('includes.head')

<body>

@include('layout.header')
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app form" id="main">
        <div class="pricing-section text-center" style="min-height: 100%">
            <div class="container">
                <div style="width: 100%">
                    <div class="banner-padding">
                        <div class="table-left wow fadeInUp" data-wow-delay="0.4s">
                            <div class="pricing-details">
                                <div style="display: inline-block">
                                    <span class="span" style="color: #ee2724;">PRIVACY</span>
                                    <div class="col-sm-12">
                                        <h2 dir="ltr" style="text-align:left">{!! nl2br('The confidentiality of the personal information you provide is of great importance to us. We also believe it is important to inform you on how we process your personal information. We thereby encourage you to carefully read our privacy policy (‘’Privacy Policy’’).') !!}
                                        </h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="banner-padding">
                        <div class="table-left wow fadeInUp" data-wow-delay="0.4s">
                            <div class="pricing-details">
                                <div style="display: inline-block">
                                    <span class="span" style="color: #ee2724;">PRIVACY POLICY</span>
                                    <div class="col-sm-12">
                                        <h2 dir="ltr" style="text-align:left">{!! nl2br('The personal information is collected privately from users while registering in the website and are only used to get generalized statistics so we can improve our website and it is done by saving users data in a secure database. Users data can only be accessed by the website OWNER.

                                            Likewise, the payment process, we are not going to save any card or bank account information we will just keep track of the transaction number so we can refer to it whenever something went wrong.') !!}
                                        </h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="banner-padding">
                        <div class="table-left wow fadeInUp" data-wow-delay="0.4s">
                            <div class="pricing-details">
                                <div style="display: inline-block">
                                    <span class="span" style="color: #ee2724;">COPYRIGHT</span>
                                    <div class="col-sm-12">
                                        <h2 dir="ltr" style="text-align:left">{!! nl2br('All videos, audio clips, texts, graphics, photographs, trademarks, logos, music, sounds, visual interfaces, artwork, programs, computer codes and any and all other intellectual properties (the “IP”) are owned, controlled or licensed by or to INSPOWER Fitness Coaching and are protected by copyrights and law. When accessing and browsing on the Website, you agree not to copy, distribute, modify, transmit, duplicate, print, reuse, re-post, publicly display, encode, translate, in any way, or infringe, in any way, the rights related to the IP.

                                            Any unauthorized use, modification, or copying of the content of this Website is strictly prohibited.
                                            ') !!}
                                        </h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Footer Section -->
        @include('layout.footer')
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
@include('includes.footerScripts')
</body>
</html>
