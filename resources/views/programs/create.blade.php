<!DOCTYPE html>
<html lang="{{ __('text.lang') }}" dir="{{ __('text.dir') }}">

@include('includes.head')

<body>

@include('layout.header')
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app" id="main">
        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe">
            <div class="container">
                <div class="cta-inner">
                    <div class="row">
                        <h1 style="text-align: center; float: none"> {{ __('text.addProgramTitle') }} </h1>
                    </div>

                    <div class="wow fadeInUp" data-wow-delay="0.3s">
                        <form class="forms-form center-form wow zoomIn" action="/program" method="post"
                              enctype="multipart/form-data">

                            @error('title') <h5 style="color: red;">{{ $message }}</h5> @enderror
                            <input class="mail" type="text" name="title"
                                   placeholder="{{ __('text.inputProgramTitle') }}"
                                   autocomplete="title" value="{{ old('title') }}">

                            @error('overview') <p style="color: red">{{ $message }}</p> @enderror
                            <p>{{ __('text.inputProgramOverview') }}</p>
                            <textarea class="mail" rows="20" name="overview" style="height: auto;" value="{{ old('overview') }}"></textarea>

                            @error('image') <h5 style="color: red">{{ $message }}</h5> @enderror
                            <p>{{ __('text.inputProgramImage') }}</p>
                            <input class="mail" type="file" name="image" id="image"
                                   style="display: inline-block;padding-top: 10px">

                            @error('price') <h5 style="color: red;">{{ $message }}</h5> @enderror
                            <input class="mail" type="number" name="price"
                                   placeholder="{{ __('text.inputProgramPrice') }}"
                                   value="{{ old('price') }}">

                            <input class="mail" type="text" name="videoUrl"
                                   placeholder="{{ __('text.inputProgramVideo') }}"
                                   autocomplete="off" value="{{ old('videoUrl') }}">

                            <p>{{ __('text.inputProgramDescription') }}</p>
                            <textarea class="mail" rows="20" name="description" style="height: auto;" value="{{ old('description') }}"></textarea>


                            @error('pdf') <h5 style="color: red">{{ $message }}</h5> @enderror
                            <label class="mail" style="border: 1.5px solid #babdc2; padding-top: 10px">
                               {{__('text.inputProgramPdf')}}
                                <input type="file" name="pdf" id="bodyPdf"
                                       style="display: inline-block"/>
                            </label>

                            @if($lang == 'en')
                                <input type="hidden" name="language" id="language" value="Eng">
                            @else
                                <input type="hidden" name="language" id="language" value="Ar">
                            @endif


                            <hr style="width: 100%; border-width:1px">

                            <div class="phases">
                                <div class="phase phase_wrapper_1">
                                    <div class="row">
                                        <h1 style="text-align: center; float: none; padding-top: 0px" id="phase_title_1"> {{__('text.phaseNew')}}
                                            <input class="add_phase_button" type="button" onclick="addPhase()" value="+" style="color: #ee2724; background: transparent; border-color: transparent; border-width: 0">
                                        </h1>
                                    </div>

                                    @error('p_title') <h5 style="color: red;">{{ $message }}</h5> @enderror
                                    <input class="mail" type="text" name="p_title[]"
                                           placeholder="{{ __('text.phaseTitle') }}"
                                           autocomplete="title" value="{{ old('p_title') }}">

                                    <p>{{ __('text.phaseOverview') }}</p>
                                    <textarea class="mail" rows="10" name="p_overview[]" style="height: auto;" value="{{ old('p_overview') }}"></textarea>

                                    <p>{{ __('text.phaseDescription') }}</p>
                                    <textarea class="mail" rows="10" name="p_description[]" style="height: auto;" value="{{ old('p_description') }}"></textarea>

                                    <input class="mail" type="text" name="p_videoUrl[]"
                                           placeholder="{{ __('text.phaseVideoURL') }}"
                                           autocomplete="off" value="{{ old('p_videoUrl') }}">

                                    <label class="mail" style="border: 1.5px solid #babdc2; padding-top: 10px">
                                        {{__('text.phasePdf')}}
                                        <input type="file" name="phasePdf[]"
                                               style="display:inline-block">
                                    </label>

                                    <hr style="width: 70%; border-width:1px">

                                    <div class="videos">
                                        <div class="video_wrapper">
                                            <input class="mail" type="text" name="ps_videoUrl[1][]"
                                                   placeholder="{{ __('text.sectionVideoURL') }}"
                                                   autocomplete="off" value="{{ old('ps_videoUrl') }}">

                                            <p>{{ __('text.phaseBriefDescription') }}</p>
                                            <textarea class="mail" rows="10" name="ps_description[1][]" style="height: auto;" value="{{ old('p_description') }}"></textarea>
                                            <div class="row">
                                                <input class="add_video_button addBtn" type="button" onclick="addVideo(1)" value="{{ __('text.addNewSection') }} +">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            @csrf
                            <div>
                                <button class="submit-button" style="width: 60%;">
                                    {{__('text.addProgramBtn')}}
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        @include('layout.footer')
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
@include('includes.footerScripts')
</body>
</html>
