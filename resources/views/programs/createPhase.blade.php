<!DOCTYPE html>
<html lang="{{ __('text.lang') }}" dir="{{ __('text.dir') }}">

@include('includes.head')

<body>
<script>
    var msg = '{{Session::get('alert')}}';
    var exist = '{{Session::has('alert')}}';
    if (exist) {
        alert(msg);
    }
</script>

@include('layout.header')
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app" id="main">
        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe">
            <div class="container">
                <div class="cta-inner">
                    <div class="row">
                        <h1 style="text-align: center; float: none"> {{ __('text.addPhaseBtn') }} </h1>
                    </div>

                    <div class="wow fadeInUp" data-wow-delay="0.3s">
                        <form class="forms-form center-form wow zoomIn" action="/phase" method="post"
                              enctype="multipart/form-data">
                            <input type="hidden" name="program_id" id="program_id" value="{{ $program_id }}">
                            <div class="phases">
                                <div class="phase phase_wrapper_1">

                                    <input class="mail" type="text" name="title"
                                           placeholder="{{ __('text.phaseTitle') }}"
                                           autocomplete="title" value="{{ old('title') }}">

                                    <p>{{ __('text.phaseOverview') }}</p>
                                    <textarea class="mail" rows="10" name="overview" style="height: auto;" value="{{ old('overview') }}"></textarea>

                                    <p>{{ __('text.phaseDescription') }}</p>
                                    <textarea class="mail" rows="10" name="description" style="height: auto;" value="{{ old('description') }}"></textarea>

                                    <input class="mail" type="text" name="videoUrl"
                                           placeholder="{{ __('text.phaseVideoURL') }}"
                                           autocomplete="off" value="{{ old('videoUrl') }}">

                                    @error('pdf') <h5 style="color: red">{{ $message }}</h5> @enderror
                                    <label class="mail" style="border: 1.5px solid #babdc2; padding-top: 10px">
                                        {{__('text.phasePdf')}}
                                        <input type="file" name="p_Pdf" id="pdf"
                                               style="display: inline-block"/>
                                    </label>

                                    <hr style="width: 70%; border-width:1px">

                                    <div class="videos">
                                        <div class="video_wrapper video_1">
                                            <input class="mail" type="text" name="section_videoUrl[]"
                                                   placeholder="{{ __('text.sectionVideoURL') }}"
                                                   autocomplete="off" value="{{ old('section_videoUrl') }}">

                                            <p>{{ __('text.phaseBriefDescription') }}</p>
                                            <textarea class="mail" rows="10" name="section_description[]" style="height: auto;" value="{{ old('section_description') }}"></textarea>
                                            <div class="row">
                                                <input class="add_video_button addBtn" type="button" onclick="addVideoPhase()" value="{{ __('text.addNewSection') }} +">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            @csrf
                            <div>
                                <div>
                                    <button class="submit-button" style="width: 60%;">
                                        {{__('text.addPhaseBtn')}}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        @include('layout.footer')
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
@include('includes.footerScripts')
</body>
</html>
