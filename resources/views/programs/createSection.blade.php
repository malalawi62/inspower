<!DOCTYPE html>
<html lang="{{ __('text.lang') }}" dir="{{ __('text.dir') }}">

@include('includes.head')

<body>
<script>
    var msg = '{{Session::get('alert')}}';
    var exist = '{{Session::has('alert')}}';
    if (exist) {
        alert(msg);
    }
</script>
@include('layout.header')
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app" id="main">
        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe">
            <div class="container">
                <div class="cta-inner">
                    <div class="row">
                        <h1 style="text-align: center; float: none"> {{ __('text.addNewSection') }} </h1>
                    </div>

                    <div class="wow fadeInUp" data-wow-delay="0.3s">
                        <form class="forms-form center-form wow zoomIn" action="/section" method="post"
                              enctype="multipart/form-data">
                            <input type="hidden" name="phase_id" id="phase_id" value="{{ $phase_id }}">
                            <div class="videos">
                                <div class="video_wrapper">
                                    <input class="mail" type="text" name="videoUrl"
                                           placeholder="{{ __('text.sectionVideoURL') }}"
                                           autocomplete="off" value="{{ old('videoUrl') }}">

                                    <p>{{ __('text.phaseBriefDescription') }}</p>
                                    <textarea class="mail" rows="10" name="description" style="height: auto;" value="{{ old('description') }}"></textarea>
                                </div>
                            </div>
                            @csrf
                            <div>
                                <div>
                                    <button class="submit-button" style="width: 60%;">
                                        {{__('text.addNewSection')}}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        @include('layout.footer')
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
@include('includes.footerScripts')
</body>
</html>
