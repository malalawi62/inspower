<!DOCTYPE html>
<html lang="{{ __('text.lang') }}" dir="{{ __('text.dir') }}">

@include('includes.head')

<body>

<script>
    var msg = '{{Session::get('alert')}}';
    var exist = '{{Session::has('alert')}}';
    if (exist) {
        alert(msg);
    }
</script>

@include('layout.header')
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app" id="main">
        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe">
            <div class="container">
                <div class="cta-inner">
                    <div class="row">
                        <h1 style="text-align: center; float: none"> {{ __('text.editProgramTitle') }} </h1>
                    </div>

                    <div class="wow fadeInUp" data-wow-delay="0.3s">
                        <form class="forms-form center-form wow zoomIn" action="/program/{{ $program->id }}" method="post"
                              enctype="multipart/form-data">
                            @method('PATCH')

                            @error('title') <h5 style="color: red;">{{ $message }}</h5> @enderror
                            <input class="mail" type="text" name="title"
                                   autocomplete="title" value="{{ $program->title }}">

                            @error('overview') <p style="color: red">{{ $message }}</p> @enderror
                            <p>{{ __('text.inputProgramOverview') }}</p>
                            <textarea class="mail" rows="20" name="overview" style="height: auto;">{{ $program->overview }}</textarea>

                            @error('image') <h5 style="color: red">{{ $message }}</h5> @enderror
                            <input class="mail" type="file" name="image" id="image"
                                   style="display: inline-block;padding-top: 10px">

                            @error('price') <h5 style="color: red;">{{ $message }}</h5> @enderror
                            <input class="mail" type="number" name="price"
                                   value="{{ $program->price }}">

                            <input class="mail" type="text" name="videoUrl"
                                   placeholder="{{ __('text.inputCourseVideo') }}"
                                   autocomplete="off" value="@if($program->videoUrl != null) https://vimeo.com/{{ $program->videoUrl }} @endif">

                            <p>{{ __('text.inputProgramDescription') }}</p>
                            <textarea class="mail" rows="20" name="description" style="height: auto;">{{ $program->description }}</textarea>

                            @error('pdf') <h5 style="color: red">{{ $message }}</h5> @enderror
                            <label class="mail" style="border: 1.5px solid #babdc2; padding-top: 10px">
                                {{__('text.inputProgramPdf')}}
                                <input type="file" name="pdf" id="Pdf"
                                       style="display: inline-block"/>
                            </label>

                            @csrf
                            <div>
                                <button class="submit-button" style="width: 60%;">
                                    {{__('text.editProgramBtn')}}
                                </button>
                            </div>
                        </form>

                        <div class="line"></div>

                        <div class="modules">
                            <?php $counter = 0; ?>
                            @foreach ($phases as $phase)
                                <?php $counter ++; ?>
                                <div class="row" style="padding-bottom: 30px">
                                    <div class="row">
                                        <form action="/phase/{{ $phase->id }}/delete" method="post">
                                            <h2 class="h2" style="text-transform: uppercase"> {{ __('text.phaseTitle2') }}: {{ $phase->title }}
                                                <a href="/phase/{{ $phase->id }}/edit" style="color: #364f87">
                                                    <i class="ion-edit"></i>
                                                </a>
                                                <button onclick="return confirm('Are you sure you want to DELETE this PHASE?')"
                                                        style="color: #364f87; background-color: transparent; border-width: 0; font-size: 24px">
                                                    <i class="ion-android-delete"></i>
                                                </button>
                                            </h2>
                                            @csrf
                                        </form>
                                    </div>

                                    <div class="row">
                                        <h2 class="h2" style="{{ __('text.textAlign') }}"> {{ __('text.phaseOverviewShow') }}: </h2>
                                        <p class="article-body">
                                            {!! nl2br($phase->overview) !!}
                                        </p>
                                    </div>

                                    <div class="row">
                                        <h2 class="h2" style="{{ __('text.textAlign') }}"> {{ __('text.phaseDescriptionShow') }}: </h2>
                                        <p class="article-body">
                                            <a href="https://vimeo.com/{{ $phase->videoUrl }}"> https://vimeo.com/{{ $phase->videoUrl }} </a>
                                        </p>
                                    </div>

                                    <div class="row">
                                        <p class="article-body">
                                            {!! nl2br($phase->description) !!}
                                        </p>
                                    </div>

                                    <div class="row">
                                        <h2 class="h2" style="{{ __('text.textAlign') }}"> {{ __('text.phaseSections') }}: </h2>
                                    </div>
                                    @foreach($sections as $section)
                                        @if($section->program_phase_id == $phase->id)
                                            <div class="row"  style="width: 80%">
                                                <p class="article-body"> {{ $section->description }} </p>
                                            </div>

                                            <div class="row">
                                                <p class="article-body"> <a href="https://vimeo.com/{{ $section->videoUrl }}"> https://vimeo.com/{{ $section->videoUrl }} </a> </p>
                                            </div>

                                            <div class="row">
                                                <div class="line"></div>
                                            </div>

                                        @endif
                                    @endforeach
                                </div>
                            @endforeach

                        <!-- this section is to add a new phase -->
                                <form action="/phase/create" method="get">
                                    <input type="hidden" name="program_id" value="{{ $program->id }}">
                                    <div>
                                        <button class="submit-button" style="width: 60%;">
                                            {{__('text.addPhaseBtn')}}
                                        </button>
                                    </div>
                                </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        @include('layout.footer')
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
@include('includes.footerScripts')
</body>
</html>
