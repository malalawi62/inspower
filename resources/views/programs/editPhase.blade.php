<!DOCTYPE html>
<html lang="{{ __('text.lang') }}" dir="{{ __('text.dir') }}">

@include('includes.head')

<body>

<script>
    var msg = '{{Session::get('alert')}}';
    var exist = '{{Session::has('alert')}}';
    if (exist) {
        alert(msg);
    }
</script>

@include('layout.header')
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app" id="main">
        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe">
            <div class="container">
                <div class="cta-inner">
                    <div class="row">
                        <h1 style="text-align: center; float: none"> {{ __('text.editPhaseTitle') }} </h1>
                    </div>

                    <div class="wow fadeInUp" data-wow-delay="0.3s">
                        <form class="forms-form center-form wow zoomIn" action="/phase/{{ $phases->id }}" method="post"
                              enctype="multipart/form-data">
                            @method('PATCH')

                            @error('title') <h5 style="color: red;">{{ $message }}</h5> @enderror
                            <input class="mail" type="text" name="title"
                                   autocomplete="title" value="{{ $phases->title }}">

                            @error('overview') <p style="color: red">{{ $message }}</p> @enderror
                            <p>{{ __('text.phaseOverview') }}</p>
                            <textarea class="mail" rows="20" name="overview" style="height: auto;">{{ $phases->overview }}</textarea>

                            <input class="mail" type="text" name="videoUrl"
                                   placeholder="{{ __('text.phaseVideoURL') }}"
                                   autocomplete="off" value="@if($phases->videoUrl != null) https://vimeo.com/{{ $phases->videoUrl }} @endif">

                            <p>{{ __('text.phaseDescription') }}</p>
                            <textarea class="mail" rows="20" name="description" style="height: auto;">{{ $phases->description }}</textarea>

                            @error('pdf') <h5 style="color: red">{{ $message }}</h5> @enderror
                            <label class="mail" style="border: 1.5px solid #babdc2; padding-top: 10px">
                                {{__('text.phasePdf')}}
                                <input type="file" name="pdf"
                                       style="display: inline-block"/>
                            </label>

                            @csrf
                            <div>
                                <button class="submit-button" style="width: 60%;">
                                    {{__('text.editPhaseBtn')}}
                                </button>
                            </div>
                        </form>

                        <hr style="width: 100%; border-width:1px">

                        <div class="modules">
                            <?php $counter = 0; ?>
                            @foreach ($sections as $section)
                                <?php $counter ++; ?>
                                <div class="col-md-12" style="padding-bottom: 30px">
                                    <div class="row">
                                        <form action="/section/{{ $section->id }}/delete" method="post">
                                            <h2 class="h2" style="text-transform: uppercase"> {{ __('text.sectionTitle') }} : {{ $counter }}
                                                <a href="/section/{{ $section->id }}/edit" style="color: #364f87">
                                                    <i class="ion-edit"></i>
                                                </a>
                                                <button onclick="return confirm('Are you sure you want to DELETE this SECTION?')"
                                                        style="color: #364f87; background-color: transparent; border-width: 0; font-size: 24px">
                                                    <i class="ion-android-delete"></i>
                                                </button>
                                            </h2>
                                            @csrf
                                        </form>
                                    </div>

                                    <div class="row">
                                        <p class="article-body"> <a href="https://vimeo.com/{{ $section->videoUrl }}"> https://vimeo.com/{{ $section->videoUrl }} </a> </p>
                                    </div>
                                    <div class="row">
                                        <p class="article-body"> {{ $section->description }} </p>
                                    </div>
                                </div>

                                <hr style="width: 100%; border-width:1px">
                        @endforeach

                                <form action="/section/create" method="get">
                                    <input type="hidden" name="phase_id" value="{{ $phases->id }}">
                                    <div>
                                        <button class="submit-button" style="width: 60%;">
                                            {{__('text.addNewSection')}}
                                        </button>
                                    </div>
                                    @csrf
                                </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        @include('layout.footer')
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
@include('includes.footerScripts')
</body>
</html>
