<!DOCTYPE html>
<html lang="{{ __('text.lang') }}" dir="{{ __('text.dir') }}">

@include('includes.head')

<body>

<script>
    var msg = '{{Session::get('alert')}}';
    var exist = '{{Session::has('alert')}}';
    if (exist) {
        alert(msg);
    }
</script>

@include('layout.header')
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app" id="main">
        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe">
            <div class="container">
                <div class="cta-inner">
                    <div class="row">
                        <h1 style="text-align: center; float: none"> {{ __('text.editSectionTitle') }} </h1>
                    </div>

                    <div class="wow fadeInUp" data-wow-delay="0.3s">
                        <form class="forms-form center-form wow zoomIn" action="/section/{{ $section2->id }}" method="post"
                              enctype="multipart/form-data">
                            @method('PATCH')

                            <input class="mail" type="text" name="videoUrl"
                                   placeholder="{{ __('text.inputSectionVideo') }}"
                                   autocomplete="off" value="https://vimeo.com/{{ $section2->videoUrl }}">

                            <p>{{ __('text.phaseBriefDescription') }}</p>
                            <textarea class="mail" rows="20" name="description" style="height: auto;">{{ $section2->description }}</textarea>

                            @csrf
                            <div>
                                <button class="submit-button" style="width: 60%;">
                                    {{__('text.editSectionBtn')}}
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        @include('layout.footer')
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
@include('includes.footerScripts')
</body>
</html>
