<html lang="{{ __('text.lang') }}" dir="{{ __('text.dir') }}">

@include('includes.head')

<body>
<script>
    var msg = '{{Session::get('alert')}}';
    var exist = '{{Session::has('alert')}}';
    if (exist) {
        alert(msg);
    }
</script>

@include('layout.header')
<div class="wrapper" style="background-color: #2D322F">
    <!-- Main Section-->
    <div class="main app form" id="main">
        <div class="pricing-section text-center" style="min-height: 100%">
            <div class="container">
                <div style="width: 100%">
                    <div class="pricing-intro banner-padding">
                        <h1 class="wow fadeInUp" data-wow-delay="0s"> {{ __('text.allProgramsTitle') }} </h1>
                        @if((App::isLocale('en')))
                            <h2>For programs in Arabic language <a href="setlocale/ar">press here</a></h2>
                        @endif
                        @if((App::isLocale('ar')))
                            <h2>للبرامج التدريبية باللغة الإنجليزية <a href="setlocale/en">اضغط هنا</a></h2>
                        @endif
                    </div>
                    @foreach($programs as $program)
                        <?php $flag = false; ?>
                            @guest
                            @else
                            @if (auth()->user()->type == 'admin')
                                <?php $flag = true; ?>
                            @elseif($orders != null)
                                @for($i=0; $i<count($orders); $i++)
                                    @if($orders[$i]->program_id == $program->id)
                                        <?php $flag = true; ?>
                                        @break
                                    @endif
                                @endfor
                            @endif
                            @endguest
                        @if ($program->status == 'inactive')
                            @guest
                            @else
                                @if (auth()->user()->type == 'admin' || $flag == true)
                                    <div class="table-left wow fadeInUp" data-wow-delay="0.4s" style="overflow: auto;">
                                        <div class="pricing-details">
                                            <div style="display: block">
                                                <div class="col-md-4">
                                                    <img class="show-img wow fadeIn"
                                                         src="{{ asset('storage/'.$program->image) }}"
                                                         style="padding-bottom: 10px"/>
                                                </div>
                                                <br>
                                                <div class="col-md-8">
                                                    <div class="row">
                                                <span class="video-title">
                                                    <a style="color: #ee2724; font-size:20px"
                                                       href="/program/{{ $program->id }}_{{ str_replace(' ', '_', $program->title) }}">
                                                        {{ $program->title }} <span style="color: #2D322F; font-weight: 700;">*{{ __('text.inactiveProgram') }}*</span>
                                                    </a>
                                                </span>
                                                        @guest
                                                        @else
                                                            @if (auth()->user()->type == 'admin')
                                                                <form action="/program/{{ $program->id }}" method="post">
                                                        <span class="editDeleteIcons">
                                                            <a href="/program/{{ $program->id }}/edit"
                                                               style="color: #364f87">
                                                                <i class="ion-edit"></i>
                                                            </a>
                                                            <a href="/sendProgram/{{ $program->id }}"
                                                               style="color: #364f87">
                                                                <i class="ion-android-send"></i>
                                                            </a>
                                                            @if($program->status == 'active')
                                                                <button
                                                                    onclick="return confirm('Are you sure you want to INACTIVE this program?')"
                                                                    style="color: #364f87; background-color: transparent; border-width: 0; font-size: 24px">
                                                                    <i class="fas fa-toggle-off"></i>
                                                                </button>
                                                            @else
                                                                <a href="/programActive/{{ $program->id }}"
                                                                   style="color: #364f87">
                                                                    <i class="fas fa-toggle-on"></i>
                                                                </a>
                                                            @endif
                                                        </span>
                                                                    @csrf
                                                                </form>
                                                            @endif
                                                        @endguest
                                                    </div>
                                                    <h2 class="articleBody" style="color: #2D322F">
                                                        {{__("text.price")}}
                                                        <span style="color: #ee2724">{{$program->price}} {{__("text.BD")}}</span>
                                                    </h2>
                                                    <h2 class="articleBody" style="color: #969696">
                                                        {!! nl2br($program->overview) !!}
                                                    </h2>
                                                    <form action="/cart" method="post">
                                                        <input type="hidden" name="program_id" value="{{ $program->id }}">
                                                        @guest
                                                        @else
                                                            <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
                                                        @endguest
                                                        <button class="submit-button"
                                                                style="{{ __('text.floating') }}; padding-left: 15px; padding-right: 15px" formaction="/program/{{ $program->id }}" formmethod="get">
                                                            {{ __('text.learnMoreBtn') }}
                                                        </button>
                                                        @if($flag == false)
                                                            <button class="cart-button" type="submit"
                                                                    style="{{ __('text.opFloating') }}; padding-left: 15px; padding-right: 15px">
                                                                {{ __('text.addToCartBtn') }}
                                                            </button>
                                                        @endif
                                                        @csrf
                                                    </form>
                                                </div>
                                                <br>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endguest
                        @else
                            <div class="table-left wow fadeInUp" data-wow-delay="0.4s" style="overflow: auto;">
                                <div class="pricing-details">
                                    <div style="display: block">
                                        <div class="col-md-4">
                                            <img class="show-img wow fadeIn"
                                                 src="{{ asset('storage/'.$program->image) }}"
                                                 style="padding-bottom: 10px"/>
                                        </div>
                                        <br>
                                        <div class="col-md-8">
                                            <div class="row">
                                                <span class="video-title">
                                                    <a style="color: #ee2724; font-size:20px; font-weight: 700"
                                                       href="/program/{{ $program->id }}_{{ str_replace(' ', '_', $program->title) }}">
                                                        {{ $program->title }}
                                                    </a>
                                                </span>
                                                @guest
                                                @else
                                                    @if (auth()->user()->type == 'admin')
                                                        <form action="/program/{{ $program->id }}" method="post">
                                                        <span class="editDeleteIcons">
                                                            <a href="/program/{{ $program->id }}/edit"
                                                               style="color: #364f87">
                                                                <i class="ion-edit"></i>
                                                            </a>
                                                            <a href="/sendProgram/{{ $program->id }}"
                                                               style="color: #364f87">
                                                                <i class="ion-android-send"></i>
                                                            </a>
                                                            <button
                                                                onclick="return confirm('Are you sure you want to INACTIVE this program?')"
                                                                style="color: #364f87; background-color: transparent; border-width: 0; font-size: 24px">
                                                                <i class="fas fa-toggle-off"></i>
                                                            </button>
                                                        </span>
                                                            @csrf
                                                        </form>
                                                    @endif
                                                @endguest
                                            </div>
                                            <h2 class="articleBody" style="color: #2D322F">
                                                {{__("text.price")}}
                                                <span style="color: #ee2724">{{$program->price}} {{__("text.BD")}}</span>
                                            </h2>
                                            <h2 class="articleBody" style="color: #969696">
                                                {!! nl2br($program->overview) !!}
                                            </h2>
                                            <form action="/cart" method="post">
                                                <input type="hidden" name="program_id" value="{{ $program->id }}">
                                                @guest
                                                @else
                                                    <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
                                                @endguest
                                                <button class="submit-button"
                                                        style="{{ __('text.floating') }}; padding-left: 15px; padding-right: 15px" formaction="/program/{{ $program->id }}" formmethod="get">
                                                    {{ __('text.learnMoreBtn') }}
                                                </button>
                                                @if($flag == false)
                                                    <button class="cart-button" type="submit"
                                                            style="{{ __('text.opFloating') }}; padding-left: 15px; padding-right: 15px">
                                                        {{ __('text.addToCartBtn') }}
                                                    </button>
                                                @endif
                                                @csrf
                                            </form>
                                        </div>
                                        <br>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
        <!-- Footer Section -->
        @include('layout.footer')
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
@include('includes.footerScripts')
</body>
</html>

