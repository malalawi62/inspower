<html lang="{{ __('text.lang') }}" dir="{{ __('text.dir') }}">

@include('includes.head')

<body>

@include('layout.header')

<?php $flag = false; ?>
@guest
@else
@if (auth()->user()->type == 'admin')
    <?php $flag = true; ?>
    @elseif($orders != null)
    @for($i=0; $i<count($orders); $i++)
        @if($orders[$i]->program_id == $program->id)
            <?php $flag = true; ?>
            @break
        @endif
    @endfor
@endif
@endguest

<div class="wrapper">
    <!-- Main Section-->
    <div class="main app form" id="main">

        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe" style="min-height: 100%">
            <div class="container">
                <div class="cta-inner">
                    <div class="col-md-12">
                        <form action="/cart" method="post">
                            <input type="hidden" name="program_id" value="{{ $program->id }}">
                            @guest
                            @else
                                <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
                            @endguest
                            <div class="row">
                                <span class="h1" style="color: #ee2724; padding: 5px; width: 100%">{{ $program->title }} @if($program->status == 'inactive') <span style="color: #2D322F; font-weight: 700;">*{{ __('text.inactiveProgram') }}*</span> @endif
                                    @if($flag == false)
                                        <button class="addToCartBtn" type="submit" title="{{ __("text.addToCartTitle") }}">
                                            <i class="ion-android-cart"></i>
                                        </button>
                                    @endif
                                </span>
                            </div>
                            @csrf
                        </form>
                        <form action="/program/{{ $program->id }}" method="post">
                            @guest
                                @else
                                @if (auth()->user()->type == 'admin')
                                    <span class="editDeleteIcons">
                                        <a href="/program/{{ $program->id }}/edit" style="color: #364f87">
                                            <i class="ion-edit"></i>
                                        </a>
                                        <a href="/sendProgram/{{ $program->id }}" style="color: #364f87">
                                            <i class="ion-android-send"></i>
                                        </a>
                                        @if ($program->status == 'active')
                                            <button onclick="return confirm('Are you sure you want to INACTIVE this program?')"
                                                    style="color: #364f87; background-color: transparent; border-width: 0; font-size: 24px">
                                                <i class="fas fa-toggle-off"></i>
                                            </button>
                                        @else
                                            <a href="/programActive/{{ $program->id }}"
                                               style="color: #364f87">
                                                <i class="fas fa-toggle-on"></i>
                                            </a>
                                        @endif
                                    </span>
                                @endif
                            @endguest
                                @csrf
                        </form>
                    </div>

                    <div class="form wow fadeInUp" data-wow-delay="0.3s">
                        <div class="row" style="padding-bottom: 20px">
                            <img class="img-responsive wow fadeIn" src="{{ asset('storage/'.$program->image) }}"
                                 alt="{{ $program->Title }}" style="max-height: 500px; display: block"/>
                        </div>
                    </div>

                    <div>
                        <form action="/cart" method="post">
                            <input type="hidden" name="program_id" value="{{ $program->id }}">
                            @guest
                            @else
                                <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
                            @endguest

                            @if($flag == false)
                                <button class="addToCartBtn2" type="submit">
                                    {{ __('text.addToCartBtn') }}
                                </button>
                            @endif
                            @csrf
                        </form>
                    </div>

                    <div class="line"> </div>

                    <div class="row">
                        <h2 class="h2"> {{ __('text.programPrice') }} </h2>
                        <h2 class="h2" style="color: #ee2724; text-align: center">
                            {{$program->price}} {{__("text.BD")}}
                        </h2>
                    </div>

                    <div class="line"></div>

                    <div class="col-md-12">
                        <h2 class="h2"> {{__(('text.programOverview'))}} </h2>
                        <p class="article-body" style="color: #2D322F">
                            {!! nl2br($program->overview) !!}
                        </p>
                    </div>

                    @if($program->videoUrl != null)
                        <div class="row">
                            <iframe class="iframe" src="https://player.vimeo.com/video/{{ $program->videoUrl }}"
                                        frameborder="0" allowfullscreen>
                            </iframe>
                        </div>
                    @endif

                    @if($program->description != null)
                        <div class="line" style="margin-top: 5px"></div>

                        <div class="col-md-12">
                            <h2 class="h2"> {{__(('text.programDescription'))}} </h2>
                            <p class="article-body" style="color: #2D322F">
                                {!! nl2br($program->description) !!}
                            </p>
                        </div>
                    @endif

                    <hr style="width: 100%; border-width:1px">

                    <div>
                        <form action="/cart" method="post">
                            <input type="hidden" name="program_id" value="{{ $program->id }}">
                            @guest
                            @else
                                <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
                            @endguest

                            @if($flag == false)
                                <button class="addToCartBtn2" type="submit">
                                    {{ __('text.addToCartBtn') }}
                                </button>
                            @endif
                            @csrf
                        </form>
                    </div>

                    @if($flag == true)
                        <div id="read" class="showPdf">
                            <a class="pdfBody" href="/programPdf/{{$program->pdf}}#toolbar=0" target="_blank" style="color: #ffffff;"> {{__('text.viewPdf')}} </a>
                        </div>

                        <div class="line" style="margin-top: 5px"></div>

                        <div class="row">
                            <h2 class="h2"> {{__(('text.programPhases'))}} </h2>
                        </div>

                        <?php $counter = 0; ?>
                        @foreach($phases as $phase)
                            <?php $counter ++; ?>
                            <button class="accordion">
                                {{__(('text.phase'))}} {{ $counter }}: {{$phase->title}}
                            </button>
                            <div class="panel">
                                <h2 class="h2"> {{__(('text.phaseOverviewShow'))}}: </h2>
                                <p style="color: #2D322F">{{ $phase->overview }}</p>
                                @if($phase->videoUrl != null)
                                    <iframe class="iframe" src="https://player.vimeo.com/video/{{ $phase->videoUrl }}"
                                            frameborder="0" allowfullscreen>
                                    </iframe>
                                @endif
                                @if($phase->description != null)
                                    <div class="line" style="margin-top: 5px"></div>
                                    <h2 class="h2"> {{__(('text.phaseDescriptionShow'))}}: </h2>
                                    <p style="color: #2D322F; padding-bottom: 10px">{{ $phase->description }}</p>
                                @endif

                                <div id="read" class="showPdf">
                                    <a class="pdfBody" href="/phasePdf/{{$phase->pdf}}#toolbar=0" target="_blank" style="color: #ffffff;"> {{__('text.viewPdf')}} </a>
                                </div>

                                <div class="line" style="margin-top: 5px"></div>

                                <h2 class="h2"> {{__(('text.phaseSections'))}}: </h2>

                                <?php $s_counter = 0; ?>
                                @foreach($sections as $section)
                                    @if($section->program_phase_id == $phase->id)
                                        <?php $s_counter ++; ?>
                                        <div id="test-popup-{{ $s_counter }}" class="white-popup mfp-hide">
                                            <iframe class="iframe-video"
                                                    src="https://player.vimeo.com/video/{{ $section->videoUrl }}"
                                                    frameborder="0" allowfullscreen></iframe>
                                        </div>
                                        <h3 class="h3"> {{ __('text.phase') }} {{ $s_counter }}:
                                            <span class="sectionPdf">
                                                <a href="#test-popup-{{ $s_counter }}" class="open-popup-link" style="color: #ee2724"> {{__(('text.watchVideo'))}} </a>
                                            </span>
                                        </h3>
                                        <p style="color: #2D322F"> {{ $section->description }} </p>
                                        <br />
                                    @endif
                                @endforeach
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        @include('layout.footer')
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
@include('includes.footerScripts')
</body>
</html>
