<html lang="{{ __('text.lang') }}" dir="{{ __('text.dir') }}">

@include('includes.head')

<body>

@include('layout.header')
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app form" id="main">
        <div class="pricing-section text-center" style="min-height: 100%">
            <div class="container">
                <div style="width: 100%">
                    <div class="pricing-intro">
                        <h1 class="wow fadeInUp" data-wow-delay="0s"> {{ __('text.searchResults') }} </h1>
                        @if(isset($articles) || isset($videos))
                            <div style="padding-top: 20px">
                            <span class="filterTag"> {{ __('text.filter') }} </span>
                            <select id="myBtnContainer" class="mail" onchange="filterSelection(this.value)">
                                <option value="all"> {{ __('text.showFilter') }} </option>
                                @foreach($categories as $category)
                                    @if($lang == 'en')
                                        <option class="btn" value="{{ $category->englishName }}"> {{ $category->englishName }} </option>
                                    @else
                                        <option class="btn" value="{{ $category->englishName }}"> {{ $category->arabicName }} </option>
                                    @endif
                                @endforeach
                            </select>

                            <!-- <span class="filterTag"> {__('text.filterType') } </span>
                            <select id="myBtnContainer" class="mail" onchange="filterSelection2(this.value)">
                                <option value="all"> { __('text.showFilter') } </option>
                                <option value="video"> if($lang == 'en') Videos else مقاطع مرئية endif </option>
                                <option value="article"> if($lang == 'en') Articles else مقالات endif </option>
                            </select> -->

                        </div>
                        @endif
                    </div>
                    @if(isset($articleResult))
                        @foreach($articleResult as $article)
                            <?php $check = true; $public = $article->public; ?>
                            @include('layout.commonCard')
                            <?php $check = false; ?>
                        @endforeach
                    @endif
                    @if(isset($videoResult))
                        @forelse($videoResult as $video)
                            <?php $public = $video->public; ?>
                            @include('layout.commonCard')
                        @empty
                            <h2>No Videos Found</h2>
                        @endforelse
                    @endif
                    @if(!isset($articleResult) && !isset($videoResult))
                    <div class="pricing-intro">
                        <h1 class="wow fadeInUp" data-wow-delay="0s"> {{ __('text.noResults') }} </h1>
                    </div>
                    @endif
                </div>
            </div>
        </div>
        <!-- Footer Section -->
        @include('layout.footer')
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
@include('includes.footerScripts')
<script>
    filterSelection("all")
    function filterSelection(c) {
        var x, i;
        x = document.getElementsByClassName("filterDiv");
        if (c === "all") c = "";
        for (i = 0; i < x.length; i++) {
            w3RemoveClass(x[i], "show");
            if (x[i].className.indexOf(c) > -1) w3AddClass(x[i], "show");
        }
    }

    filterSelection2("all")
    function filterSelection2(c) {
        var x, i;
        x = document.getElementsByClassName("filterDiv");
        if (c === "all") c = "";
        for (i = 0; i < x.length; i++) {
            w3RemoveClass(x[i], "show");
            if (x[i].className.indexOf(c) > -1) w3AddClass(x[i], "show");
        }
    }

    function w3AddClass(element, name) {
        var i, arr1, arr2;
        arr1 = element.className.split(" ");
        arr2 = name.split(" ");
        for (i = 0; i < arr2.length; i++) {
            if (arr1.indexOf(arr2[i]) == -1) {element.className += " " + arr2[i];}
        }
    }

    function w3RemoveClass(element, name) {
        var i, arr1, arr2;
        arr1 = element.className.split(" ");
        arr2 = name.split(" ");
        for (i = 0; i < arr2.length; i++) {
            while (arr1.indexOf(arr2[i]) > -1) {
                arr1.splice(arr1.indexOf(arr2[i]), 1);
            }
        }
        element.className = arr1.join(" ");
    }

</script>
</body>
</html>
