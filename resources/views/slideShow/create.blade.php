<html lang="{{ __('text.lang') }}" dir="{{ __('text.dir') }}">

@include('includes.head')

<body>

@include('layout.header')
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app" id="main">

        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe" style="min-height: 90%">
            <div class="container">
                <div class="cta-inner">
                    <div class="row">
                        <h1 style="float: none; text-align: center"> {{ __('text.addImageTitle')  }} </h1>
                    </div>
                    <div class="wow fadeInUp" data-wow-delay="0.3s">
                        <form class="forms-form center-form wow zoomIn" action="/slideShow" method="post" enctype="multipart/form-data">

                            @error('imageEng') <h5 style="color: red">{{ $message }}</h5> @enderror
                            <h3 style="padding-bottom: 10px">{{__('text.slideShowEng')}}</h3>
                            <input class="mail" type="file" name="imageEng" id="imageEng"
                                   style="display: inline-block;padding-top: 10px">

                            @error('imageAr') <h5 style="color: red">{{ $message }}</h5> @enderror
                            <h3 style="padding-bottom: 10px">{{__('text.slideShowAr')}}</h3>
                            <input class="mail" type="file" name="imageAr" id="imageAr"
                                   style="display: inline-block;padding-top: 10px">

                            @error('show') <h5 style="color: red">{{ $message }}</h5> @enderror
                            <select class="mail" name="show" id="show">
                                <option value="Yes" selected>{{__('text.showImage')}}</option>
                                <option value="No">{{__('text.hideImage')}}</option>
                            </select>

                            @csrf
                            <div>
                                <button class="submit-button" style="width: 60%;">
                                    {{__('text.addImage')}}
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        @include('layout.footer')
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
@include('includes.footerScripts')
</body>
</html>
