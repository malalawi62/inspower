<html lang="{{ __('text.lang') }}" dir="{{ __('text.dir') }}">

@include('includes.head')

<body>

@include('layout.header')
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app form" id="main">
        <div class="pricing-section text-center" style="min-height: 90%">
            <div class="container">
                <div class="col-md-12 col-sm-12 nopadding">
                    <div class="pricing-intro">
                        <h1 class="wow fadeInUp" data-wow-delay="0s"> {{ __('text.allCategoriesTitle') }} </h1>
                    </div>
                    <div class="col-sm-12">
                        @foreach($data['images'] as $image)
                        <div class="table-left wow fadeInUp" data-wow-delay="0.4s">
                            <div class="pricing-details">
                                        <form action="/slideShow/{{ $image->id }}" method="post">
                                            @method('DELETE')
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <img class="show-img wow fadeIn" @if($data['lang'] == 'en')
                                                        src="storage/{{ $image->imageEng }}"
                                                        @else
                                                        src="storage/{{ $image->imageAr }}"
                                                        @endif>
                                                </div>
                                                <div class="editDeleteIcons">
                                                    @if($image->show == "Yes")
                                                        <a href="/slideShowInactive/{{ $image->id }}" title="Hide Image From Home Page" style="color: #364f87">
                                                            <i class="fas fa-toggle-on"></i>
                                                        </a>
                                                    @else
                                                        <a href="/slideShowActive/{{ $image->id }}" title="Show Image In Home Page" style="color: #364f87">
                                                            <i class="fas fa-toggle-off"></i>
                                                        </a>
                                                    @endif
                                                    <button onclick="return confirm('Are you sure you want to delete the image?')" style="color: #364f87; background-color: transparent; border-width: 0; font-size: 24px">
                                                        <i class="ion-android-delete"></i>
                                                    </button>
                                                </div>
                                            </div>
                                            <br>
                                            @csrf
                                        </form>
                            </div>
                        </div>
                        @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        @include('layout.footer')
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
@include('includes.footerScripts')
</body>
</html>
