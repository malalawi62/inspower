<html lang="{{ __('text.lang') }}" dir="{{ __('text.dir') }}">

@include('includes.head')

<body>

@include('layout.header')
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app form" id="main">
        <div class="pricing-section text-center" style="min-height: 100%">
            <div class="container">
                <div class="col-md-12 col-sm-12 banner-padding">
                    <div class="row pricing-intro">
                        <h1 style="text-align: center; float: none" class="wow fadeInUp" data-wow-delay="0s"> {{__("text.subscribers")}}
                            <span style="color: #364f87">( {{ $count }} {{__("text.subscribersNum")}} )</span>
                        </h1>
                    </div>
                    <div class="col-sm-12">
                        <div class="table-left wow fadeInUp" data-wow-delay="0.4s">
                            <div class="pricing-details">
                                <div>
                                    @forelse($subscribers as $subscriber)
                                            <div class="row">
                                                <span class="article-title" style="float: left">
                                                    <a style="color: #ee2724;">
                                                        {{ $subscriber->email }}
                                                    </a>
                                                </span>
                                            </div>
                                    @empty
                                        <h2>No Subscribers Found</h2>
                                    @endforelse
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        @include('layout.footer')
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
@include('includes.footerScripts')
</body>
</html>
