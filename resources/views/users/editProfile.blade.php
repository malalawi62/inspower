<html lang="{{ __('text.lang') }}" dir="{{ __('text.dir') }}">

@include('includes.head')

<body>

@include('layout.header')
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app form" id="main">

        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe" style="min-height: 100%">
            <div class="container">
                <div class="cta-inner">
                    <div class="form wow fadeInUp" data-wow-delay="0.3s">
                        <div class="row banner-padding" style="padding-bottom: 20px">
                            @if($user->gender == 'f')
                                @if($membership == 'year' || $membership == 'month')
                                    <div class="profile-header-img">
                                        <img class="img-responsive img-circle wow fadeIn" src="{{ asset('storage/female_avatar.png') }}"
                                             alt=""/>
                                        <!-- badge -->
                                        <div class="rank-label-container">
                                            <span class="label label-default rank-label">{{__("text.premiumUser")}}</span>
                                        </div>
                                    </div>
                                @else
                                    <div class="profile-header-img">
                                        <img class="img-responsive img-circle wow fadeIn" style=" @if($membership != 'year' || $membership != 'month') border: 2px solid #ee2724;@endif" src="{{ asset('storage/female_avatar.png') }}"
                                             alt=""/>
                                        <div class="rank-label-container">
                                            <span class="label label-default rank-label" style="background-color: #ee2724">{{__("text.freeUser")}}</span>
                                        </div>
                                    </div>
                                @endif
                            @else
                                @if($membership == 'year' || $membership == 'month')
                                    <div class="profile-header-img">
                                        <img class="img-responsive img-circle wow fadeIn" src="{{ asset('storage/male_avatar.png') }}"
                                             alt="">
                                        <div class="rank-label-container">
                                            <span class="label label-default rank-label">{{__("text.premiumUser")}}</span>
                                        </div>
                                    </div>
                                @else
                                    <div class="profile-header-img">
                                        <img class="img-responsive img-circle wow fadeIn" style="border: 2px solid #ee2724;"src="{{ asset('storage/male_avatar.png') }}"
                                             alt=""/>
                                        <!-- badge -->
                                        <div class="rank-label-container">
                                            <span class="label label-default rank-label"  style="background-color: #ee2724">{{__("text.freeUser")}}</span>
                                        </div>
                                    </div>
                                @endif
                            <!--<img class="img-responsive wow fadeIn" src="{{ asset('storage/male_avatar.png') }}"
                                                 alt="" style="max-height: 200px; display: block"/>-->
                            @endif
                        </div>
                    </div>

                    <hr style="width: 100%; border-width:1px">

                    <form class="subscribe-form center-form wow zoomIn" method="POST" action="/profile/{{$user->id}}">
                        @method('PATCH')

                        @error('name') <h5 style="color: red;">{{ $message }}</h5> @enderror
                        <input class="mail" style="margin-bottom: 5px" type="text" name="name" placeholder="{{ __('text.registerName') }}"
                               required autocomplete="name" autofocus value="{{ $user->name }}">

                        @error('email') <h5 style="color: red;">{{ $message }}</h5> @enderror
                        <input class="mail" style="margin-bottom: 5px" type="email" name="email" placeholder="{{ __('text.registerEmail') }}"
                               required autocomplete="email" value="{{ $user->email }}">

                        @error('DOB') <h5 style="color: red;">{{ $message }}</h5> @enderror
                        <input class="mail" style="margin-bottom: 5px" type="date" name="DOB" id="DOB"
                               required value="{{ $user->DOB }}">

                        <div>
                            <button class="submit-button" style="width: 50%; margin-top: 30px; margin-bottom: 15px">
                                {{ __('text.editProfile') }}
                            </button>
                        </div>
                        @csrf
                    </form>

                </div>
            </div>
        </div>

        <!-- Footer Section -->
        @include('layout.footer')
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
@include('includes.footerScripts')
</body>
</html>
