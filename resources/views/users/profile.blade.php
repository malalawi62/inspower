<html lang="{{ __('text.lang') }}" dir="{{ __('text.dir') }}">

@include('includes.head')

<body>

@include('layout.header')
<div class="wrapper" style="background-color: #2D322F">
    <!-- Main Section-->
    <div class="main app form" id="main">
        <div class="pricing-section text-center" style="min-height: 100%">
            <div class="container">
                <div class="col-md-12 col-sm-12 banner-padding text-center" style="float: none">

                    <div class="pricing-intro">
                        <h1 class="wow fadeInUp" data-wow-delay="0s"> {{ __('text.personalInfo') }} </h1>
                    </div>
                    <div class="table-left wow fadeInUp" data-wow-delay="0.4s" style="overflow: auto; border-radius: 10px; border: none">
                            <div class="pricing-details">
                                <div style="display: block">
                                    <div class="col-md-4">
                                        @if($user->gender == 'f')
                                            @if($membership == 'year' || $membership == 'month')
                                                <div class="profile-header-img">
                                                    <img class="img-responsive img-circle wow fadeIn" src="{{ asset('storage/female_avatar.png') }}"
                                                         alt=""/>
                                                    <!-- badge -->
                                                    <div class="rank-label-container">
                                                        <span class="label label-default rank-label">{{__("text.premiumUser")}}</span>
                                                    </div>
                                                </div>
                                            @else
                                                <div class="profile-header-img">
                                                    <img class="img-responsive img-circle wow fadeIn" style=" @if($membership != 'year' || $membership != 'month') border: 2px solid #ee2724;@endif" src="{{ asset('storage/female_avatar.png') }}"
                                                         alt=""/>
                                                    <div class="rank-label-container">
                                                        <span class="label label-default rank-label" style="background-color: #ee2724">{{__("text.freeUser")}}</span>
                                                    </div>
                                                </div>
                                            @endif
                                        @else
                                            @if($membership == 'year' || $membership == 'month')
                                                <div class="profile-header-img">
                                                    <img class="img-responsive img-circle wow fadeIn" src="{{ asset('storage/male_avatar.png') }}"
                                                         alt="">
                                                    <div class="rank-label-container">
                                                        <span class="label label-default rank-label">{{__("text.premiumUser")}}</span>
                                                    </div>
                                                </div>
                                            @else
                                                <div class="profile-header-img">
                                                    <img class="img-responsive img-circle wow fadeIn" style="border: 2px solid #ee2724;"src="{{ asset('storage/male_avatar.png') }}"
                                                         alt=""/>
                                                    <!-- badge -->
                                                    <div class="rank-label-container">
                                                        <span class="label label-default rank-label"  style="background-color: #ee2724">{{__("text.freeUser")}}</span>
                                                    </div>
                                                </div>
                                            @endif
                                            <!--<img class="img-responsive wow fadeIn" src="{{ asset('storage/male_avatar.png') }}"
                                                 alt="" style="max-height: 200px; display: block"/>-->
                                        @endif
                                    </div>
                                    <div class="col-md-8" style="position:relative; top: -25%;transform: translateY(25%);">
                                        <div class="row">
                                            <h1 class="text-profile">
                                                {{ $user->name }}
                                            </h1>
                                        </div>

                                        <div class="row">
                                            <h1 class="text-profile">
                                                {{ $user->email }}
                                            </h1>
                                        </div>

                                        <div>
                                            <button class="addToCartBtn2" style="margin-bottom: 15px" onclick="location.href = '/profile/{{ $user->id }}/edit';">
                                                {{ __('text.editProfile') }}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <div class="line"></div>

                    @if (count($courses) > 0)
                    <div class="pricing-intro">
                        <h1 class="wow fadeInUp" data-wow-delay="0s"> {{ __('text.myCourses') }} </h1>
                    </div>
                    <div class="table-left wow fadeInUp" data-wow-delay="0.4s" style="overflow: auto; border-radius: 10px; border: none;">
                        <div class="pricing-details">
                            <div style="display: block">
                                <div class="col-md-12" style="margin-bottom: 20px">
                                        @foreach($courses as $course)
                                            <div class="row">
                                                <h1 class="courseTitle" onclick="location.href = '/course/{{ $course->course->id }}';"> {{ $course->course->title }} </h1>
                                            </div>
                                        @endforeach

                                </div>
                            </div>
                        </div>
                    </div>
                        <div class="line"></div>
                    @endif

                    @if (count($programs) > 0)
                    <div class="pricing-intro">
                        <h1 class="wow fadeInUp" data-wow-delay="0s"> {{ __('text.myPrograms') }} </h1>
                    </div>
                    <div class="table-left wow fadeInUp" data-wow-delay="0.4s" style="overflow: auto; border-radius: 10px; border: none">
                        <div class="pricing-details">
                            <div style="display: block">
                                <div class="col-md-12" style="margin-bottom: 20px">
                                        @foreach($programs as $program)
                                            <div class="row">
                                                <h1 class="courseTitle" onclick="location.href = '/program/{{ $program->program->id }}';"> {{ $program->program->title }} </h1>
                                            </div>
                                        @endforeach

                                </div>
                            </div>
                        </div>
                    </div>

                        <div class="line"></div>
                    @endif

                    @if (count($orders) > 0)
                    <div class="pricing-intro">
                        <h1 class="wow fadeInUp" data-wow-delay="0s"> {{ __('text.myOrders') }} </h1>
                    </div>
                    <div class="table-left wow fadeInUp" data-wow-delay="0.4s" style="overflow: auto; border-radius: 10px; border: none">
                        <div class="pricing-details">
                            <div style="display: block">
                                <div class="col-md-12" style="margin-bottom: 20px">
                                        @foreach($orders as $order)
                                            <div class="row">
                                                <button class="accordion">
                                                    {{ __('text.orderId') }} {{ $order->id }}: {{$order->created_at->toDateString()}}
                                                </button>
                                                <div class="panel">
                                                    <h2 class="h2"> {{ __('text.orderPrice') }}: {{ $order->amount }} {{__("text.BD")}}</h2>
                                                    <h1 class="courseTitle" onclick="location.href = '/program/';"> {{ __('text.view') }} </h1>
                                                </div>
                                            </div>
                                        @endforeach

                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
        @include('layout.footer')
    </div>
</div>
<!-- Jquery and Js Plugins -->
@include('includes.footerScripts')
</body>
</html>

