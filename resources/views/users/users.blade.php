<html lang="{{ __('text.lang') }}" dir="{{ __('text.dir') }}">

@include('includes.head')

<body>

@include('layout.header')
<div class="wrapper" style="background-color: #2D322F">
    <!-- Main Section-->
    <div class="main app form" id="main">
        <div class="pricing-section text-center" style="min-height: 100%">
            <div class="container">
                <div class="col-md-12 col-sm-12 banner-padding text-center" style="float: none">

                    <div class="pricing-intro">
                        <h1 class="wow fadeInUp" data-wow-delay="0s"> {{ __('text.someStatistics') }} </h1>
                    </div>

                    <div class="table-left wow fadeInUp" data-wow-delay="0.4s" style="overflow: auto; border-radius: 10px; border: none">
                        <div class="pricing-details">
                            <div style="display: block">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <h1 class="h1"> {{ __('text.allUsers') }}: {{ $females+$males }} </h1>
                                        </div>
                                        <div class="col-md-4">
                                            <h1 class="h1"> {{ __('text.female') }}: {{ $females }} <span style="color: #ee2724;"> ({{ number_format((float)$f_percentage, 2, '.', '') }}%) </span></h1>
                                        </div>
                                        <div class="col-md-4">
                                            <h1 class="h1"> {{ __('text.male') }}: {{ $males }} <span style="color: #ee2724;"> ({{ number_format((float)$m_percentage, 2, '.', '') }}%) </span></h1>
                                        </div>
                                    </div>

                                    <hr style="width: 100%; border-width:1px;">

                                    <div class="row">
                                        <div class="col-md-4">
                                            <h1 class="h1"> {{ __('text.less20') }}: {{ $lessThanTwenties }} <span style="color: #ee2724;"> ({{ number_format((float)$pl, 2, '.', '') }}%) </span> </h1>
                                        </div>
                                        <div class="col-md-4">
                                            <h1 class="h1"> {{ __('text.20s') }}: {{ $twenties }} <span style="color: #ee2724;"> ({{ number_format((float)$pt, 2, '.', '') }}%) </span></h1>
                                        </div>
                                        <div class="col-md-4">
                                            <h1 class="h1"> {{ __('text.30s') }}: {{ $thirties }} <span style="color: #ee2724;"> ({{ number_format((float)$pth, 2, '.', '') }}%) </span></h1>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <h1 class="h1"> {{ __('text.40s') }}: {{ $forties }} <span style="color: #ee2724;"> ({{ number_format((float)$pf, 2, '.', '') }}%) </span> </h1>
                                        </div>
                                        <div class="col-md-6">
                                            <h1 class="h1"> {{ __('text.greater40') }}: {{ $greaterThanForties }} <span style="color: #ee2724;"> ({{ number_format((float)$pg, 2, '.', '') }}%) </span></h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr style="width: 100%; border-width:2px;">

                    <div class="pricing-intro">
                        <h1 class="wow fadeInUp" data-wow-delay="0s"> {{ __('text.registeredInfo') }} </h1>
                    </div>

                    <div class="table-left wow fadeInUp" data-wow-delay="0.4s" style="overflow: auto;">
                        <div class="pricing-details">
                            <div style="display: block">
                                <div class="col-md-12">
                                    @if ($users != null)
                                        <?php $counter = 0; ?>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <h1 class="h1" style="color: #ee2724;"> </h1>
                                            </div>
                                            <div class="col-md-4">
                                                <h1 class="h1" style="color: #ee2724;"> {{ __('text.registerName') }} </h1>
                                            </div>
                                            <div class="col-md-4">
                                                <h1 class="h1" style="color: #ee2724;"> {{ __('text.registerEmail') }} </h1>
                                            </div>
                                            <div class="col-md-2">
                                                <h1 class="h1" style="color: #ee2724;"> {{ __('text.DOB') }} </h1>
                                            </div>
                                        </div>

                                            <hr style="width: 100%; border-width:1px;">

                                        @foreach($users as $user)
                                            <?php $counter ++; ?>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <h1 class="h1"> {{ $counter }} </h1>
                                                </div>
                                                <div class="col-md-4">
                                                    <h1 class="h1"> {{ $user->name }} </h1>
                                                </div>
                                                <div class="col-md-4">
                                                    <h1 class="h1"> {{ $user->email }} </h1>
                                                </div>
                                                <div class="col-md-42">
                                                    <h1 class="h1"> {{ $user->DOB }} </h1>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        @include('layout.footer')
    </div>
</div>
<!-- Jquery and Js Plugins -->
@include('includes.footerScripts')
</body>
</html>

