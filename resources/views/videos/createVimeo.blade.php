<!DOCTYPE html>
<html lang="{{ __('text.lang') }}" dir="{{ __('text.dir') }}">

@include('includes.head')

<body>

@include('layout.header')

<div class="wrapper">
    <!-- Main Section-->
    <div class="main app" id="main">
        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe" style="height: 100%">
            <div class="container">
                <div class="cta-inner">
                    <div class="row">
                        <h1 style="text-align: center; float: none"> {{ __('text.addVimeoTitle') }} </h1>
                    </div>
                    <div class="wow fadeInUp" data-wow-delay="0.3s">
                        <form class="forms-form center-form wow zoomIn" action="/videosVimeo" method="post">

                            @error('videoId') <h5 style="color: red;">{{ $message }}</h5> @enderror
                            <input class="mail" type="text" name="videoId" placeholder="Video URL"
                                   autocomplete="off" id="videoId" value="{{ old('videoId') }}">

                            @error('title') <h5 style="color: red;">{{ $message }}</h5> @enderror
                            <input class="mail" type="text" name="title" placeholder="Video Title"
                                   autocomplete="off" id="title" value="{{ old('title') }}">

                            @error('category_id') <h5 style="color: red">{{ $message }}</h5> @enderror
                            <select class="mail" name="category_id" id="category_id">
                                <option hidden value="" selected>{{__('text.chooseCategory')}}</option>
                                @forelse($data['categories'] as $category)
                                    <option value="{{ $category->id }}">
                                        @if($data['lang'] == 'en')
                                            {{ $category->englishName }}
                                        @else
                                            {{ $category->arabicName }}
                                        @endif
                                    </option>
                                @empty
                                    <option>No Categories Found</option>
                                @endforelse
                            </select>

                            @error('public') <h5 style="color: red">{{ $message }}</h5> @enderror
                            <select class="mail" name="public" id="public">
                                <option value="YES" selected>{{__('text.publicContent')}}</option>
                                <option value="NO">{{__('text.privateContent')}}</option>
                            </select>

                            <p style="color: #2D322F">{{__('text.videoDescription')}}</p>
                            <textarea class="mail" rows="5" name="description" id="description"
                                      style="height: auto;"></textarea>

                            <input type="hidden" name="type" id="type" value="vimeo">

                            @if($data['lang'] == 'en')
                                <input type="hidden" name="language" id="language" value="Eng">
                            @else
                                <input type="hidden" name="language" id="language" value="Ar">
                            @endif

                            @csrf
                            <div>
                                <button class="submit-button" style="width: 60%;">
                                    {{__('text.addYoutubeBtn')}}
                                </button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        @include('layout.footer')
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
@include('includes.footerScripts')

</body>
</html>
