<!DOCTYPE html>
<html lang="{{ __('text.lang') }}" dir="{{ __('text.dir') }}">

@include('includes.head')

<body>

@include('layout.header')

<div class="wrapper">
    <!-- Main Section-->
    <div class="main app" id="main">
        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe" style="height: 100%">
            <div class="container">
                <div class="cta-inner">
                    <h1> {{ __('text.editYoutubeTitle') }} </h1>
                    <div class="wow fadeInUp" data-wow-delay="0.3s">
                        <form class="forms-form center-form wow zoomIn" action="/videos/{{ $data['video']->id }}" method="post">
                            @method('PATCH')
                            @error('videoId') <h5 style="color: red;">{{ $message }}</h5> @enderror
                            <input class="mail" type="hidden" name="videoId"
                                   autocomplete="off" id="videoId" value="{{ $data['video']->videoId }}">

                            @error('title') <h5 style="color: red;">{{ $message }}</h5> @enderror
                            <input class="mail" type="text" name="title"
                                   autocomplete="off" id="title" value="{{ $data['video']->title }}">

                            @error('category_id') <h5 style="color: red">{{ $message }}</h5> @enderror
                            <select class="mail" name="category_id" id="category_id" style="color: #FFF">
                                <option hidden value="">{{__('text.chooseCategory')}}</option>
                                @forelse($data['categories'] as $category)
                                    <option value="{{ $category->id }}"
                                            @if($category->id == $data['video']->category_id)
                                                selected
                                            @endif
                                            style="background-color: #2D322F">
                                        @if($data['lang'] == 'en')
                                            {{ $category->englishName }}
                                        @else
                                            {{ $category->arabicName }}
                                        @endif
                                    </option>
                                @empty
                                    <option>No Categories Found</option>
                                @endforelse
                            </select>

                            @error('public') <h5 style="color: red">{{ $message }}</h5> @enderror
                            <select class="mail" name="public" id="public" style="color: #FFF">
                                <option value="YES" @if($data['video']->public == "YES") selected @endif>{{__('text.publicContent')}}</option>
                                <option value="NO" style="background-color: #2D322F" @if($data['video']->public == "NO") selected @endif>{{__('text.privateContent')}}</option>
                            </select>

                            <p>Video Description in english</p>
                            <textarea class="mail" rows="5" name="description" id="description"
                                      style="height: auto;">{{ $data['video']->description }}</textarea>

                             <input type="hidden" name="type" id="type" value="{{ $data['video']->type}}">

                            <input type="hidden" name="language" id="language" value="{{ $data['video']->language}}">

                            @csrf
                            <div>
                                <button class="btn btn-primary"
                                        style="background-color: #EC3642; border:0; padding: 10px 40px 10px 40px; width:60%;">
                                    {{__('text.editYoutubeBtn')}}
                                </button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        @include('layout.footer')
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
@include('includes.footerScripts')

</body>
</html>
