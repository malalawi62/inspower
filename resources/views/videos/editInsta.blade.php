<!DOCTYPE html>
<html lang="{{ __('text.lang') }}" dir="{{ __('text.dir') }}">

@include('includes.head')

<body>

@include('layout.header')

<div class="wrapper">
    <!-- Main Section-->
    <div class="main app" id="main">
        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe" style="height: 100%">
            <div class="container">
                <div class="cta-inner">
                    <h1> {{ __('text.editInstagramTitle') }} </h1>
                    <div class="wow fadeInUp" data-wow-delay="0.3s">
                        <form class="forms-form center-form wow zoomIn" action="/instagram/{{ $data['instagram']->id }}" method="post">
                            @method('PATCH')
                            @error('url') <h5 style="color: red;">{{ $message }}</h5> @enderror
                            <input class="mail" type="text" name="url"
                                   autocomplete="off" id="url" placeholder="Video URL" value="{{ $data['instagram']->url }}">

                            @error('title') <h5 style="color: red;">{{ $message }}</h5> @enderror
                            <input class="mail" type="text" name="title"
                                   autocomplete="off" id="title" placeholder="Video Title in English" value="{{ $data['instagram']->title }}">

                            @error('titleAr') <h5 style="color: red;">{{ $message }}</h5> @enderror
                            <input class="mail" type="text" name="titleAr"
                                   autocomplete="off" id="titleAr" placeholder="عنوان الفيديو باللغة العربية"
                                   value="{{ $data['instagram']->titleAr }}">

                            @error('category_id') <h5 style="color: red">{{ $message }}</h5> @enderror
                            <select class="mail" name="category_id" id="category_id" style="color: #FFF">
                                <option hidden>{{__('text.chooseCategory')}}</option>
                                @forelse($data['categories'] as $category)
                                    <option value="{{ $category->id }}"
                                            style="background-color: #2D322F"
                                            @if($category->id == $data['instagram']->category_id)
                                                selected
                                            @endif>
                                        @if($data['lang'] == 'en')
                                            {{ $category->englishName }}
                                        @else
                                            {{ $category->arabicName }}
                                        @endif
                                    </option>
                                @empty
                                    <option>No Categories Found</option>
                                @endforelse
                            </select>

                            <p>Video Description in english</p>
                            <textarea class="mail" rows="5" name="description" id="description"
                                      style="height: auto;">{{ $data['instagram']->description }}</textarea>

                            <p >وصف الفيديو باللغة العربية</p>
                            <textarea class="mail" rows="5" name="descriptionAr" id="descriptionAr"
                                      style="height: auto;">{{ $data['instagram']->descriptionAr }}</textarea>

                            @csrf
                            <div>
                                <button class="btn btn-primary"
                                        style="background-color: #EC3642; border:0; padding: 10px 40px 10px 40px; width:60%;">
                                    {{__('text.editInstagramBtn')}}
                                </button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        @include('layout.footer')
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
@include('includes.footerScripts')

</body>
</html>
