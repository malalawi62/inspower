<html lang="{{ __('text.lang') }}" dir="{{ __('text.dir') }}">

@include('includes.head')

<body>

@include('layout.header')
<div class="wrapper" style="background-color: #2D322F">
    <!-- Main Section-->
    <div class="main app form" id="main">
        <div class="pricing-section text-center" style="min-height: 100%">
            <div class="container">
                <div style="width: 100%">
                    <div class="pricing-intro banner-padding">
                        <h1 class="wow fadeInUp" data-wow-delay="0s"> {{ __('text.allVideosTitle') }} </h1>
                        @if((App::isLocale('en')))
                            <h2>For videos in Arabic language <a href="setlocale/ar">press here</a></h2>
                        @endif
                        @if((App::isLocale('ar')))
                            <h2>للمقاطع المرئية باللغة الإنجليزية <a href="setlocale/en">اضغط هنا</a></h2>
                        @endif
                        <div style="padding-top: 20px">
                            <span class="filterTag"> {{ __('text.filter') }} </span>
                            <select name="form" onchange="location = this.value;" class="mail">
                                <option value="https://www.inspower-fitnesscoaching.com/videos"
                                        @if($data['cat_id'] == null) selected @endif> {{ __('text.showFilter') }} </option>
                                @foreach($data['categories'] as $category)
                                    @if($data['lang'] == 'en')
                                        <option
                                            value="https://www.inspower-fitnesscoaching.com/videos?category_id={{ $category->id }}"
                                            @if($category->id == $data['cat_id']) selected @endif> {{ $category->englishName }} </option>
                                    @else
                                        <option
                                            value="https://www.inspower-fitnesscoaching.com/videos?category_id={{ $category->id }}"
                                            @if($category->id == $data['cat_id']) selected @endif> {{ $category->arabicName }} </option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    @forelse($data['videos'] as $video)
                        <?php $public = $video->public; ?>
                        @include('layout.commonCard')
                    @empty
                        <h2>No Videos Found</h2>
                    @endforelse
                </div>
            </div>
        </div>
        <div class="container" style="background-color:#2D322F; width: 100%">
            <div class="col-md-12 text-center">
                {{ $data['videos']->links() }}
            </div>
        </div>
        <!-- Footer Section -->
        @include('layout.footer')
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
@include('includes.footerScripts')
<script>
    filterSelection("all")

    function filterSelection(c) {
        var x, i;
        x = document.getElementsByClassName("filterDiv");
        if (c === "all") c = "";
        for (i = 0; i < x.length; i++) {
            w3RemoveClass(x[i], "show");
            if (x[i].className.indexOf(c) > -1) w3AddClass(x[i], "show");
        }
    }

    function w3AddClass(element, name) {
        var i, arr1, arr2;
        arr1 = element.className.split(" ");
        arr2 = name.split(" ");
        for (i = 0; i < arr2.length; i++) {
            if (arr1.indexOf(arr2[i]) == -1) {
                element.className += " " + arr2[i];
            }
        }
    }

    function w3RemoveClass(element, name) {
        var i, arr1, arr2;
        arr1 = element.className.split(" ");
        arr2 = name.split(" ");
        for (i = 0; i < arr2.length; i++) {
            while (arr1.indexOf(arr2[i]) > -1) {
                arr1.splice(arr1.indexOf(arr2[i]), 1);
            }
        }
        element.className = arr1.join(" ");
    }

</script>

</body>
</html>
