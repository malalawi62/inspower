<html lang="{{ __('text.lang') }}" dir="{{ __('text.dir') }}">

@include('includes.head')

<body>

@include('layout.header')
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app form" id="main">

        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe" style="min-height: 100%">
            <div class="container">
                <div class="cta-inner">
                    <div class="row">
                        <form action="/videos/{{ $data['video']->id }}" method="post">
                            @method('DELETE')
                            <h1 style="color: #ee2724;">{{ $data['video']->title }}</h1>
                            @guest
                                @else
                                @if (auth()->user()->type == 'admin')
                                    <span class="editDeleteIcons">
                                        <a href="/videos/{{ $data['video']->id }}/edit" style="color: #ee2724">
                                            <i class="ion-edit"></i>
                                        </a>
                                        <button onclick="return confirm('Are you sure you want to delete the video?')"
                                            style="color: #ee2724; background-color: transparent; border-width: 0; font-size: 24px">
                                            <i class="ion-android-delete"></i>
                                        </button>
                                    </span>
                                @endif
                                @endguest
                        </form>
                    </div>
                    <div class="row" style="padding: 10px">
                        @if($data['lang'] == 'en')
                            <h3  style="color: #ee2724; {{ __('text.textAlign') }}">
                                Category: <a style="color: #364f87; {{ __('text.textAlign') }}" href="https://www.inspower-fitnesscoaching.com/videos?category_id={{ $data['video']->category->id }}"> {{ $data['video']->category->englishName }} </a></h3>
                        @else
                            <h3  style="color: #ee2724; {{ __('text.textAlign') }}">
                                الفئة: <a style="color: #364f87; {{ __('text.textAlign') }}" href="https://www.inspower-fitnesscoaching.com/videos?category_id={{ $data['video']->category->id }}"> {{ $data['video']->category->arabicName }} </a></h3>

                        @endif
                    </div>
                    <hr style="width: 100%; border-width:1px">
                        <div class="row">
                            @if($data['video']->type == 'vimeo')
                                <iframe class="iframe"
                                        src="https://player.vimeo.com/video/{{ $data['video']->videoId }}"
                                        frameborder="0" allowfullscreen></iframe>
                            @else
                                <iframe width="100%" height="100%"
                                        src="https://www.youtube.com/embed/{{ $data['video']->videoId }}"
                                        frameborder="0" allowfullscreen ></iframe>
                            @endif
                        </div>
                </div>

                <div class="line"></div>

                <p class="article-body" style="color: #2D322F">
                    {!! nl2br($data['video']->description) !!}
                </p>
            </div>
        </div>

        <!-- Footer Section -->
        @include('layout.footer')
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
@include('includes.footerScripts')
</body>
</html>
