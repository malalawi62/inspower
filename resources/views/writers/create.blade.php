<html lang="{{ __('text.lang') }}" dir="{{ __('text.dir') }}">

@include('includes.head')

<body>

@include('layout.header')
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app" id="main">

        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe" style="height: 90%">
            <div class="container">
                <div class="cta-inner">
                    <div class="row">
                        <h1 style="text-align: center; float: none"> {{ __('text.addWriterTitle') }} </h1>
                    </div>
                    <div class="wow fadeInUp" data-wow-delay="0.3s">
                        <form class="forms-form center-form wow zoomIn" action="/writers" method="post">

                            @error('englishName') <h5 style="color: red">{{ $message }}</h5> @enderror
                            <input class="mail" type="text" name="englishName" placeholder="Writer English Name *"
                                   autocomplete="off" id="englishName" value="{{ old('englishName') }}">

                            @error('arabicName') <h5 style="color: red">{{ $message }}</h5> @enderror
                            <input dir="rtl" class="mail" type="text" name="arabicName"
                                   placeholder="اسم الكاتب باللغة العربية *" autocomplete="off" id="arabicName"
                                   value="{{ old('arabicName') }}">

                            @csrf
                            <div>
                                <button class="submit-button" style="width: 60%;">
                                    {{__('text.addWriterBtn')}}
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        @include('layout.footer')
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
@include('includes.footerScripts')
</body>
</html>
