<html lang="{{ __('text.lang') }}" dir="{{ __('text.dir') }}">

@include('includes.head')

<body>

@include('layout.header')
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app form" id="main">

        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe" style="height: 90%">
            <div class="container">
                <div class="cta-inner">
                    <h1> {{ __('text.EditWriterTitle') }} </h1>
                    <div class="form wow fadeInUp" data-wow-delay="0.3s">
                        <form class="forms-form center-form wow zoomIn" action="/writers/{{ $writer->id }}" method="post">
                            @method('PATCH')
                            @error('englishName') <h5 style="color: red">{{ $message }}</h5> @enderror
                            <input class="mail" type="text" name="englishName" placeholder="Writer English Name *"
                                   autocomplete="off" id="englishName" value="{{ $writer->englishName }}">

                            @error('arabicName') <h5 style="color: red">{{ $message }}</h5> @enderror
                            <input dir="rtl" class="mail" type="text" name="arabicName"
                                   placeholder="اسم الكاتب باللغة العربية *" autocomplete="off" id="arabicName"
                                   value="{{ $writer->arabicName }}">

                            @csrf
                            <div>
                                <button class="btn btn-primary"
                                        style="background-color: #EC3642; border:0; padding: 10px 40px 10px 40px; width:60%;">
                                    {{ __('text.EditWriterBtn') }}
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        @include('layout.footer')
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
@include('includes.footerScripts')
</body>
</html>
