<html lang="{{ __('text.lang') }}" dir="{{ __('text.dir') }}">

@include('includes.head')

<body>

@include('layout.header')
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app form" id="main">
        <div class="pricing-section text-center" style="min-height: 100%">
            <div class="container">
                <div style="width: 100%">
                    <div class="pricing-intro">
                        <h1 class="wow fadeInUp" data-wow-delay="0s"> {{ __('text.allWritersTitle') }} </h1>
                    </div>
                    <div class="col-sm-12">
                        <div class="table-left wow fadeInUp" data-wow-delay="0.4s">
                            <div class="pricing-details">
                                <div>
                                    @forelse($data['writers'] as $writer)
                                        <form action="/writers/{{ $writer->id }}" method="post">
                                            @method('DELETE')
                                            <div class="row">
                                                <span class="article-title">
                                                    <a style="color: #FFF;">
                                                        @if($data['lang'] == 'en')
                                                            {{ $writer->englishName }}
                                                        @else
                                                            {{ $writer->arabicName }}
                                                        @endif
                                                    </a>
                                                </span>
                                                <span class="editDeleteIcons">
                                                    <a href="/writers/{{ $writer->id }}/edit" style="color: #FCD400">
                                                        <i class="ion-edit"></i>
                                                    </a>
                                                    <button style="color: #FCD400; background-color: transparent; border-width: 0; font-size: 24px">
                                                        <i class="ion-android-delete"></i>
                                                    </button>
                                                </span>
                                            </div>
                                            <br>
                                            @csrf
                                        </form>
                                        <br>
                                    @empty
                                        <h2>No Writers Found</h2>
                                    @endforelse
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        @include('layout.footer')
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
@include('includes.footerScripts')
</body>
</html>
