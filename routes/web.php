<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/setlocale/{locale}',function($lang){
    Session::put('locale',$lang);
    return redirect()->back();
});

Route::get('/youtube', 'YoutubeController@index');

Route::get('/adminPortal', 'HomeController@index');

//Subscription Routes
Route::post('/subscribe', 'SubscribersController@store');
Route::get('/subscribe/{email}', 'SubscribersController@confirm');
Route::get('/unsubscribe/{email}', 'SubscribersController@unsubscribe');
Route::get('/sendVideo/{videoId}', 'SubscribersController@sendVideo');
Route::get('/sendArticle/{articleId}', 'SubscribersController@sendArticle');
Route::get('/sendCourse/{courseId}', 'SubscribersController@sendCourse');
Route::get('/sendProgram/{programId}', 'SubscribersController@sendProgram');

Route::group(['middleware'=>'language'],function ()
{
    //Search
    Route::any('/search',function(\Illuminate\Http\Request $request){
        if (Auth::user()){
            $membership = true;
            $membership_details = \App\memberships::where('user_id',auth()->user()->id)->value('type');
        }
        else {
            $membership = false;
            $membership_details = null;
        }

        $q = $request->get( 'q' );
        $array = explode(' ', $q);
        for ($i=0; $i<count($array); $i++){
            $videos[$i] = \App\youtube::where('title','LIKE','%'.$array[$i].'%')->orWhere('description', 'LIKE', '%' . $array[$i] . '%')->get();
            $articles[$i] = \App\articles::where('title','LIKE','%'.$array[$i].'%')->orWhere('body', 'LIKE', '%' . $array[$i] . '%')->get();
        }

        $categories = \App\categories::all();
        if (App::isLocale('en'))
            $lang = 'en';
        else
            $lang = 'ar';

        if(count($videos) > 0){
            $videoView = 'videos';
            $exist = false;
            for ($i=0;$i<count($videos);$i++){
                foreach ($videos[$i] as $video){
                    if(isset($videoResult)){
                        for ($j=0; $j<count($videoResult); $j++){
                            if($videoResult[$j]->id == $video->id)
                                $exist = true;
                        }
                    }
                    if(!$exist)
                        $videoResult[]=$video;
                }
            }
        }

        if(count($articles) > 0){
            $exist = false;
            $articleView = 'articles';
            for ($i=0;$i<count($articles);$i++){
                foreach ($articles[$i] as $article){
                    if(isset($articleResult)){
                        for ($j=0; $j<count($articleResult); $j++){
                            if($articleResult[$j]->id == $article->id)
                                $exist = true;
                        }
                    }
                    if(!$exist)
                        $articleResult[]=$article;
                }
            }
        }

        $data = [
            'categories' => $categories,
            'lang' => $lang
        ];

        if(isset($videoResult) && isset($articleResult))
            return view('searchResults', compact('videoResult', 'articleResult', 'categories', 'lang', 'membership', 'membership_details', 'articleView', 'videoView', 'data'));

        elseif (isset($videoResult) > 0 && !isset($articleResult))
            return view('searchResults', compact('videoResult', 'categories', 'lang', 'membership', 'membership_details', 'videoView', 'data'));

        elseif (!isset($videoResult) && isset($articleResult))
            return view('searchResults', compact('articleResult', 'categories', 'lang', 'membership', 'membership_details', 'articleView', 'data'));

        //if(count($videos) == 0)
        else
            return view ('searchResults')->withMessage('No Details found. Try to search again !');
    });

    Auth::routes();
    Route::get('/registerExclusive', 'Auth\RegisterController@exclusive');
    Route::get('/registerUpgrade', 'MembershipsController@upgrade');

    //Subscription Routes
    Route::get('/subscribersList', 'SubscribersController@subscribersList')->middleware('auth')->middleware('AdminMiddleware');
    Route::get('/usersList', 'UsersController@users')->middleware('auth')->middleware('AdminMiddleware');

    //Terms & conditions Routes
    Route::get('/terms', function () {
        return view('termAndConditions');
    });

    Route::get('/privacy', function () {
        return view('privacyPolicy');
    });

    Route::get('/pricing', function () {
        return view('pricingPolicy');
    });

    //Home Routes
    Route::get('/', function () {
        return view('welcome');
    });
    Route::get('/home', function () {
        return view('welcome');
    });
    Route::get('/about', function () {
        return redirect('welcome#about');
    });
    Route::get('/team', function () {
        return redirect('welcome#team');
    });
    Route::get('/services', function () {
        return redirect('welcome#services');
    });
    Route::get('/contact', function () {
        return redirect('welcome#contact');
    });
    Route::get('/welcome', function () {
        return view('welcome');
    });

    //Slide Show Routes
    Route::get('/slideShow', 'SlideShowController@index')->middleware('auth')->middleware('AdminMiddleware');
    Route::get('/slideShow/create', 'SlideShowController@create')->middleware('auth')->middleware('AdminMiddleware');
    Route::post('/slideShow', 'SlideShowController@store')->middleware('auth')->middleware('AdminMiddleware');
    Route::delete('/slideShow/{image}', 'SlideShowController@destroy')->middleware('auth')->middleware('AdminMiddleware');
    Route::get('/slideShowActive/{image}', 'SlideShowController@active')->middleware('auth')->middleware('AdminMiddleware');
    Route::get('/slideShowInactive/{image}', 'SlideShowController@inactive')->middleware('auth')->middleware('AdminMiddleware');

    //Users Routes
    Route::get('/profile/{user}', 'UsersController@profile')->middleware('auth');
    Route::get('/profile/{user}/edit', 'UsersController@edit')->middleware('auth');
    Route::patch('/profile/{user}', 'UsersController@update')->middleware('auth');

    //Articles Routes
    Route::get('/articles', 'ArticlesController@index');
    Route::get('/articles/create', 'ArticlesController@create')->middleware('auth')->middleware('AdminMiddleware');
    Route::post('/articles', 'ArticlesController@store')->middleware('auth')->middleware('AdminMiddleware');
    Route::get('/articles/{article}', 'ArticlesController@show');
    Route::get('/articles/{article}/edit', 'ArticlesController@edit')->middleware('auth')->middleware('AdminMiddleware');
    Route::patch('/articles/{article}', 'ArticlesController@update')->middleware('auth')->middleware('AdminMiddleware');
    Route::delete('/articles/{article}', 'ArticlesController@destroy')->middleware('auth')->middleware('AdminMiddleware');

    //Categories Routes
    Route::get('/categories', 'CategoriesController@index')->middleware('auth')->middleware('AdminMiddleware');
    Route::get('/categories/create', 'CategoriesController@create')->middleware('auth')->middleware('AdminMiddleware');
    Route::post('/categories', 'CategoriesController@store')->middleware('auth')->middleware('AdminMiddleware');
    Route::get('/categories/{category}/edit', 'CategoriesController@edit')->middleware('auth')->middleware('AdminMiddleware');
    Route::patch('/categories/{category}', 'CategoriesController@update')->middleware('auth')->middleware('AdminMiddleware');
    Route::delete('/categories/{category}', 'CategoriesController@destroy')->middleware('auth')->middleware('AdminMiddleware');

    //Writers Routes
    Route::get('/writers', 'WritersController@index')->middleware('auth')->middleware('AdminMiddleware');
    Route::get('/writers/create', 'WritersController@create')->middleware('auth')->middleware('AdminMiddleware');
    Route::post('/writers', 'WritersController@store')->middleware('auth')->middleware('AdminMiddleware');
    Route::get('/writers/{writer}/edit', 'WritersController@edit')->middleware('auth')->middleware('AdminMiddleware');
    Route::patch('/writers/{writer}', 'WritersController@update')->middleware('auth')->middleware('AdminMiddleware');
    Route::delete('/writers/{writer}', 'WritersController@destroy')->middleware('auth')->middleware('AdminMiddleware');

    //Youtube Videos Routes
    Route::get('/videos', 'YoutubeController@index');
    Route::get('/videoUrl', 'YoutubeController@getVideoUrl')->middleware('auth')->middleware('AdminMiddleware');
    Route::get('/videoInfo', 'YoutubeController@create')->middleware('auth')->middleware('AdminMiddleware');
    Route::get('/videos/create', 'YoutubeController@create')->middleware('auth')->middleware('AdminMiddleware');
    Route::get('/videos/createVimeo', 'YoutubeController@createVimeo')->middleware('auth')->middleware('AdminMiddleware');
    Route::post('/videos', 'YoutubeController@store')->middleware('auth')->middleware('AdminMiddleware');
    Route::post('/videosVimeo', 'YoutubeController@storeVimeo')->middleware('auth')->middleware('AdminMiddleware');
    Route::get('/videos/{video}', 'YoutubeController@show');
    Route::get('/videos/{video}/edit', 'YoutubeController@edit')->middleware('auth')->middleware('AdminMiddleware');
    Route::patch('/videos/{video}', 'YoutubeController@update')->middleware('auth')->middleware('AdminMiddleware');
    Route::post('/videos/{video}', 'YoutubeController@destroy')->middleware('auth')->middleware('AdminMiddleware');

    //Instagram Videos Routes
    Route::get('/instagram/create', 'InstagramController@create')->middleware('auth')->middleware('AdminMiddleware');
    Route::post('/instagram', 'InstagramController@store')->middleware('auth')->middleware('AdminMiddleware');
    Route::get('/instagram/{instagram}/edit', 'InstagramController@edit')->middleware('auth')->middleware('AdminMiddleware');
    Route::patch('/instagram/{instagram}', 'InstagramController@update')->middleware('auth')->middleware('AdminMiddleware');
    Route::delete('/instagram/{instagram}', 'InstagramController@destroy')->middleware('auth')->middleware('AdminMiddleware');

    //Courses Routes
    Route::get('/courses', 'CourseController@index');
    Route::get('/course/create', 'CourseController@create')->middleware('auth')->middleware('AdminMiddleware');
    Route::post('/course', 'CourseController@store')->middleware('auth')->middleware('AdminMiddleware');
    Route::get('/course/{course}', 'CourseController@show');
    Route::get('/course/{course}/edit', 'CourseController@edit')->middleware('auth')->middleware('AdminMiddleware');
    Route::patch('/course/{course}', 'CourseController@update')->middleware('auth')->middleware('AdminMiddleware');
    Route::post('/course/{course}', 'CourseController@inactive')->middleware('auth')->middleware('AdminMiddleware');
    Route::get('/courseActive/{course}', 'CourseController@active')->middleware('auth')->middleware('AdminMiddleware');
    Route::get('/course/updateStatus/{time}/{status_id}/{fullTime}', 'CourseController@updateStatus');

    //Modules Routes
    Route::get('/module/create', 'CourseController@createModule')->middleware('auth')->middleware('AdminMiddleware');
    Route::post('/module', 'CourseController@storeModule')->middleware('auth')->middleware('AdminMiddleware');
    Route::get('/module/{module}/edit', 'CourseController@editModule')->middleware('auth')->middleware('AdminMiddleware');
    Route::patch('/module/{module}', 'CourseController@updateModule')->middleware('auth')->middleware('AdminMiddleware');
    Route::post('/module/{module}/delete', 'CourseController@destroyModule')->middleware('auth')->middleware('AdminMiddleware');

    //Lessons Routes
    Route::get('/lesson/create', 'CourseController@createLesson')->middleware('auth')->middleware('AdminMiddleware');
    Route::post('/lesson', 'CourseController@storeLesson')->middleware('auth')->middleware('AdminMiddleware');
    Route::get('/lesson/{lesson}/edit', 'CourseController@editLesson')->middleware('auth')->middleware('AdminMiddleware');
    Route::patch('/lesson/{lesson}', 'CourseController@updateLesson')->middleware('auth')->middleware('AdminMiddleware');
    Route::post('/lesson/{lesson}/delete', 'CourseController@destroyLesson')->middleware('auth')->middleware('AdminMiddleware');
    Route::get('/lessonPdf/{lesson}', 'CourseController@showPdfLesson')->middleware('auth');

    //Programs Routes
    Route::get('/programs', 'ProgramController@index');
    Route::get('/program/create', 'ProgramController@create')->middleware('auth')->middleware('AdminMiddleware');
    Route::post('/program', 'ProgramController@store')->middleware('auth')->middleware('AdminMiddleware');
    Route::get('/program/{program}', 'ProgramController@show');
    Route::get('/program/{program}/edit', 'ProgramController@edit')->middleware('auth')->middleware('AdminMiddleware');
    Route::patch('/program/{program}', 'ProgramController@update')->middleware('auth')->middleware('AdminMiddleware');
    Route::post('/program/{program}', 'ProgramController@inactive')->middleware('auth')->middleware('AdminMiddleware');
    Route::get('/programActive/{program}', 'ProgramController@active')->middleware('auth')->middleware('AdminMiddleware');
    Route::get('/programPdf/{program}', 'ProgramController@showPdf')->middleware('auth');
    Route::get('/phasePdf/{phase}', 'ProgramController@showPhasePdf')->middleware('auth');

    //Phases Routes
    Route::get('/phase/create', 'ProgramController@createPhase')->middleware('auth')->middleware('AdminMiddleware');
    Route::post('/phase', 'ProgramController@storePhase')->middleware('auth')->middleware('AdminMiddleware');
    Route::get('/phase/{phase}/edit', 'ProgramController@editPhase')->middleware('auth')->middleware('AdminMiddleware');
    Route::patch('/phase/{phase}', 'ProgramController@updatePhase')->middleware('auth')->middleware('AdminMiddleware');
    Route::post('/phase/{phase}/delete', 'ProgramController@destroyPhase')->middleware('auth')->middleware('AdminMiddleware');

    //Sections Routes
    Route::get('/section/create', 'ProgramController@createSection')->middleware('auth')->middleware('AdminMiddleware');
    Route::post('/section', 'ProgramController@storeSection')->middleware('auth')->middleware('AdminMiddleware');
    Route::get('/section/{section}/edit', 'ProgramController@editSection')->middleware('auth')->middleware('AdminMiddleware');
    Route::patch('/section/{section}', 'ProgramController@updateSection')->middleware('auth')->middleware('AdminMiddleware');
    Route::post('/section/{section}/delete', 'ProgramController@destroySection')->middleware('auth')->middleware('AdminMiddleware');

    //Carts Routes
    Route::get('/addToCart', 'CartController@addToCart')->middleware('auth');
    Route::post('/cart', 'CartController@store')->middleware('auth');
    Route::get('/viewCart', 'CartController@viewCart')->middleware('auth');
    Route::delete('/removeProgramFromCart/{program_id}', 'CartController@removeProgramFromCart')->middleware('auth');
    Route::delete('/removeCourseFromCart/{course_id}', 'CartController@removeCourseFromCart')->middleware('auth');
    Route::get('/confirm/{order_id}', 'CartController@confirm')->middleware('auth');
    Route::get('/confirmOrder', 'CartController@storeOrder')->middleware('auth');
    Route::get('/storeMembership', 'MembershipsController@storeOrderMembership');
    Route::get('/confirmMembership/{order_id}', 'MembershipsController@confirm')->middleware('auth');


    //Payments Routes
    Route::get('/invoice', 'PaymentController@invoice')->middleware('auth');
    Route::get('/mastercardCheckout', function () {
        return view('payment.mastercardPayment');
    })->middleware('auth');
    Route::get('/mastercardApproved', 'PaymentController@mastercardApproved')->middleware('auth');
    Route::Any('/benefit-check-out/{order_id}/{counter}', 'PaymentController@benefitRedirect')->name('benefitCheckout');
    Route::Any('/benefit-response', 'PaymentController@benefitResponse');
    Route::Any('/benefit-response-err', 'PaymentController@benefitResponseErr');
    Route::Any('/benefitApproved', 'PaymentController@benefitApproved');
    Route::Any('/benefitDeclined', 'PaymentController@benefitDeclined');

    //Membership Types Routes
    Route::get('/membershipType', 'MembershipTypesController@index')->middleware('auth')->middleware('AdminMiddleware');
    Route::get('/membershipType/{membershipType}/edit', 'MembershipTypesController@edit')->middleware('auth')->middleware('AdminMiddleware');
    Route::patch('/membershipType/{membershipType}', 'MembershipTypesController@update')->middleware('auth')->middleware('AdminMiddleware');
    Route::get('/membershipType/{membershipType}', 'MembershipTypesController@inactive')->middleware('auth')->middleware('AdminMiddleware');
    Route::get('/membershipTypeActive/{membershipType}', 'MembershipTypesController@active')->middleware('auth')->middleware('AdminMiddleware');
});
