<!DOCTYPE html>
<html lang="<?php echo e(__('text.lang')); ?>" dir="<?php echo e(__('text.dir')); ?>">

<?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<body>
<script>
    var msg = '<?php echo e(Session::get('alert')); ?>';
    var exist = '<?php echo e(Session::has('alert')); ?>';
    if (exist) {
        alert(msg);
    }
</script>

<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app" id="main">
        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe">
            <div class="container">
                <div class="cta-inner">
                    <div class="row">
                        <h1 style="text-align: center; float: none"> <?php echo e(__('text.addPhaseBtn')); ?> </h1>
                    </div>

                    <div class="wow fadeInUp" data-wow-delay="0.3s">
                        <form class="forms-form center-form wow zoomIn" action="/phase" method="post"
                              enctype="multipart/form-data">
                            <input type="hidden" name="program_id" id="program_id" value="<?php echo e($program_id); ?>">
                            <div class="phases">
                                <div class="phase phase_wrapper_1">

                                    <input class="mail" type="text" name="title"
                                           placeholder="<?php echo e(__('text.phaseTitle')); ?>"
                                           autocomplete="title" value="<?php echo e(old('title')); ?>">

                                    <p><?php echo e(__('text.phaseOverview')); ?></p>
                                    <textarea class="mail" rows="10" name="overview" style="height: auto;" value="<?php echo e(old('overview')); ?>"></textarea>

                                    <p><?php echo e(__('text.phaseDescription')); ?></p>
                                    <textarea class="mail" rows="10" name="description" style="height: auto;" value="<?php echo e(old('description')); ?>"></textarea>

                                    <input class="mail" type="text" name="videoUrl"
                                           placeholder="<?php echo e(__('text.phaseVideoURL')); ?>"
                                           autocomplete="off" value="<?php echo e(old('videoUrl')); ?>">

                                    <?php $__errorArgs = ['pdf'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> <h5 style="color: red"><?php echo e($message); ?></h5> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                    <label class="mail" style="border: 1.5px solid #babdc2; padding-top: 10px">
                                        <?php echo e(__('text.phasePdf')); ?>

                                        <input type="file" name="p_Pdf" id="pdf"
                                               style="display: inline-block"/>
                                    </label>

                                    <hr style="width: 70%; border-width:1px">

                                    <div class="videos">
                                        <div class="video_wrapper video_1">
                                            <input class="mail" type="text" name="section_videoUrl[]"
                                                   placeholder="<?php echo e(__('text.sectionVideoURL')); ?>"
                                                   autocomplete="off" value="<?php echo e(old('section_videoUrl')); ?>">

                                            <p><?php echo e(__('text.phaseBriefDescription')); ?></p>
                                            <textarea class="mail" rows="10" name="section_description[]" style="height: auto;" value="<?php echo e(old('section_description')); ?>"></textarea>
                                            <div class="row">
                                                <input class="add_video_button addBtn" type="button" onclick="addVideoPhase()" value="<?php echo e(__('text.addNewSection')); ?> +">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php echo csrf_field(); ?>
                            <div>
                                <div>
                                    <button class="submit-button" style="width: 60%;">
                                        <?php echo e(__('text.addPhaseBtn')); ?>

                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        <?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
<?php echo $__env->make('includes.footerScripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>
</html>
<?php /**PATH C:\Users\Maryam Alalawi\inspower\resources\views/programs/createPhase.blade.php ENDPATH**/ ?>