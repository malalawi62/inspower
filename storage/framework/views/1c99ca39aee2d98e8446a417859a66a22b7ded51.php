<html lang="<?php echo e(__('text.lang')); ?>" dir="<?php echo e(__('text.dir')); ?>">

<?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<body>

<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app form" id="main">

        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe" style="min-height: 100%">
            <div class="container">
                <div class="cta-inner">
                    <div class="row">
                        <form action="/videos/<?php echo e($data['video']->id); ?>" method="post">
                            <?php echo method_field('DELETE'); ?>
                            <h1 style="color: #ee2724;"><?php echo e($data['video']->title); ?></h1>
                            <?php if(auth()->guard()->guest()): ?>
                                <?php else: ?>
                                <?php if(auth()->user()->type == 'admin'): ?>
                                    <span class="editDeleteIcons">
                                        <a href="/videos/<?php echo e($data['video']->id); ?>/edit" style="color: #ee2724">
                                            <i class="ion-edit"></i>
                                        </a>
                                        <button onclick="return confirm('Are you sure you want to delete the video?')"
                                            style="color: #ee2724; background-color: transparent; border-width: 0; font-size: 24px">
                                            <i class="ion-android-delete"></i>
                                        </button>
                                    </span>
                                <?php endif; ?>
                                <?php endif; ?>
                        </form>
                    </div>
                    <div class="row" style="padding: 10px">
                        <?php if($data['lang'] == 'en'): ?>
                            <h3  style="color: #ee2724; <?php echo e(__('text.textAlign')); ?>">
                                Category: <a style="color: #364f87; <?php echo e(__('text.textAlign')); ?>" href="https://www.inspower-fitnesscoaching.com/videos?category_id=<?php echo e($data['video']->category->id); ?>"> <?php echo e($data['video']->category->englishName); ?> </a></h3>
                        <?php else: ?>
                            <h3  style="color: #ee2724; <?php echo e(__('text.textAlign')); ?>">
                                الفئة: <a style="color: #364f87; <?php echo e(__('text.textAlign')); ?>" href="https://www.inspower-fitnesscoaching.com/videos?category_id=<?php echo e($data['video']->category->id); ?>"> <?php echo e($data['video']->category->arabicName); ?> </a></h3>

                        <?php endif; ?>
                    </div>
                    <hr style="width: 100%; border-width:1px">
                        <div class="row">
                            <?php if($data['video']->type == 'vimeo'): ?>
                                <iframe class="iframe"
                                        src="https://player.vimeo.com/video/<?php echo e($data['video']->videoId); ?>"
                                        frameborder="0" allowfullscreen></iframe>
                            <?php else: ?>
                                <iframe width="100%" height="100%"
                                        src="https://www.youtube.com/embed/<?php echo e($data['video']->videoId); ?>"
                                        frameborder="0" allowfullscreen ></iframe>
                            <?php endif; ?>
                        </div>
                </div>

                <div class="line"></div>

                <p class="article-body" style="color: #2D322F">
                    <?php echo nl2br($data['video']->description); ?>

                </p>
            </div>
        </div>

        <!-- Footer Section -->
        <?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
<?php echo $__env->make('includes.footerScripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>
</html>
<?php /**PATH C:\Users\Maryam Alalawi\inspower\resources\views/videos/show.blade.php ENDPATH**/ ?>