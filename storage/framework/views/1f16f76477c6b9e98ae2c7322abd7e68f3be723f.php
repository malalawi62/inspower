<html lang="<?php echo e(__('text.lang')); ?>" dir="<?php echo e(__('text.dir')); ?>">
<?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<body>

<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<div class="wrapper">
    <!-- Main Section-->
    <div class="main app" id="main">
        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe" style="height: 90%">
            <div class="container">
                <div class="cta-inner">
                    <div class="row">
                        <h1 style="text-align: center; float: none"> <?php echo e(__('text.login')); ?> </h1>
                    </div>
                    <div class="wow fadeInUp" data-wow-delay="0.3s">
                        <form class="forms-form center-form wow zoomIn" method="POST" action="<?php echo e(route('login')); ?>">

                            <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> <h5 style="color: red;"><?php echo e($message); ?></h5> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            <input class="mail" type="email" name="email" placeholder="<?php echo e(__('text.registerEmail')); ?>"
                                   required autocomplete="email" autofocus value="<?php echo e(old('email')); ?>">

                            <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> <h5 style="color: red;"><?php echo e($message); ?></h5> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            <input id="password" class="mail" type="password" name="password" placeholder="<?php echo e(__('text.registerPassword')); ?>"
                                   autocomplete="new-password">

                            <?php echo csrf_field(); ?>
                            <div class="form-group row mb-0" style="padding-top: 20px">
                                <div class="row">
                                    <button class="submit-button" style="width: 25%; margin-bottom: 15px;">
                                        <?php echo e(__('text.login')); ?>

                                    </button>
                                </div>

                                <div class="row" style="align-content: center">
                                        <?php if(Route::has('password.request')): ?>
                                            <a class="btn-link" href="<?php echo e(route('password.request')); ?>" style="color: #ee2724">
                                                <?php echo e(__('text.forgetPassword')); ?>

                                            </a>
                                        <?php endif; ?>

                                        <a class="btn-link" href="<?php echo e(route('register')); ?>">
                                            <?php echo e(__('text.dontHaveAccount')); ?>

                                        </a>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        <?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->


<!-- Jquery and Js Plugins -->
<?php echo $__env->make('includes.footerScripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

</body>
</html>

<?php /**PATH /Users/maryamal-alawi/inspower/resources/views/auth/login.blade.php ENDPATH**/ ?>