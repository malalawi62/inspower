<html lang="<?php echo e(__('text.lang')); ?>" dir="<?php echo e(__('text.dir')); ?>">

<?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<body>

<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app form" id="main">
        <div class="pricing-section text-center" style="min-height: 90%">
            <div class="container">
                <div class="col-md-12 col-sm-12 nopadding">
                    <div class="pricing-intro">
                        <h1 class="wow fadeInUp" data-wow-delay="0s"> <?php echo e(__('text.allCategoriesTitle')); ?> </h1>
                    </div>
                    <div class="col-sm-12">
                        <?php $__currentLoopData = $data['images']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="table-left wow fadeInUp" data-wow-delay="0.4s">
                            <div class="pricing-details">
                                        <form action="/slideShow/<?php echo e($image->id); ?>" method="post">
                                            <?php echo method_field('DELETE'); ?>
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <img class="show-img wow fadeIn" <?php if($data['lang'] == 'en'): ?>
                                                        src="storage/<?php echo e($image->imageEng); ?>"
                                                        <?php else: ?>
                                                        src="storage/<?php echo e($image->imageAr); ?>"
                                                        <?php endif; ?>>
                                                </div>
                                                <div class="editDeleteIcons">
                                                    <?php if($image->show == "Yes"): ?>
                                                        <a href="/slideShowInactive/<?php echo e($image->id); ?>" title="Hide Image From Home Page" style="color: #364f87">
                                                            <i class="fas fa-toggle-on"></i>
                                                        </a>
                                                    <?php else: ?>
                                                        <a href="/slideShowActive/<?php echo e($image->id); ?>" title="Show Image In Home Page" style="color: #364f87">
                                                            <i class="fas fa-toggle-off"></i>
                                                        </a>
                                                    <?php endif; ?>
                                                    <button onclick="return confirm('Are you sure you want to delete the image?')" style="color: #364f87; background-color: transparent; border-width: 0; font-size: 24px">
                                                        <i class="ion-android-delete"></i>
                                                    </button>
                                                </div>
                                            </div>
                                            <br>
                                            <?php echo csrf_field(); ?>
                                        </form>
                            </div>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        <?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
<?php echo $__env->make('includes.footerScripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>
</html>
<?php /**PATH /Users/maryamal-alawi/inspower/resources/views/slideShow/index.blade.php ENDPATH**/ ?>