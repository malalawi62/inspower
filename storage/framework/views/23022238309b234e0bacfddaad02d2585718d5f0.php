<!DOCTYPE html>
<html lang="<?php echo e(__('text.lang')); ?>" dir="<?php echo e(__('text.dir')); ?>">

<?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<body>

<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app" id="main">
        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe">
            <div class="container">
                <div class="cta-inner">
                    <div class="row">
                        <h1 style="text-align: center; float: none"> <?php echo e(__('text.addNewSection')); ?> </h1>
                    </div>

                    <div class="wow fadeInUp" data-wow-delay="0.3s">
                        <form class="forms-form center-form wow zoomIn" action="/section" method="post"
                              enctype="multipart/form-data">
                            <input type="hidden" name="program_id" id="program_id" value="">
                            <div class="videos">
                                <div class="video_wrapper">
                                    <input class="mail" type="text" name="ps_videoUrl[1][]"
                                           placeholder="<?php echo e(__('text.sectionVideoURL')); ?>"
                                           autocomplete="off" value="<?php echo e(old('ps_videoUrl')); ?>">

                                    <p><?php echo e(__('text.phaseBriefDescription')); ?></p>
                                    <textarea class="mail" rows="10" name="ps_description[1][]" style="height: auto;" value="<?php echo e(old('p_description')); ?>"></textarea>
                                    <div class="row">
                                        <input class="add_video_button addBtn" type="button" onclick="addVideo(1)" value="<?php echo e(__('text.addNewSection')); ?> +">
                                    </div>
                                </div>
                            </div>
                            <?php echo csrf_field(); ?>
                            <div>
                                <div>
                                    <button class="btn btn-primary"
                                            style="background-color: #EC3642; border:0; padding: 10px 40px 10px 40px; width:60%;">
                                        <?php echo e(__('text.addNewSection')); ?>

                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        <?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
<?php echo $__env->make('includes.footerScripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>
</html>
<?php /**PATH C:\Users\ZAINAB ALMOOT\inspower\resources\views/programs/createSection.blade.php ENDPATH**/ ?>