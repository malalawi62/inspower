<html lang="<?php echo e(__('text.lang')); ?>" dir="<?php echo e(__('text.dir')); ?>">

<?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<body>

<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app form" id="main">

        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe" style="min-height: 100%">
            <div class="container">
                <div class="cta-inner">
                    <div class="col-md-12">
                        <form action="/articles/<?php echo e($data['article']->id); ?>" method="post">
                            <?php echo method_field('DELETE'); ?>
                            <div class="row banner-padding">
                                <h1 style="color: #ee2724;"><?php echo e($data['article']->title); ?></h1>
                                <?php if(auth()->guard()->guest()): ?>
                                <?php else: ?>
                                    <?php if(auth()->user()->type == 'admin'): ?>
                                        <div class="banner-padding">
                                            <span class="editDeleteIcons">
                                                <a href="/articles/<?php echo e($data['article']->id); ?>/edit" style="color: #364f87">
                                                    <i class="ion-edit"></i>
                                                </a>
                                                <a href="/sendArticle/<?php echo e($data['article']->id); ?>" style="color: #364f87">
                                                    <i class="ion-android-send"></i>
                                                </a>
                                                <button onclick="return confirm('Are you sure you want to delete the article?')"
                                                        style="color: #364f87; background-color: transparent; border-width: 0; font-size: 24px">
                                                    <i class="ion-android-delete"></i>
                                                </button>
                                            </span>
                                        </div>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                            <?php echo csrf_field(); ?>
                        </form>

                        <div class="row" style="padding: 10px 0 10px 0">
                            <?php if($data['lang'] == 'en'): ?>
                                <h3 style="color: #ee2724; <?php echo e(__('text.textAlign')); ?>"> Category: <a style="color: #364f87;" href="https://www.inspower-fitnesscoaching.com/articles?category_id=<?php echo e($data['article']->category->id); ?>"> <?php echo e($data['article']->category->englishName); ?> </a> </h3>
                            <?php else: ?>
                                <h3 style="color: #ee2724; <?php echo e(__('text.textAlign')); ?>"> الفئة:<a style="color: #364f87;" href="https://www.inspower-fitnesscoaching.com/articles?category_id=<?php echo e($data['article']->category->id); ?>"> <?php echo e($data['article']->category->arabicName); ?> </a> </h3>
                            <?php endif; ?>
                        </div>

                        <div class="row" style="padding: 10px 0 10px 0">
                            <?php if($data['article']->writerName != null): ?>
                                <h3 style="color: #ee2724; <?php echo e(__('text.textAlign')); ?>">Written
                                    By: <span style="color: #364f87"> <?php echo e($data['article']->writerName); ?></span></h3>
                            <?php else: ?>
                                <?php if($data['lang'] == 'en'): ?>
                                    <h3 style="color: #ee2724; <?php echo e(__('text.textAlign')); ?>">Written
                                        By: <span style="color: #364f87"> <?php echo e($data['article']->writer->englishName); ?> </span></h3>
                                <?php else: ?>
                                    <h3 style="color: #ee2724; <?php echo e(__('text.textAlign')); ?>">
                                        <span style="color: #364f87">الكاتب: <?php echo e($data['article']->writer->arabicName); ?> </span></h3>
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>

                        <div class="row" style="padding: 10px 0 10px 0">
                            <?php if($data['lang'] == 'en'): ?>
                                <h3 style="color: #ee2724; <?php echo e(__('text.textAlign')); ?>">
                                    Date: <span style="color: #364f87"> <?php echo e($data['article']->date); ?> </span></h3>
                            <?php else: ?>
                                <h3 style="color: #ee2724; <?php echo e(__('text.textAlign')); ?>">
                                    <span style="color: #364f87">التاريخ: <?php echo e($data['article']->date); ?> </span></h3>
                            <?php endif; ?>
                        </div>
                    </div>

                    <hr style="width: 100%; border-width:1px">

                    <div class="form wow fadeInUp" data-wow-delay="0.3s">
                            <img class="img-responsive wow fadeIn" src="<?php echo e(asset('storage/'.$data['article']->image)); ?>"
                                 alt="<?php echo e($data['article']->title); ?>" style="max-height: 500px; display: block"/>
                    </div>

                    <div class="line"></div>

                <p class="article-body" style="color: #2D322F"> <?php echo nl2br($data['article']->body); ?>

                </div>
            </div>
        </div>

        <!-- Footer Section -->
        <?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
<?php echo $__env->make('includes.footerScripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>
</html>
<?php /**PATH C:\Users\ZAINAB ALMOOT\inspower\resources\views/articles/show.blade.php ENDPATH**/ ?>