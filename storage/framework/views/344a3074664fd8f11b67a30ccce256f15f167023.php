<div class="search-bar text-center">

    <form action="/search" method="get" role="search" class="">
        <?php echo e(csrf_field()); ?>

        <div>
            <input type="text" class="mail" name="q"
                   placeholder="<?php echo e(__('text.search')); ?>">

            <?php if(auth()->guard()->guest()): ?>
                <button title="<?php echo e(__("text.search")); ?>" type="submit" class="mail" style="width: 12%; font-size: 20px;">
                    <i class="ion-search"></i>
                </button>

                <button title="<?php echo e(__("text.login")); ?>" class="mail" style="width: 12%; color: #ee2724" formaction="<?php echo e(route('login')); ?>">
                    <i class="ion-person"></i>
                </button>

                <?php if((App::isLocale('ar'))): ?>
                    <button title="<?php echo e(__("text.english")); ?>" class="mail" style="width: 12%;" formaction="/setlocale/en">
                        <i class="ion-ios-world-outline"></i>
                    </button>
                <?php endif; ?>
                <?php if((App::isLocale('en'))): ?>
                    <button title="<?php echo e(__("text.arabic")); ?>" class="mail" style="width: 12%;" formaction="/setlocale/ar">
                        <i class="ion-ios-world-outline"></i>
                    </button>
                <?php endif; ?>
            <?php else: ?>
                <?php
                    $cartData = \App\cart::where('user_id', auth()->user()->id)->get();
                    $count = count($cartData);
                ?>
                <button title="<?php echo e(__("text.search")); ?>" type="submit" class="mail" style="width: 10%; font-size: 20px;">
                    <i class="ion-search"></i>
                </button>

               <button title="<?php echo e(__("text.account")); ?>" class="mail" style="width: 10%;" formaction="<?php echo e(url('/profile/'.auth()->user()->id)); ?>">
                    <i class="ion-person" ></i>
                </button>








                <button title="<?php echo e(__("text.logout")); ?>" class="mail" style="width: 10%; font-size: 20px" formaction="<?php echo e(route('logout')); ?>" formmethod="post">
                    <i class="ion-log-out" ></i>
                </button>

                <!--<div class="dropdown">
                    <button class="btn" formaction="<?php echo e(url('/profile/'.auth()->user()->id)); ?>" style="width: 10%; font-size: 20px">
                        <i class="ion-android-settings"></i>
                    </button>
                    <div class="dropdown-content">
                        <a href="<?php echo e(url('/profile/'.auth()->user()->id)); ?>"><i class="ion-person" ></i><?php echo e(__("text.profile")); ?></a>
                        <a href="<?php echo e(route('logout')); ?>"><i class="ion-log-out" ></i><?php echo e(__("text.logout")); ?></a>
                    </div>
                </div> -->

                <?php if((App::isLocale('ar'))): ?>
                    <button title="<?php echo e(__("text.english")); ?>" class="mail" style="width: 10%;" formaction="/setlocale/en">
                        <i class="ion-ios-world-outline"></i>
                    </button>
                <?php endif; ?>
                <?php if((App::isLocale('en'))): ?>
                    <button title="<?php echo e(__("text.arabic")); ?>" class="mail" style="width: 10%;" formaction="/setlocale/ar">
                        <i class="ion-ios-world-outline"></i>
                    </button>
                <?php endif; ?>
            <?php endif; ?>
        </div>
    </form>
</div>
<?php /**PATH /Users/maryamal-alawi/inspower/resources/views/layout/searchBar.blade.php ENDPATH**/ ?>