<?php if($paginator->hasPages()): ?>
    <nav dir='rtl'>
        <ul class="pagination">
            
            <?php if($paginator->onFirstPage()): ?>
                <li style="float: right;" class="page-item disabled" aria-disabled="true" aria-label="<?php echo app('translator')->get('pagination.next'); ?>">
                    <span class="page-link" aria-hidden="true" style="color: #8c0000 ;border-color: #8c0000">&lsaquo;</span>
                </li>
            <?php else: ?>
                <li style="float: right;" class="page-item">
                    <a class="page-link" href="<?php echo e($paginator->previousPageUrl()); ?>" rel="next" aria-label="<?php echo app('translator')->get('pagination.next'); ?>"style="color: #8c0000 ;border-color: #8c0000">&lsaquo;</a>
                </li>
            <?php endif; ?>

            
            <?php $__currentLoopData = $elements; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $element): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                
                <?php if(is_string($element)): ?>
                    <li style="float: right;" class="page-item disabled" aria-disabled="true"><span class="page-link" style="color: #8c0000 ;border-color: #8c0000"><?php echo e($element); ?></span></li>
                <?php endif; ?>

                
                <?php if(is_array($element)): ?>
                    <?php $__currentLoopData = $element; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $page => $url): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($page == $paginator->currentPage()): ?>
                            <li style="float: right;" class="page-item active" aria-current="page"><span class="page-link" style="color: #8c0000 ;border-color: #8c0000; background-color: #B2C6BC;"><?php echo e($page); ?></span></li>
                        <?php else: ?>
                            <li style="float: right;"class="page-item"><a class="page-link" href="<?php echo e($url); ?>" style="color: #8c0000 ;border-color: #8c0000"><?php echo e($page); ?></a></li>
                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            
            <?php if($paginator->hasMorePages()): ?>
                <li style="float: right;"class="page-item">
                    <a class="page-link" href="<?php echo e($paginator->nextPageUrl()); ?>" rel="prev" aria-label="<?php echo app('translator')->get('pagination.previous'); ?>" style="color: #8c0000 ;border-color: #8c0000">&rsaquo;</a>
                </li>
            <?php else: ?>
                <li style="float: right;"class="page-item disabled" aria-disabled="true" aria-label="<?php echo app('translator')->get('pagination.previous'); ?>">
                    <span class="page-link" aria-hidden="true" style="color: #8c0000 ;border-color: #8c0000">&rsaquo;</span>
                </li>
            <?php endif; ?>
        </ul>
    </nav>
<?php endif; ?>
<?php /**PATH C:\Users\ZAINAB ALMOOT\inspower\vendor\laravel\framework\src\Illuminate\Pagination/resources/views/bootstrap-4.blade.php ENDPATH**/ ?>