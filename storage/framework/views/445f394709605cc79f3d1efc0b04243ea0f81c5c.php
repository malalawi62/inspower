<!DOCTYPE html>
<html lang="<?php echo e(__('text.lang')); ?>" dir="<?php echo e(__('text.dir')); ?>">

<?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<body>
<script>
    var msg = '<?php echo e(Session::get('alert')); ?>';
    var exist = '<?php echo e(Session::has('alert')); ?>';
    if (exist) {
        alert(msg);
    }
</script>
<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app form" id="main">
<<<<<<< HEAD
        <img class="img-responsive" src="<?php echo e(__('text.bannerImg')); ?>" alt="inspower">
=======

        <img class="img-responsive banner-padding" src="<?php echo e(__('text.bannerImg')); ?>" alt="inspower">
>>>>>>> 68063f6772344442635537c26f6420f83f85c557

        <!-- About Section -->
        <div class="split-features" id="about">
            <div class="col-md-6 nopadding">
                <div class="split-image">
                    <img class="img-responsive wow fadeIn" src="images/about.jpg" alt="about Us"/>
                </div>
            </div>
            <div class="col-md-6 nopadding">
                <div class="split-content">
                    <h1 class="wow fadeInUp"> <?php echo e(__('text.aboutTitle')); ?> </h1>
                    <p class="wow fadeInUp">
                        <?php echo nl2br(__('text.aboutText')); ?>

                    </p>
                </div>
            </div>
        </div>

        <!-- Teams Section -->
        <div class="pricing-section text-center" id="team">
            <div class="container">
                <div class="col-md-12 col-sm-12 nopadding">
                    <div class="pricing-intro">
                        <h1 class="wow fadeInUp" data-wow-delay="0s"><?php echo e(__('text.teamTitle')); ?></h1>
                    </div>
                    <div class="">
                        <div class="table-left wow fadeInUp" data-wow-delay="0.4s">
                            <div class="pricing-details">
                                <div style="display: inline-block">
                                    <span class="span"><?php echo e(__('text.talalName')); ?></span>
                                    <div class="col-sm-4">
                                        <img class="img-team wow fadeIn img-circle" src="images/Talal_Nabeel.jpeg"
                                             style="width: 90%"/>
                                    </div>
                                    <div class="col-sm-8">
                                        <h2><?php echo nl2br(__('text.talalInfo')); ?></h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="">
                        <div class="table-left wow fadeInUp" data-wow-delay="0.4s">
                            <div class="pricing-details">
                                <div style="display: inline-block">
                                    <span class="span"><?php echo e(__('text.monaName')); ?></span>
                                    <div class="col-sm-4">
                                        <img class="img-team wow fadeIn img-circle" src="images/Mona_Faraj.jpeg"
                                             style="width: 90%"/>
                                    </div>
                                    <div class="col-sm-8">
                                        <h2><?php echo nl2br(__('text.monaInfo')); ?></h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="">
                        <div class="table-left wow fadeInUp" data-wow-delay="0.4s">
                            <div class="pricing-details">
                                <div style="display: inline-block">
                                    <span class="span"><?php echo e(__('text.hasanName')); ?></span>
                                    <div class="col-sm-4">
                                        <img class="img-team wow fadeIn img-circle" src="images/Hasan_Aradi.jpeg"
                                             style="width: 90%"/>
                                    </div>
                                    <div class="col-sm-8">
                                        <h2><?php echo nl2br(__('text.hasanInfo')); ?></h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Services Section -->
        <div class="split-features" id="services"
<<<<<<< HEAD
             style="background: linear-gradient(to right, rgba(236, 54, 66, 0.9), rgba(252, 116, 125, 0.5)), url(<?php echo e(asset('images/services.jpg')); ?>) no-repeat center center;
=======
             style="background: linear-gradient(to right, rgba(140, 0, 0, 0.9), rgba(140, 0, 0, 0.3)), url(<?php echo e(asset('images/services.jpg')); ?>) no-repeat center center;
>>>>>>> 68063f6772344442635537c26f6420f83f85c557
                 background-size: cover;">
            <div class="col-md-12 nopadding">
                <div class="split-content">
                    <h1 class="wow fadeInUp" data-wow-delay="0s"><?php echo e(__('text.servicesTitle')); ?></h1>
                    <h2 class="wow fadeInUp" data-wow-delay="0s"><?php echo e(__('text.personalTitle')); ?></h2>
                    <h3 class="wow fadeInUp" data-wow-delay="0.2s"><?php echo e(__('text.personal1')); ?></h3>
                    <h3 class="wow fadeInUp" data-wow-delay="0.2s"><?php echo e(__('text.personal2')); ?></h3>
                    <h3 class="wow fadeInUp" data-wow-delay="0.2s"><?php echo e(__('text.personal3')); ?></h3>
                    <hr/>
                    <h2 class="wow fadeInUp" data-wow-delay="0s"><?php echo e(__('text.otherTitle')); ?></h2>
                    <h3 class="wow fadeInUp" data-wow-delay="0.2s"><?php echo e(__('text.other1')); ?></h3>
                    <h3 class="wow fadeInUp" data-wow-delay="0.2s"><?php echo e(__('text.other2')); ?></h3>
                    <h3 class="wow fadeInUp" data-wow-delay="0.2s"><?php echo e(__('text.other3')); ?></h3>
                </div>
            </div>
        </div>

        <!-- Contact Section -->
        <div class="container" style="background-color: #2D322F; width: 100%; padding-top: 80px; padding-bottom: 80px"
             id="contact">
            <div class="col-sm-6" style="padding-top: 30px">
                <div class="pricing-intro">
                    <h1 class="wow fadeInUp" data-wow-delay="0s" style="text-align: center"><?php echo e(__('text.contactTitle')); ?></h1>
                </div>
                <div style="padding-left: 20px; padding-right: 20px">
                    <h2 style="color: #FFF; text-align: center">
                        Inspower Fitness Coaching <br>
                        Building 1226, Block 351, Road 5124, <br>
                        Manama, Kingdom of Bahrain<br> <br>
                        <a href="https://wa.me/97339188159" style="color:#FFF">Tel: (+973) 39188159</a> <br> <br>
                        Email: inspowerpt@gmail.com
                    </h2>
                    <h2 style="color: #FFF; text-align: center; padding-top: 30px; padding-bottom: 30px; font-weight: 700; font-size: 22px"> <?php echo e(__('text.followTitle')); ?> </h2>
                    <ul>
                        <li style="text-align: center"><i class="ion-social-instagram"
                                                          style="color: #FFF; font-size: 18px; vertical-align:center; text-align: center">
                                <a href="https://www.instagram.com/inspower_pt/" style="color:#FFF">inspower_pt</a></i>
                        </li>
                    </ul>
                </div>

            </div>
            <div class="col-sm-6" style="padding-top: 30px">
                <div class="pricing-intro">
                    <h1 class="wow fadeInUp" data-wow-delay="0s" style="text-align: center"><?php echo e(__('text.locationTitle')); ?></h1>
                    <div id="map"></div>
                </div>
            </div>

        </div>

        <!-- Subscribe Form -->
        <div class="subscribe-section no-color" id="subscribe">
            <div class="container">
                <div class="cta-inner">
                    <div class="form wow fadeInUp" data-wow-delay="0.3s">
                        <form class="subscribe-form center-form wow zoomIn" action="/subscribe" method="post">
                            <input class="mail" type="text" name="email" placeholder="<?php echo e(__('text.inputPlaceholder')); ?>"
                                   autocomplete="off" id="email" style="color: #FFF">
                            <?php echo csrf_field(); ?>
                            <button class="submit-button">
                                <?php echo e(__('text.button')); ?>

                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        <?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
<?php echo $__env->make('includes.footerScripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>
</html>
<?php /**PATH C:\Users\Maryam\inspower\resources\views/welcome.blade.php ENDPATH**/ ?>