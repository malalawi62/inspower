<html lang="<?php echo e(__('text.lang')); ?>" dir="<?php echo e(__('text.dir')); ?>">

<?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<body>

<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app form" id="main">

        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe" style="min-height: 100%">
            <div class="container">
                <div class="cta-inner">
                    <div class="form wow fadeInUp" data-wow-delay="0.3s">
                        <div class="row banner-padding" style="padding-bottom: 20px">
                            <?php if($user->gender == 'f'): ?>
                                <?php if($membership == 'year' || $membership == 'month'): ?>
                                    <div class="profile-header-img">
                                        <img class="img-responsive img-circle wow fadeIn" src="<?php echo e(asset('storage/female_avatar.png')); ?>"
                                             alt=""/>
                                        <!-- badge -->
                                        <div class="rank-label-container">
                                            <span class="label label-default rank-label"><?php echo e(__("text.premiumUser")); ?></span>
                                        </div>
                                    </div>
                                <?php else: ?>
                                    <div class="profile-header-img">
                                        <img class="img-responsive img-circle wow fadeIn" style=" <?php if($membership != 'year' || $membership != 'month'): ?> border: 2px solid #ee2724;<?php endif; ?>" src="<?php echo e(asset('storage/female_avatar.png')); ?>"
                                             alt=""/>
                                        <div class="rank-label-container">
                                            <span class="label label-default rank-label" style="background-color: #ee2724"><?php echo e(__("text.freeUser")); ?></span>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            <?php else: ?>
                                <?php if($membership == 'year' || $membership == 'month'): ?>
                                    <div class="profile-header-img">
                                        <img class="img-responsive img-circle wow fadeIn" src="<?php echo e(asset('storage/male_avatar.png')); ?>"
                                             alt="">
                                        <div class="rank-label-container">
                                            <span class="label label-default rank-label"><?php echo e(__("text.premiumUser")); ?></span>
                                        </div>
                                    </div>
                                <?php else: ?>
                                    <div class="profile-header-img">
                                        <img class="img-responsive img-circle wow fadeIn" style="border: 2px solid #ee2724;"src="<?php echo e(asset('storage/male_avatar.png')); ?>"
                                             alt=""/>
                                        <!-- badge -->
                                        <div class="rank-label-container">
                                            <span class="label label-default rank-label"  style="background-color: #ee2724"><?php echo e(__("text.freeUser")); ?></span>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            <!--<img class="img-responsive wow fadeIn" src="<?php echo e(asset('storage/male_avatar.png')); ?>"
                                                 alt="" style="max-height: 200px; display: block"/>-->
                            <?php endif; ?>
                        </div>
                    </div>

                    <hr style="width: 100%; border-width:1px">

                    <form class="subscribe-form center-form wow zoomIn" method="POST" action="/profile/<?php echo e($user->id); ?>">
                        <?php echo method_field('PATCH'); ?>

                        <?php $__errorArgs = ['name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> <h5 style="color: red;"><?php echo e($message); ?></h5> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                        <input class="mail" style="margin-bottom: 5px" type="text" name="name" placeholder="<?php echo e(__('text.registerName')); ?>"
                               required autocomplete="name" autofocus value="<?php echo e($user->name); ?>">

                        <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> <h5 style="color: red;"><?php echo e($message); ?></h5> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                        <input class="mail" style="margin-bottom: 5px" type="email" name="email" placeholder="<?php echo e(__('text.registerEmail')); ?>"
                               required autocomplete="email" value="<?php echo e($user->email); ?>">

                        <?php $__errorArgs = ['DOB'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> <h5 style="color: red;"><?php echo e($message); ?></h5> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                        <input class="mail" style="margin-bottom: 5px" type="date" name="DOB" id="DOB"
                               required value="<?php echo e($user->DOB); ?>">

                        <div>
                            <button class="submit-button" style="width: 50%; margin-top: 30px; margin-bottom: 15px">
                                <?php echo e(__('text.editProfile')); ?>

                            </button>
                        </div>
                        <?php echo csrf_field(); ?>
                    </form>

                </div>
            </div>
        </div>

        <!-- Footer Section -->
        <?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
<?php echo $__env->make('includes.footerScripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>
</html>
<?php /**PATH /Users/maryamal-alawi/inspower/resources/views/users/editProfile.blade.php ENDPATH**/ ?>