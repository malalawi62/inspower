<html lang="<?php echo e(__('text.lang')); ?>" dir="<?php echo e(__('text.dir')); ?>">

<?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<body>

<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="wrapper" style="background-color: #2D322F">
    <!-- Main Section-->
    <div class="main app form" id="main">
        <div class="pricing-section text-center" style="min-height: 100%">
            <div class="container">
                <div class="col-md-12 col-sm-12 banner-padding text-center" style="float: none">

                    <div class="pricing-intro">
                        <h1 class="wow fadeInUp" data-wow-delay="0s"> <?php echo e(__('text.someStatistics')); ?> </h1>
                    </div>

                    <div class="table-left wow fadeInUp" data-wow-delay="0.4s" style="overflow: auto; border-radius: 10px; border: none">
                        <div class="pricing-details">
                            <div style="display: block">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <h1 class="h1"> <?php echo e(__('text.allUsers')); ?>: <?php echo e($females+$males); ?> </h1>
                                        </div>
                                        <div class="col-md-4">
                                            <h1 class="h1"> <?php echo e(__('text.female')); ?>: <?php echo e($females); ?> <span style="color: #ee2724;"> (<?php echo e(number_format((float)$f_percentage, 2, '.', '')); ?>%) </span></h1>
                                        </div>
                                        <div class="col-md-4">
                                            <h1 class="h1"> <?php echo e(__('text.male')); ?>: <?php echo e($males); ?> <span style="color: #ee2724;"> (<?php echo e(number_format((float)$m_percentage, 2, '.', '')); ?>%) </span></h1>
                                        </div>
                                    </div>

                                    <hr style="width: 100%; border-width:1px;">

                                    <div class="row">
                                        <div class="col-md-4">
                                            <h1 class="h1"> <?php echo e(__('text.less20')); ?>: <?php echo e($lessThanTwenties); ?> <span style="color: #ee2724;"> (<?php echo e(number_format((float)$pl, 2, '.', '')); ?>%) </span> </h1>
                                        </div>
                                        <div class="col-md-4">
                                            <h1 class="h1"> <?php echo e(__('text.20s')); ?>: <?php echo e($twenties); ?> <span style="color: #ee2724;"> (<?php echo e(number_format((float)$pt, 2, '.', '')); ?>%) </span></h1>
                                        </div>
                                        <div class="col-md-4">
                                            <h1 class="h1"> <?php echo e(__('text.30s')); ?>: <?php echo e($thirties); ?> <span style="color: #ee2724;"> (<?php echo e(number_format((float)$pth, 2, '.', '')); ?>%) </span></h1>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <h1 class="h1"> <?php echo e(__('text.40s')); ?>: <?php echo e($forties); ?> <span style="color: #ee2724;"> (<?php echo e(number_format((float)$pf, 2, '.', '')); ?>%) </span> </h1>
                                        </div>
                                        <div class="col-md-6">
                                            <h1 class="h1"> <?php echo e(__('text.greater40')); ?>: <?php echo e($greaterThanForties); ?> <span style="color: #ee2724;"> (<?php echo e(number_format((float)$pg, 2, '.', '')); ?>%) </span></h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr style="width: 100%; border-width:2px;">

                    <div class="pricing-intro">
                        <h1 class="wow fadeInUp" data-wow-delay="0s"> <?php echo e(__('text.registeredInfo')); ?> </h1>
                    </div>

                    <div class="table-left wow fadeInUp" data-wow-delay="0.4s" style="overflow: auto;">
                        <div class="pricing-details">
                            <div style="display: block">
                                <div class="col-md-12">
                                    <?php if($users != null): ?>
                                        <?php $counter = 0; ?>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <h1 class="h1" style="color: #ee2724;"> </h1>
                                            </div>
                                            <div class="col-md-4">
                                                <h1 class="h1" style="color: #ee2724;"> <?php echo e(__('text.registerName')); ?> </h1>
                                            </div>
                                            <div class="col-md-4">
                                                <h1 class="h1" style="color: #ee2724;"> <?php echo e(__('text.registerEmail')); ?> </h1>
                                            </div>
                                            <div class="col-md-2">
                                                <h1 class="h1" style="color: #ee2724;"> <?php echo e(__('text.DOB')); ?> </h1>
                                            </div>
                                        </div>

                                            <hr style="width: 100%; border-width:1px;">

                                        <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php $counter ++; ?>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <h1 class="h1"> <?php echo e($counter); ?> </h1>
                                                </div>
                                                <div class="col-md-4">
                                                    <h1 class="h1"> <?php echo e($user->name); ?> </h1>
                                                </div>
                                                <div class="col-md-4">
                                                    <h1 class="h1"> <?php echo e($user->email); ?> </h1>
                                                </div>
                                                <div class="col-md-42">
                                                    <h1 class="h1"> <?php echo e($user->DOB); ?> </h1>
                                                </div>
                                            </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
</div>
<!-- Jquery and Js Plugins -->
<?php echo $__env->make('includes.footerScripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>
</html>

<?php /**PATH C:\Users\ZAINAB ALMOOT\inspower\resources\views/users/users.blade.php ENDPATH**/ ?>