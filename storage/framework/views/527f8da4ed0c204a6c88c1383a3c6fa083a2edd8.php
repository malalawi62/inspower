<?php
// init the resource
$ch = curl_init();
$merchant_id = "TEST100067231";
$PWD = "8d4ec52a5782a2e4fbbf32625236e873";
$currency_code = "BHD";
$merchant_name = "INSPOWER FITNESS COACHING";
$logo = "https://inspower-fitnesscoaching.com/images/payment-logo.png";
$lang= __('text.lang');

if ($courseTitles != null && $programTitles != null)
    $description = "Courses: " . $courseTitles . " || Programs: " . $programTitles;
else if ($courseTitles != null && $programTitles == null)
    $description = "Courses: " . $courseTitles;
else if ($courseTitles == null && $programTitles != null)
    $description = "Programs: " . $programTitles;

$post_data = array("apiOperation" => "CREATE_CHECKOUT_SESSION",
    "order" => array("id" => $order_id,"currency"=>$currency_code),
    'interaction' => array('operation' => "PURCHASE"));

$data_string = json_encode($post_data);
curl_setopt($ch, CURLOPT_USERPWD, "merchant.$merchant_id:$PWD");
curl_setopt($ch, CURLOPT_URL,"https://afs.gateway.mastercard.com/api/rest/version/57/merchant/$merchant_id/session");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_TIMEOUT, 30);
curl_setopt($ch, CURLOPT_POSTFIELDS,$data_string);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data_string))
);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);

$content = curl_exec($ch);
$output = json_decode($content, true);

//dd($order_id);

?>
<html lang="<?php echo e(__('text.lang')); ?>" dir="<?php echo e(__('text.dir')); ?>">

<?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<head>
    <script src="https://afs.gateway.mastercard.com/checkout/version/57/checkout.js"
            data-error="errorCallback"
            data-cancel="cancelCallback"
            data-beforeRedirect="Checkout.saveFormFields"
            data-afterRedirect="Checkout.restoreFormFields"
            data-timeout="timeoutCallback"
            data-complete="completeCallback">
    </script>

    <script type="text/javascript">
        function errorCallback(error) {
            console.log(JSON.stringify(error));
        }

        function cancelCallback() {
            confirm('Are you sure you want to cancel?');
            console.log('Payment cancelled');
        }

        timeoutCallback = "https://www.test.inspower-fitnesscoaching.com/viewCart";
        completeCallback = "https://www.test.inspower-fitnesscoaching.com/mastercardApproved?order_id=<?php echo $order_id; ?>&description=<?php echo $description; ?>"


        Checkout.configure({
            merchant: '<?php echo $merchant_id; ?>',
            session: {
                id: "<?php echo $output['session']['id']; ?>"
            },
            order: {
                amount: '<?php echo $counter; ?>',
                currency: 'BHD',
                description: '<?php echo $description; ?>',
                id: "<?php echo $order_id; ?>",
            },
            interaction: {
                //returnUrl: "https://www.test.inspower-fitnesscoaching.com/paymentState?order_id=<?php echo $order_id; ?>&description=<?php echo $description; ?>&SI=<?php echo $output['successIndicator']; ?>",
                merchant: {
                    name: '<?php echo $merchant_name; ?>',
                    logo : '<?php echo $logo; ?>',
                    url: "https://inspower-fitnesscoaching.com",
                    email: "inspowerpt@gmail.com",
                    address: {
                        line1: "Building 1226, Block 351, Road 5124",
                        line2: "Manama, Kingdom of Bahrain"
                    }
                },
                displayControl: {
                    billingAddress  : 'HIDE',
                    orderSummary    : 'SHOW'
                }
            }
        });

        //window.onload = Checkout.showPaymentPage();

    </script>
</head>
<html lang="<?php echo e(__('text.lang')); ?>" dir="<?php echo e(__('text.dir')); ?>">

<?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<body>

<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="wrapper" style="background-color: #2D322F">
    <!-- Main Section-->
    <div class="main app form" id="main">
        <div class="pricing-section text-center" style="min-height: 100%">
            <div class="container">
                <div class="col-md-8 col-md-offset-2 col-sm-12 nopadding text-center" style="float: left">
                    <div class="pricing-intro">
                        <h1 class="wow fadeInUp" data-wow-delay="0s"> <?php echo e(__('text.confirmTitle')); ?> </h1>
                    </div>

                    <?php if($courses != null || $programs != null): ?>
                        <div class="table-left wow fadeInUp" data-wow-delay="0.4s" style="overflow: auto; background-color: rgba(255, 255, 255, 0.8)">
                            <div class="pricing-details">
                                <div style="display: block">
                                    <div class="col-md-12" style="margin-bottom: 15px">
                                        <h1 class="wow fadeInUp" data-wow-delay="0s" style="font-weight: 800; font-size: 24px; color: black"> <?php echo e(__('text.detailsTitle')); ?> </h1>
                                        <?php if($courses != null): ?>
                                            <div class="row">
                                                <span class="video-title">
                                                    <h1 style="color: black; font-size:20px; font-weight: 800"> <?php echo e(__('text.courses')); ?>: </h1>
                                                </span>
                                            </div>

                                            <?php $__currentLoopData = $courses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $course): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <div class="row">
                                                    <div class="col-md-8">
                                                    <span class="video-title">
                                                        <h1 style="color: black; font-size:20px">
                                                            <?php echo e($course->title); ?>

                                                        </h1>
                                                    </span>
                                                    </div>

                                                    <div class="col-md-3">
                                                    <span class="video-title">
                                                        <h1 style="color: black; font-size:20px;"> <?php echo e($course->price); ?> <?php echo e(__('text.BD')); ?> </h1>
                                                    </span>
                                                    </div>
                                                </div>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <hr style="width: 100%; border-width:0; border-color: rgba(0, 0, 0, 0.2)">
                                        <?php endif; ?>

                                        <?php if($programs != null): ?>
                                            <div class="row">
                                                <span class="video-title">
                                                    <h1 style="color: black; font-size:20px; font-weight: 800"> <?php echo e(__('text.programs')); ?>: </h1>
                                                </span>
                                            </div>
                                            <?php $__currentLoopData = $programs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $program): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <div class="row">
                                                    <div class="col-md-8">
                                                    <span class="video-title">
                                                        <h1 style="color: black; font-size:20px">
                                                        <?php echo e($program->title); ?>

                                                        </h1>
                                                    </span>
                                                    </div>

                                                    <div class="col-md-3">
                                                    <span class="video-title">
                                                        <h1 style="color: black; font-size:20px;"> <?php echo e($program ->price); ?> <?php echo e(__('text.BD')); ?> </h1>
                                                    </span>
                                                    </div>
                                                </div>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>

                                    </div>
                                </div>
                                <hr style="width: 100%; border-width:1px; border-color: black">

                                <div class="row">
                                    <div class="col-md-8">
                                            <span class="video-title">
                                                <h1 style="color: red; font-size:20px; font-weight: 800;"> <?php echo e(__('text.cartTotal')); ?></h1>
                                            </span>
                                    </div>

                                    <div class="col-md-3">
                                            <span class="video-title">
                                                <h1 style="color: red; font-size:20px; font-weight: 800;"> <?php echo e($counter); ?> <?php echo e(__('text.BD')); ?> </h1>
                                            </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <hr style="width: 100%; border-width:1px">

                        <div class="row" style="margin-bottom: 15px">
                            <form action="/courses" method="get">
                                <div class="col-md-6">
                                    <button class="btn btn-primary" style="margin-bottom: 10px; background-color: rgba(255, 255, 255, 0.3); border:0; padding: 10px 20px 10px 20px; font-size:22px; width: 100%; color: white">
                                        <?php echo e(__('text.addMoreCourses')); ?>

                                    </button>
                                </div>
                                <?php echo csrf_field(); ?>
                            </form>

                            <form action="/programs" method="get">
                                <div class="col-md-6">
                                    <button class="btn btn-primary" style="background-color: rgba(255, 255, 255, 0.3); border:0; padding: 10px 20px 10px 20px; font-size:22px; width: 100%; color: white">
                                        <?php echo e(__('text.addMorePrograms')); ?>

                                    </button>
                                </div>
                                <?php echo csrf_field(); ?>
                            </form>
                        </div>

                        <div class="row">

                            <div class="col-md-6">
                                <button class="btn btn-primary" style="margin-bottom: 10px; background-color: #fdca00; border:0; font-size:22px; width: 100%" title="Credit Card" onclick="Checkout.showPaymentPage();">
                                    <img src="<?php echo e(asset('images/payment.png')); ?>" style="height:40px">
                                </button>
                            </div>


                            <div class="col-md-6">
                                <a  href="<?php echo e(route('benefitCheckout',['order_id' => $order_id, 'counter' => $counter])); ?>" class="btn btn-primary" style="background-color: #fdca00; border:0; font-size:22px; width: 100%" title="Debit Card">
                                    <img src="<?php echo e(asset('images/benifit.png')); ?>" style="height:40px">
                                </a>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <!-- Footer Section -->
        <?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
<?php echo $__env->make('includes.footerScripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>
</html>
<?php /**PATH C:\Users\Maryam Alalawi\inspower\resources\views/cart/confirmation.blade.php ENDPATH**/ ?>