<html lang="<?php echo e(__('text.lang')); ?>" dir="<?php echo e(__('text.dir')); ?>">

<?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<body>

<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app form" id="main">

        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe" style="min-height: 100%">
            <div class="container">
                <div class="cta-inner">
                    <div class="col-md-12">
                            <form action="/cart" method="post">
                                <input type="hidden" name="program_id" value="<?php echo e($program->id); ?>">
                                <?php if(auth()->guard()->guest()): ?>
                                <?php else: ?>
                                    <input type="hidden" name="user_id" value="<?php echo e(auth()->user()->id); ?>">
                                <?php endif; ?>
                                <div class="row">
                                    <span class="h1" style="color: #fdca00; padding: 5px; width: 100%"><?php echo e($program->title); ?>

                                        <button class="addToCartBtn" type="submit" title="<?php echo e(__("text.addToCartTitle")); ?>">
                                            <i class="ion-android-cart"></i>
                                        </button>
                                    </span>
                                </div>
                                <?php echo csrf_field(); ?>
                            </form>
                        <form action="/course/<?php echo e($program->id); ?>" method="post">
                            <?php echo method_field('DELETE'); ?>
                            <?php if(auth()->guard()->guest()): ?>
                                <?php else: ?>
                                <?php if(auth()->user()->type == 'admin'): ?>
                                    <span class="editDeleteIcons">
                                        <a href="/program/<?php echo e($program->id); ?>/edit" style="color: #fdca00">
                                            <i class="ion-edit"></i>
                                        </a>
                                        <a href="/sendProgram/<?php echo e($program->id); ?>" style="color: #fdca00">
                                            <i class="ion-android-send"></i>
                                        </a>
                                        <!--<button onclick="return confirm('Are you sure you want to delete the program?')"
                                                style="color: #fdca00; background-color: transparent; border-width: 0; font-size: 24px">
                                            <i class="ion-android-delete"></i>
                                        </button>-->
                                    </span>
                                <?php endif; ?>
                            <?php endif; ?>
                        </form>
                    </div>

                    <div class="form wow fadeInUp" data-wow-delay="0.3s">
                        <div class="row" style="padding-bottom: 20px">
                            <img class="img-responsive wow fadeIn" src="<?php echo e(asset('storage/'.$program->image)); ?>"
                                 alt="Talal Nabeel" style="max-height: 500px; display: block"/>
                        </div>
                    </div>

                    <hr style="width: 100%; border-width:1px">

                    <div class="row">
                        <h2 class="h2"> PROGRAM OVERVIEW </h2>
                        <p class="article-body">
                            <?php echo nl2br($program->overview); ?>

                        </p>
                    </div>
                        <?php if($program->videoUrl != null): ?>
                            <div class="row">
                                <iframe class="iframe" src="https://player.vimeo.com/video/<?php echo e($program->videoUrl); ?>"
                                        frameborder="0" allowfullscreen></iframe>
                            </div>
                        <?php endif; ?>
                    <?php if($program->description != null): ?>
                        <hr style="width: 100%; border-width:1px">
                        <div class="row">
                            <h2 class="h2"> PROGRAM DESCRIPTION </h2>
                            <p class="article-body">
                                <?php echo nl2br($program->description); ?>

                            </p>
                        </div>
                    <?php endif; ?>
                    <div id="read" class="showPdf">
                        <h2 class="pdfBody" onclick="showPdf()" style="color: #2D322F;"><?php echo e(__('text.viewPdf')); ?></h2>
                    </div>
                    <div class="pdf" id="pdf" style="display: none">
                    <iframe src="/storage/<?php echo e($program->pdf); ?>#toolbar=0" width="100%" height="500px">
                    </iframe>
                </div>
                    <hr style="width: 100%; border-width:1px">

                    <div>
                        <form action="/cart" method="post">
                            <input type="hidden" name="program_id" value="<?php echo e($program->id); ?>">
                            <?php if(auth()->guard()->guest()): ?>
                            <?php else: ?>
                                <input type="hidden" name="user_id" value="<?php echo e(auth()->user()->id); ?>">
                            <?php endif; ?>
                            <button class="addToCartBtn2" type="submit">
                                <?php echo e(__('text.addToCartBtn')); ?>

                            </button>
                            <?php echo csrf_field(); ?>
                        </form>
                    </div>
                </div>
            </div>

            <section class="why-us" style="display: none">
                <div class="container-fluid" data-aos="fade-up">
                    <div class="row">

                        <div class="justify-content-center align-items-stretch" style="width: 80%; padding: 50px 0; text-align: center; margin: 0 auto;">
                            <div class="content">
                                <h3><strong>PROGRAM PHASES</strong></h3>
                            </div>

                            <div class="accordion-list">
                                <ul>
                                    <li style="background-color: rgba(178, 198, 188, 0.4);">
                                        <a data-toggle="collapse" class="collapse" href="#accordion-list-1"><span class="icofont-id icofont-5x"></span> PHASE 1 <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                                        <div id="accordion-list-1" class="collapse show" data-parent=".accordion-list">
                                            <p>
                                                Zainab Basel Salman Ali Ahmed
                                            </p>
                                            <p>
                                                <b>PROVIDER OF</b><br>
                                                Web design and development
                                            </p>
                                            <p>
                                                <b>LANGUAGES</b><br>
                                                • Arabic – Mother Language<br>
                                                • English – Fluent in writing and speaking
                                            </p>
                                        </div>
                                    </li>

                                    <li style="background-color: rgba(178, 198, 188, 0.4);">
                                        <a data-toggle="collapse" href="#accordion-list-2" class="collapsed"><span class="icofont-dart"></span> OBJECTIVES <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                                        <div id="accordion-list-2" class="collapse" data-parent=".accordion-list">
                                            <p style="text-align: justify;">
                                                To secure a responsible career opportunity where I can fully utilize my training, software development and IT skills, while making a significant contribution to the success of my employer
                                            </p>
                                        </div>
                                    </li>

                                    <li style="background-color: rgba(178, 198, 188, 0.4);">
                                        <a data-toggle="collapse" href="#accordion-list-3" class="collapsed"><span class="icofont-automation"></span> SKILLS <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                                        <div id="accordion-list-3" class="collapse" data-parent=".accordion-list">
                                            <p>
                                                • Fast learner<br>
                                                • Can work under pressure<br>
                                                • Can work in teams<br>
                                                • Very good communication skills<br>
                                                • Problem solving<br>
                                                • Time management<br>
                                                • Hand crafts<br>
                                            </p>
                                        </div>
                                    </li>

                                </ul>
                            </div>

                        </div>

                        <div class="col-lg-5 align-items-stretch order-1 order-lg-2 img" style='background-image: url("assets/img/why-us.png");' data-aos="zoom-in" data-aos-delay="150">&nbsp;</div>
                    </div>

                </div>
            </section>
        </div>

        <!-- Footer Section -->
        <?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
<?php echo $__env->make('includes.footerScripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>
</html>
<?php /**PATH /Users/maryam/inspower/resources/views/programs/show.blade.php ENDPATH**/ ?>