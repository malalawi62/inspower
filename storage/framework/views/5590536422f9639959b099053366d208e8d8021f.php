<html lang="<?php echo e(__('text.lang')); ?>" dir="<?php echo e(__('text.dir')); ?>">

<?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<body>

<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="wrapper" style="background-color: #2D322F">
    <!-- Main Section-->
    <div class="main app form" id="main">
        <div class="pricing-section text-center" style="min-height: 100%">
            <div class="container">
                <div style="width: 100%">
                    <div class="pricing-intro banner-padding">
                        <h1 class="wow fadeInUp" data-wow-delay="0s"> <?php echo e(__('text.allVideosTitle')); ?> </h1>
                        <?php if((App::isLocale('en'))): ?>
                            <h2>For videos in Arabic language <a href="setlocale/ar">press here</a></h2>
                        <?php endif; ?>
                        <?php if((App::isLocale('ar'))): ?>
                            <h2>للمقاطع المرئية باللغة الإنجليزية <a href="setlocale/en">اضغط هنا</a></h2>
                        <?php endif; ?>
                        <div style="padding-top: 20px">
                            <span class="filterTag"> <?php echo e(__('text.filter')); ?> </span>
                            <select name="form" onchange="location = this.value;" class="mail">
                                <option value="https://www.inspower-fitnesscoaching.com/videos"
                                        <?php if($data['cat_id'] == null): ?> selected <?php endif; ?>> <?php echo e(__('text.showFilter')); ?> </option>
                                <?php $__currentLoopData = $data['categories']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($data['lang'] == 'en'): ?>
                                        <option
                                            value="https://www.inspower-fitnesscoaching.com/videos?category_id=<?php echo e($category->id); ?>"
                                            <?php if($category->id == $data['cat_id']): ?> selected <?php endif; ?>> <?php echo e($category->englishName); ?> </option>
                                    <?php else: ?>
                                        <option
                                            value="https://www.inspower-fitnesscoaching.com/videos?category_id=<?php echo e($category->id); ?>"
                                            <?php if($category->id == $data['cat_id']): ?> selected <?php endif; ?>> <?php echo e($category->arabicName); ?> </option>
                                    <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>
                    </div>
                    <?php $__empty_1 = true; $__currentLoopData = $data['videos']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $video): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                        <div class="filterDiv <?php echo e($video->category->englishName); ?>">
                            <div class="table-left wow fadeInUp" data-wow-delay="0.4s"
                                 style="overflow: auto;  <?php if($membership_details == null): ?> background-color:#ccc; <?php endif; ?>">
                                <div class="pricing-details">
                                    <div style="display: block">
                                        <form action="/videos/<?php echo e($video->id); ?>" method="post">
                                            <?php echo method_field('DELETE'); ?>

                                            <div class="col-md-4">
                                                <?php if($video->type == 'vimeo'): ?>
                                                    <iframe width="100%" height="250px"
                                                            style="padding-bottom: 10px; <?php if(!auth() || !$membership): ?> pointer-events: none <?php endif; ?>"
                                                            src="https://player.vimeo.com/video/<?php echo e($video->videoId); ?>"
                                                            frameborder="0" allowfullscreen></iframe>
                                                <?php else: ?>
                                                    <iframe width="100%" height="250px"
                                                            style="padding-bottom: 10px; display: block; <?php if($membership_details == null): ?> pointer-events: none <?php endif; ?>"
                                                            src="https://www.youtube.com/embed/<?php echo e($video->videoId); ?>"
                                                            frameborder="0" allowfullscreen></iframe>
                                                <?php endif; ?>
                                            </div>
                                            <br>
                                            <div class="col-md-8">
                                                <div class="row">
                                                    <?php if(auth()->guard()->guest()): ?>
                                                        <span class="editDeleteIcons">
                                                            <a href="#test-popup-1" class="open-popup-link"
                                                               style="color: #ee2724; font-size: 60px">
                                                                <i class="ion-ios-locked"></i>
                                                            </a>
                                                        </span>

                                                        <?php if($membership_details == null): ?>
                                                            <div id="test-popup-1" class="white-popup mfp-hide">
                                                                <h3 class="exclusive"> <?php echo e(__('text.exclusiveSentence')); ?> </h3>
                                                                <iframe class="iframe-exclusive"
                                                                        src="/registerExclusive"
                                                                        frameborder="0" allowfullscreen></iframe>
                                                            </div>
                                                            <span class="video-title">
                                                            <a href="#test-popup-1" class="open-popup-link"
                                                               style="color: #737373; font-size:20px"> <?php echo e($video->title); ?> </a>
                                                        </span>
                                                        <?php else: ?>
                                                            <span class="video-title">
                                                            <a style="color: #ee2724; font-size:20px"
                                                               href="/videos/<?php echo e($video->id); ?>_<?php echo e(str_replace(' ', '_', $video->title)); ?>">
                                                                <?php echo e($video->title); ?>

                                                            </a>
                                                        </span>
                                                        <?php endif; ?>
                                                    <?php else: ?>
                                                        <?php if(auth()->user()->type == 'admin'): ?>
                                                            <span class="editDeleteIcons">
                                                                <a href="/videos/<?php echo e($video->id); ?>/edit"
                                                                   style="color: #364f87">
                                                                    <i class="ion-edit"></i>
                                                                </a>
                                                                <a href="/sendVideo/<?php echo e($video->id); ?>"
                                                                   style="color: #364f87">
                                                                    <i class="ion-android-send"></i>
                                                                </a>
                                                                <button
                                                                    onclick="return confirm('Are you sure you want to delete the video?')"
                                                                    style="color: #364f87; background-color: transparent; border-width: 0; font-size: 24px">
                                                                    <i class="ion-android-delete"></i>
                                                                </button>
                                                            </span>
                                                            <span class="video-title">
                                                                <a href="/videos/<?php echo e($video->id); ?>_<?php echo e(str_replace(' ', '_', $video->title)); ?>"
                                                                   style="color: #364f87; font-size:20px"> <?php echo e($video->title); ?> </a>
                                                            </span>
                                                            <!-- Signed in with Premium membership -->
                                                        <?php elseif(auth()->user()->type != 'admin' && $membership_details != null): ?>
                                                            <span class="editDeleteIcons">
                                                                <a title="<?php echo e(__('text.premium')); ?>"
                                                                   style="color: #009933; font-size: 60px">
                                                                    <i class="ion-ios-unlocked"></i>
                                                                </a>
                                                            </span>
                                                            <span class="video-title">
                                                                <a href="/videos/<?php echo e($video->id); ?>_<?php echo e(str_replace(' ', '_', $video->title)); ?>"
                                                                   style="color: #364f87; font-size:20px"> <?php echo e($video->title); ?> </a>
                                                            </span>
                                                            <!-- Signed in with free membership -->
                                                        <?php elseif(auth()->user()->type != 'admin' && $membership_details == null): ?>
                                                            <span class="editDeleteIcons">
                                                            <a href="#test-popup-2" class="open-popup-link"
                                                               style="color: #ee2724; font-size: 60px">
                                                                <i class="ion-ios-locked"></i>
                                                            </a>
                                                        </span>
                                                            <div id="test-popup-2" class="white-popup mfp-hide">
                                                                <h3 class="exclusive"> <?php echo e(__('text.exclusiveSentence')); ?> </h3>
                                                                <iframe class="iframe-exclusive"
                                                                        src="/registerUpgrade"
                                                                        frameborder="0" allowfullscreen></iframe>
                                                            </div>
                                                            <span class="video-title">
                                                            <a href="#test-popup-2" class="open-popup-link"
                                                               style="color: #737373; font-size:20px"> <?php echo e($video->title); ?> </a>
                                                        </span>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                </div>
                                                <h5 class="articleCategory"
                                                    <?php if($membership_details == null): ?> style="color: #737373" <?php endif; ?>>
                                                    <?php if($data['lang'] == 'en'): ?>
                                                        Category: <a
                                                            <?php if($membership_details == null): ?> style="color: #737373"
                                                            <?php endif; ?> class="articleCategory"
                                                            href="https://www.inspower-fitnesscoaching.com/videos?category_id=<?php echo e($video->category->id); ?>"> <?php echo e($video->category->englishName); ?> </a>
                                                    <?php else: ?>
                                                        الفئة: <a
                                                            <?php if($membership_details == null): ?> style="color: #737373"
                                                            <?php endif; ?> class="articleCategory"
                                                            href="https://www.inspower-fitnesscoaching.com/videos?category_id=<?php echo e($video->category->id); ?>"> <?php echo e($video->category->arabicName); ?> </a>
                                                    <?php endif; ?>
                                                </h5>
                                                <h2 class="articleBody"
                                                    <?php if($membership_details == null): ?> style="color: #737373" <?php endif; ?>>
                                                    <?php echo nl2br($video->description); ?>

                                                </h2>
                                            </div>
                                            <?php echo csrf_field(); ?>
                                        </form>
                                        <br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                        <h2>No Videos Found</h2>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="container" style="background-color:#2D322F; width: 100%">
            <div class="col-md-12 text-center">
                <?php echo e($data['videos']->links()); ?>

            </div>
        </div>
        <!-- Footer Section -->
        <?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
<?php echo $__env->make('includes.footerScripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<script>
    filterSelection("all")

    function filterSelection(c) {
        var x, i;
        x = document.getElementsByClassName("filterDiv");
        if (c === "all") c = "";
        for (i = 0; i < x.length; i++) {
            w3RemoveClass(x[i], "show");
            if (x[i].className.indexOf(c) > -1) w3AddClass(x[i], "show");
        }
    }

    function w3AddClass(element, name) {
        var i, arr1, arr2;
        arr1 = element.className.split(" ");
        arr2 = name.split(" ");
        for (i = 0; i < arr2.length; i++) {
            if (arr1.indexOf(arr2[i]) == -1) {
                element.className += " " + arr2[i];
            }
        }
    }

    function w3RemoveClass(element, name) {
        var i, arr1, arr2;
        arr1 = element.className.split(" ");
        arr2 = name.split(" ");
        for (i = 0; i < arr2.length; i++) {
            while (arr1.indexOf(arr2[i]) > -1) {
                arr1.splice(arr1.indexOf(arr2[i]), 1);
            }
        }
        element.className = arr1.join(" ");
    }

</script>

</body>
</html>
<?php /**PATH C:\Users\ZAINAB ALMOOT\inspower\resources\views/videos/index.blade.php ENDPATH**/ ?>