<!DOCTYPE html>
<html lang="<?php echo e(__('text.lang')); ?>" dir="<?php echo e(__('text.dir')); ?>">

<?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<body>

<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app" id="main">
        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe">
            <div class="container">
                <div class="cta-inner">
                    <div class="row">
                        <h1 style="text-align: center; float: none"> <?php echo e(__('text.addProgramTitle')); ?> </h1>
                    </div>

                    <div class="wow fadeInUp" data-wow-delay="0.3s">
                        <form class="forms-form center-form wow zoomIn" action="/program" method="post"
                              enctype="multipart/form-data">

                            <?php $__errorArgs = ['title'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> <h5 style="color: red;"><?php echo e($message); ?></h5> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            <input class="mail" type="text" name="title"
                                   placeholder="<?php echo e(__('text.inputProgramTitle')); ?>"
                                   autocomplete="title" value="<?php echo e(old('title')); ?>">

                            <?php $__errorArgs = ['overview'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> <p style="color: red"><?php echo e($message); ?></p> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            <p><?php echo e(__('text.inputProgramOverview')); ?></p>
                            <textarea class="mail" rows="20" name="overview" style="height: auto;" value="<?php echo e(old('overview')); ?>"></textarea>

                            <?php $__errorArgs = ['image'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> <h5 style="color: red"><?php echo e($message); ?></h5> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            <p><?php echo e(__('text.inputProgramImage')); ?></p>
                            <input class="mail" type="file" name="image" id="image"
                                   style="display: inline-block;padding-top: 10px">

                            <?php $__errorArgs = ['price'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> <h5 style="color: red;"><?php echo e($message); ?></h5> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            <input class="mail" type="number" name="price"
                                   placeholder="<?php echo e(__('text.inputProgramPrice')); ?>"
                                   value="<?php echo e(old('price')); ?>">

                            <input class="mail" type="text" name="videoUrl"
                                   placeholder="<?php echo e(__('text.inputProgramVideo')); ?>"
                                   autocomplete="off" value="<?php echo e(old('videoUrl')); ?>">

                            <p><?php echo e(__('text.inputProgramDescription')); ?></p>
                            <textarea class="mail" rows="20" name="description" style="height: auto;" value="<?php echo e(old('description')); ?>"></textarea>


                            <?php $__errorArgs = ['pdf'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> <h5 style="color: red"><?php echo e($message); ?></h5> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            <label class="mail" style="border: 1.5px solid #babdc2; padding-top: 10px">
                               <?php echo e(__('text.inputProgramPdf')); ?>

                                <input type="file" name="pdf" id="bodyPdf"
                                       style="display: inline-block"/>
                            </label>

                            <?php if($lang == 'en'): ?>
                                <input type="hidden" name="language" id="language" value="Eng">
                            <?php else: ?>
                                <input type="hidden" name="language" id="language" value="Ar">
                            <?php endif; ?>


                            <hr style="width: 100%; border-width:1px">

                            <div class="phases">
                                <div class="phase phase_wrapper_1">
                                    <div class="row">
                                        <h1 style="text-align: center; float: none; padding-top: 0px" id="phase_title_1"> <?php echo e(__('text.phaseNew')); ?>

                                            <input class="add_phase_button" type="button" onclick="addPhase()" value="+" style="color: #8c0000; background: transparent; border-color: transparent; border-width: 0">
                                        </h1>
                                    </div>

                                    <?php $__errorArgs = ['p_title'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> <h5 style="color: red;"><?php echo e($message); ?></h5> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                    <input class="mail" type="text" name="p_title[]"
                                           placeholder="<?php echo e(__('text.phaseTitle')); ?>"
                                           autocomplete="title" value="<?php echo e(old('p_title')); ?>">

                                    <p><?php echo e(__('text.phaseOverview')); ?></p>
                                    <textarea class="mail" rows="10" name="p_overview[]" style="height: auto;" value="<?php echo e(old('p_overview')); ?>"></textarea>

                                    <p><?php echo e(__('text.phaseDescription')); ?></p>
                                    <textarea class="mail" rows="10" name="p_description[]" style="height: auto;" value="<?php echo e(old('p_description')); ?>"></textarea>

                                    <input class="mail" type="text" name="p_videoUrl[]"
                                           placeholder="<?php echo e(__('text.phaseVideoURL')); ?>"
                                           autocomplete="off" value="<?php echo e(old('p_videoUrl')); ?>">

                                    <label class="mail" style="border: 1.5px solid #babdc2; padding-top: 10px">
                                        <?php echo e(__('text.phasePdf')); ?>

                                        <input type="file" name="phasePdf[]"
                                               style="display:inline-block">
                                    </label>

                                    <hr style="width: 70%; border-width:1px">

                                    <div class="videos">
                                        <div class="video_wrapper">
                                            <input class="mail" type="text" name="ps_videoUrl[1][]"
                                                   placeholder="<?php echo e(__('text.sectionVideoURL')); ?>"
                                                   autocomplete="off" value="<?php echo e(old('ps_videoUrl')); ?>">

                                            <p><?php echo e(__('text.phaseBriefDescription')); ?></p>
                                            <textarea class="mail" rows="10" name="ps_description[1][]" style="height: auto;" value="<?php echo e(old('p_description')); ?>"></textarea>
                                            <div class="row">
                                                <input class="add_video_button addBtn" type="button" onclick="addVideo(1)" value="<?php echo e(__('text.addNewSection')); ?> +">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php echo csrf_field(); ?>
                            <div>
                                <button class="submit-button" style="width: 60%;">
                                    <?php echo e(__('text.addProgramBtn')); ?>

                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        <?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
<?php echo $__env->make('includes.footerScripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>
</html>
<?php /**PATH C:\Users\Maryam Alalawi\inspower\resources\views/programs/create.blade.php ENDPATH**/ ?>