<html lang="<?php echo e(__('text.lang')); ?>" dir="<?php echo e(__('text.dir')); ?>">

<?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<body>

<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app form" id="main">
        <div class="pricing-section text-center" style="min-height: 100%">
            <div class="container">
                <div style="width: 100%">
                    <div class="banner-padding">
                        <div class="pricing-intro">
                            <h1 class="wow fadeInUp" data-wow-delay="0s" style="margin-top: 30px">PRICING POLICY</h1>
                        </div>
                        <div class="table-left wow fadeInUp" data-wow-delay="0.4s">
                            <div class="pricing-details">
                                <div style="display: inline-block">
                                    <span class="span" style="color: #ee2724;">PRICE</span>
                                    <div class="col-sm-12">
                                        <h2 dir="ltr" style="text-align:left"><?php echo nl2br('All prices displayed on the Website are in Bahraini Dinars and subject to applicable taxes. The price displayed or advertised is subject to change without prior notice and at the sole discretion of INSPOWER Fitness Coaching.

                                            Once you pay successfully for your purchases, you will receive the conformation by email then the materials of the product will be available for you.

                                            THE WEBSITE AND ITS CONTENT SHALL NOT BE CONSIDERED AS AN OFFER TO SELL PRODUCTS OR SERVICES, THEY ARE PROVIDED FOR INFORMATION PURPOSES ONLY.
                                            '); ?>

                                        </h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="pricing-intro">
                        <h1 class="wow fadeInUp" data-wow-delay="0s" style="margin-top: 30px">WARRANTY, REFUND AND EXCHANGE POLICY</h1>
                    </div>

                    <div class="">
                        <div class="table-left wow fadeInUp" data-wow-delay="0.4s">
                            <div class="pricing-details">
                                <div style="display: inline-block">
                                    <span class="span" style="color: #ee2724;">PROGRAMS</span>
                                    <div class="col-sm-12">
                                        <h2 dir="ltr" style="text-align:left"><?php echo nl2br('All product sales are final. No exchanges or refunds will be accepted.'); ?>

                                        </h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="banner-padding">
                        <div class="table-left wow fadeInUp" data-wow-delay="0.4s">
                            <div class="pricing-details">
                                <div style="display: inline-block">
                                    <span class="span" style="color: #ee2724;">ONLINE COACHING</span>
                                    <div class="col-sm-12">
                                        <h2 dir="ltr" style="text-align:left"><?php echo nl2br('You must be of legal majority age as per the laws where you reside to subscribe to Online Coaching services through the Website. By placing any order for Online Coaching services or by using the services, you confirm and warrant that you have the right, authority, and capacity to conform to all Conditions applicable.

                                            With the exception of medical reasons preventing your continued participation, online coaching packages are non-refundable.
                                            '); ?>

                                        </h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="banner-padding">
                        <div class="table-left wow fadeInUp" data-wow-delay="0.4s">
                            <div class="pricing-details">
                                <div style="display: inline-block">
                                    <span class="span" style="color: #ee2724;">COURSES</span>
                                    <div class="col-sm-12">
                                        <h2 dir="ltr" style="text-align:left"><?php echo nl2br('ONLINE Courses fees are non-refundable, except registering in LIVR courses hence, please check the dates and locations carefully to be certain that you can attend the event. INSPOWER Fitness Coaching reserves the right to approve a refund for the following condition. Registration cancellations will be refunded if received at least 30 days prior to the LIVE course date, but an administrative fee of 22.578 BHD will apply.

                                            If refund accepted, we could refund the client, and the refund method will be the same as the client payment method within maximum of 10 working days.

                                            All requests must be submitted by email to: inspowerpt@gmail.com

                                            In the unlikely event that INSPOWER Fitness Coaching or the seminar sponsor/host must cancel an event in its entirety, registrants may choose to receive a refund of paid event fees or credit towards a future event within the following year.  Should an event need to be postponed due to reasons outside of INSPOWER Fitness Coaching’s or the sponsor/host’s control and/or reasons that make it impossible to hold the course as scheduled, INSPOWER Fitness Coaching is committed to providing an alternative course within a reasonable delay.  Your registration fee will be credited towards the new course.

                                            All online course sales are final. No exchanges or refunds will be accepted.
                                            '); ?>

                                        </h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Footer Section -->
        <?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
<?php echo $__env->make('includes.footerScripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>
</html>
<?php /**PATH /Users/maryamal-alawi/inspower/resources/views/pricingPolicy.blade.php ENDPATH**/ ?>