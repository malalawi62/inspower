<html lang="<?php echo e(__('text.lang')); ?>" dir="<?php echo e(__('text.dir')); ?>">

<?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<body>

<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app form" id="main">
        <div class="pricing-section text-center" style="min-height: 100%">
            <div class="container">
                <div style="width: 100%">
                    <div class="pricing-intro banner-padding">
                        <h1 class="wow fadeInUp" data-wow-delay="0s"> <?php echo e(__('text.allArticlesTitle')); ?> </h1>
                        <?php if((App::isLocale('en'))): ?>
                            <h2>For articles in Arabic language <a href="setlocale/ar">press here</a></h2>
                        <?php endif; ?>
                        <?php if((App::isLocale('ar'))): ?>
                            <h2>للمقالات باللغة الإنجليزية <a href="setlocale/en">اضغط هنا</a></h2>
                        <?php endif; ?>
                        <div style="padding-top: 20px">
                            <span class="filterTag"> <?php echo e(__('text.filter')); ?> </span>
                            <select id="myBtnContainer" class="mail" onchange="filterSelection(this.value)">
                                <option value="all"> <?php echo e(__('text.showFilter')); ?> </option>
                                <?php $__currentLoopData = $data['categories']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($data['lang'] == 'en'): ?>
                                        <option class="btn"
                                                value="<?php echo e($category->englishName); ?>"> <?php echo e($category->englishName); ?> </option>
                                    <?php else: ?>
                                        <option class="btn"
                                                value="<?php echo e($category->englishName); ?>"> <?php echo e($category->arabicName); ?> </option>
                                    <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>
                    </div>
                    <?php $__empty_1 = true; $__currentLoopData = $data['articles']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $article): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                        <?php $check = true; $public = $article->public; ?>
                        <?php echo $__env->make('layout.commonCard', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                        <?php $check = false; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                        <h2>No Articles Found</h2>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="container" style="background-color:#2D322F; width: 100%">
            <div class="col-md-12 text-center">
                <?php echo e($data['articles']->links()); ?>

            </div>
        </div>
        <!-- Footer Section -->
        <?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
<?php echo $__env->make('includes.footerScripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<script>
    filterSelection("all")

    function filterSelection(c) {
        var x, i;
        x = document.getElementsByClassName("filterDiv");
        if (c === "all") c = "";
        for (i = 0; i < x.length; i++) {
            w3RemoveClass(x[i], "show");
            if (x[i].className.indexOf(c) > -1) w3AddClass(x[i], "show");
        }
    }

    function w3AddClass(element, name) {
        var i, arr1, arr2;
        arr1 = element.className.split(" ");
        arr2 = name.split(" ");
        for (i = 0; i < arr2.length; i++) {
            if (arr1.indexOf(arr2[i]) == -1) {
                element.className += " " + arr2[i];
            }
        }
    }

    function w3RemoveClass(element, name) {
        var i, arr1, arr2;
        arr1 = element.className.split(" ");
        arr2 = name.split(" ");
        for (i = 0; i < arr2.length; i++) {
            while (arr1.indexOf(arr2[i]) > -1) {
                arr1.splice(arr1.indexOf(arr2[i]), 1);
            }
        }
        element.className = arr1.join(" ");
    }

</script>
</body>
</html>
<?php /**PATH /Users/maryamal-alawi/inspower/resources/views/articles/index.blade.php ENDPATH**/ ?>