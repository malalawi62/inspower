<?php
$yearMTP = \Illuminate\Support\Facades\DB::table('membership_types')->where('type', 'Yearly')->value('fees');
$monthMTP = \Illuminate\Support\Facades\DB::table('membership_types')->where('type', 'Monthly')->value('fees');
?>

<html lang="<?php echo e(__('text.lang')); ?>" dir="<?php echo e(__('text.dir')); ?>">
<?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<body>

<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<div class="wrapper">
    <!-- Main Section-->
    <div class="main app" id="main">
        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe" style="min-height: 100%">
            <div class="container">
                <div class="cta-inner">
                    <div class="row">
                    <h1 style="text-align: center; float: none"> <?php echo e(__('text.register')); ?> </h1>
                    </div>
                    <div class="wow fadeInUp" data-wow-delay="0.3s">
                        <form class="forms-form center-form wow zoomIn" method="POST" action="<?php echo e(route('register')); ?>">

                            <?php $__errorArgs = ['name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> <h5 style="color: red;"><?php echo e($message); ?></h5> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            <input class="mail" type="text" name="name" placeholder="<?php echo e(__('text.registerName')); ?>"
                                   required autocomplete="name" autofocus value="<?php echo e(old('name')); ?>">

                            <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> <h5 style="color: red;"><?php echo e($message); ?></h5> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            <input id="email" class="mail" type="email" name="email" placeholder="<?php echo e(__('text.registerEmail')); ?>"
                                   required autocomplete="email" value="<?php echo e(old('email')); ?>">

                            <input id="email-confirm" class="mail" type="email" name="email_confirmation" placeholder="<?php echo e(__('text.registerConfirmEmail')); ?>"
                                   required autocomplete="off" value="<?php echo e(old('email')); ?>">

                            <?php $__errorArgs = ['DOB'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> <h5 style="color: red;"><?php echo e($message); ?></h5> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            <div><label for="DOB"><?php echo e(__('text.registerDOB')); ?></label></div>
                            <input class="mail" type="date" name="DOB" id="DOB" required value="<?php echo e(old('DOB')); ?>">

                            <div style="padding-bottom: 20px;">
                                <?php $__errorArgs = ['gender'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> <h5 style="color: red;"><?php echo e($message); ?></h5> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                <span style="color: #2D322F"> <?php echo e(__('text.registerGender')); ?>: </span>
                                <input type="radio" name="gender" value="f"> <span style="color: #2D322F;"> <?php echo e(__('text.registerGenderFemale')); ?> </span>
                                <input type="radio" name="gender" value="m"> <span style="color: #2D322F;"> <?php echo e(__('text.registerGenderMale')); ?> </span>
                            </div>

                            <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> <h5 style="color: red;"><?php echo e($message); ?></h5> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            <input id="password" class="mail" type="password" name="password" placeholder="<?php echo e(__('text.registerPassword')); ?>"
                                   autocomplete="new-password">

                            <input id="password-confirm" class="mail" type="password" name="password_confirmation" placeholder="<?php echo e(__('text.registerConfirmPassword')); ?>"
                                   required autocomplete="new-password">

                            <div class="col-md-12">
                                <div class="col-md-4" style="padding: 15px">
                                    <div class="pricing-col">
                                        <h4><input type="radio" name="type" value="free" style="height: 20px"> <?php echo nl2br(__('text.freePlan')); ?></h4>
                                        <h2><i class="ion-checkmark" style="color: green"></i> <?php echo nl2br(__('text.free1')); ?></h2>
                                        <h2><i class="ion-close" style="color: red"></i> <?php echo nl2br(__('text.monthly1')); ?></h2>
                                        <h2><i class="ion-close" style="color: red"></i> <?php echo nl2br(__('text.monthly2')); ?></h2>
                                        <h2><i class="ion-close" style="color: red"></i> <?php echo nl2br(__('text.monthly3')); ?></h2>
                                        <h2><i class="ion-close" style="color: red"></i> <?php echo nl2br(__('text.monthly4')); ?></h2>
                                        <h3><i class="ion-social-usd" style="color: #ee2724"></i> <?php echo nl2br(__('text.freePlanPricing')); ?></h3>
                                    </div>
                                </div>

                                <div class="col-md-4" style="padding: 15px">
                                    <div class="pricing-col">
                                        <h4><input type="radio" name="type" value="month" style="height: 20px"> <?php echo nl2br(__('text.monthlyPlan')); ?></h4>
                                        <h2><i class="ion-checkmark" style="color: green"></i> <?php echo nl2br(__('text.free1')); ?></h2>
                                        <h2><i class="ion-checkmark" style="color: green"></i> <?php echo nl2br(__('text.monthly1')); ?></h2>
                                        <h2><i class="ion-checkmark" style="color: green"></i> <?php echo nl2br(__('text.monthly2')); ?></h2>
                                        <h2><i class="ion-checkmark" style="color: green"></i> <?php echo nl2br(__('text.monthly3')); ?></h2>
                                        <h2><i class="ion-checkmark" style="color: green"></i> <?php echo nl2br(__('text.monthly4')); ?></h2>
                                        <h3><i class="ion-social-usd" style="color: #ee2724"></i> <?php echo e($monthMTP); ?> <?php echo e(__("text.BD")); ?> </h3>
                                    </div>
                                </div>

                                <div class="col-md-4" style="padding: 15px">
                                    <div class="pricing-col">
                                        <h4><input type="radio" name="type" value="year" style="height: 20px"> <?php echo nl2br(__('text.yearlyPlan')); ?></h4>
                                        <h2><i class="ion-checkmark" style="color: green"></i> <?php echo nl2br(__('text.free1')); ?></h2>
                                        <h2><i class="ion-checkmark" style="color: green"></i> <?php echo nl2br(__('text.monthly1')); ?></h2>
                                        <h2><i class="ion-checkmark" style="color: green"></i> <?php echo nl2br(__('text.monthly2')); ?></h2>
                                        <h2><i class="ion-checkmark" style="color: green"></i> <?php echo nl2br(__('text.monthly3')); ?></h2>
                                        <h2><i class="ion-checkmark" style="color: green"></i> <?php echo nl2br(__('text.monthly4')); ?></h2>
                                        <h3><i class="ion-social-usd" style="color: #ee2724"></i> <?php echo e($yearMTP); ?> <?php echo e(__("text.BD")); ?> </h3>
                                    </div>
                                </div>
                            </div>

                            <div>
                                <button class="submit-button"
                                        style="width:60%; margin-top: 30px">
                                    <?php echo e(__('text.register')); ?>

                                </button>
                            </div>
                            <?php echo csrf_field(); ?>
                        </form>

                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        <?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->


<!-- Jquery and Js Plugins -->
<?php echo $__env->make('includes.footerScripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

</body>
</html>
<?php /**PATH C:\Users\ZAINAB ALMOOT\inspower\resources\views/auth/register.blade.php ENDPATH**/ ?>