<div class="filterDiv <?php echo e($video->category->englishName ?? $article->category->englishName); ?>">
    <div class="table-left wow fadeInUp" data-wow-delay="0.4s"
         style="overflow: auto;  <?php if($membership_details == null && $public == "NO"): ?> background-color:#ccc; <?php endif; ?>">
        <div class="pricing-details">
            <div style="display: block">
                <form action="/<?php echo e($videoView ?? $articleView); ?>/<?php echo e($video->id ?? $article->id); ?>" method="post">
                    <?php echo method_field('DELETE'); ?>

                    <div class="col-md-4">
                        <?php if(isset($video)): ?>
                            <?php if($video->type == 'vimeo'): ?>
                                <iframe width="100%" height="250px" frameborder="0" allowfullscreen
                                        style="padding-bottom: 10px; <?php if($membership_details == null && $public == "NO"): ?> pointer-events: none <?php endif; ?>"
                                        src="https://player.vimeo.com/video/<?php echo e($video->videoId); ?>"></iframe>
                            <?php else: ?>
                                <iframe width="100%" height="250px" frameborder="0" allowfullscreen
                                        style="padding-bottom: 10px; display: block; <?php if($membership_details == null && $public == "NO"): ?> pointer-events: none <?php endif; ?>"
                                        src="https://www.youtube.com/embed/<?php echo e($video->videoId); ?>"></iframe>
                            <?php endif; ?>
                        <?php elseif(isset($article)): ?>
                            <div class="articleImg">
                                <?php if($article->image != null): ?>
                                    <img class="show-img wow fadeIn" src="<?php echo e(asset('storage/'.$article->image)); ?>"
                                         style="padding-bottom: 10px"/>
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                    <br>
                    <div class="col-md-8">
                        <div class="row">
                            <?php if(auth()->guard()->guest()): ?>
                                <?php if($public == "NO"): ?>
                                    <span class="editDeleteIcons">
                                        <a href="#test-popup-1" class="open-popup-link" style="color: #ee2724; font-size: 60px">
                                            <i class="ion-ios-locked"></i>
                                        </a>
                                    </span>
                                <?php endif; ?>
                                <?php if($membership_details == null && $public == "NO"): ?> <!-- Guest or Free membership -->
                                    <div id="test-popup-1" class="white-popup mfp-hide">
                                        <h3 class="exclusive"> <?php echo e(__('text.exclusiveSentence')); ?> </h3>
                                        <iframe class="iframe-exclusive" src="/registerExclusive" frameborder="0" allowfullscreen></iframe>
                                    </div>
                                    <span class="video-title">
                                        <a href="#test-popup-1" class="open-popup-link"
                                           style="color: #737373; font-size:20px"> <?php echo e($video->title ?? $article->title); ?> </a>
                                    </span>
                                <?php else: ?>
                                    <span class="video-title">
                                        <a style="color: #ee2724; font-size:20px"
                                           href="/<?php echo e($videoView ?? $articleView); ?>/<?php echo e($video->id ?? $article->id); ?>_<?php echo e(str_replace(' ', '_', $video->title ?? $article->title)); ?>">
                                            <?php echo e($video->title ?? $article->title); ?>

                                        </a>
                                    </span>
                                <?php endif; ?>
                            <?php else: ?>
                                <?php if(auth()->user()->type == 'admin'): ?>
                                    <span class="editDeleteIcons">
                                        <a href="/<?php echo e($videoView ?? $articleView); ?>/<?php echo e($video->id ?? $article->id); ?>/edit" style="color: #364f87">
                                            <i class="ion-edit"></i>
                                        </a>
                                        <a href="/sendVideo/<?php echo e($video->id ?? $article->id); ?>" style="color: #364f87">
                                            <i class="ion-android-send"></i>
                                        </a>
                                        <button onclick="return confirm('Are you sure you want to delete?')"
                                                style="color: #364f87; background-color: transparent; border-width: 0; font-size: 24px">
                                            <i class="ion-android-delete"></i>
                                        </button>
                                    </span>
                                    <span class="video-title">
                                        <a href="/<?php echo e($videoView ?? $articleView); ?>/<?php echo e($video->id ?? $article->id); ?>_<?php echo e(str_replace(' ', '_', $video->title ?? $article->title)); ?>"
                                           style="color: #364f87; font-size:20px"> <?php echo e($video->title ?? $article->title); ?> </a>
                                    </span>
                                    <!-- Signed in with Premium membership -->
                                <?php elseif(auth()->user()->type != 'admin' && $membership_details != null && $public == "NO"): ?>
                                    <span class="editDeleteIcons">
                                        <a title="<?php echo e(__('text.premium')); ?>" style="color: #009933; font-size: 60px">
                                            <i class="ion-ios-unlocked"></i>
                                        </a>
                                    </span>
                                    <span class="video-title">
                                        <a href="/<?php echo e($videoView ?? $articleView); ?>/<?php echo e($video->id ?? $article->id); ?>_<?php echo e(str_replace(' ', '_', $video->title ?? $article->title)); ?>"
                                           style="color: #364f87; font-size:20px"> <?php echo e($video->title ?? $article->title); ?> </a>
                                    </span>
                                    <!-- Signed in with free membership -->
                                <?php elseif(auth()->user()->type != 'admin' && $membership_details == null && $public == "NO"): ?>
                                    <span class="editDeleteIcons">
                                        <a href="#test-popup-2" class="open-popup-link" style="color: #ee2724; font-size: 60px">
                                            <i class="ion-ios-locked"></i>
                                        </a>
                                    </span>
                                    <div id="test-popup-2" class="white-popup mfp-hide">
                                        <h3 class="exclusive"> <?php echo e(__('text.exclusiveSentence')); ?> </h3>
                                        <iframe class="iframe-exclusive" src="/registerUpgrade" frameborder="0" allowfullscreen></iframe>
                                    </div>
                                    <span class="video-title">
                                        <a href="#test-popup-2" class="open-popup-link"
                                           style="color: #737373; font-size:20px"> <?php echo e($video->title ?? $article->title); ?> </a>
                                    </span>
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>
                        <h5 class="articleCategory" <?php if($membership_details == null && $public == "NO"): ?> style="color: #737373" <?php endif; ?>>
                            <?php if($data['lang'] == 'en'): ?>
                                Category:
                                <a <?php if($membership_details == null && $public == "NO"): ?> style="color: #737373" <?php endif; ?> class="articleCategory"
                                   href="https://www.inspower-fitnesscoaching.com/videos?category_id=<?php echo e($video->category->id ?? $article->category->id); ?>">
                                    <?php echo e($video->category->englishName ?? $article->category->englishName); ?>

                                </a>
                            <?php else: ?>
                                الفئة:
                                <a <?php if($membership_details == null && $public == "NO"): ?> style="color: #737373" <?php endif; ?> class="articleCategory"
                                   href="https://www.inspower-fitnesscoaching.com/videos?category_id=<?php echo e($video->category->id ?? $article->category->id); ?>">
                                    <?php echo e($video->category->arabicName ?? $article->category->arabicName); ?>

                                </a>
                            <?php endif; ?>
                        </h5>
                        <?php if(isset($check) && $check): ?>
                            <h5 class="articleWriter"
                                style=" <?php if($membership_details == null && $public == "NO"): ?> color: #737373 <?php else: ?> color: #364f87 <?php endif; ?>">
                                <?php if($article->writerName != null): ?>
                                    <?php if($data['lang'] == 'en'): ?>
                                        Written By: <?php echo e($article->writerName); ?>

                                    <?php else: ?>
                                        الكاتب: <?php echo e($article->writerName); ?>

                                    <?php endif; ?>
                                <?php else: ?>
                                    <?php if($data['lang'] == 'en'): ?>
                                        Written By: <?php echo e($article->writer->englishName); ?>

                                    <?php else: ?>
                                        الكاتب: <?php echo e($article->writer->arabicName); ?>

                                    <?php endif; ?>
                                <?php endif; ?>
                                <span class="articleDate"
                                      style="<?php if($membership_details == null): ?> color: #737373 <?php else: ?> color: #364f87 <?php endif; ?>"><?php echo e($article->date); ?></span>
                            </h5>
                        <?php endif; ?>
                        <h2 class="articleBody"
                            <?php if($membership_details == null && $public == "NO"): ?> style="color: #737373" <?php endif; ?>>
                            <?php echo nl2br($video->description ?? substr($article->body, 0, 330)." ..."); ?>

                        </h2>
                    </div>
                    <?php echo csrf_field(); ?>
                </form>
                <br>
            </div>
        </div>
    </div>
</div><?php /**PATH /Users/maryamal-alawi/inspower/resources/views/layout/commonCard.blade.php ENDPATH**/ ?>