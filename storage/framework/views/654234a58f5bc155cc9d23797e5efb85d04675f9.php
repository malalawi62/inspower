<html lang="<?php echo e(__('text.lang')); ?>" dir="<?php echo e(__('text.dir')); ?>">

<?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<body>

<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app form" id="main">
        <div class="pricing-section text-center" style="min-height: 100%">
            <div class="container">
                <div style="width: 100%">
                    <div class="pricing-intro">
                        <h1 class="wow fadeInUp" data-wow-delay="0s"> <?php echo e(__('text.searchResults')); ?> </h1>
                        <?php if(isset($articles) || isset($videos)): ?>
                            <div style="padding-top: 20px">
                            <span class="filterTag"> <?php echo e(__('text.filter')); ?> </span>
                            <select id="myBtnContainer" class="mail" onchange="filterSelection(this.value)">
                                <option value="all"> <?php echo e(__('text.showFilter')); ?> </option>
                                <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($lang == 'en'): ?>
                                        <option class="btn" value="<?php echo e($category->englishName); ?>"> <?php echo e($category->englishName); ?> </option>
                                    <?php else: ?>
                                        <option class="btn" value="<?php echo e($category->englishName); ?>"> <?php echo e($category->arabicName); ?> </option>
                                    <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
<<<<<<< HEAD

                            <!-- <span class="filterTag"> {__('text.filterType') } </span>
                            <select id="myBtnContainer" class="mail" onchange="filterSelection2(this.value)">
                                <option value="all"> { __('text.showFilter') } </option>
                                <option value="video"> if($lang == 'en') Videos else مقاطع مرئية endif </option>
                                <option value="article"> if($lang == 'en') Articles else مقالات endif </option>
                            </select> -->

=======
                            
                            <span class="filterTag"> <?php echo e(__('text.filterType')); ?> </span>
                        <select id="myBtnContainer" class="mail" onchange="filterSelection2(this.value)">
                            <option value="all"> <?php echo e(__('text.showFilter')); ?> </option>
                            <option value="video"> <?php if($lang == 'en'): ?> Videos <?php else: ?> مقاطع مرئية <?php endif; ?> </option>
                            <option value="article"> <?php if($lang == 'en'): ?> Articles <?php else: ?> مقالات <?php endif; ?> </option>
                        </select>
>>>>>>> 68063f6772344442635537c26f6420f83f85c557
                        </div>
                        <?php endif; ?>
                    </div>
                    <?php if(isset($articles)): ?>
                        <?php $__currentLoopData = $articles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $article): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="filterDiv article <?php echo e($article->category->englishName); ?>">
                            <div class="table-left wow fadeInUp" data-wow-delay="0.4s" style="overflow: auto;">
                                <div class="pricing-details">
                                    <div style="display: block">
                                        <form action="/articles/<?php echo e($article->id); ?>" method="post">
                                            <?php echo method_field('DELETE'); ?>
                                            <div class="row">
                                                <span class="article-title">
                                                    <a href="/articles/<?php echo e($article->id); ?>"
                                                       style="color: #fdca00;">
                                                        <?php echo e($article->title); ?>

                                                    </a>
                                                </span>
                                                <?php if(auth()->guard()->guest()): ?>
                                                <?php else: ?>
                                                    <span class="editDeleteIcons">
                                                        <a href="/articles/<?php echo e($article->id); ?>/edit"
                                                           style="color: #fdca00">
                                                            <i class="ion-edit"></i>
                                                        </a>
                                                        <button
                                                            style="color: #fdca00; background-color: transparent; border-width: 0; font-size: 24px">
                                                            <i class="ion-android-delete"></i>
                                                        </button>
                                                    </span>
                                                <?php endif; ?>
                                            </div>
                                            <div class="col-sm-3 articleImg">
                                                <img class="show-img wow fadeIn"
                                                     src="<?php echo e(asset('storage/'.$article->image)); ?>"
                                                     style="padding-bottom: 10px"/>
                                            </div>
                                            <br>
                                            <div class="col-sm-9">
                                                <h5 class="articleCategory">
                                                    <?php if($lang == 'en'): ?>
                                                        Category: <?php echo e($article->category->englishName); ?>

                                                    <?php else: ?>
                                                        الفئة: <?php echo e($article->category->arabicName); ?>

                                                    <?php endif; ?>
                                                </h5>
                                                <h5 class="articleWriter">
                                                    <?php if($article->writerName != null): ?>
                                                        <?php if($lang == 'en'): ?>
                                                            Written By: <?php echo e($article->writerName); ?>

                                                        <?php else: ?>
                                                            الكاتب: <?php echo e($article->writerName); ?>

                                                        <?php endif; ?>
                                                    <?php else: ?>
                                                        <?php if($lang == 'en'): ?>
                                                            Written By: <?php echo e($article->writer->englishName); ?>

                                                        <?php else: ?>
                                                            الكاتب: <?php echo e($article->writer->arabicName); ?>

                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                    <span class="articleDate"><?php echo e($article->date); ?></span>
                                                </h5>
                                                <h2 class="articleBody"> <?php echo e(substr($article->body, 0, 330)." ..."); ?></h2>
                                            </div>
                                            <?php echo csrf_field(); ?>
                                        </form>
                                        <br>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
<<<<<<< HEAD
                    <?php if(isset($videoResult)): ?>
                        <?php $__empty_1 = true; $__currentLoopData = $videoResult; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $video): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
=======
                    <?php if(isset($videos)): ?>
                        <?php $__empty_1 = true; $__currentLoopData = $videos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $video): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
>>>>>>> 68063f6772344442635537c26f6420f83f85c557
                            <div class="filterDiv video <?php echo e($video->category->englishName); ?>">
                                <div class="table-left wow fadeInUp" data-wow-delay="0.4s" style="overflow: auto;">
                                    <div class="pricing-details">
                                        <div style="display: block">
                                            <form action="/videos/<?php echo e($video->id); ?>" method="post">
                                                <?php echo method_field('DELETE'); ?>
                                                <div class="row">
                                                <span class="article-title">
                                                    <a style="color: #fdca00;">
                                                        <?php echo e($video->title); ?>

                                                    </a>
                                                </span>
                                                    <?php if(auth()->guard()->guest()): ?>
                                                    <?php else: ?>
                                                        <span class="editDeleteIcons">
                                                        <a href="/videos/<?php echo e($video->id); ?>/edit"
                                                           style="color: #fdca00">
                                                            <i class="ion-edit"></i>
                                                        </a>
                                                        <button
                                                            style="color: #fdca00; background-color: transparent; border-width: 0; font-size: 24px">
                                                            <i class="ion-android-delete"></i>
                                                        </button>
                                                    </span>
                                                    <?php endif; ?>
                                                </div>
                                                <div class="col-md-4">
                                                    <?php if($video->type == 'vimeo'): ?>
                                                        <iframe width="100%" style="padding-bottom: 10px"
                                                                src="https://player.vimeo.com/video/<?php echo e($video->videoId); ?>"
                                                                frameborder="0" allowfullscreen></iframe>
                                                    <?php else: ?>
                                                        <iframe width="100%" style="padding-bottom: 10px; display: block;"
                                                                src="https://www.youtube.com/embed/<?php echo e($video->videoId); ?>"
                                                                frameborder="0" allowfullscreen ></iframe>
                                                    <?php endif; ?>
                                                </div>
                                                <br>
                                                <div class="col-sm-8">
                                                    <h5 class="articleCategory">
                                                        <?php if($lang == 'en'): ?>
                                                            Category: <?php echo e($video->category->englishName); ?>

                                                        <?php else: ?>
                                                            الفئة: <?php echo e($video->category->arabicName); ?>

                                                        <?php endif; ?>
                                                    </h5>
                                                    <h2 class="articleBody">
                                                        <?php echo nl2br($video->description); ?>

                                                    </h2>
                                                </div>
                                                <?php echo csrf_field(); ?>
                                            </form>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                            <h2>No Videos Found</h2>
                        <?php endif; ?>
                    <?php endif; ?>
<<<<<<< HEAD
                    <?php if(!isset($articles) && !isset($videoResult)): ?>
=======
                    <?php if(!isset($articles) && !isset($videos)): ?>
>>>>>>> 68063f6772344442635537c26f6420f83f85c557
                    <div class="pricing-intro">
                        <h1 class="wow fadeInUp" data-wow-delay="0s"> <?php echo e(__('text.noResults')); ?> </h1>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <!-- Footer Section -->
        <?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
<?php echo $__env->make('includes.footerScripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<script>
    filterSelection("all")
    function filterSelection(c) {
        var x, i;
        x = document.getElementsByClassName("filterDiv");
        if (c === "all") c = "";
        for (i = 0; i < x.length; i++) {
            w3RemoveClass(x[i], "show");
            if (x[i].className.indexOf(c) > -1) w3AddClass(x[i], "show");
        }
    }
<<<<<<< HEAD

=======
    
>>>>>>> 68063f6772344442635537c26f6420f83f85c557
    filterSelection2("all")
    function filterSelection2(c) {
        var x, i;
        x = document.getElementsByClassName("filterDiv");
        if (c === "all") c = "";
        for (i = 0; i < x.length; i++) {
            w3RemoveClass(x[i], "show");
            if (x[i].className.indexOf(c) > -1) w3AddClass(x[i], "show");
        }
    }

    function w3AddClass(element, name) {
        var i, arr1, arr2;
        arr1 = element.className.split(" ");
        arr2 = name.split(" ");
        for (i = 0; i < arr2.length; i++) {
            if (arr1.indexOf(arr2[i]) == -1) {element.className += " " + arr2[i];}
        }
    }

    function w3RemoveClass(element, name) {
        var i, arr1, arr2;
        arr1 = element.className.split(" ");
        arr2 = name.split(" ");
        for (i = 0; i < arr2.length; i++) {
            while (arr1.indexOf(arr2[i]) > -1) {
                arr1.splice(arr1.indexOf(arr2[i]), 1);
            }
        }
        element.className = arr1.join(" ");
    }

</script>
</body>
</html>
<?php /**PATH /home2/inspower/inspower/resources/views/searchResults.blade.php ENDPATH**/ ?>