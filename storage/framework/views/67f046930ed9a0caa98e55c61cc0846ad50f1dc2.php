<script type="text/javascript" src="<?php echo e(asset('js/jquery-2.1.1.js')); ?>"></script>
<script src="<?php echo e(asset('js/jquery.subscribe.js')); ?>"></script>
<script src="<?php echo e(asset('js/app.js')); ?>" defer></script>
<script type="text/javascript" src="<?php echo e(asset('js/bootstrap.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('js/plugins.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('js/menu.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('js/custom.js')); ?>"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAJ5BZftoKtBStvTZ_6NZy0eIqDuR6h5UM&callback=initMap"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="js.js"></script>
<script src="https://unpkg.com/boxicons@latest/dist/boxicons.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<!-- Magnific Popup core JS file -->
<script src="<?php echo e(asset('js/jquery.magnific-popup.js')); ?>"></script>
<script>
    <?php if(Session::has('message')): ?>
        var type="<?php echo e(Session::get('alert-type','info')); ?>"

        toastr.options.positionClass = "toast-top-center";
        toastr.options.hideMethod = 'slideUp';

        <?php if(App::isLocale('ar')): ?>
            toastr.options.rtl = true;
            switch(type){
                case 'info':
                    toastr.info("<?php echo e(Session::get('messageAr')); ?>");
                    break;
                case 'success':
                    toastr.success("<?php echo e(Session::get('messageAr')); ?>");
                    break;
                case 'warning':
                    toastr.warning("<?php echo e(Session::get('messageAr')); ?>");
                    break;
                case 'error':
                    toastr.error("<?php echo e(Session::get('messageAr')); ?>");
                    break;
            }
        <?php else: ?>
            switch(type){
                case 'info':
                    toastr.info("<?php echo e(Session::get('message')); ?>");
                    break;
                case 'success':
                    toastr.success("<?php echo e(Session::get('message')); ?>");
                    break;
                case 'warning':
                    toastr.warning("<?php echo e(Session::get('message')); ?>");
                    break;
                case 'error':
                    toastr.error("<?php echo e(Session::get('message')); ?>");
                    break;
            }
        <?php endif; ?>
    <?php endif; ?>
</script>
<script>
    var counter = 2;
    var wrapper = $('.phases'); //Input field wrapper
    function addPhase(){
        var phaseNew='<?php echo e(__('text.phaseNew')); ?>';
        var phaseTitle='<?php echo e(__('text.phaseTitle')); ?>';
        var phaseDescription ='<?php echo e(__('text.phaseDescription')); ?>';
        var phasePdf='<?php echo e(__('text.phasePdf')); ?>';
        var phaseVideoURL='<?php echo e(__('text.phaseVideoURL')); ?>';
        var phaseOverview = '<?php echo e(__('text.phaseOverview')); ?>';
        var phaseBriefDescription = '<?php echo e(__('text.phaseBriefDescription')); ?>';
        var sectionVideoUrl = '<?php echo e(__('text.sectionVideoURL')); ?>';
        var addNewSection = '<?php echo e(__('text.addNewSection')); ?>';
        var PhaseContent = '' +
            '<div class="phase phase_wrapper_'+counter+'">' +
                '<hr style="width: 100%; border-width:1px">' +
                '<div class="row">'+
                    '<h1 style="text-align: center; float: none; padding-top: 0" id="phase_title_'+counter+'"> ' + phaseNew +
                        '<input class="add_phase_button" type="button" onclick="addPhase()" value="+" style="color: #ee2724; background: transparent; border-color: transparent; border-width: 0">' +
                        '<input class="delete_phase_button" type="button" onclick="deletePhase('+counter+')" value="-" style="color: #ee2724; background: transparent; border-color: transparent; border-width: 0">' +
                    '</h1>'+
                '</div>'+
                '<input class="mail" type="text" name="p_title[]" placeholder="' + phaseTitle + '" autocomplete="title" value="">' +
                '<p>' + phaseOverview + '</p>' +
                '<textarea class="mail" rows="10" name="p_overview[]" style="height:auto;" value="value"></textarea>' +
                '<p>'+ phaseDescription +'</p>' +
                '<textarea class="mail" rows="10" name="p_description[]" style="height: auto; width: 60%;" value=""></textarea>'+
            '<input class="mail" type="text" name="p_videoUrl[]" placeholder="' + phaseVideoURL + '"' +
            'autocomplete="off" value="">' +
                '<label class="mail" style="border: 1.5px solid #babdc2; padding-top: 10px">' +
                    '' + phasePdf + ''+
                '<input type="file" name="phasePdf[]" style="display: inline-block"/>' +
                '</label>'+
            '<hr style="width: 70%; border-width:1px">' +
                '<div class="videos">' +
                    '<div class="video_wrapper">'+
                            '<input class="mail" type="text" name="ps_videoUrl['+counter+'][]" placeholder="' + sectionVideoUrl + '" autocomplete="off" value="">'+
                            '<p>'+ phaseBriefDescription +'</p>' +
                            '<textarea class="mail" rows="10" name="ps_description['+counter+'][]" style="height: auto;" value=""></textarea>'+
                            '<div class="row">'+
                                '<input class="add_video_button addBtn" type="button" onclick="addVideo('+counter+')" value="' + addNewSection + '+">' +
                        '</div>'+
                    '</div>'+
                '</div>' +
            '</div>'; //New input field html

        $(".phases:last").append(PhaseContent);
        counter ++;
    }

    function deletePhase(x){
        $('.phase_wrapper_'+x).remove();
        counter--;
    }
</script>
<script>
    var videoCounter = 2;
    function addVideo(x){
        var wrapper1 = $('.phase_wrapper_'+x); //Input field wrapper
        var phaseVideoURL='<?php echo e(__('text.phaseVideoURL')); ?>';
        var phaseBriefDescription = '<?php echo e(__('text.phaseBriefDescription')); ?>';
        var addNewSection = '<?php echo e(__('text.addNewSection')); ?>';
        var removeSection = '<?php echo e(__('text.removeSection')); ?>';
        var VideoURL = '' +
            '<div class="video_wrapper video_'+videoCounter+'">' +
                    '<input class="mail" type="text" name="ps_videoUrl['+x+'][]" placeholder="' + phaseVideoURL + '" autocomplete="off" value="">' +
                    '<p>'+ phaseBriefDescription +'</p>' +
                    '<textarea class="mail" rows="10" name="ps_description['+x+'][]" style="height: auto; width: 60%;" value=""></textarea>'+
                    '<div class="row">'+
                        '<input class="add_video_button addBtn" type="button" onclick="addVideo('+x+')" value="' + addNewSection + '+">'+
                        '<input class="delete_video_button addBtn" type="button" onclick="deleteVideo('+videoCounter+')" value="' + removeSection + '-">' +
                '</div>'+
            '</div>';//New input field html

        $(VideoURL).clone().appendTo(wrapper1);
        videoCounter ++;
    }

    function addVideoPhase(){
        var wrapper1 = $('.phase_wrapper_1'); //Input field wrapper
        var phaseVideoURL='<?php echo e(__('text.phaseVideoURL')); ?>';
        var phaseBriefDescription = '<?php echo e(__('text.phaseBriefDescription')); ?>';
        var addNewSection = '<?php echo e(__('text.addNewSection')); ?>';
        var removeSection = '<?php echo e(__('text.removeSection')); ?>';
        var VideoURL = '' +
            '<div class="video_wrapper video_'+videoCounter+'">' +
            '<input class="mail" type="text" name="section_videoUrl[]" placeholder="' + phaseVideoURL + '" autocomplete="off" value="">' +
            '<p>'+ phaseBriefDescription +'</p>' +
            '<textarea class="mail" rows="10" name="section_description[]" style="height: auto; width: 60%;" value=""></textarea>'+
            '<div class="row">'+
            '<input class="add_video_button addBtn" type="button" onclick="addVideoPhase()" value="' + addNewSection + '+">'+
            '<input class="delete_video_button addBtn" type="button" onclick="deleteVideo('+videoCounter+')" value="' + removeSection + '-">' +
            '</div>'+
            '</div>';//New input field html

        $(VideoURL).clone().appendTo(wrapper1);
        videoCounter ++;
    }

    function deleteVideo(y){
        $('.video_'+y).remove();
    }
</script>

<script>
    var counter_m = 2;
    var wrapper_m = $('.modules'); //Input field wrapper
    function addModule(){
        var moduleNew='<?php echo e(__('text.moduleNew')); ?>';
        var moduleTitle='<?php echo e(__('text.moduleTitle')); ?>';
        var moduleDescription ='<?php echo e(__('text.moduleDescription')); ?>';
        var moduleVideoURL='<?php echo e(__('text.moduleVideoURL')); ?>';
        var lessonPdf='<?php echo e(__('text.inputLessonPdf')); ?>';
        var lessonDesc='<?php echo e(__('text.lessonDescription')); ?>';
        var addNewLesson = '<?php echo e(__('text.addNewLesson')); ?>';
        var ModuleContent = '' +
            '<div class="module module_wrapper_'+counter_m+'">' +
                '<div class="row">' +
                    '<h1 style="text-align: center; float: none; padding-top: 0" id="module_title_'+counter_m+'">' + moduleNew +
                        '<input class="add_module_button" type="button" onclick="addModule()" value="+" style="color: #ee2724; background: transparent; border-color: transparent; border-width: 0">' +
                        '<input class="delete_module_button" type="button" onclick="deleteModule('+counter_m+')" value="-" style="color: #ee2724; background: transparent; border-color: transparent; border-width: 0">' +
                    '</h1>' +
                '</div>' +
                '<input class="mail" type="text" name="m_title[]"' +
                    'placeholder="' + moduleTitle + '"' +
                    'autocomplete="m_title">' +
                '<p>' + moduleDescription + '</p>' +
                '<textarea class="mail" rows="10" name="m_description[]" style="height: auto;"></textarea>' +
                '<div class="videos">' +
                    '<div class="video_wrapper">' +
                        '<div class="row">' +
                            '<input class="mail" type="text" name="m_videoUrl['+counter_m+'][]"' +
                                'placeholder="' + moduleVideoURL +'"' +
                                'autocomplete="off" />' +
                            '<label class="mail" style="border: 1.5px solid #babdc2; padding-top: 10px">' +
                                lessonPdf +
                                '<input type="file" name="pdf['+counter_m+'][]" id="bodyPdf" style="display: inline-block"/>' +
                            '</label>' +
                            '<p>' + lessonDesc + '</p>' +
                            '<textarea class="mail" rows="10" name="v_description['+counter_m+'][]" style="height: auto;"></textarea>' +
                        '</div>' +
                        '<div class="row">' +
                            '<input class="add_video_course_button addBtn" type="button" onclick="addCourseVideo('+counter_m+')" value="' + addNewLesson + ' +">' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>'; //New input field html

        $(".modules:last").append(ModuleContent);
        counter_m ++;
    }

    function deleteModule(x){
        $('.module_wrapper_'+x).remove();
        counter_m --;
    }
</script>
<script>
    var videoCounterLesson = 2;
    function addCourseVideo(x){
        var wrapperLesson = $('.module_wrapper_'+x); //Input field wrapper
        var moduleNew='<?php echo e(__('text.moduleNew')); ?>';
        var moduleTitle='<?php echo e(__('text.moduleTitle')); ?>';
        var moduleDescription ='<?php echo e(__('text.moduleDescription')); ?>';
        var moduleVideoURL='<?php echo e(__('text.moduleVideoURL')); ?>';
        var lessonNew='<?php echo e(__('text.lessonNew')); ?>';
        var lessonPdf='<?php echo e(__('text.inputLessonPdf')); ?>';
        var lessonDesc='<?php echo e(__('text.lessonDescription')); ?>';
        var addNewLesson = '<?php echo e(__('text.addNewLesson')); ?>';
        var removeLesson = '<?php echo e(__('text.removeLesson')); ?>';
        var VideoURLLesson = '' +
            '<div class="video_wrapper video_'+videoCounterLesson+'">' +
                '<div class="row">' +
                    '<input class="mail" type="text" name="m_videoUrl['+x+'][]"' +
                        'placeholder="' + moduleVideoURL +'"' +
                        'autocomplete="off" />' +
                    '<label class="mail" style="border: 1.5px solid #babdc2; padding-top: 10px">' +
                        lessonPdf +
                        '<input type="file" name="pdf['+x+'][]" id="bodyPdf" style="display: inline-block"/>' +
                    '</label>' +
                    '<p>' + lessonDesc + '</p>' +
                    '<textarea class="mail" rows="10" name="v_description['+x+'][]" style="height: auto;"></textarea>' +
                '</div>' +
                '<div class="row">' +
                    '<input class="add_video_course_button addBtn" type="button" onclick="addCourseVideo('+x+')" value="' + addNewLesson + ' +">' +
                    '<input class="delete_video_course_button addBtn" type="button" onclick="deleteCourseVideo('+videoCounterLesson+')" value="' + removeLesson + '-">' +
                '</div>' +
            '</div>';

        $(VideoURLLesson).clone().appendTo(wrapperLesson);
        videoCounterLesson ++;
    }

    function deleteCourseVideo(y){
        $('.video_'+y).remove();
    }
</script>

<script>
    function showPdf() {
        var x = document.getElementById("pdf");
        var r= document.getElementById("read");
        x.style.display = "block";
        r.style.display="none";
    }
</script>
<script>
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function() {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.display === "block") {
                panel.style.display = "none";
            } else {
                panel.style.display = "block";
            }
        });
    }
</script>
<script>
    $('.open-popup-link').magnificPopup({
        type:'inline',
        midClick: true, // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
    });
</script>
<script src="https://player.vimeo.com/api/player.js"></script>
<script>
    var acc2 = document.getElementsByClassName("accordion2");
    var i;

    for (i = 0; i < acc2.length; i++) {
        acc2[i].addEventListener("click", function() {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.display === "block") {
                panel.style.display = "none";
            } else {
                panel.style.display = "block";
            }
        });
    }

    function myfunction(x, state_id, y){
        var iframe = document.getElementById(x);
        var span = document.getElementById(y)
        var player = new Vimeo.Player(iframe);
        var fullTime = 0;
        var time = 0;
        var complete = '<?php echo e(__('text.complete')); ?>';
        var incomplete = '<?php echo e(__('text.incomplete')); ?>';

        player.on('play', function() {
            console.log('Played the video',x);
        });

        player.on('pause', function() {
            player.getDuration().then(function(duration) {
                fullTime = duration;
            });
            player.getCurrentTime().then(function(seconds) {
                console.log('time:', seconds);
                time = seconds;
                $.ajax({
                    url: "/course/updateStatus/"+time+"/"+state_id+"/"+fullTime,
                    type: "GET",
                    success: function(response) {
                        console.log(response['response']);
                        if(response['response'] == 'complete'){
                            span.innerHTML = '<i class="fas fa-check-circle complete" title="'+complete+'"></i> '+complete+'';
                        }
                        else {
                            span.innerHTML = '<i class="fas fa-exclamation-circle incomplete" title="'+incomplete+'"></i> '+incomplete+'';
                        }
                    }
                })
            });
        });

        player.on('ended', function() {
            console.log('Video Ended',x);
            player.getDuration().then(function(duration) {
                fullTime = duration;
            });
            player.getCurrentTime().then(function(seconds) {
                time = seconds;
            });
            $.ajax({
                url: "/course/updateStatus/"+time+"/"+state_id+"/"+fullTime,
                type: "GET",
                success: function(response) {
                    console.log(response['response']);
                    if(response['response'] == 'complete'){
                        span.innerHTML = '<i class="fas fa-check-circle complete" title="'+complete+'"></i> '+complete+'';
                    }
                    else {
                        span.innerHTML = '<i class="fas fa-exclamation-circle incomplete" title="'+incomplete+'"></i> '+incomplete+'';
                    }
                }
            })
        });
    }
</script>

<script>
    var slideIndex = 0;
    showSlides();
    var slides,dots;

    function showSlides() {
        var i;
        slides = document.getElementsByClassName("mySlides");
        dots = document.getElementsByClassName("dot");
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        slideIndex++;
        if (slideIndex> slides.length) {slideIndex = 1}
        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" active", "");
        }
        slides[slideIndex-1].style.display = "block";
        dots[slideIndex-1].className += " active";
        setTimeout(showSlides, 10000); // Change image every 8 seconds
    }

    function plusSlides(position) {
        slideIndex +=position;
        if (slideIndex> slides.length) {slideIndex = 1}
        else if(slideIndex<1){slideIndex = slides.length}
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" active", "");
        }
        slides[slideIndex-1].style.display = "block";
        dots[slideIndex-1].className += " active";
    }

    function currentSlide(index) {
        if (index> slides.length) {index = 1}
        else if(index<1){index = slides.length}
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" active", "");
        }
        slides[index-1].style.display = "block";
        dots[index-1].className += " active";
    }
</script>
<?php /**PATH /Users/maryamal-alawi/inspower/resources/views/includes/footerScripts.blade.php ENDPATH**/ ?>