<!DOCTYPE html>
<html>

<body style="background-color: #cccccc">

<div class="wrapper" style="background-color: #cccccc; padding: 20px;">
    <!-- Main Section-->
    <div class="main app form" id="main" style="background-color: #cccccc">
        <img style="display: block; margin-left: auto; margin-right: auto; width: 50%;"
             src="<?php echo e(asset('images/logoColored.png')); ?>" alt="inspower">

        <hr style="margin: 20px">

        <!-- About Section -->
        <?php if($ArticleInfo->language == "Eng"): ?>
            <div class="split-features" id="about" lang="en" dir="ltr">
                <div class="col-md-12 nopadding">
                    <div>
                        <h1 class="wow fadeInUp" style="text-align: center"> Dear <?php echo e($subscriberName); ?>, </h1>
                        <h1 class="wow fadeInUp" style="text-align: center"> Check out our latest Article added to INSPOWER website </h1>
                        <p class="wow fadeInUp" style="text-align: center">
                            <a href="https://www.inspower-fitnesscoaching.com/articles/<?php echo e($ArticleInfo->id); ?>"> <?php echo e($ArticleInfo->title); ?> </a>
                        </p>
                        <p style="text-align: center">
                            <?php echo nl2br(\Illuminate\Support\Str::limit($ArticleInfo->body, 300, $end='...')); ?>

                        </p>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <?php if($ArticleInfo->language == "Ar"): ?>
            <div class="split-features" id="about" lang="ar" dir="rtl">
                <div class="col-md-12 nopadding">
                    <div>
                        <h1 class="wow fadeInUp" style="text-align: center"> أهلُا <?php echo e($subscriberName); ?>, </h1>
                        <h1 class="wow fadeInUp" style="text-align: center"> يمكنك رؤية آخر مقال أُضيف في موقعنا </h1>
                        <p class="wow fadeInUp" style="text-align: center">
                            <a href="https://www.inspower-fitnesscoaching.com/articles/<?php echo e($ArticleInfo->id); ?>"> <?php echo e($ArticleInfo->title); ?> </a>
                        </p>
                        <p style="text-align: center">

                            <?php echo e(str_limit($ArticleInfo->body, $limit = 150, $end = '...')); ?>

                        </p>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <hr style="margin: 20px">

        <div style="text-align: center">
            <a href="https://inspower-fitnesscoaching.com"><img src="<?php echo e(asset('images/domain.png')); ?>" style="padding: 10px; width: 50px; display:inline-block;"></a>
            <a href="https://www.instagram.com/inspower_pt/"><img src="<?php echo e(asset('images/instagram.png')); ?>" style="padding: 10px; width: 50px"></a>
            <a href="https://www.youtube.com/channel/UCkTu54pUXDLzMp8j0T1Ping"><img src="<?php echo e(asset('images/youtube.png')); ?>" style="padding: 10px; width: 50px"></a>
        </div>

        <div style="text-align: center">
            <?php if($ArticleInfo->language == "Eng"): ?>
                <?php $__env->startComponent('mail::button', ['url'=>'https://www.inspower-fitnesscoaching.com/unsubscribe/'.$mail]); ?>
                    UnSubscribe
                <?php if (isset($__componentOriginalb8f5c8a6ad1b73985c32a4b97acff83989288b9e)): ?>
<?php $component = $__componentOriginalb8f5c8a6ad1b73985c32a4b97acff83989288b9e; ?>
<?php unset($__componentOriginalb8f5c8a6ad1b73985c32a4b97acff83989288b9e); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
            <?php endif; ?>
            <?php if($ArticleInfo->language == "Ar"): ?>
                <?php $__env->startComponent('mail::button', ['url'=>'https://www.inspower-fitnesscoaching.com/unsubscribe/'.$mail]); ?>
                    إلغاء الاشتراك
                <?php if (isset($__componentOriginalb8f5c8a6ad1b73985c32a4b97acff83989288b9e)): ?>
<?php $component = $__componentOriginalb8f5c8a6ad1b73985c32a4b97acff83989288b9e; ?>
<?php unset($__componentOriginalb8f5c8a6ad1b73985c32a4b97acff83989288b9e); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
            <?php endif; ?>
        </div>

    </div>
</div>

</body>
</html>
<?php /**PATH C:\Users\Maryam Alalawi\inspower\resources\views/emails/NewArticle.blade.php ENDPATH**/ ?>