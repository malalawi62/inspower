<html lang="<?php echo e(__('text.lang')); ?>" dir="<?php echo e(__('text.dir')); ?>">

<?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<body>

<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app form" id="main">

        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe" style="height: 90%">
            <div class="container">
                <div class="cta-inner">
                    <div class="row">
                        <h1 style="text-align: center; float: none"> <?php echo e(__('text.editMembershipType')); ?> </h1>
                    </div>
                    <div class="form wow fadeInUp" data-wow-delay="0.3s">
                        <form class="subscribe-form center-form wow zoomIn" action="/membershipType/<?php echo e($membershipType->id); ?>" method="post">
                            <?php echo method_field('PATCH'); ?>

                            <input class="mail" disabled type="text" name="type" autocomplete="off" id="type" value="<?php echo e($membershipType->type); ?>" style="margin-bottom: 10px">

                            <input dir="rtl" disabled class="mail" type="text" name="typeAr" autocomplete="off" id="typeAr" value="<?php echo e($membershipType->typeAr); ?>" style="margin-bottom: 10px">

                            <?php $__errorArgs = ['fees'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> <h5 style="color: red"><?php echo e($message); ?></h5> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            <input class="mail" type="text" name="fees" autocomplete="off" id="fees" value="<?php echo e($membershipType->fees); ?>" style="margin-bottom: 10px">

                            <?php echo csrf_field(); ?>
                            <div>
                                <button class="submit-button" style="width: 80%; margin-top: 30px; margin-bottom: 15px">
                                    <?php echo e(__('text.editMembershipType')); ?>

                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        <?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
<?php echo $__env->make('includes.footerScripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>
</html>
<?php /**PATH /Users/maryamal-alawi/inspower/resources/views/membershipTypes/edit.blade.php ENDPATH**/ ?>