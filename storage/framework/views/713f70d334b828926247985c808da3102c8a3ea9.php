<html lang="<?php echo e(__('text.lang')); ?>" dir="<?php echo e(__('text.dir')); ?>">

<?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<body>

<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app" id="main">

        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe" style="min-height: 90%">
            <div class="container">
                <div class="cta-inner">
                    <div class="row">
                        <h1 style="float: none; text-align: center"> <?php echo e(__('text.addCategoryTitle')); ?> </h1>
                    </div>
                    <div class="wow fadeInUp" data-wow-delay="0.3s">
                        <form class="forms-form center-form wow zoomIn" action="/categories" method="post">

                            <?php $__errorArgs = ['englishName'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> <h5 style="color: red"><?php echo e($message); ?></h5> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            <input class="mail" type="text" name="englishName" placeholder="Category English Name *"
                                   autocomplete="off" id="englishName" value="<?php echo e(old('englishName')); ?>">

                            <?php $__errorArgs = ['arabicName'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> <h5 style="color: red"><?php echo e($message); ?></h5> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            <input dir="rtl" class="mail" type="text" name="arabicName"
                                   placeholder="اسم الفئة باللغة العربية *" autocomplete="off" id="arabicName"
                                   value="<?php echo e(old('arabicName')); ?>">

                            <?php echo csrf_field(); ?>
                            <div>
                                <button class="submit-button" style="width: 60%;">
                                    <?php echo e(__('text.addCategoryBtn')); ?>

                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        <?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
<?php echo $__env->make('includes.footerScripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>
</html>
<?php /**PATH C:\Users\Maryam Alalawi\inspower\resources\views/categories/create.blade.php ENDPATH**/ ?>