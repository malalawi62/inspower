<!DOCTYPE html>
<html lang="<?php echo e(__('text.lang')); ?>" dir="<?php echo e(__('text.dir')); ?>">

<?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<body>

<script>
    var msg = '<?php echo e(Session::get('alert')); ?>';
    var exist = '<?php echo e(Session::has('alert')); ?>';
    if (exist) {
        alert(msg);
    }
</script>

<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app" id="main">
        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe">
            <div class="container">
                <div class="cta-inner">
                    <div class="row">
                        <h1 style="text-align: center; float: none"> <?php echo e(__('text.editPhaseTitle')); ?> </h1>
                    </div>

                    <div class="wow fadeInUp" data-wow-delay="0.3s">
                        <form class="forms-form center-form wow zoomIn" action="/phase/<?php echo e($phases->id); ?>" method="post"
                              enctype="multipart/form-data">
                            <?php echo method_field('PATCH'); ?>

                            <?php $__errorArgs = ['title'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> <h5 style="color: red;"><?php echo e($message); ?></h5> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            <input class="mail" type="text" name="title"
                                   autocomplete="title" value="<?php echo e($phases->title); ?>">

                            <?php $__errorArgs = ['overview'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> <p style="color: red"><?php echo e($message); ?></p> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            <p><?php echo e(__('text.phaseOverview')); ?></p>
                            <textarea class="mail" rows="20" name="overview" style="height: auto;"><?php echo e($phases->overview); ?></textarea>

                            <input class="mail" type="text" name="videoUrl"
                                   placeholder="<?php echo e(__('text.phaseVideoURL')); ?>"
                                   autocomplete="off" value="<?php if($phases->videoUrl != null): ?> https://vimeo.com/<?php echo e($phases->videoUrl); ?> <?php endif; ?>">

                            <p><?php echo e(__('text.phaseDescription')); ?></p>
                            <textarea class="mail" rows="20" name="description" style="height: auto;"><?php echo e($phases->description); ?></textarea>

                            <?php $__errorArgs = ['pdf'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> <h5 style="color: red"><?php echo e($message); ?></h5> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            <label class="mail" style="border: 1.5px solid #babdc2; padding-top: 10px">
                                <?php echo e(__('text.phasePdf')); ?>

                                <input type="file" name="pdf"
                                       style="display: inline-block"/>
                            </label>

                            <?php echo csrf_field(); ?>
                            <div>
                                <button class="submit-button" style="width: 60%;">
                                    <?php echo e(__('text.editPhaseBtn')); ?>

                                </button>
                            </div>
                        </form>

                        <hr style="width: 100%; border-width:1px">

                        <div class="modules">
                            <?php $counter = 0; ?>
                            <?php $__currentLoopData = $sections; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $section): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php $counter ++; ?>
                                <div class="col-md-12" style="padding-bottom: 30px">
                                    <div class="row">
                                        <form action="/section/<?php echo e($section->id); ?>/delete" method="post">
                                            <h2 class="h2" style="text-transform: uppercase"> <?php echo e(__('text.sectionTitle')); ?> : <?php echo e($counter); ?>

                                                <a href="/section/<?php echo e($section->id); ?>/edit" style="color: #364f87">
                                                    <i class="ion-edit"></i>
                                                </a>
                                                <button onclick="return confirm('Are you sure you want to DELETE this SECTION?')"
                                                        style="color: #364f87; background-color: transparent; border-width: 0; font-size: 24px">
                                                    <i class="ion-android-delete"></i>
                                                </button>
                                            </h2>
                                            <?php echo csrf_field(); ?>
                                        </form>
                                    </div>

                                    <div class="row">
                                        <p class="article-body"> <a href="https://vimeo.com/<?php echo e($section->videoUrl); ?>"> https://vimeo.com/<?php echo e($section->videoUrl); ?> </a> </p>
                                    </div>
                                    <div class="row">
                                        <p class="article-body"> <?php echo e($section->description); ?> </p>
                                    </div>
                                </div>

                                <hr style="width: 100%; border-width:1px">
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                <form action="/section/create" method="get">
                                    <input type="hidden" name="phase_id" value="<?php echo e($phases->id); ?>">
                                    <div>
                                        <button class="submit-button" style="width: 60%;">
                                            <?php echo e(__('text.addNewSection')); ?>

                                        </button>
                                    </div>
                                    <?php echo csrf_field(); ?>
                                </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        <?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
<?php echo $__env->make('includes.footerScripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>
</html>
<?php /**PATH C:\Users\Maryam Alalawi\inspower\resources\views/programs/editPhase.blade.php ENDPATH**/ ?>