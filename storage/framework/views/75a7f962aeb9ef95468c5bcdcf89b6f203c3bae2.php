<html lang="<?php echo e(__('text.lang')); ?>" dir="<?php echo e(__('text.dir')); ?>">

<?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<body>

<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app form" id="main">
        <div class="pricing-section text-center" style="height: 90%">
            <div class="container">
                <div class="col-md-12 col-sm-12 nopadding">
                    <div class="pricing-intro banner-padding">
                        <h1 class="wow fadeInUp" data-wow-delay="0s"> <?php echo e(__('text.allMembershipTypesTitle')); ?> </h1>
                    </div>
                    <div class="col-sm-12">
                        <div class="table-left wow fadeInUp" data-wow-delay="0.4s">
                            <div class="pricing-details">
                                <div>
                                    <?php $__empty_1 = true; $__currentLoopData = $membershipTypes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $membershipType): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                        <form action="/membershipType/<?php echo e($membershipType->id); ?>" method="post">
                                            <div class="row">
                                                <span class="article-title">
                                                    <a style="color: #ee2724;">
                                                        <?php if($lang == 'en'): ?>
                                                            <?php echo e($membershipType->type); ?>

                                                        <?php else: ?>
                                                            <?php echo e($membershipType->typeAr); ?>

                                                        <?php endif; ?>
                                                    </a>
                                                </span>
                                                <span class="editDeleteIcons">
                                                    <a href="/membershipType/<?php echo e($membershipType->id); ?>/edit" style="color: #364f87">
                                                        <i class="ion-edit"></i>
                                                    </a>
                                                    <?php if($membershipType->status == 'active'): ?>
                                                        <a href="/membershipType/<?php echo e($membershipType->id); ?>" onclick="return confirm('Are you sure you want to INACTIVE this type of memberships?')" style="color: #364f87">
                                                            <i class="fas fa-toggle-on"></i>
                                                        </a>
                                                    <?php else: ?>
                                                        <a href="/membershipTypeActive/<?php echo e($membershipType->id); ?>" style="color: #364f87">
                                                            <i class="fas fa-toggle-off"></i>
                                                        </a>
                                                    <?php endif; ?>
                                                </span>
                                            </div>
                                            <?php echo csrf_field(); ?>
                                        </form>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                        <h2>No Membership Types Found</h2>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        <?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
<?php echo $__env->make('includes.footerScripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>
</html>
<?php /**PATH C:\Users\Maryam Alalawi\inspower\resources\views/membershipTypes/index.blade.php ENDPATH**/ ?>