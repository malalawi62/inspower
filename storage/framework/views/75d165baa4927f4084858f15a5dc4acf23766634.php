<style>
    @media (min-width: 1025px) {
        .navbar-nav.navbar-center {
            position: absolute;
            left: 50%;
            transform: translatex(-50%);
        }
    }
    @media (max-width: 1025px) {
        .background-language {
            background-color: rgba(178, 198, 188, 0.4);
            color: #2D322F;
        }
    }
</style>

<div class="container">
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <a href="/"><img src="<?php echo e(asset('images/logo.png')); ?>" class="navbar-brand" alt="INSPOWER" title="INSPOWER"/></a>
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false" aria-controls="navbar">
                    <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
                </button>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-center">
                    <li class="search-screen-size-small">
                        <?php echo $__env->make('layout.searchBar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    </li>
                    <li><a href="/"> <?php echo e(__('text.home')); ?> </a>

                    <?php if(auth()->guard()->guest()): ?>
                    <?php else: ?>
                        <?php if(auth()->user()->type == 'admin'): ?>
                        <li class="dropdown">
                            <a id="navbarDropdown" class="dropdown-toggle" href="#" role="button" data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false" v-pre>
                                <?php echo e(__('text.adminPanel')); ?> <span class="caret"></span>
                            </a>
                            <ul class="nav navbar-nav dropdown-menu" role="menu" style="text-align: center;">
                                <li><a href="<?php echo e(url('/subscribersList')); ?>"> <?php echo e(__('text.subscribers')); ?> </a></li>
                                <li><a href="<?php echo e(url('/usersList')); ?>"> <?php echo e(__('text.allUsers')); ?> </a></li>
                                <li class="nav-divider"></li>
                                <li><a href="<?php echo e(url('/articles/create')); ?>"> <?php echo e(__('text.addArticle')); ?> </a></li>
                                <li class="nav-divider"></li>
                                <li><a href="/categories/create"> <?php echo e(__('text.addCategory')); ?> </a></li>
                                <li><a href="/categories"> <?php echo e(__('text.editDeleteCategory')); ?> </a></li>
                                <li class="nav-divider"></li>
                                <li><a href="/writers/create"><?php echo e(__('text.addWriter')); ?></a></li>
                                <li><a href="/writers"><?php echo e(__('text.editDeleteWriter')); ?></a></li>
                                <li class="nav-divider"></li>
                                <li><a href="/videos/createVimeo"><?php echo e(__('text.addVimeoTitle')); ?></a></li>
                                <li class="nav-divider"></li>
                                <li><a href="/course/create"><?php echo e(__('text.addCourses')); ?></a></li>
                                <li><a href="/program/create"><?php echo e(__('text.addPrograms')); ?></a></li>
                            </ul>
                        </li>
                            <?php endif; ?>
                    <?php endif; ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <?php echo e(__('text.products')); ?> <span class="caret"></span></a>
                        <ul class="nav navbar-nav dropdown-menu" role="list" style="text-align: center;">
                            <li class="dropdown">
                               <a href="<?php echo e(url('/courses/')); ?>"><?php echo e(__('text.courses')); ?></a>
                            </li>
                            <li class="dropdown">
                                <a href="<?php echo e(url('/programs/')); ?>"><?php echo e(__('text.programs')); ?></a>
                            </li>
                        </ul>
                    </li>

                    <li><a href="<?php echo e(url('/articles')); ?>"> <?php echo e(__('text.articles')); ?> </a></li>

                    <li><a href="/videos"> <?php echo e(__('text.videos')); ?> </a></li>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <?php echo e(__('text.about')); ?> <span class="caret"></span></a>
                        <ul class="nav navbar-nav dropdown-menu" role="menu" style="text-align: center;">
                            <li><a href="<?php echo e(url('/about')); ?>"><?php echo e(__('text.aboutUs')); ?></a></li>
                            <li><a href="<?php echo e(url('/team')); ?>"><?php echo e(__('text.ourTeam')); ?></a></li>
                            <li><a href="<?php echo e(url('/services')); ?>"><?php echo e(__('text.services')); ?></a></li>
                            <li><a href="<?php echo e(url('/contact')); ?>"><?php echo e(__('text.contact')); ?></a></li>
                        </ul>
                    </li>

                    <?php if((App::isLocale('ar'))): ?>
                        <li><a href="/setlocale/en" class="background-language" style="text-align: center;"><?php echo e(__('text.english')); ?></a></li>
                    <?php endif; ?>
                    <?php if((App::isLocale('en'))): ?>
                        <li><a href="/setlocale/ar" class="background-language" style="text-align: center;"><?php echo e(__('text.arabic')); ?></a></li>
                    <?php endif; ?>

                </ul>

                <div class="form-inline my-2 my-lg-0 search-screen-size-big <?php if((App::isLocale('ar'))): ?> navbar-left <?php endif; ?> <?php if((App::isLocale('en'))): ?> navbar-right <?php endif; ?>">
                    <?php echo $__env->make('layout.searchBar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                </div>

            </div>
        </div>
    </nav>
    <!-- /.navbar-collapse -->
</div>
<?php /**PATH /Users/maryam/inspower/resources/views/layout/header.blade.php ENDPATH**/ ?>