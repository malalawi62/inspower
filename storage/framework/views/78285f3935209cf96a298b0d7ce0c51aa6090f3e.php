<html lang="<?php echo e(__('text.lang')); ?>" dir="<?php echo e(__('text.dir')); ?>">

<?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<body>

<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<?php $flag = false; ?>
<?php if($orders != null): ?>
    <?php for($i=0; $i<count($orders); $i++): ?>
        <?php if($orders[$i]->course_id == $course->id): ?>
            <?php $flag = true; ?>
            <?php break; ?>
        <?php endif; ?>
    <?php endfor; ?>
<?php endif; ?>
<?php if(auth()->guard()->guest()): ?>
<?php else: ?>
<?php if(auth()->user()->type == 'admin'): ?>
    <?php $flag = true; ?>
<?php endif; ?>
<?php endif; ?>
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app form" id="main">

        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe" style="min-height: 100%">
            <div class="container">
                <div class="cta-inner">
                    <div class="col-md-12">
                        <form class="subscribe-form" action="/cart" method="post">
                            <input type="hidden" name="course_id" value="<?php echo e($course->id); ?>">
                            <?php if(auth()->guard()->guest()): ?>
                            <?php else: ?>
                                <input type="hidden" name="user_id" value="<?php echo e(auth()->user()->id); ?>">
                            <?php endif; ?>
                            <div class="row">
                                <span class="h1" style="color: #ee2724; padding: 5px; width: 100%"><?php echo e($course->title); ?> <?php if($course->status == 'inactive'): ?> <span style="color: #2D322F; font-weight: 700;">*<?php echo e(__('text.inactiveCourse')); ?>*</span> <?php endif; ?>
                                    <?php if($flag == false): ?>
                                        <button class="addToCartBtn" title="<?php echo e(__("text.addToCartTitle")); ?>">
                                            <i class="ion-android-cart"></i>
                                        </button>
                                    <?php endif; ?>
                                </span>
                            </div>
                            <?php echo csrf_field(); ?>
                        </form>
                        <form action="/course/<?php echo e($course->id); ?>" method="post">
                            <?php if(auth()->guard()->guest()): ?>
                                <?php else: ?>
                                <?php if(auth()->user()->type == 'admin'): ?>
                                    <span class="editDeleteIcons">
                                        <a href="/course/<?php echo e($course->id); ?>/edit" style="color: #364f87">
                                            <i class="ion-edit"></i>
                                        </a>
                                        <a href="/sendCourse/<?php echo e($course->id); ?>" style="color: #364f87">
                                            <i class="ion-android-send"></i>
                                        </a>
                                        <?php if($course->status == 'active'): ?>
                                            <button onclick="return confirm('Are you sure you want to INACTIVE this course?')"
                                                    style="color: #364f87; background-color: transparent; border-width: 0; font-size: 24px">
                                                <i class="fas fa-toggle-off"></i>
                                            </button>
                                        <?php else: ?>
                                            <a href="/courseActive/<?php echo e($course->id); ?>"
                                               style="color: #364f87">
                                                <i class="fas fa-toggle-on"></i>
                                            </a>
                                        <?php endif; ?>
                                    </span>
                                <?php endif; ?>
                            <?php endif; ?>
                            <?php echo csrf_field(); ?>
                        </form>
                    </div>

                    <div class="form wow fadeInUp" data-wow-delay="0.3s">
                        <div class="row" style="padding-bottom: 20px">
                            <img class="img-responsive wow fadeIn" src="<?php echo e(asset('storage/'.$course->image)); ?>"
                                 alt="Talal Nabeel" style="max-height: 500px; display: block"/>
                        </div>
                    </div>

                    <div>
                        <form action="/cart" method="post">
                            <input type="hidden" name="course_id" value="<?php echo e($course->id); ?>">
                            <?php if(auth()->guard()->guest()): ?>
                            <?php else: ?>
                                <input type="hidden" name="user_id" value="<?php echo e(auth()->user()->id); ?>">
                            <?php endif; ?>

                            <?php if($flag == false): ?>
                                <button class="addToCartBtn2" type="submit">
                                    <?php echo e(__('text.addToCartBtn')); ?>

                                </button>
                            <?php endif; ?>
                            <?php echo csrf_field(); ?>
                        </form>
                    </div>

                    <div class="line"> </div>

                    <div class="row">
                        <h2 class="h2"> <?php echo e(__('text.courseOverview')); ?> </h2>
                        <p class="article-body" style="color: #2D322F">
                            <?php echo nl2br($course->overview); ?>

                        </p>
                    </div>


                    <?php if($course->videoUrl != null): ?>
                        <div class="row">
                            <iframe class="iframe-courses"
                                    src="https://player.vimeo.com/video/<?php echo e($course->videoUrl); ?>"
                                    frameborder="0" allowfullscreen></iframe>
                        </div>
                    <?php endif; ?>

                    <?php if($course->description != null): ?>
                        <div class="line"> </div>

                        <div class="row">
                            <h2 class="h2"> <?php echo e(__('text.courseDescription')); ?> </h2>
                            <p class="article-body"  style="color: #2D322F">
                                <?php echo nl2br($course->description); ?>

                            </p>
                        </div>
                    <?php endif; ?>



                    <form action="/cart" method="post">
                        <input type="hidden" name="course_id" value="<?php echo e($course->id); ?>">
                        <?php if(auth()->guard()->guest()): ?>
                        <?php else: ?>
                            <input type="hidden" name="user_id" value="<?php echo e(auth()->user()->id); ?>">
                        <?php endif; ?>

                        <?php if($flag == false): ?>
                                <button class="addToCartBtn2" type="submit">
                                    <?php echo e(__('text.addToCartBtn')); ?>

                                </button>
                        <?php endif; ?>
                        <?php echo csrf_field(); ?>
                    </form>
                <?php if($flag == true): ?>
                        <div class="line"> </div>

                    <div class="row">
                        <h2 class="h2"> <?php echo e(__('text.courseModules')); ?> </h2>
                    </div>

                    <?php $counter = 0; ?>
                    <?php $__currentLoopData = $modules; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $module): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php $counter ++; ?>
                        <button class="accordion">
                            <?php echo e(__('text.module')); ?> <?php echo e($counter); ?>: <?php echo e($module->m_title); ?>

                        </button>
                        <div class="panel">
                            <h2 class="h2"> <?php echo e(__('text.moduleDescriptionShow')); ?>: </h2>
                            <p style="color: #2D322F; text-align: justify"><?php echo e($module->m_description); ?></p>

                            <h2 class="h2"> <?php echo e(__('text.moduleLessons')); ?>: </h2>

                            <?php $v_counter = 0; ?>
                            <?php $__currentLoopData = $lessons; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lesson): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if($lesson->course_module_id == $module->id): ?>
                                    <?php $v_counter ++; ?>
                                        <?php $__currentLoopData = $status; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $state): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if($state->course_module_video_id == $lesson->id): ?>
                                                <button class="accordion2" onclick="myfunction('iframe-<?php echo e($counter); ?>-<?php echo e($v_counter); ?>', '<?php echo e($state->id); ?>', 'status-<?php echo e($counter); ?>-<?php echo e($v_counter); ?>')">
                                                    <?php echo e(__('text.lesson')); ?> <?php echo e($v_counter); ?>

                                                <?php if($state->status == 'incomplete'): ?>
                                                    <span id="status-<?php echo e($counter); ?>-<?php echo e($v_counter); ?>"> <i class="fas fa-exclamation-circle incomplete" title="<?php echo e(__('text.incomplete')); ?>"></i> <?php echo e(__('text.incomplete')); ?></span>
                                                <?php else: ?>
                                                    <span id="status-<?php echo e($counter); ?>-<?php echo e($v_counter); ?>"> <i class="fas fa-check-circle complete" title="<?php echo e(__('text.complete')); ?>"></i> <?php echo e(__('text.complete')); ?></span>
                                                <?php endif; ?>
                                                </button>
                                                <div class="panel">
                                                    <iframe class="iframe-video" id="iframe-<?php echo e($counter); ?>-<?php echo e($v_counter); ?>"
                                                            src="https://player.vimeo.com/video/<?php echo e($lesson->m_videoUrl); ?>?autoplay=1#t=<?php echo e($state->currentSecond); ?>s"
                                                            frameborder="0" allowfullscreen>
                                                    </iframe>
                                                    <p style="color: #2D322F; padding-bottom: 10px; text-align: justify"><?php echo e($lesson->v_description); ?></p>
                                                    <div id="read" class="showPdf">
                                                        <a class="pdfBody" href="/lessonPdf/<?php echo e($lesson->pdf); ?>#toolbar=0" target="_blank" style="color: #ffffff;"> <?php echo e(__('text.viewPdf')); ?> </a>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <br />
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
            </div>
        </div>
        </div>

        <?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>

        <!-- Footer Section -->

    </div>
    <!-- Main Section -->
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
<?php echo $__env->make('includes.footerScripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>
</html>
<?php /**PATH C:\Users\Maryam Alalawi\inspower\resources\views/courses/show.blade.php ENDPATH**/ ?>