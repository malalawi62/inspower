<!DOCTYPE html>
<html lang="<?php echo e(__('text.lang')); ?>" dir="<?php echo e(__('text.dir')); ?>">

<?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<body>
<script>
    var msg = '<?php echo e(Session::get('alert')); ?>';
    var exist = '<?php echo e(Session::has('alert')); ?>';
    if (exist) {
        alert(msg);
    }
</script>
<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app" id="main">
        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe">
            <div class="container">
                <div class="cta-inner">
                    <div class="row">
                        <h1 style="text-align: center; float: none"> <?php echo e(__('text.addModuleBtn')); ?> </h1>
                    </div>

                    <div class="wow fadeInUp" data-wow-delay="0.3s">
                        <form class="forms-form center-form wow zoomIn" action="/module" method="post"
                              enctype="multipart/form-data">
                            <input type="hidden" name="course_id" id="course_id" value="<?php echo e($course_id); ?>">
                            <div class="modules">
                                <div class="module module_wrapper_1">
                                    <div class="row">
                                        <h1 style="text-align: center; float: none; padding-top: 0" id="module_title_1"> <?php echo e(__('text.moduleNew')); ?>

                                            <input class="add_module_button" type="button" onclick="addModule(1)" value="+" style="color: #ee2724; background: transparent; border-color: transparent; border-width: 0">
                                        </h1>
                                    </div>

                                    <?php $__errorArgs = ['m_title'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> <h5 style="color: red;"><?php echo e($message); ?></h5> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                    <input class="mail" type="text" name="m_title[]"
                                           placeholder="<?php echo e(__('text.moduleTitle')); ?>"
                                           autocomplete="m_title" value="<?php echo e(old('m_title')); ?>">

                                    <p><?php echo e(__('text.moduleDescription')); ?></p>
                                    <textarea class="mail" rows="10" name="m_description[]" style="height: auto;" value="<?php echo e(old('m_description')); ?>"></textarea>

                                    <div class="videos_1">
                                        <div class="video_wrapper video_1">
                                            <div class="row">
                                                <h1 style="text-align: center; float: none; padding-top: 0" id="lesson_title_1"> <?php echo e(__('text.lessonNew')); ?>

                                                    <input class="add_video_course_button" type="button" onclick="addCourseVideo(1)" value="+" style="color: #ee2724; background: transparent; border-color: transparent; border-width: 0">
                                                </h1>
                                            </div>

                                            <div class="row">
                                                <input class="mail" type="text" name="m_videoUrl[1][]"
                                                       placeholder="<?php echo e(__('text.moduleVideoURL')); ?>"
                                                       autocomplete="off" value="<?php echo e(old('m_videoUrl')); ?>" />
                                                <label class="mail" style="border: 1.5px solid #babdc2; padding-top: 10px">
                                                    <?php echo e(__('text.inputLessonPdf')); ?>

                                                    <input type="file" name="pdf[1][]" id="bodyPdf" style="display: inline-block"/>
                                                </label>
                                                <p><?php echo e(__('text.lessonDescription')); ?></p>
                                                <textarea class="mail" rows="10" name="v_description[1][]" style="height: auto;" value="<?php echo e(old('v_description')); ?>"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php echo csrf_field(); ?>
                            <div>
                                <div>
                                    <button class="submit-button" style="width: 60%;">
                                        <?php echo e(__('text.addModuleBtn')); ?>

                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        <?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
<?php echo $__env->make('includes.footerScripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>
</html>
<?php /**PATH C:\Users\Maryam Alalawi\inspower\resources\views/courses/createModule.blade.php ENDPATH**/ ?>