<head>
    <meta charset="utf-8">
    <title> <?php echo e($title ?? "INSPOWER FITNESS COACHING"); ?> </title>
    <link rel="icon" href="<?php echo e(asset('images/v-logo.png')); ?>" type="image/png" sizes="10x10">
    <link rel="apple-touch-icon" href="<?php echo e(asset('images/v-logo.png')); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Inspire The Mind Empower The Body">
    <meta name="keywords" content="inspower, training, fitness, coaching, sport, articles">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <link href="<?php echo e(asset('css/bootstrap3.min.css')); ?>" rel="stylesheet" type="text/css" media="all" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900">
    <link rel="stylesheet" href="https://use.typekit.net/vkr4ypu.css">
    <link rel="stylesheet" href="<?php echo e(asset('css/animate.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/ar.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/owl.carousel.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/owl.theme.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/ionicons.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/style5.css')); ?>" type="text/css" media="all" />
    <link href="<?php echo e(asset('fonts/fontawesome/css/fontawesome.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('fonts/fontawesome/css/all.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('fonts/fontawesome/css/brands.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('fonts/fontawesome/css/solid.css')); ?>" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Calibri:400,700,400italic,700italic" rel="stylesheet">
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
    <link href='https://cdn.jsdelivr.net/npm/boxicons@2.0.5/css/boxicons.min.css' rel='stylesheet'>
    <link rel="stylesheet" href="<?php echo e(asset('css/magnific-popup.css')); ?>">
    <!-- or -->
    <link href='https://unpkg.com/boxicons@2.0.5/css/boxicons.min.css' rel='stylesheet'>
    <link href="//db.onlinewebfonts.com/c/6184b9bbcb2982ca518dc0c37b9d3199?family=HelveticaNeueLT+Arabic+55+Roman" rel="stylesheet" type="text/css"/>
</head>
<?php /**PATH /Users/maryamal-alawi/inspower/resources/views/includes/head.blade.php ENDPATH**/ ?>