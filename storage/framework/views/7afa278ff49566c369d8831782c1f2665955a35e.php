<html lang="<?php echo e(__('text.lang')); ?>" dir="<?php echo e(__('text.dir')); ?>">

<?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<body>

<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="wrapper" style="background-color: #2D322F">
    <!-- Main Section-->
    <div class="main app form" id="main">
        <div class="pricing-section text-center" style="min-height: 100%">
            <div class="container">
                <div class="col-md-8 col-md-offset-2 col-sm-12 nopadding text-center" style="float: left">
                    <div class="pricing-intro">
                        <h1 class="wow fadeInUp" data-wow-delay="0s"> <?php echo e(__('text.cartTitle')); ?> </h1>
                    </div>

                    <?php if($courses != null || $programs != null): ?>
                        <div class="table-left wow fadeInUp" data-wow-delay="0.4s" style="overflow: auto;">
                            <div class="pricing-details">
                                <div style="display: block">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-8">
                                            <span class="video-title">
                                                <h1 style="color: #ee2724; font-size:20px"> <?php echo e(__('text.cartRowTitle')); ?> </h1>
                                            </span>
                                            </div>

                                            <div class="col-md-3">
                                            <span class="video-title">
                                                <h1 style="color: #ee2724; font-size:20px;"> <?php echo e(__('text.cartPrice')); ?> </h1>
                                            </span>
                                            </div>
                                        </div>

                                        <hr style="width: 100%; border-width:1px">

                                        <?php if($courses != null): ?>
                                            <?php $__currentLoopData = $courses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $course): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <div class="row">
                                                    <div class="col-md-8">
                                                    <span class="video-title">
                                                        <a style="color: #364f87; font-size:20px" href="/course/<?php echo e($course->id); ?>_<?php echo e(str_replace(' ', '_', $course->title)); ?>">
                                                            <?php echo e($course->title); ?>

                                                        </a>
                                                    </span>
                                                    </div>

                                                    <div class="col-md-3">
                                                    <span class="video-title">
                                                        <h1 style="color: #364f87; font-size:20px;"> <?php echo e($course->price); ?> <?php echo e(__('text.BD')); ?> </h1>
                                                    </span>
                                                    </div>

                                                    <div class="col-md-1">
                                                    <span class="video-title">
                                                        <form action="/removeCourseFromCart/<?php echo e($course->id); ?>" method="post">
                                                            <?php echo method_field('DELETE'); ?>
                                                            <button onclick="return confirm('Are you sure you want to remove this course from your cart?')"
                                                                    style="color: #ee2724; background-color: transparent; border-width: 0; font-size: 24px">
                                                                <i class="ion-android-delete"></i>
                                                            </button>
                                                            <?php echo csrf_field(); ?>
                                                        </form>
                                                    </span>
                                                    </div>
                                                </div>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>

                                        <?php if($programs != null): ?>
                                            <?php $__currentLoopData = $programs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $program): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <div class="row">
                                                    <div class="col-md-8">
                                                    <span class="video-title">
                                                        <a style="color: #364f87; font-size:20px" href="/program/<?php echo e($program->id); ?>_<?php echo e(str_replace(' ', '_', $program->title)); ?>">
                                                        <?php echo e($program->title); ?>

                                                        </a>
                                                    </span>
                                                    </div>

                                                    <div class="col-md-3">
                                                    <span class="video-title">
                                                        <h1 style="color: #364f87; font-size:20px;"> <?php echo e($program ->price); ?> <?php echo e(__('text.BD')); ?> </h1>
                                                    </span>
                                                    </div>

                                                    <div class="col-md-1">
                                                    <span class="video-title">
                                                        <form action="/removeProgramFromCart/<?php echo e($program->id); ?>" method="post">
                                                            <?php echo method_field('DELETE'); ?>
                                                            <button onclick="return confirm('Are you sure you want to remove this program from your cart?')"
                                                                    style="color: #ee2724; background-color: transparent; border-width: 0; font-size: 24px">
                                                                <i class="ion-android-delete"></i>
                                                            </button>
                                                            <?php echo csrf_field(); ?>
                                                        </form>
                                                    </span>
                                                    </div>
                                                </div>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </div>
                                    <br>
                                </div>
                            </div>
                        </div>

                        <hr style="width: 100%; border-width:1px">

                        <div class="row">
                            <div class="col-md-9">
                            <span class="video-title">
                                <h1 style="color: #2D322F; font-size:24px; font-weight: 800;"> <?php echo e(__('text.cartTotal')); ?>: <span style="color: red"> <?php echo e($counter); ?> <?php echo e(__('text.BD')); ?> </span></h1>
                            </span>
                            </div>

                            <form action="/confirmOrder" method="GET">
                                <div class="col-md-3">
                                    <button class="cart-button" type="submit"
                                            style="<?php echo e(__('text.opFloating')); ?>; padding-left: 15px; padding-right: 15px">
                                        <?php echo e(__('text.checkoutBtn')); ?>

                                    </button>
                                </div>
                                <?php echo csrf_field(); ?>
                            </form>

                        </div>
                    <?php else: ?>
                        <div class="row">
                            <div class="col-md-12">
                                <span class="h1">
                                    <h1 style="color: #ee2724; font-size:20px; text-align: center; float: none"> <?php echo e(__('text.empty')); ?> </h1>
                                </span>
                                <hr style="width: 100%; border-width:1px">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <form action="" method="get">
                                    <div class="col-md-6">
                                        <button class="checkoutBtn" formaction="/programs">
                                            <?php echo e(__('text.viewPrograms')); ?>

                                        </button>
                                    </div>

                                    <div class="col-md-6">
                                        <button class="checkoutBtn"  formaction="/courses">
                                            <?php echo e(__('text.viewCourses')); ?>

                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <!-- Footer Section -->
        <?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
<?php echo $__env->make('includes.footerScripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>
</html>
<?php /**PATH C:\Users\ZAINAB ALMOOT\inspower\resources\views/cart/cart.blade.php ENDPATH**/ ?>