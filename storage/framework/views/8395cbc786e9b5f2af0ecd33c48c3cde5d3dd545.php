<!DOCTYPE html>
<html lang="<?php echo e(__('text.lang')); ?>" dir="<?php echo e(__('text.dir')); ?>">

<?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<body>

<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app" id="main">
        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe">
            <div class="container">
                <div class="cta-inner">
                    <div class="row">
                        <h1 style="text-align: center; float: none"> <?php echo e(__('text.addCourseTitle')); ?> </h1>
                    </div>

                    <div class="wow fadeInUp" data-wow-delay="0.3s">
                        <form class="forms-form center-form wow zoomIn" action="/course" method="post"
                              enctype="multipart/form-data">

                            <div class="row">
                                <h1 style="text-align: center; float: none"> <?php echo e(__('text.courseDetails')); ?> </h1>
                            </div>

                            <?php $__errorArgs = ['title'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> <h5 style="color: red;"><?php echo e($message); ?></h5> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            <input class="mail" type="text" name="title"
                                   placeholder="<?php echo e(__('text.inputCourseTitle')); ?>"
                                   autocomplete="title" value="<?php echo e(old('title')); ?>">

                            <?php $__errorArgs = ['overview'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> <p style="color: red"><?php echo e($message); ?></p> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            <p><?php echo e(__('text.inputCourseOverview')); ?></p>
                            <textarea class="mail" rows="10" name="overview" style="height: auto;" value="<?php echo e(old('overview')); ?>"></textarea>

                            <?php $__errorArgs = ['image'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> <h5 style="color: red"><?php echo e($message); ?></h5> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            <input class="mail" type="file" name="image" id="image"
                                   style="display: inline-block;padding-top: 10px">

                            <?php $__errorArgs = ['price'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> <h5 style="color: red;"><?php echo e($message); ?></h5> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            <input class="mail" type="number" name="price"
                                   placeholder="<?php echo e(__('text.inputCoursePrice')); ?>"
                                   value="<?php echo e(old('price')); ?>">

                            <input class="mail" type="text" name="videoUrl"
                                   placeholder="<?php echo e(__('text.inputCourseVideo')); ?>"
                                   autocomplete="off" value="<?php echo e(old('videoUrl')); ?>">

                            <p><?php echo e(__('text.inputCourseDescription')); ?></p>
                            <textarea class="mail" rows="10" name="description" style="height: auto;" value="<?php echo e(old('description')); ?>"></textarea>

                            <?php if($lang == 'en'): ?>
                                <input type="hidden" name="language" id="language" value="Eng">
                            <?php else: ?>
                                <input type="hidden" name="language" id="language" value="Ar">
                            <?php endif; ?>

                            <hr style="width: 100%; border-width:1px">

                                <div class="modules">
                                    <div class="module_wrapper">
                                        <div class="row">
                                            <h1 style="text-align: center; float: none; padding-top: 0px"> <?php echo e(__('text.moduleNew')); ?> 1
                                                <input class="add_module_button" type="button" onclick="addModule()" value="+" style="color: #8c0000; background: transparent; border-color: transparent; border-width: 0">
                                            </h1>
                                        </div>

                                        <?php $__errorArgs = ['title'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> <h5 style="color: red;"><?php echo e($message); ?></h5> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                        <input class="mail" type="text" name="title"
                                               placeholder="<?php echo e(__('text.moduleTitle')); ?>"
                                               autocomplete="title" value="<?php echo e(old('title')); ?>">

                                        <p><?php echo e(__('text.moduleDescription')); ?></p>
                                        <textarea class="mail" rows="10" name="description" style="height: auto;" value="<?php echo e(old('description')); ?>"></textarea>

                                        <div class="video_wrapper">
                                            <div class="row">
                                                <input class="mail" type="text" name="videoUrl[]"
                                                       placeholder="<?php echo e(__('text.moduleVideoURL')); ?>"
                                                       autocomplete="off" value="<?php echo e(old('videoUrl')); ?>" style="width: 50%">
                                                <input class="add_video_button" type="button" onclick="addVideo()" value="+" style="color: #8c0000; background: transparent; border-color: transparent; border-width: 0">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <?php echo csrf_field(); ?>
                            <div>
                                <button class="btn btn-primary"
                                        style="background-color: #EC3642; border:0; padding: 10px 40px 10px 40px; width:60%;">
                                    <?php echo e(__('text.addCourseBtn')); ?>

                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        <?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
<?php echo $__env->make('includes.footerScripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>
</html>
<?php /**PATH /Users/maryam/inspower/resources/views/courses/create.blade.php ENDPATH**/ ?>