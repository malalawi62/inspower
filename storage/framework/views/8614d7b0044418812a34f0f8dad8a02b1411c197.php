<!DOCTYPE html>
<html lang="<?php echo e(__('text.lang')); ?>" dir="<?php echo e(__('text.dir')); ?>">

<?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<body>

<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app" id="main">
        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe">
            <div class="container">
                <div class="cta-inner">
                    <div class="row">
                        <h1 style="text-align: center; float: none"> <?php echo e(__('text.addLessonBtn')); ?> </h1>
                    </div>

                    <div class="wow fadeInUp" data-wow-delay="0.3s">
                        <form class="forms-form center-form wow zoomIn" action="" method="post"
                              enctype="multipart/form-data">
                            <input type="hidden" name="course_id" id="course_id" value="">
                            <div class="modules">
                                <div class="module module_wrapper_1">
                                    <div class="videos_1">
                                        <div class="video_wrapper video_1">
                                            <div class="row">
                                                <h1 style="text-align: center; float: none; padding-top: 0" id="lesson_title_1"> <?php echo e(__('text.lessonNew')); ?>

                                                    <input class="add_video_course_button" type="button" onclick="addCourseVideo(1)" value="+" style="color: #8c0000; background: transparent; border-color: transparent; border-width: 0">
                                                </h1>
                                            </div>

                                            <div class="row">
                                                <input class="mail" type="text" name="m_videoUrl[1][]"
                                                       placeholder="<?php echo e(__('text.moduleVideoURL')); ?>"
                                                       autocomplete="off" value="<?php echo e(old('m_videoUrl')); ?>" />
                                                <label class="mail" style="border: 1.5px solid #babdc2; padding-top: 10px">
                                                    <?php echo e(__('text.inputLessonPdf')); ?>

                                                    <input type="file" name="pdf" id="bodyPdf" style="display: inline-block"/>
                                                </label>
                                                <p><?php echo e(__('text.lessonDescription')); ?></p>
                                                <textarea class="mail" rows="10" name="v_description[1][]" style="height: auto;" value="<?php echo e(old('v_description')); ?>"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php echo csrf_field(); ?>
                            <div>
                                <div>
                                    <button class="btn btn-primary"
                                            style="background-color: #EC3642; border:0; padding: 10px 40px 10px 40px; width:60%;">
                                        <?php echo e(__('text.addLessonBtn')); ?>

                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        <?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
<?php echo $__env->make('includes.footerScripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>
</html>
<?php /**PATH C:\Users\ZAINAB ALMOOT\inspower\resources\views/courses/createLesson.blade.php ENDPATH**/ ?>