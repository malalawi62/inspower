<!DOCTYPE html>
<html lang="<?php echo e(__('text.lang')); ?>" dir="<?php echo e(__('text.dir')); ?>">

<?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<body>

<script>
    var msg = '<?php echo e(Session::get('alert')); ?>';
    var exist = '<?php echo e(Session::has('alert')); ?>';
    if (exist) {
        alert(msg);
    }
</script>

<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app" id="main">
        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe">
            <div class="container">
                <div class="cta-inner">
                    <div class="row">
                        <h1 style="text-align: center; float: none"> <?php echo e(__('text.editProgramTitle')); ?> </h1>
                    </div>

                    <div class="wow fadeInUp" data-wow-delay="0.3s">
                        <form class="forms-form center-form wow zoomIn" action="/program/<?php echo e($program->id); ?>" method="post"
                              enctype="multipart/form-data">
                            <?php echo method_field('PATCH'); ?>

                            <?php $__errorArgs = ['title'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> <h5 style="color: red;"><?php echo e($message); ?></h5> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            <input class="mail" type="text" name="title"
                                   autocomplete="title" value="<?php echo e($program->title); ?>">

                            <?php $__errorArgs = ['overview'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> <p style="color: red"><?php echo e($message); ?></p> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            <p><?php echo e(__('text.inputProgramOverview')); ?></p>
                            <textarea class="mail" rows="20" name="overview" style="height: auto;"><?php echo e($program->overview); ?></textarea>

                            <?php $__errorArgs = ['image'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> <h5 style="color: red"><?php echo e($message); ?></h5> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            <input class="mail" type="file" name="image" id="image"
                                   style="display: inline-block;padding-top: 10px">

                            <?php $__errorArgs = ['price'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> <h5 style="color: red;"><?php echo e($message); ?></h5> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            <input class="mail" type="number" name="price"
                                   value="<?php echo e($program->price); ?>">

                            <input class="mail" type="text" name="videoUrl"
                                   placeholder="<?php echo e(__('text.inputCourseVideo')); ?>"
                                   autocomplete="off" value="<?php if($program->videoUrl != null): ?> https://vimeo.com/<?php echo e($program->videoUrl); ?> <?php endif; ?>">

                            <p><?php echo e(__('text.inputProgramDescription')); ?></p>
                            <textarea class="mail" rows="20" name="description" style="height: auto;"><?php echo e($program->description); ?></textarea>

                            <?php $__errorArgs = ['pdf'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> <h5 style="color: red"><?php echo e($message); ?></h5> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            <label class="mail" style="border: 1.5px solid #babdc2; padding-top: 10px">
                                <?php echo e(__('text.inputProgramPdf')); ?>

                                <input type="file" name="pdf" id="Pdf"
                                       style="display: inline-block"/>
                            </label>

                            <?php echo csrf_field(); ?>
                            <div>
                                <button class="submit-button" style="width: 60%;">
                                    <?php echo e(__('text.editProgramBtn')); ?>

                                </button>
                            </div>
                        </form>

                        <div class="line"></div>

                        <div class="modules">
                            <?php $counter = 0; ?>
                            <?php $__currentLoopData = $phases; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $phase): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php $counter ++; ?>
                                <div class="row" style="padding-bottom: 30px">
                                    <div class="row">
                                        <form action="/phase/<?php echo e($phase->id); ?>/delete" method="post">
                                            <h2 class="h2" style="text-transform: uppercase"> <?php echo e(__('text.phaseTitle2')); ?>: <?php echo e($phase->title); ?>

                                                <a href="/phase/<?php echo e($phase->id); ?>/edit" style="color: #364f87">
                                                    <i class="ion-edit"></i>
                                                </a>
                                                <button onclick="return confirm('Are you sure you want to DELETE this PHASE?')"
                                                        style="color: #364f87; background-color: transparent; border-width: 0; font-size: 24px">
                                                    <i class="ion-android-delete"></i>
                                                </button>
                                            </h2>
                                            <?php echo csrf_field(); ?>
                                        </form>
                                    </div>

                                    <div class="row">
                                        <h2 class="h2" style="<?php echo e(__('text.textAlign')); ?>"> <?php echo e(__('text.phaseOverviewShow')); ?>: </h2>
                                        <p class="article-body">
                                            <?php echo nl2br($phase->overview); ?>

                                        </p>
                                    </div>

                                    <div class="row">
                                        <h2 class="h2" style="<?php echo e(__('text.textAlign')); ?>"> <?php echo e(__('text.phaseDescriptionShow')); ?>: </h2>
                                        <p class="article-body">
                                            <a href="https://vimeo.com/<?php echo e($phase->videoUrl); ?>"> https://vimeo.com/<?php echo e($phase->videoUrl); ?> </a>
                                        </p>
                                    </div>

                                    <div class="row">
                                        <p class="article-body">
                                            <?php echo nl2br($phase->description); ?>

                                        </p>
                                    </div>

                                    <div class="row">
                                        <h2 class="h2" style="<?php echo e(__('text.textAlign')); ?>"> <?php echo e(__('text.phaseSections')); ?>: </h2>
                                    </div>
                                    <?php $__currentLoopData = $sections; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $section): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if($section->program_phase_id == $phase->id): ?>
                                            <div class="row"  style="width: 80%">
                                                <p class="article-body"> <?php echo e($section->description); ?> </p>
                                            </div>

                                            <div class="row">
                                                <p class="article-body"> <a href="https://vimeo.com/<?php echo e($section->videoUrl); ?>"> https://vimeo.com/<?php echo e($section->videoUrl); ?> </a> </p>
                                            </div>

                                            <div class="row">
                                                <div class="line"></div>
                                            </div>

                                        <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        <!-- this section is to add a new phase -->
                                <form action="/phase/create" method="get">
                                    <input type="hidden" name="program_id" value="<?php echo e($program->id); ?>">
                                    <div>
                                        <button class="submit-button" style="width: 60%;">
                                            <?php echo e(__('text.addPhaseBtn')); ?>

                                        </button>
                                    </div>
                                </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        <?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
<?php echo $__env->make('includes.footerScripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>
</html>
<?php /**PATH C:\Users\Maryam Alalawi\inspower\resources\views/programs/edit.blade.php ENDPATH**/ ?>