<html lang="<?php echo e(__('text.lang')); ?>" dir="<?php echo e(__('text.dir')); ?>">

<?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<body>

<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app form" id="main">

        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe" style="min-height: 100%">
            <div class="container">
                <div class="cta-inner">
                    <div class="row">
                        <form action="/articles/<?php echo e($data['article']->id); ?>" method="post">
                            <?php echo method_field('DELETE'); ?>
                            <h1 style="color: #fdca00;"><?php echo e($data['article']->title); ?></h1>
                            <?php if(auth()->guard()->guest()): ?>
                            <?php else: ?>
                                <span class="editDeleteIcons">
                                    <a href="/articles/<?php echo e($data['article']->id); ?>/edit" style="color: #fdca00">
                                        <i class="ion-edit"></i>
                                    </a>
                                    <button
                                        style="color: #fdca00; background-color: transparent; border-width: 0; font-size: 24px">
                                        <i class="ion-android-delete"></i>
                                    </button>
                                </span>
                            <?php endif; ?>
                        </form>
                    </div>
                    <div class="row" style="padding: 10px">
                        <?php if($data['lang'] == 'en'): ?>
                            <h3 style="color: #fdca00; <?php echo e(__('text.textAlign')); ?>">
                                Category: <?php echo e($data['article']->category->englishName); ?></h3>
                        <?php else: ?>
                            <h3 style="color: #fdca00; <?php echo e(__('text.textAlign')); ?>">
                                الفئة: <?php echo e($data['article']->category->arabicName); ?></h3>
                        <?php endif; ?>
                    </div>
                    <div class="row" style="padding: 10px">
                        <?php if($data['article']->writerName != null): ?>
                            <h3 style="color: #F0E299; <?php echo e(__('text.textAlign')); ?>">Written
                                By: <?php echo e($data['article']->writerName); ?></h3>
                        <?php else: ?>
                            <?php if($data['lang'] == 'en'): ?>
                                <h3 style="color: #F0E299; <?php echo e(__('text.textAlign')); ?>">Written
                                    By: <?php echo e($data['article']->writer->englishName); ?></h3>
                            <?php else: ?>
                                <h3 style="color: #F0E299; <?php echo e(__('text.textAlign')); ?>">
                                    الكاتب: <?php echo e($data['article']->writer->arabicName); ?></h3>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                    <div class="row" style="padding: 10px">
                        <?php if($data['lang'] == 'en'): ?>
                            <h3 style="color: #F0E299; <?php echo e(__('text.textAlign')); ?>">
                                Date: <?php echo e($data['article']->date); ?></h3>
                        <?php else: ?>
                            <h3 style="color: #F0E299; <?php echo e(__('text.textAlign')); ?>">
                                التاريخ: <?php echo e($data['article']->date); ?></h3>
                        <?php endif; ?>
                    </div>
                    <hr style="width: 100%; border-width:1px">
                    <div class="form wow fadeInUp" data-wow-delay="0.3s">
                        <div class="row" style="padding-bottom: 20px">
                            <img class="img-responsive wow fadeIn" src="<?php echo e(asset('storage/'.$data['article']->image)); ?>"
                                 alt="Talal Nabeel" style="max-height: 500px; display: block"/>
                        </div>
                    </div>
                </div>
                <h2 class="article-body"> <?php echo nl2br(e($data['article']->body)); ?> </h2>
            </div>
        </div>

        <!-- Footer Section -->
        <?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
<?php echo $__env->make('includes.footerScripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>
</html>
<?php /**PATH C:\Users\Maryam\inspower\resources\views/articles/show.blade.php ENDPATH**/ ?>