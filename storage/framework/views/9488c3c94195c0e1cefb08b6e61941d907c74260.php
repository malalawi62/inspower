<html lang="<?php echo e(__('text.lang')); ?>" dir="<?php echo e(__('text.dir')); ?>">

<?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<body>

<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<?php $flag = false; ?>
<?php if(auth()->guard()->guest()): ?>
<?php else: ?>
<?php if(auth()->user()->type == 'admin'): ?>
    <?php $flag = true; ?>
    <?php elseif($orders != null): ?>
    <?php for($i=0; $i<count($orders); $i++): ?>
        <?php if($orders[$i]->program_id == $program->id): ?>
            <?php $flag = true; ?>
            <?php break; ?>
        <?php endif; ?>
    <?php endfor; ?>
<?php endif; ?>
<?php endif; ?>

<div class="wrapper">
    <!-- Main Section-->
    <div class="main app form" id="main">

        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe" style="min-height: 100%">
            <div class="container">
                <div class="cta-inner">
                    <div class="col-md-12">
                        <form action="/cart" method="post">
                            <input type="hidden" name="program_id" value="<?php echo e($program->id); ?>">
                            <?php if(auth()->guard()->guest()): ?>
                            <?php else: ?>
                                <input type="hidden" name="user_id" value="<?php echo e(auth()->user()->id); ?>">
                            <?php endif; ?>
                            <div class="row">
                                <span class="h1" style="color: #ee2724; padding: 5px; width: 100%"><?php echo e($program->title); ?> <?php if($program->status == 'inactive'): ?> <span style="color: #2D322F; font-weight: 700;">*<?php echo e(__('text.inactiveProgram')); ?>*</span> <?php endif; ?>
                                    <?php if($flag == false): ?>
                                        <button class="addToCartBtn" type="submit" title="<?php echo e(__("text.addToCartTitle")); ?>">
                                            <i class="ion-android-cart"></i>
                                        </button>
                                    <?php endif; ?>
                                </span>
                            </div>
                            <?php echo csrf_field(); ?>
                        </form>
                        <form action="/program/<?php echo e($program->id); ?>" method="post">
                            <?php if(auth()->guard()->guest()): ?>
                                <?php else: ?>
                                <?php if(auth()->user()->type == 'admin'): ?>
                                    <span class="editDeleteIcons">
                                        <a href="/program/<?php echo e($program->id); ?>/edit" style="color: #364f87">
                                            <i class="ion-edit"></i>
                                        </a>
                                        <a href="/sendProgram/<?php echo e($program->id); ?>" style="color: #364f87">
                                            <i class="ion-android-send"></i>
                                        </a>
                                        <?php if($program->status == 'active'): ?>
                                            <button onclick="return confirm('Are you sure you want to INACTIVE this program?')"
                                                    style="color: #364f87; background-color: transparent; border-width: 0; font-size: 24px">
                                                <i class="fas fa-toggle-off"></i>
                                            </button>
                                        <?php else: ?>
                                            <a href="/programActive/<?php echo e($program->id); ?>"
                                               style="color: #364f87">
                                                <i class="fas fa-toggle-on"></i>
                                            </a>
                                        <?php endif; ?>
                                    </span>
                                <?php endif; ?>
                            <?php endif; ?>
                                <?php echo csrf_field(); ?>
                        </form>
                    </div>

                    <div class="form wow fadeInUp" data-wow-delay="0.3s">
                        <div class="row" style="padding-bottom: 20px">
                            <img class="img-responsive wow fadeIn" src="<?php echo e(asset('storage/'.$program->image)); ?>"
                                 alt="<?php echo e($program->Title); ?>" style="max-height: 500px; display: block"/>
                        </div>
                    </div>

                    <div>
                        <form action="/cart" method="post">
                            <input type="hidden" name="program_id" value="<?php echo e($program->id); ?>">
                            <?php if(auth()->guard()->guest()): ?>
                            <?php else: ?>
                                <input type="hidden" name="user_id" value="<?php echo e(auth()->user()->id); ?>">
                            <?php endif; ?>

                            <?php if($flag == false): ?>
                                <button class="addToCartBtn2" type="submit">
                                    <?php echo e(__('text.addToCartBtn')); ?>

                                </button>
                            <?php endif; ?>
                            <?php echo csrf_field(); ?>
                        </form>
                    </div>

                    <div class="line"> </div>

                    <div class="row">
                        <h2 class="h2"> <?php echo e(__('text.programPrice')); ?> </h2>
                        <h2 class="h2" style="color: #ee2724; text-align: center">
                            <?php echo e($program->price); ?> <?php echo e(__("text.BD")); ?>

                        </h2>
                    </div>

                    <div class="line"></div>

                    <div class="col-md-12">
                        <h2 class="h2"> <?php echo e(__(('text.programOverview'))); ?> </h2>
                        <p class="article-body" style="color: #2D322F">
                            <?php echo nl2br($program->overview); ?>

                        </p>
                    </div>

                    <?php if($program->videoUrl != null): ?>
                        <div class="row">
                            <iframe class="iframe" src="https://player.vimeo.com/video/<?php echo e($program->videoUrl); ?>"
                                        frameborder="0" allowfullscreen>
                            </iframe>
                        </div>
                    <?php endif; ?>

                    <?php if($program->description != null): ?>
                        <div class="line" style="margin-top: 5px"></div>

                        <div class="col-md-12">
                            <h2 class="h2"> <?php echo e(__(('text.programDescription'))); ?> </h2>
                            <p class="article-body" style="color: #2D322F">
                                <?php echo nl2br($program->description); ?>

                            </p>
                        </div>
                    <?php endif; ?>

                    <hr style="width: 100%; border-width:1px">

                    <div>
                        <form action="/cart" method="post">
                            <input type="hidden" name="program_id" value="<?php echo e($program->id); ?>">
                            <?php if(auth()->guard()->guest()): ?>
                            <?php else: ?>
                                <input type="hidden" name="user_id" value="<?php echo e(auth()->user()->id); ?>">
                            <?php endif; ?>

                            <?php if($flag == false): ?>
                                <button class="addToCartBtn2" type="submit">
                                    <?php echo e(__('text.addToCartBtn')); ?>

                                </button>
                            <?php endif; ?>
                            <?php echo csrf_field(); ?>
                        </form>
                    </div>

                    <?php if($flag == true): ?>
                        <div id="read" class="showPdf">
                            <a class="pdfBody" href="/programPdf/<?php echo e($program->pdf); ?>#toolbar=0" target="_blank" style="color: #ffffff;"> <?php echo e(__('text.viewPdf')); ?> </a>
                        </div>

                        <div class="line" style="margin-top: 5px"></div>

                        <div class="row">
                            <h2 class="h2"> <?php echo e(__(('text.programPhases'))); ?> </h2>
                        </div>

                        <?php $counter = 0; ?>
                        <?php $__currentLoopData = $phases; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $phase): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php $counter ++; ?>
                            <button class="accordion">
                                <?php echo e(__(('text.phase'))); ?> <?php echo e($counter); ?>: <?php echo e($phase->title); ?>

                            </button>
                            <div class="panel">
                                <h2 class="h2"> <?php echo e(__(('text.phaseOverviewShow'))); ?>: </h2>
                                <p style="color: #2D322F"><?php echo e($phase->overview); ?></p>
                                <?php if($phase->videoUrl != null): ?>
                                    <iframe class="iframe" src="https://player.vimeo.com/video/<?php echo e($phase->videoUrl); ?>"
                                            frameborder="0" allowfullscreen>
                                    </iframe>
                                <?php endif; ?>
                                <?php if($phase->description != null): ?>
                                    <div class="line" style="margin-top: 5px"></div>
                                    <h2 class="h2"> <?php echo e(__(('text.phaseDescriptionShow'))); ?>: </h2>
                                    <p style="color: #2D322F; padding-bottom: 10px"><?php echo e($phase->description); ?></p>
                                <?php endif; ?>

                                <div id="read" class="showPdf">
                                    <a class="pdfBody" href="/phasePdf/<?php echo e($phase->pdf); ?>#toolbar=0" target="_blank" style="color: #ffffff;"> <?php echo e(__('text.viewPdf')); ?> </a>
                                </div>

                                <div class="line" style="margin-top: 5px"></div>

                                <h2 class="h2"> <?php echo e(__(('text.phaseSections'))); ?>: </h2>

                                <?php $s_counter = 0; ?>
                                <?php $__currentLoopData = $sections; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $section): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($section->program_phase_id == $phase->id): ?>
                                        <?php $s_counter ++; ?>
                                        <div id="test-popup-<?php echo e($s_counter); ?>" class="white-popup mfp-hide">
                                            <iframe class="iframe-video"
                                                    src="https://player.vimeo.com/video/<?php echo e($section->videoUrl); ?>"
                                                    frameborder="0" allowfullscreen></iframe>
                                        </div>
                                        <h3 class="h3"> <?php echo e(__('text.phase')); ?> <?php echo e($s_counter); ?>:
                                            <span class="sectionPdf">
                                                <a href="#test-popup-<?php echo e($s_counter); ?>" class="open-popup-link" style="color: #ee2724"> <?php echo e(__(('text.watchVideo'))); ?> </a>
                                            </span>
                                        </h3>
                                        <p style="color: #2D322F"> <?php echo e($section->description); ?> </p>
                                        <br />
                                    <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        <?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
<?php echo $__env->make('includes.footerScripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>
</html>
<?php /**PATH C:\Users\Maryam Alalawi\inspower\resources\views/programs/show.blade.php ENDPATH**/ ?>