<html lang="<?php echo e(__('text.lang')); ?>" dir="<?php echo e(__('text.dir')); ?>">

<?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<body>
<script>
    var msg = '<?php echo e(Session::get('alert')); ?>';
    var exist = '<?php echo e(Session::has('alert')); ?>';
    if (exist) {
        alert(msg);
    }
</script>

<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app form" id="main">
        <div class="pricing-section text-center" style="min-height: 100%">
            <div class="container">
                <div  style="width: 100%">
                    <div class="pricing-intro banner-padding">
                        <h1 class="wow fadeInUp" data-wow-delay="0s"> <?php echo e(__('text.allCoursesTitle')); ?> </h1>
                        <?php if((App::isLocale('en'))): ?>
                            <h2>For courses in Arabic language <a href="setlocale/ar">press here</a></h2>
                        <?php endif; ?>
                        <?php if((App::isLocale('ar'))): ?>
                            <h2>للدورات التدريبية باللغة الإنجليزية  <a href="setlocale/en">اضغط هنا</a></h2>
                        <?php endif; ?>
                    </div>
                    <?php $__currentLoopData = $courses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $course): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php $flag = false; ?>
                            <?php if(auth()->guard()->guest()): ?>
                            <?php else: ?>
                        <?php if(auth()->user()->type == 'admin'): ?>
                            <?php $flag = true; ?>
                        <?php elseif($orders != null): ?>
                            <?php for($i=0; $i<count($orders); $i++): ?>
                                <?php if($orders[$i]->course_id == $course->id): ?>
                                    <?php $flag = true; ?>
                                    <?php break; ?>
                                <?php endif; ?>
                            <?php endfor; ?>
                        <?php endif; ?>
                            <?php endif; ?>
                            <?php if($course->status == 'inactive'): ?>
                                <?php if(auth()->guard()->guest()): ?>
                                <?php else: ?>
                                    <?php if(auth()->user()->type == 'admin' || $flag == true): ?>
                                        <div class="table-left wow fadeInUp" data-wow-delay="0.4s" style="overflow: auto;">
                                            <div class="pricing-details">
                                                <div style="display: block">
                                                    <div class="col-md-4 articleImg">
                                                        <img class="show-img wow fadeIn"
                                                             src="<?php echo e(asset('storage/'.$course->image)); ?>" style="padding-bottom: 10px"/>
                                                    </div>
                                                    <br>
                                                    <div class="col-md-8">
                                                        <div class="row">
                                                            <span class="video-title">
                                                                <a style="color: #ee2724; font-size:20px" href="/course/<?php echo e($course->id); ?>_<?php echo e(str_replace(' ', '_', $course->title)); ?>">
                                                                    <?php echo e($course->title); ?> <span style="color: #2D322F; font-weight: 700;">*<?php echo e(__('text.inactiveCourse')); ?>*</span>
                                                                </a>
                                                            </span>
                                                            <?php if(auth()->guard()->guest()): ?>
                                                            <?php else: ?>
                                                                <?php if(auth()->user()->type == 'admin'): ?>
                                                                    <form action="/course/<?php echo e($course->id); ?>" method="post">
                                                                        <span class="editDeleteIcons">
                                                                            <a href="/course/<?php echo e($course->id); ?>/edit"
                                                                                style="color: #364f87">
                                                                                <i class="ion-edit"></i>
                                                                            </a>
                                                                            <a href="/sendCourse/<?php echo e($course->id); ?>"
                                                                               style="color: #364f87">
                                                                                <i class="ion-android-send"></i>
                                                                            </a>
                                                                            <?php if($course->status == 'active'): ?>
                                                                                <button onclick="return confirm('Are you sure you want to INACTIVE this course?')"
                                                                                        style="color: #364f87; background-color: transparent; border-width: 0; font-size: 24px">
                                                                                    <i class="fas fa-toggle-off"></i>
                                                                                </button>
                                                                            <?php else: ?>
                                                                                <a href="/courseActive/<?php echo e($course->id); ?>"
                                                                                   style="color: #364f87">
                                                                                    <i class="fas fa-toggle-on"></i>
                                                                                </a>
                                                                            <?php endif; ?>
                                                                        </span>
                                                                        <?php echo csrf_field(); ?>
                                                                    </form>
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        </div>
                                                        <h2 class="articleBody" style="color: #2D322F">
                                                            <?php echo e(__("text.price")); ?>

                                                            <span style="color: #ee2724"><?php echo e($course->price); ?> <?php echo e(__("text.BD")); ?></span>
                                                        </h2>
                                                        <h2 class="articleBody" style="color: #969696">
                                                            <?php echo nl2br($course->overview); ?>

                                                        </h2>
                                                        <form action="/cart" method="post">
                                                            <input type="hidden" name="course_id" value="<?php echo e($course->id); ?>">
                                                            <?php if(auth()->guard()->guest()): ?>
                                                            <?php else: ?>
                                                                <input type="hidden" name="user_id" value="<?php echo e(auth()->user()->id); ?>">
                                                            <?php endif; ?>
                                                            <button class="submit-button"
                                                                    style="<?php echo e(__('text.floating')); ?>; padding-left: 15px; padding-right: 15px" formaction="/course/<?php echo e($course->id); ?>" formmethod="get">
                                                                <?php echo e(__('text.learnMoreBtn')); ?>

                                                            </button>

                                                            <?php if($flag == false): ?>
                                                                <button class="submit-button" type="submit"
                                                                        style="<?php echo e(__('text.opFloating')); ?>; padding-left: 15px; padding-right: 15px">
                                                                    <?php echo e(__('text.addToCartBtn')); ?>

                                                                </button>
                                                            <?php endif; ?>
                                                            <?php echo csrf_field(); ?>
                                                        </form>
                                                    </div>

                                                    <?php echo csrf_field(); ?>
                                                    <br>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                <?php endif; ?>
                            <?php else: ?>
                                <div class="table-left wow fadeInUp" data-wow-delay="0.4s" style="overflow: auto;">
                                    <div class="pricing-details">
                                        <div style="display: block">
                                                <div class="col-md-4 articleImg">
                                                    <img class="show-img wow fadeIn"
                                                         src="<?php echo e(asset('storage/'.$course->image)); ?>" style="padding-bottom: 10px"/>
                                                </div>
                                                <br>
                                                <div class="col-md-8">
                                                    <div class="row">
                                                        <span class="video-title">
                                                            <a style="color: #ee2724; font-size:20px" href="/course/<?php echo e($course->id); ?>_<?php echo e(str_replace(' ', '_', $course->title)); ?>">
                                                                <?php echo e($course->title); ?>

                                                            </a>
                                                        </span>
                                                        <?php if(auth()->guard()->guest()): ?>
                                                            <?php else: ?>
                                                            <?php if(auth()->user()->type == 'admin'): ?>
                                                                <form action="/course/<?php echo e($course->id); ?>" method="post">
                                                                <span class="editDeleteIcons">
                                                                    <a href="/course/<?php echo e($course->id); ?>/edit"
                                                                       style="color: #364f87">
                                                                        <i class="ion-edit"></i>
                                                                    </a>
                                                                    <a href="/sendCourse/<?php echo e($course->id); ?>"
                                                                       style="color: #364f87">
                                                                        <i class="ion-android-send"></i>
                                                                    </a>
                                                                    <button onclick="return confirm('Are you sure you want to INACTIVE this course?')"
                                                                            style="color: #364f87; background-color: transparent; border-width: 0; font-size: 24px">
                                                                        <i class="fas fa-toggle-off"></i>
                                                                    </button>
                                                                </span>
                                                                    <?php echo csrf_field(); ?>
                                                                </form>
                                                            <?php endif; ?>
                                                            <?php endif; ?>
                                                    </div>
                                                    <h2 class="articleBody" style="color: #2D322F">
                                                        <?php echo e(__("text.price")); ?>

                                                        <span style="color: #ee2724"><?php echo e($course->price); ?> <?php echo e(__("text.BD")); ?></span>
                                                    </h2>
                                                    <h2 class="articleBody" style="color: #969696">
                                                        <?php echo nl2br($course->overview); ?>

                                                    </h2>
                                                    <form action="/cart" method="post">
                                                        <input type="hidden" name="course_id" value="<?php echo e($course->id); ?>">
                                                        <?php if(auth()->guard()->guest()): ?>
                                                        <?php else: ?>
                                                            <input type="hidden" name="user_id" value="<?php echo e(auth()->user()->id); ?>">
                                                        <?php endif; ?>
                                                        <button class="submit-button"
                                                            style="<?php echo e(__('text.floating')); ?>; padding-left: 15px; padding-right: 15px" formaction="/course/<?php echo e($course->id); ?>" formmethod="get">
                                                            <?php echo e(__('text.learnMoreBtn')); ?>

                                                        </button>

                                                        <?php if($flag == false): ?>
                                                            <button class="cart-button"
                                                                    style="<?php echo e(__('text.opFloating')); ?>; padding-left: 15px; padding-right: 15px">
                                                                <?php echo e(__('text.addToCartBtn')); ?>

                                                            </button>
                                                        <?php endif; ?>
                                                        <?php echo csrf_field(); ?>
                                                    </form>
                                                </div>

                                                <?php echo csrf_field(); ?>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
        </div>
        <!-- Footer Section -->
        <?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
<?php echo $__env->make('includes.footerScripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

</body>
</html>

<?php /**PATH /Users/maryamal-alawi/inspower/resources/views/courses/index.blade.php ENDPATH**/ ?>