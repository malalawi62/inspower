<!DOCTYPE html>
<html lang="<?php echo e(__('text.lang')); ?>" dir="<?php echo e(__('text.dir')); ?>">

<?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<body>
<script>
    var msg = '<?php echo e(Session::get('alert')); ?>';
    var exist = '<?php echo e(Session::has('alert')); ?>';
    if (exist) {
        alert(msg);
    }
</script>
<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app" id="main">
        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe">
            <div class="container">
                <div class="cta-inner">
                    <div class="row">
                        <h1 style="text-align: center; float: none"> <?php echo e(__('text.editModuleTitle')); ?> </h1>
                    </div>

                    <div class="wow fadeInUp" data-wow-delay="0.3s">
                        <form class="forms-form center-form wow zoomIn" action="/module/<?php echo e($modules->id); ?>" method="post"
                              enctype="multipart/form-data">
                            <?php echo method_field('PATCH'); ?>
                            <div class="modules">
                                <div class="module module_wrapper_1">

                                    <?php $__errorArgs = ['m_title'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> <h5 style="color: red;"><?php echo e($message); ?></h5> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                    <input class="mail" type="text" name="m_title"
                                           value="<?php echo e($modules->m_title); ?>"
                                           autocomplete="m_title">

                                    <p><?php echo e(__('text.moduleDescription')); ?></p>
                                    <textarea class="mail" rows="10" name="m_description" style="height: auto;"> <?php echo e($modules->m_description); ?> </textarea>
                                </div>
                            </div>
                            <?php echo csrf_field(); ?>
                            <div>
                                <button class="submit-button" style="width: 60%;">
                                    <?php echo e(__('text.editModuleBtn')); ?>

                                </button>
                            </div>
                        </form>
                                    <div class="line"></div>

                                    <?php $counter = 1; ?>
                                    <?php $__currentLoopData = $lessons; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lesson): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php $counter ++; ?>
                                        <div class="">
                                            <div class="row" style="padding-bottom: 30px">
                                                <div class="row">
                                                    <form action="/lesson/<?php echo e($lesson->id); ?>/delete" method="post">
                                                    <h2 class="h2" style="<?php echo e(__('text.textAlign')); ?> ; text-transform: uppercase"> <?php echo e(__('text.lesson')); ?> <?php echo e($counter); ?>

                                                        <a href="/lesson/<?php echo e($lesson->id); ?>/edit" style="color: #364f87">
                                                            <i class="ion-edit"></i>
                                                        </a>
                                                        <button onclick="return confirm('Are you sure you want to DELETE this lesson?')"
                                                                style="color: #364f87; background-color: transparent; border-width: 0; font-size: 24px">
                                                            <i class="ion-android-delete"></i>
                                                        </button>
                                                    </h2>
                                                        <?php echo csrf_field(); ?>
                                                    </form>
                                                </div>

                                                <div class="row">
                                                    <p class="article-body"> <a href="https://vimeo.com/<?php echo e($lesson->m_videoUrl); ?>"> https://vimeo.com/<?php echo e($lesson->m_videoUrl); ?> </a> </p>
                                                </div>
                                                <?php if($lesson->v_description != null): ?>
                                                    <div class="row"  style="margin-top: 20px">
                                                        <p class="article-body">
                                                            <?php echo nl2br($lesson->v_description); ?>

                                                        </p>
                                                    </div>
                                                <?php endif; ?>

                                                <?php if($lesson->pdf != null): ?>
                                                    <div class="row">
                                                        <div id="read" class="showPdf" style="margin-top: 20px">
                                                            <a class="pdfBody" href="/lessonPdf/<?php echo e($lesson->pdf); ?>#toolbar=0" target="_blank" style="color: #ffffff;"> <?php echo e(__('text.viewPdf')); ?> </a>
                                                        </div>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                            <div class="line"></div>

                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                            <form action="/lesson/create" method="get">
                                <input type="hidden" name="module_id" value="<?php echo e($modules->id); ?>">
                                <div>
                                    <button class="submit-button" style="width: 60%;">
                                        <?php echo e(__('text.addLessonBtn')); ?>

                                    </button>
                                </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        <?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
<?php echo $__env->make('includes.footerScripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>
</html>
<?php /**PATH C:\Users\Maryam Alalawi\inspower\resources\views/courses/editModule.blade.php ENDPATH**/ ?>