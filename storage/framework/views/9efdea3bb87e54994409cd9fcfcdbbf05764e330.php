<html lang="<?php echo e(__('text.lang')); ?>" dir="<?php echo e(__('text.dir')); ?>">

<?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<body>

<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app form" id="main">
        <div class="pricing-section text-center" style="min-height: 100%">
            <div class="container">
                <div style="width: 100%">
                    <div class="banner-padding">
                        <div class="table-left wow fadeInUp" data-wow-delay="0.4s">
                            <div class="pricing-details">
                                <div style="display: inline-block">
                                    <span class="span" style="color: #ee2724;">TERMS AND CONDITIONS</span>
                                    <div class="col-sm-12">
                                        <h2 dir="ltr" style="text-align:left"><?php echo nl2br('Welcome to https://www.inspower-fitnesscoaching.com (the “Website”). This Website has been created to provide you with videos, articles and online products about physical training for your personal entertainment and education. The terms “we”, “us”, and “our” refer to INSPOWER Fitness, an organization incorporated in Bahrain. The term ‘you’ refers to the customer visiting the Website.

                                            By visiting or using this Website, you consent and agree to be bound by these terms and conditions and the Privacy Policy. All applicable laws and regulations, and agree that you take responsibility for compliance with any applicable locals laws. Your account information is confidential then we disclaim if you share your account information.

                                            Please read these terms and conditions carefully.

                                            IF YOU DO NOT ACCEPT THESE CONDITIONS OF USE AND PURCHASE AND THE PRIVACY POLICY, PLEASE REFRAIN FROM USING THIS WEBSITE.
                                            '); ?>

                                        </h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="banner-padding">
                        <div class="table-left wow fadeInUp" data-wow-delay="0.4s">
                            <div class="pricing-details">
                                <div style="display: inline-block">
                                    <span class="span" style="color: #ee2724;">GENERAL</span>
                                    <div class="col-sm-12">
                                        <h2 dir="ltr" style="text-align:left"><?php echo nl2br('The Conditions of use and purchase exclusively govern access, use of this Website and possible purchases.

                                            INSPOWER Fitness Coaching reserves the right to change these Conditions of use and purchase at any time at its sole discretion. Any change becomes effective from the date of posting on this Website. Your use of this Website will be subject to the latest version of the Conditions of use and purchase at the time of your usage. Your continued use of this Website after such changes constitutes your acceptance of these changes. Please review the Conditions of use and purchase regularly. If you do not agree to these Conditions of use and purchase or any changes made to them, please cease your use of this Website immediately.

                                            The terms of these Conditions of use and purchase bind the user or customer personally and may not be assigned, transferred or exchanged.
                                            '); ?>

                                        </h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="banner-padding">
                        <div class="table-left wow fadeInUp" data-wow-delay="0.4s">
                            <div class="pricing-details">
                                <div style="display: inline-block">
                                    <span class="span" style="color: #ee2724;">MEDICAL DISCLAIMER</span>
                                    <div class="col-sm-12">
                                        <h2 dir="ltr" style="text-align:left"><?php echo nl2br('The Website has been created to provide you with videos and information about physical training for your personal use and education. You should not rely on this information as a substitute for, nor does it replace, professional health care advice, diagnosis, or treatment. The information posted on this site or available through its social media pages must not be taken to be the practice of medical or therapeutic care.

                                            If you have any concerns or questions about your health, you should always consult with a physician or other health care professional. If you experience any pain or difficulty with any exercises provided, discontinue immediately and consult your physician.

                                            By subscribing to INSPOWER Fitness Coaching or otherwise consulting or purchasing its content, you agree and understand that all the use of any information provided on this site is solely at your own risk.
                                            '); ?>

                                        </h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="banner-padding">
                        <div class="table-left wow fadeInUp" data-wow-delay="0.4s">
                            <div class="pricing-details">
                                <div style="display: inline-block">
                                    <span class="span" style="color: #ee2724;">RELEASE OF LIABILITY</span>
                                    <div class="col-sm-12">
                                        <h2 dir="ltr" style="text-align:left"><?php echo nl2br('Neither INSPOWER Fitness Coaching, its partners, directors, employees, affiliated companies and suppliers, and sponsors, as well as their respective directors, officers, employees, consultants, agents, representatives, and respective suppliers be liable for any direct, indirect, special, consequential, or punitive damages (including, but not limited to, damage resulting from loss of profit, loss of data, or interruption of activities) resulting from the use, the inability to use, or the results from the use of this Website (including the content and information contained on this Website or any related Website), as well as for any security problem or related loss arising from the use of this Website, whether based on warranty, contract, civil responsibility, or any other legal theory, and whether or not INSPOWER Fitness Coaching is advised or not of the possibility of such damages.

                                            INSPOWER Fitness Coaching makes no representation or warranty that the content, information, or material, including downloadable software, which may be accessed from the Website will be exempt or free of interruptions, errors, defects, viruses, or other harmful components, or that any such problems which are discovered will be corrected.
                                            '); ?>

                                        </h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="banner-padding">
                        <div class="table-left wow fadeInUp" data-wow-delay="0.4s">
                            <div class="pricing-details">
                                <div style="display: inline-block">
                                    <span class="span" style="color: #ee2724;">RELEASE OF RESPONSIBILITY</span>
                                    <div class="col-sm-12">
                                        <h2 dir="ltr" style="text-align:left"><?php echo nl2br('You acknowledge that all sports or fitness activities which you may perform, your participation in any activity offered by INSPOWER Fitness Coaching, and your use of the Online Coaching services or Website carry inherent risks, and that they are entirely at your own risk. Consequently, you hereby release and grant a full, final, complete and definitive discharge to INSPOWER Fitness Coaching and its representatives, employees, shareholders, directors, officers, management, agents, representatives, insurers, heirs, executors, successors, and assigns, or any other person for whom INSPOWER Fitness Coaching is responsible, from all liability, demand, cause of action, claim, recourse, action, conflict, dispute and/or pursuit of any kind or nature whatsoever, in law or in equity, present or future, resulting directly or indirectly from the use of any of the Services or the Website, or from your participation in any of the sports or fitness activities provided by INSPOWER Fitness Coaching through the Website, renouncing to such a demand, cause of action, claim, recourse, action, conflict, dispute and/or pursuit, save in the event of the personal acts of INSPOWER Fitness Coaching or its representatives and for which the onus falls upon you.'); ?>

                                        </h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
        <!-- Footer Section -->
        <?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
<?php echo $__env->make('includes.footerScripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>
</html>
<?php /**PATH C:\Users\Maryam Alalawi\inspower\resources\views/termAndConditions.blade.php ENDPATH**/ ?>