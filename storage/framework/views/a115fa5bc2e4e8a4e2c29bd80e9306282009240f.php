<div class="search-bar text-center">
    <form action="/search" method="get" role="search" class="">
        <?php echo e(csrf_field()); ?>

        <div>
            <input type="text" class="mail" name="q"
                   placeholder="<?php echo e(__('text.search')); ?>">

            <?php if(auth()->guard()->guest()): ?>
                <button title="<?php echo e(__("text.search")); ?>" type="submit" class="mail" style="width: 12%;  border-radius: 10px; border: 1px solid rgb(178, 198, 188); font-size: 20px">
                    <i class="ion-search"></i>
                </button>

                <button title="<?php echo e(__("text.login")); ?>" class="mail" style="width: 12%;  border-radius: 10px; border: 1px solid rgb(178, 198, 188); font-size: 20px" formaction="<?php echo e(route('login')); ?>">
                    <i class="ion-person"></i>
                </button>
            <?php else: ?>
                <button title="<?php echo e(__("text.search")); ?>" type="submit" class="mail" style="width: 10%;  border-radius: 10px; border: 1px solid rgb(178, 198, 188); font-size: 20px">
                    <i class="ion-search"></i>
                </button>

                <button title="<?php echo e(__("text.account")); ?>" class="mail" style="width: 10%;  border-radius: 10px; border: 1px solid rgb(178, 198, 188); font-size: 20px" formaction="<?php echo e(url('/profile/'.auth()->user()->id)); ?>">
                    <i class="ion-person" ></i>
                </button>

                <button title="<?php echo e(__("text.cartTitle")); ?>" class="mail" style="width: 10%;  border-radius: 10px; border: 1px solid rgb(178, 198, 188); font-size: 20px" formaction="<?php echo e(url('/viewCart')); ?>">
                    <i class="ion-android-cart"></i>
                </button>

                <button title="<?php echo e(__("text.logout")); ?>" class="mail" style="width: 10%;  border-radius: 10px; border: 1px solid rgb(178, 198, 188); font-size: 20px" formaction="<?php echo e(route('logout')); ?>" formmethod="post">
                    <i class="ion-log-out" ></i>
                </button>
            <?php endif; ?>


        </div>
    </form>
</div>
<?php /**PATH /Users/maryam/inspower/resources/views/layout/searchBar.blade.php ENDPATH**/ ?>