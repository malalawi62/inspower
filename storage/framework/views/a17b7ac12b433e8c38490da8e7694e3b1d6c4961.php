<html lang="<?php echo e(__('text.lang')); ?>" dir="<?php echo e(__('text.dir')); ?>">

<?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<body>

<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<<<<<<< HEAD
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app form" id="main">
        <div class="pricing-section text-center" style="min-height: 100%">
            <div class="container">
                <div style="width: 100%">
                    <div class="pricing-intro">
                        <h1 class="wow fadeInUp" data-wow-delay="0s"> <?php echo e(__('text.allVideosTitle')); ?> </h1>
                    </div>
                    <?php $__currentLoopData = $data['videos']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $videos): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="col-md-4">
                            <div class="table-left wow fadeInUp" data-wow-delay="0.4s"
                                 style="overflow: auto; height: 400px">
                                <div class="pricing-details">
                                    <div style="display: block">
                                        <form action="/videos/<?php echo e($videos->id); ?>" method="post">
                                            <?php echo method_field('DELETE'); ?>
                                            <div class="row titleHeight">
                                                <span class="article-title">
                                                    <a style="color: #fdca00;">
                                                        <?php if($data['lang'] == 'en'): ?>
                                                            <?php echo e($videos->title); ?>

                                                        <?php else: ?>
                                                            <?php echo e($videos->titleAr); ?>

                                                        <?php endif; ?>
                                                    </a>
                                                </span>
                                            </div>
                                            <div class="row" style="padding: 0 8px 0 8px">
                                                <?php if(auth()->guard()->guest()): ?>
                                                <?php else: ?>
                                                    <span class="editDeleteIcons">
                                                        <a href="/videos/<?php echo e($videos->id); ?>/edit"
                                                           style="color: #fdca00">
                                                            <i class="ion-edit"></i>
                                                        </a>
                                                        <button
                                                            style="color: #fdca00; background-color: transparent; border-width: 0; font-size: 24px">
                                                            <i class="ion-android-delete"></i>
                                                        </button>
                                                    </span>
                                                <?php endif; ?>

                                                <h5 class="articleCategory">
                                                    <?php if($data['lang'] == 'en'): ?>
                                                        Category: <?php echo e($videos->category->englishName); ?>

                                                    <?php else: ?>
                                                        الفئة: <?php echo e($videos->category->arabicName); ?>

                                                    <?php endif; ?>
                                                </h5>
                                            </div>
                                            <div class="col-sm-12">
                                                <iframe height="200px" width="100%" style="padding-bottom: 10px"
                                                        src="https://www.youtube.com/embed/<?php echo e($videos->videoId); ?>"
                                                        frameborder="0" allowfullscreen></iframe>
                                            </div>
                                            <div class="col-sm-12">
                                                <h2 class="articleBody">
                                                    <?php if($data['lang'] == 'en'): ?>
                                                        <?php echo e($videos->description); ?>

                                                    <?php else: ?>
                                                        <?php echo e($videos->descriptionAr); ?>

                                                    <?php endif; ?>
                                                </h2>
                                            </div>
                                            <?php echo csrf_field(); ?>
                                        </form>
                                        <br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <hr style="border-color: #2D322F">
                    <?php $__currentLoopData = $data['instagramVideos']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $videos): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="col-md-4">
                            <div class="table-left wow fadeInUp" data-wow-delay="0.4s"
                                 style="overflow: auto; height: 500px">
                                <div class="pricing-details">
                                    <div style="display: block">
                                        <form action="/instagram/<?php echo e($videos->id); ?>" method="post">
                                            <?php echo method_field('DELETE'); ?>
                                            <div class="row titleHeight">
                                                <span class="article-title">
                                                    <a style="color: #fdca00;">
                                                        <?php if($data['lang'] == 'en'): ?>
                                                            <?php echo e($videos->title); ?>

                                                        <?php else: ?>
                                                            <?php echo e($videos->titleAr); ?>

                                                        <?php endif; ?>
                                                    </a>
                                                </span>
                                            </div>
                                            <div class="row" style="padding: 0 8px 0 8px">
                                                <?php if(auth()->guard()->guest()): ?>
                                                <?php else: ?>
                                                    <span class="editDeleteIcons">
                                                        <a href="/instagram/<?php echo e($videos->id); ?>/edit"
                                                           style="color: #fdca00">
                                                            <i class="ion-edit"></i>
                                                        </a>
=======
<div class="wrapper" style="background-color: #2D322F">
    <!-- Main Section-->
    <div class="main app form" id="main">
        <div class="pricing-section text-center">
            <div class="container">
                <div class="col-md-8 col-md-offset-2 col-sm-12 nopadding text-center" style="float: left">
                    <div class="pricing-intro">
                        <h1 class="wow fadeInUp" data-wow-delay="0s"> <?php echo e(__('text.allVideosTitle')); ?> </h1>
                        <div style="padding-top: 20px">
                        <span class="filterTag"> <?php echo e(__('text.filter')); ?> </span>
                            <select id="myBtnContainer" class="mail" onchange="filterSelection(this.value)">
                                <option value="all" style="padding:10px"> <?php echo e(__('text.showFilter')); ?> </option>
                                <?php $__currentLoopData = $data['categories']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($data['lang'] == 'en'): ?>
                                        <option class="btn" value="<?php echo e($category->englishName); ?>"> <?php echo e($category->englishName); ?> </option>
                                    <?php else: ?>
                                        <option class="btn" value="<?php echo e($category->englishName); ?>"> <?php echo e($category->arabicName); ?> </option>
                                    <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>
                    </div>
                    <?php $__empty_1 = true; $__currentLoopData = $data['videos']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $video): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                        <div class="filterDiv <?php echo e($video->category->englishName); ?>">
                            <div class="table-left wow fadeInUp" data-wow-delay="0.4s" style="overflow: auto;">
                                <div class="pricing-details">
                                    <div style="display: block">
                                        <form action="/videos/<?php echo e($video->id); ?>" method="post">
                                            <?php echo method_field('DELETE'); ?>

                                            <div class="col-md-6">
                                                <?php if($video->type == 'vimeo'): ?>
                                                    <iframe width="100%" height="250px" style="padding-bottom: 10px"
                                                            src="https://player.vimeo.com/video/<?php echo e($video->videoId); ?>"
                                                            frameborder="0" allowfullscreen></iframe>
                                                <?php else: ?>
                                                    <iframe width="100%" height="250px" style="padding-bottom: 10px; display: block;"
                                                            src="https://www.youtube.com/embed/<?php echo e($video->videoId); ?>"
                                                            frameborder="0" allowfullscreen ></iframe>
                                                <?php endif; ?>
                                            </div>
                                            <br>
                                            <div class="col-md-6">
                                                <div class="row">
                                                <span class="video-title">
                                                    <a style="color: #fdca00; font-size:20px">
                                                        <?php echo e($video->title); ?>

                                                    </a>
                                                </span>
                                                <?php if(auth()->guard()->guest()): ?>
                                                <?php else: ?>
                                                    <span class="editDeleteIcons">
                                                        <a href="/videos/<?php echo e($video->id); ?>/edit"
                                                           style="color: #fdca00">
                                                            <i class="ion-edit"></i>
                                                        </a>
                                                        <a href="/sendVideo/<?php echo e($video->id); ?>"
                                                           style="color: #fdca00">
                                                            <i class="ion-android-send"></i>
                                                        </a>
>>>>>>> 68063f6772344442635537c26f6420f83f85c557
                                                        <button
                                                            style="color: #fdca00; background-color: transparent; border-width: 0; font-size: 24px">
                                                            <i class="ion-android-delete"></i>
                                                        </button>
                                                    </span>
                                                <?php endif; ?>
<<<<<<< HEAD

                                                <h5 class="articleCategory">
                                                    <?php if($data['lang'] == 'en'): ?>
                                                        Category: <?php echo e($videos->category->englishName); ?>

                                                    <?php else: ?>
                                                        الفئة: <?php echo e($videos->category->arabicName); ?>

                                                    <?php endif; ?>
                                                </h5>
                                            </div>
                                            <div class="col-sm-12">
                                                <iframe height='400px' width="110%" style="padding-bottom: 10px"
                                                        src="<?php echo e($videos->url); ?>embed"
                                                        frameborder="0" allowfullscreen></iframe>
                                            </div>
                                            <div class="col-sm-12">
                                                <h2 class="articleBody">
                                                    <?php if($data['lang'] == 'en'): ?>
                                                        <?php echo e($videos->description); ?>

                                                    <?php else: ?>
                                                        <?php echo e($videos->descriptionAr); ?>

                                                    <?php endif; ?>
=======
                                            </div>
                                                <h5 class="articleCategory">
                                                    <?php if($data['lang'] == 'en'): ?>
                                                        Category: <?php echo e($video->category->englishName); ?>

                                                    <?php else: ?>
                                                        الفئة: <?php echo e($video->category->arabicName); ?>

                                                    <?php endif; ?>
                                                </h5>
                                                <h2 class="articleBody">
                                                    <?php echo nl2br($video->description); ?>

>>>>>>> 68063f6772344442635537c26f6420f83f85c557
                                                </h2>
                                            </div>
                                            <?php echo csrf_field(); ?>
                                        </form>
                                        <br>
                                    </div>
                                </div>
                            </div>
                        </div>
<<<<<<< HEAD
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </div>
            </div>
        </div>

=======
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                        <h2>No Videos Found</h2>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    <div class="container" style="background-color:#2D322F; width: 100%">
        <div class="col-md-12 text-center">
            <?php echo e($data['videos']->links()); ?>

        </div>
    </div>
>>>>>>> 68063f6772344442635537c26f6420f83f85c557
        <!-- Footer Section -->
        <?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
<?php echo $__env->make('includes.footerScripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<<<<<<< HEAD
=======
<script>
    filterSelection("all")
    function filterSelection(c) {
        var x, i;
        x = document.getElementsByClassName("filterDiv");
        if (c === "all") c = "";
        for (i = 0; i < x.length; i++) {
            w3RemoveClass(x[i], "show");
            if (x[i].className.indexOf(c) > -1) w3AddClass(x[i], "show");
        }
    }

    function w3AddClass(element, name) {
        var i, arr1, arr2;
        arr1 = element.className.split(" ");
        arr2 = name.split(" ");
        for (i = 0; i < arr2.length; i++) {
            if (arr1.indexOf(arr2[i]) == -1) {element.className += " " + arr2[i];}
        }
    }

    function w3RemoveClass(element, name) {
        var i, arr1, arr2;
        arr1 = element.className.split(" ");
        arr2 = name.split(" ");
        for (i = 0; i < arr2.length; i++) {
            while (arr1.indexOf(arr2[i]) > -1) {
                arr1.splice(arr1.indexOf(arr2[i]), 1);
            }
        }
        element.className = arr1.join(" ");
    }

</script>
>>>>>>> 68063f6772344442635537c26f6420f83f85c557
</body>
</html>
<?php /**PATH C:\Users\Maryam\inspower\resources\views/videos/index.blade.php ENDPATH**/ ?>