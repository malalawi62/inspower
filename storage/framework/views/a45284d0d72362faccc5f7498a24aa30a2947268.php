<!DOCTYPE html>
<html lang="en" dir="ltr">

<body style="background-color: #cccccc">

<div class="wrapper" style="background-color: #cccccc; padding: 20px;">
    <!-- Main Section-->
    <div class="main app form" id="main" style="background-color: #cccccc">
        <img style="display: block; margin-left: auto; margin-right: auto; width: 50%;"
             src="<?php echo e(asset('images/logoColored.png')); ?>" alt="inspower">

    <!-- About Section -->
        <div class="split-features" id="about">
            <div class="col-md-12 nopadding">
                <div>
                    <h1 class="wow fadeInUp" style="text-align: center"> Check out our latest Video added to INSPOWER website </h1>
                    <p class="wow fadeInUp" style="text-align: center">
                        <a href="https://www.inspower-fitnesscoaching.com/videos"> <?php echo e($VideoInfo->title); ?> </a>
                    </p>
                    <p style="text-align: center">
                        <?php echo nl2br($VideoInfo->description); ?>

                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

</body>
</html>
<?php /**PATH C:\Users\Maryam\inspower\resources\views/emails/NewVideo.blade.php ENDPATH**/ ?>