<html lang="<?php echo e(__('text.lang')); ?>" dir="<?php echo e(__('text.dir')); ?>">

<?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<body>

<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="wrapper" style="background-color: #2D322F">
    <!-- Main Section-->
    <div class="main app form" id="main">
        <div class="pricing-section text-center" style="min-height: 100%">
            <div class="container">
                <div class="col-md-12 col-sm-12 banner-padding text-center" style="float: none">

                    <div class="pricing-intro">
                        <h1 class="wow fadeInUp" data-wow-delay="0s"> <?php echo e(__('text.personalInfo')); ?> </h1>
                    </div>
                    <div class="table-left wow fadeInUp" data-wow-delay="0.4s" style="overflow: auto; border-radius: 10px; border: none">
                            <div class="pricing-details">
                                <div style="display: block">
                                    <div class="col-md-4">
                                        <?php if($user->gender == 'f'): ?>
                                            <?php if($membership == 'year' || $membership == 'month'): ?>
                                                <div class="profile-header-img">
                                                    <img class="img-responsive img-circle wow fadeIn" src="<?php echo e(asset('storage/female_avatar.png')); ?>"
                                                         alt=""/>
                                                    <!-- badge -->
                                                    <div class="rank-label-container">
                                                        <span class="label label-default rank-label"><?php echo e(__("text.premiumUser")); ?></span>
                                                    </div>
                                                </div>
                                            <?php else: ?>
                                                <div class="profile-header-img">
                                                    <img class="img-responsive img-circle wow fadeIn" style=" <?php if($membership != 'year' || $membership != 'month'): ?> border: 2px solid #ee2724;<?php endif; ?>" src="<?php echo e(asset('storage/female_avatar.png')); ?>"
                                                         alt=""/>
                                                    <div class="rank-label-container">
                                                        <span class="label label-default rank-label" style="background-color: #ee2724"><?php echo e(__("text.freeUser")); ?></span>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                        <?php else: ?>
                                            <?php if($membership == 'year' || $membership == 'month'): ?>
                                                <div class="profile-header-img">
                                                    <img class="img-responsive img-circle wow fadeIn" src="<?php echo e(asset('storage/male_avatar.png')); ?>"
                                                         alt="">
                                                    <div class="rank-label-container">
                                                        <span class="label label-default rank-label"><?php echo e(__("text.premiumUser")); ?></span>
                                                    </div>
                                                </div>
                                            <?php else: ?>
                                                <div class="profile-header-img">
                                                    <img class="img-responsive img-circle wow fadeIn" style="border: 2px solid #ee2724;"src="<?php echo e(asset('storage/male_avatar.png')); ?>"
                                                         alt=""/>
                                                    <!-- badge -->
                                                    <div class="rank-label-container">
                                                        <span class="label label-default rank-label"  style="background-color: #ee2724"><?php echo e(__("text.freeUser")); ?></span>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                            <!--<img class="img-responsive wow fadeIn" src="<?php echo e(asset('storage/male_avatar.png')); ?>"
                                                 alt="" style="max-height: 200px; display: block"/>-->
                                        <?php endif; ?>
                                    </div>
                                    <div class="col-md-8" style="position:relative; top: -25%;transform: translateY(25%);">
                                        <div class="row">
                                            <h1 class="text-profile">
                                                <?php echo e($user->name); ?>

                                            </h1>
                                        </div>

                                        <div class="row">
                                            <h1 class="text-profile">
                                                <?php echo e($user->email); ?>

                                            </h1>
                                        </div>

                                        <div>
                                            <button class="addToCartBtn2" style="margin-bottom: 15px" onclick="location.href = '/profile/<?php echo e($user->id); ?>/edit';">
                                                <?php echo e(__('text.editProfile')); ?>

                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <div class="line"></div>

                    <?php if(count($courses) > 0): ?>
                    <div class="pricing-intro">
                        <h1 class="wow fadeInUp" data-wow-delay="0s"> <?php echo e(__('text.myCourses')); ?> </h1>
                    </div>
                    <div class="table-left wow fadeInUp" data-wow-delay="0.4s" style="overflow: auto; border-radius: 10px; border: none;">
                        <div class="pricing-details">
                            <div style="display: block">
                                <div class="col-md-12" style="margin-bottom: 20px">
                                        <?php $__currentLoopData = $courses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $course): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <div class="row">
                                                <h1 class="courseTitle" onclick="location.href = '/course/<?php echo e($course->course->id); ?>';"> <?php echo e($course->course->title); ?> </h1>
                                            </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                </div>
                            </div>
                        </div>
                    </div>
                        <div class="line"></div>
                    <?php endif; ?>

                    <?php if(count($programs) > 0): ?>
                    <div class="pricing-intro">
                        <h1 class="wow fadeInUp" data-wow-delay="0s"> <?php echo e(__('text.myPrograms')); ?> </h1>
                    </div>
                    <div class="table-left wow fadeInUp" data-wow-delay="0.4s" style="overflow: auto; border-radius: 10px; border: none">
                        <div class="pricing-details">
                            <div style="display: block">
                                <div class="col-md-12" style="margin-bottom: 20px">
                                        <?php $__currentLoopData = $programs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $program): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <div class="row">
                                                <h1 class="courseTitle" onclick="location.href = '/program/<?php echo e($program->program->id); ?>';"> <?php echo e($program->program->title); ?> </h1>
                                            </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                </div>
                            </div>
                        </div>
                    </div>

                        <div class="line"></div>
                    <?php endif; ?>

                    <?php if(count($orders) > 0): ?>
                    <div class="pricing-intro">
                        <h1 class="wow fadeInUp" data-wow-delay="0s"> <?php echo e(__('text.myOrders')); ?> </h1>
                    </div>
                    <div class="table-left wow fadeInUp" data-wow-delay="0.4s" style="overflow: auto; border-radius: 10px; border: none">
                        <div class="pricing-details">
                            <div style="display: block">
                                <div class="col-md-12" style="margin-bottom: 20px">
                                        <?php $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <div class="row">
                                                <button class="accordion">
                                                    <?php echo e(__('text.orderId')); ?> <?php echo e($order->id); ?>: <?php echo e($order->created_at->toDateString()); ?>

                                                </button>
                                                <div class="panel">
                                                    <h2 class="h2"> <?php echo e(__('text.orderPrice')); ?>: <?php echo e($order->amount); ?> <?php echo e(__("text.BD")); ?></h2>
                                                    <h1 class="courseTitle" onclick="location.href = '/program/';"> <?php echo e(__('text.view')); ?> </h1>
                                                </div>
                                            </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
</div>
<!-- Jquery and Js Plugins -->
<?php echo $__env->make('includes.footerScripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>
</html>

<?php /**PATH /Users/maryamal-alawi/inspower/resources/views/users/profile.blade.php ENDPATH**/ ?>