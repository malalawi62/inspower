<!DOCTYPE html>
<html lang="<?php echo e(__('text.lang')); ?>" dir="<?php echo e(__('text.dir')); ?>">

<?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<body>
<script>
    var msg = '<?php echo e(Session::get('alert')); ?>';
    var exist = '<?php echo e(Session::has('alert')); ?>';
    if (exist) {
        alert(msg);
    }
</script>
<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app" id="main">
        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe">
            <div class="container">
                <div class="cta-inner">
                    <div class="row">
                        <h1 style="text-align: center; float: none"> <?php echo e(__('text.addNewSection')); ?> </h1>
                    </div>

                    <div class="wow fadeInUp" data-wow-delay="0.3s">
                        <form class="forms-form center-form wow zoomIn" action="/section" method="post"
                              enctype="multipart/form-data">
                            <input type="hidden" name="phase_id" id="phase_id" value="<?php echo e($phase_id); ?>">
                            <div class="videos">
                                <div class="video_wrapper">
                                    <input class="mail" type="text" name="videoUrl"
                                           placeholder="<?php echo e(__('text.sectionVideoURL')); ?>"
                                           autocomplete="off" value="<?php echo e(old('videoUrl')); ?>">

                                    <p><?php echo e(__('text.phaseBriefDescription')); ?></p>
                                    <textarea class="mail" rows="10" name="description" style="height: auto;" value="<?php echo e(old('description')); ?>"></textarea>
                                </div>
                            </div>
                            <?php echo csrf_field(); ?>
                            <div>
                                <div>
                                    <button class="submit-button" style="width: 60%;">
                                        <?php echo e(__('text.addNewSection')); ?>

                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        <?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
<?php echo $__env->make('includes.footerScripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>
</html>
<?php /**PATH C:\Users\Maryam Alalawi\inspower\resources\views/programs/createSection.blade.php ENDPATH**/ ?>