<?php
// init the resource
$ch = curl_init();
$merchant_id = "TEST100067231";
$PWD = "8d4ec52a5782a2e4fbbf32625236e873";
$currency_code = "BHD";
$merchant_name = "INSPOWER FITNESS COACHING";
$logo = "https://inspower-fitnesscoaching.com/images/payment-logo.png";
$lang = __('text.lang');

$description = "Premium Membership";

$post_data = array("apiOperation" => "CREATE_CHECKOUT_SESSION",
    "order" => array("id" => $order_id, "currency" => $currency_code),
    'interaction' => array('operation' => "PURCHASE"));

$data_string = json_encode($post_data);
curl_setopt($ch, CURLOPT_USERPWD, "merchant.$merchant_id:$PWD");
curl_setopt($ch, CURLOPT_URL, "https://afs.gateway.mastercard.com/api/rest/version/57/merchant/$merchant_id/session");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_TIMEOUT, 30);
curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data_string))
);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);

$content = curl_exec($ch);
$output = json_decode($content, true);

//dd($order_id);

?>
<html lang="<?php echo e(__('text.lang')); ?>" dir="<?php echo e(__('text.dir')); ?>">

<?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<head>
    <script src="https://afs.gateway.mastercard.com/checkout/version/57/checkout.js"
            data-error="errorCallback"
            data-cancel="cancelCallback"
            data-beforeRedirect="Checkout.saveFormFields"
            data-afterRedirect="Checkout.restoreFormFields"
            data-timeout="timeoutCallback"
            data-complete="completeCallback">
    </script>

    <script type="text/javascript">
        function errorCallback(error) {
            console.log(JSON.stringify(error));
        }

        function cancelCallback() {
            confirm('Are you sure you want to cancel?');
            console.log('Payment cancelled');
        }

        timeoutCallback = "https://www.test.inspower-fitnesscoaching.com/viewCart";
        completeCallback = "https://www.test.inspower-fitnesscoaching.com/mastercardApproved?order_id=<?php echo $order_id; ?>&description=<?php echo $description; ?>"


        Checkout.configure({
            merchant: '<?php echo $merchant_id; ?>',
            session: {
                id: "<?php echo $output['session']['id']; ?>"
            },
            order: {
                amount: '<?php echo $amount; ?>',
                currency: 'BHD',
                description: '<?php echo $description; ?>',
                id: "<?php echo $order_id; ?>",
            },
            interaction: {
                //returnUrl: "https://www.test.inspower-fitnesscoaching.com/paymentState?order_id=<?php echo $order_id; ?>&description=<?php echo $description; ?>&SI=<?php echo $output['successIndicator']; ?>",
                merchant: {
                    name: '<?php echo $merchant_name; ?>',
                    logo: '<?php echo $logo; ?>',
                    url: "https://inspower-fitnesscoaching.com",
                    email: "inspowerpt@gmail.com",
                    address: {
                        line1: "Building 1226, Block 351, Road 5124",
                        line2: "Manama, Kingdom of Bahrain"
                    }
                },
                displayControl: {
                    billingAddress: 'HIDE',
                    orderSummary: 'SHOW'
                }
            }
        });

        //window.onload = Checkout.showPaymentPage();

    </script>
</head>
<html lang="<?php echo e(__('text.lang')); ?>" dir="<?php echo e(__('text.dir')); ?>">

<?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<body>

<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="wrapper" style="background-color: #2D322F">
    <!-- Main Section-->
    <div class="main app form" id="main">
        <div class="pricing-section text-center" style="min-height: 100%">
            <div class="container">
                <div class="col-md-8 col-md-offset-2 col-sm-12 nopadding text-center" style="float: left">
                    <div class="pricing-intro">
                        <h1 class="wow fadeInUp" data-wow-delay="0s"> <?php echo e(__('text.confirmTitle')); ?> </h1>
                    </div>

                    <div class="table-left wow fadeInUp" data-wow-delay="0.4s"
                         style="overflow: auto; background-color: rgba(255, 255, 255, 0.8)">
                        <div class="pricing-details">
                            <div style="display: block">
                                <div class="col-md-12" style="margin-bottom: 15px">
                                    <h1 class="wow fadeInUp" data-wow-delay="0s"
                                        style="font-weight: 800; font-size: 24px; color: black"> <?php echo e(__('text.detailsTitle')); ?> </h1>
                                    <div class="row">
                                                <span class="video-title">
                                                    <h1 style="color: black; font-size:20px; font-weight: 800"> <?php echo e(__('text.membershipConfirm')); ?>: </h1>
                                                </span>
                                    </div>

                                    <span class="video-title">
                                        <h1 style="color: #364f87; font-size:20px">
                                            <?php if($type=='Monthly'): ?>
                                                <?php echo e(__('text.monthlyPlan')); ?>

                                                <?php else: ?>
                                                <?php echo e(__('text.yearlyPlan')); ?>

                                                <?php endif; ?>
                                        </h1>
                                    </span>

                                </div>
                            </div>

                            <hr style="width: 100%; border-width:1px; border-color: black">

                            <div class="row">
                                <div class="col-md-8">
                                            <span class="video-title">
                                                <h1 style="color: red; font-size:20px; font-weight: 800;"> <?php echo e(__('text.cartTotal')); ?></h1>
                                            </span>
                                </div>

                                <div class="col-md-3">
                                            <span class="video-title">
                                                <h1 style="color: red; font-size:20px; font-weight: 800;"> <?php echo e($amount); ?> <?php echo e(__('text.BD')); ?> </h1>
                                            </span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr style="width: 100%; border-width:1px">
                    <div class="row">

                        <div class="col-md-6">
                            <button class="checkoutBtn2" title="Credit Card" onclick="Checkout.showPaymentPage();">
                                <img src="<?php echo e(asset('images/payment.png')); ?>" style="height:40px">
                            </button>
                        </div>


                        <div class="col-md-6">
                            <button class="checkoutBtn2">
                                <a  href="<?php echo e(route('benefitCheckout',['order_id' => $order_id, 'counter' => $amount])); ?>" title="Debit Card">
                                    <img src="<?php echo e(asset('images/benifit.png')); ?>" style="height:40px">
                                </a>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Footer Section -->
        <?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
<?php echo $__env->make('includes.footerScripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>
</html>
<?php /**PATH C:\Users\ZAINAB ALMOOT\inspower\resources\views/users/confirmMembershipOrder.blade.php ENDPATH**/ ?>