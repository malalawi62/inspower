<!doctype html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">

<body>

<div id="app" class="wrapper" style="padding-top: 50px">
    <div class="split-features" style="background-color: #FFF">
        <div class="split-content">
            <?php echo $__env->yieldContent('content'); ?>
        </div>
    </div>
</div>

</body>
</html>

<?php echo $__env->make('includes.footerScripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Maryam\inspower\resources\views/layouts/app.blade.php ENDPATH**/ ?>