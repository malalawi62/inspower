<div class="search-bar text-center">
    <form action="/search" method="get" role="search">
        <?php echo e(csrf_field()); ?>

        <div>
            <input type="text" class="mail" name="q"
                   placeholder="<?php echo e(__('text.search')); ?>">
            <button type="submit" class="mail" style="width: 13%;  border-radius: 10px; border: 1px solid rgb(178, 198, 188);">
                <i class="ion-search"></i>
            </button>
        </div>
    </form>
</div>
<?php /**PATH C:\Users\Maryam\inspower\resources\views/layout/searchBar.blade.php ENDPATH**/ ?>