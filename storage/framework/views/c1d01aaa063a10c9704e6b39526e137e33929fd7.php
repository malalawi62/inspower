<html lang="<?php echo e(__('text.lang')); ?>" dir="<?php echo e(__('text.dir')); ?>">

<?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<body>

<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="wrapper" style="background-color: #2D322F">
    <!-- Main Section-->
    <div class="main app form" id="main">
        <div class="pricing-section text-center" style="min-height: 100%">
            <div class="container">
                <div class="col-md-12 col-sm-12 banner-padding text-center" style="float: none">

                    <div class="pricing-intro">
                        <h1 class="wow fadeInUp" data-wow-delay="0s"> <?php echo e(__('text.personalInfo')); ?> </h1>
                    </div>
                    <div class="table-left wow fadeInUp" data-wow-delay="0.4s" style="overflow: auto; border-radius: 10px; border: none">
                            <div class="pricing-details">
                                <div style="display: block">
                                    <div class="col-md-12">
                                        <?php if($user->gender == 'f'): ?>
                                            <img class="img-responsive wow fadeIn" src="<?php echo e(asset('storage/female_avatar.png')); ?>"
                                                 alt="Talal Nabeel" style="max-height: 200px; display: block"/>
                                        <?php else: ?>
                                            <img class="img-responsive wow fadeIn" src="<?php echo e(asset('storage/male_avatar.png')); ?>"
                                                 alt="Talal Nabeel" style="max-height: 500px; display: block"/>
                                        <?php endif; ?>
                                        <div class="row">
                                            <h1 style="color: #F0E299; font-weight: 700; margin-top: 15px; margin-bottom: 0; text-align: center; float: none; font-size: 20px">
                                                <span style="color: #fdca00;" > <?php echo e(__('text.registerName')); ?>: </span> <?php echo e($user->name); ?></h1>
                                        </div>
                                        <div class="row">
                                            <h1 style="color: #F0E299; font-weight: 700; margin-top: 15px; margin-bottom: 0; text-align: center; float: none; font-size: 20px">
                                                <span style="color: #fdca00;" > <?php echo e(__('text.registerEmail')); ?>: </span> <?php echo e($user->email); ?></h1>
                                        </div>
                                        <div class="row">
                                            <h1 style="color: #F0E299; font-weight: 700; margin-top: 15px; margin-bottom: 0; text-align: center; float: none; font-size: 20px">
                                                <span style="color: #fdca00;" > <?php echo e(__('text.DOB')); ?>: </span> <?php echo e($user->DOB); ?></h1>
                                        </div>

                                        <div>
                                            <button class="btn btn-primary" onclick="location.href = '/profile/<?php echo e($user->id); ?>/edit';"
                                                    style="background-color: #EC3642; font-weight:700; border:0; padding: 10px 20px 10px 20px; margin-top: 50px; font-size: 18px; margin-bottom: 15px; width: 60%">
                                                <?php echo e(__('text.editProfile')); ?>

                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <hr style="width: 100%; border-width:2px;">

                    <div class="pricing-intro">
                        <h1 class="wow fadeInUp" data-wow-delay="0s"> <?php echo e(__('text.myCourses')); ?> </h1>
                    </div>
                    <div class="table-left wow fadeInUp" data-wow-delay="0.4s" style="overflow: auto; border-radius: 10px; border: none">
                        <div class="pricing-details">
                            <div style="display: block">
                                <div class="col-md-12">
                                    <?php if($courses != null): ?>
                                        <?php $c_counter = 0; ?>
                                        <?php $__currentLoopData = $courses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $course): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php $c_counter ++; ?>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <h1 class="h1"> <?php echo e($c_counter); ?> </h1>
                                                </div>
                                                <div class="col-md-6">
                                                    <h1 class="courseTitle" onclick="location.href = '/course/<?php echo e($course->course->id); ?>';"> <?php echo e($course->course->title); ?> </h1>
                                                </div>
                                            </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr style="width: 100%; border-width:2px;">

                    <div class="pricing-intro">
                        <h1 class="wow fadeInUp" data-wow-delay="0s"> <?php echo e(__('text.myPrograms')); ?> </h1>
                    </div>
                    <div class="table-left wow fadeInUp" data-wow-delay="0.4s" style="overflow: auto; border-radius: 10px; border: none">
                        <div class="pricing-details">
                            <div style="display: block">
                                <div class="col-md-12">
                                    <?php if($programs != null): ?>
                                        <?php $p_counter = 0; ?>
                                        <?php $__currentLoopData = $programs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $program): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php $p_counter ++; ?>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <h1 class="h1"> <?php echo e($p_counter); ?> </h1>
                                                </div>
                                                <div class="col-md-6">
                                                    <h1 class="courseTitle" onclick="location.href = '/program/<?php echo e($program->program->id); ?>';"> <?php echo e($program->program->title); ?> </h1>
                                                </div>
                                            </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr style="width: 100%; border-width:2px;">

                    <div class="pricing-intro">
                        <h1 class="wow fadeInUp" data-wow-delay="0s"> <?php echo e(__('text.myOrders')); ?> </h1>
                    </div>
                    <div class="table-left wow fadeInUp" data-wow-delay="0.4s" style="overflow: auto; border-radius: 10px; border: none">
                        <div class="pricing-details">
                            <div style="display: block">
                                <div class="col-md-12">
                                    <?php if($orders != null): ?>
                                        <?php $counter = 0; ?>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <h1 class="h1" style="color: #fdca00;"> <?php echo e(__('text.orderId')); ?> </h1>
                                                </div>
                                                <div class="col-md-4">
                                                    <h1 class="h1" style="color: #fdca00;"> <?php echo e(__('text.orderDate')); ?> </h1>
                                                </div>
                                                <div class="col-md-2">
                                                    <h1 class="h1" style="color: #fdca00;"> <?php echo e(__('text.orderPrice')); ?> </h1>
                                                </div>
                                                <div class="col-md-4">
                                                    <h1 class="h1"> </h1>
                                                </div>
                                            </div>

                                            <hr style="width: 100%; border-width:1px;">

                                        <?php $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php $counter ++; ?>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <h1 class="h1"> <?php echo e($order->id); ?> </h1>
                                                </div>
                                                <div class="col-md-4">
                                                    <h1 class="h1"> <?php echo e($order->created_at->toDateString()); ?> </h1>
                                                </div>
                                                <div class="col-md-2">
                                                    <h1 class="h1"> 100 <?php echo e(__('text.BD')); ?> </h1>
                                                </div>
                                                <div class="col-md-4">
                                                    <h1 class="courseTitle" onclick="location.href = '/program/';"> <?php echo e(__('text.view')); ?> </h1>
                                                </div>
                                            </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
</div>
<!-- Jquery and Js Plugins -->
<?php echo $__env->make('includes.footerScripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>
</html>

<?php /**PATH /Users/maryam/inspower/resources/views/users/profile.blade.php ENDPATH**/ ?>