<!DOCTYPE html>
<html lang="<?php echo e(__('text.lang')); ?>" dir="<?php echo e(__('text.dir')); ?>">

<?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<body>
<script>
    var msg = '<?php echo e(Session::get('alert')); ?>';
    var exist = '<?php echo e(Session::has('alert')); ?>';
    if (exist) {
        alert(msg);
    }
</script>
<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app" id="main">
        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe">
            <div class="container">
                <div class="cta-inner">
                    <div class="row">
                        <h1 style="text-align: center; float: none"> <?php echo e(__('text.editCourseTitle')); ?> </h1>
                    </div>

                    <div class="wow fadeInUp" data-wow-delay="0.3s">
                        <form class="forms-form center-form wow zoomIn" action="/course/<?php echo e($course->id); ?>" method="post"
                              enctype="multipart/form-data">
                            <?php echo method_field('PATCH'); ?>

                            <div class="row">
                                <h1 style="text-align: center; float: none"> <?php echo e(__('text.courseDetails')); ?> </h1>
                            </div>

                            <?php $__errorArgs = ['title'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> <h5 style="color: red;"><?php echo e($message); ?></h5> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            <input class="mail" type="text" name="title"
                                   placeholder="<?php echo e(__('text.inputCourseTitle')); ?>"
                                   autocomplete="title" value="<?php echo e($course->title); ?>">

                            <?php $__errorArgs = ['overview'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> <p style="color: red"><?php echo e($message); ?></p> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            <p><?php echo e(__('text.inputCourseOverview')); ?></p>
                            <textarea class="mail" rows="10" name="overview" style="height: auto;"><?php echo e($course->overview); ?></textarea>

                            <?php $__errorArgs = ['image'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> <h5 style="color: red"><?php echo e($message); ?></h5> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            <p><?php echo e(__('text.inputImage')); ?></p>
                            <input class="mail" type="file" name="image" id="image"
                                   style="display: inline-block;padding-top: 10px">

                            <?php $__errorArgs = ['price'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> <h5 style="color: red;"><?php echo e($message); ?></h5> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            <input class="mail" type="number" name="price"
                                   placeholder="<?php echo e(__('text.inputCoursePrice')); ?>"
                                   value="<?php echo e($course->price); ?>">

                            <input class="mail" type="text" name="videoUrl"
                                   placeholder="<?php echo e(__('text.inputCourseVideo')); ?>"
                                   autocomplete="off" value="https://vimeo.com/<?php echo e($course->videoUrl); ?>">

                            <p><?php echo e(__('text.inputCourseDescription')); ?></p>
                            <textarea class="mail" rows="10" name="description" style="height: auto;"><?php echo e($course->description); ?></textarea>

                            <?php echo csrf_field(); ?>
                            <div>
                                <button class="btn btn-primary"
                                        style="background-color: #EC3642; border:0; padding: 10px 40px 10px 40px; width:60%;">
                                    <?php echo e(__('text.editCourseBtn')); ?>

                                </button>
                            </div>
                        </form>
                            <hr style="width: 100%; border-width:1px">

                            <div class="modules">
                                <?php $counter = 0; ?>
                                <?php $__currentLoopData = $modules; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $module): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php $counter ++; ?>
                                        <div class="col-md-12" style="padding-bottom: 30px">
                                            <div class="row">
                                                <form action="/module/<?php echo e($module->id); ?>/delete" method="post">
                                                <h2 class="h2" style="text-transform: uppercase"> <?php echo e(__('text.moduleTitle2')); ?>: <?php echo e($module->m_title); ?>

                                                    <a href="/module/<?php echo e($module->id); ?>/edit" style="color: #8c0000">
                                                        <i class="ion-edit"></i>
                                                    </a>
                                                    <button onclick="return confirm('Are you sure you want to DELETE this module?')"
                                                            style="color: #8c0000; background-color: transparent; border-width: 0; font-size: 24px">
                                                        <i class="ion-android-delete"></i>
                                                    </button>
                                                </h2>
                                                    <?php echo csrf_field(); ?>
                                                </form>
                                            </div>

                                            <div class="row">
                                                <h2 class="h2" style="<?php echo e(__('text.textAlign')); ?>"> <?php echo e(__('text.moduleDescriptionShow')); ?>: </h2>
                                                <p class="article-body">
                                                    <?php echo nl2br($module->m_description); ?>

                                                </p>
                                            </div>

                                            <div class="row">
                                                <h2 class="h2" style="<?php echo e(__('text.textAlign')); ?>"> <?php echo e(__('text.moduleLessons')); ?>: </h2>
                                            </div>
                                            <?php $__currentLoopData = $lessons; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lesson): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php if($lesson->course_module_id == $module->id): ?>
                                                    <div class="row" style="width: 80%">
                                                        <p class="article-body">
                                                            <?php echo nl2br($lesson->v_description); ?>

                                                        </p>
                                                    </div>

                                                    <div class="row">
                                                        <p class="article-body"> <a href="https://vimeo.com/<?php echo e($lesson->m_videoUrl); ?>"> https://vimeo.com/<?php echo e($lesson->m_videoUrl); ?> </a> </p>
                                                    </div>

                                                    <div class="row">
                                                        <hr style="width: 80%; border-width:1px; <?php echo e(__('text.floating')); ?>; padding: 0">
                                                    </div>
                                                <?php endif; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </div>

                                        <hr style="width: 100%; border-width:1px">
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                    <!-- this section is to add a new module -->
                                    <form action="/module/create" method="get">
                                        <input type="hidden" name="course_id" value="<?php echo e($course->id); ?>">
                                        <div>
                                            <button class="btn btn-primary"
                                                    style="background-color: #EC3642; border:0; padding: 10px 40px 10px 40px; width:60%;">
                                                <?php echo e(__('text.addModuleBtn')); ?>

                                            </button>
                                        </div>
                                        <?php echo csrf_field(); ?>
                                    </form>
                            </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        <?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
<?php echo $__env->make('includes.footerScripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>
</html>
<?php /**PATH C:\Users\ZAINAB ALMOOT\inspower\resources\views/courses/edit.blade.php ENDPATH**/ ?>