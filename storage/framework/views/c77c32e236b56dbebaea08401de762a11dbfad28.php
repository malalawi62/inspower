<html lang="<?php echo e(__('text.lang')); ?>" dir="<?php echo e(__('text.dir')); ?>">

<?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<body>

<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app form" id="main">
        <div class="pricing-section text-center" style="height: 90%">
            <div class="container">
                <div class="col-md-12 col-sm-12 nopadding">
                    <div class="pricing-intro">
                        <h1 class="wow fadeInUp" data-wow-delay="0s"> <?php echo e(__('text.allWritersTitle')); ?> </h1>
                    </div>
                    <div class="col-sm-12">
                        <div class="table-left wow fadeInUp" data-wow-delay="0.4s">
                            <div class="pricing-details">
                                <div>
                                    <?php $__empty_1 = true; $__currentLoopData = $data['writers']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $writer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                        <form action="/writers/<?php echo e($writer->id); ?>" method="post">
                                            <?php echo method_field('DELETE'); ?>
                                            <div class="row">
                                                <span class="article-title">
                                                    <a style="color: #FFF;">
                                                        <?php if($data['lang'] == 'en'): ?>
                                                            <?php echo e($writer->englishName); ?>

                                                        <?php else: ?>
                                                            <?php echo e($writer->arabicName); ?>

                                                        <?php endif; ?>
                                                    </a>
                                                </span>
                                                <span class="editDeleteIcons">
                                                    <a href="/writers/<?php echo e($writer->id); ?>/edit" style="color: #FCD400">
                                                        <i class="ion-edit"></i>
                                                    </a>
                                                    <button style="color: #FCD400; background-color: transparent; border-width: 0; font-size: 24px">
                                                        <i class="ion-android-delete"></i>
                                                    </button>
                                                </span>
                                            </div>
                                            <br>
                                            <?php echo csrf_field(); ?>
                                        </form>
                                        <br>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                        <h2>No Writers Found</h2>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        <?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
<?php echo $__env->make('includes.footerScripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>
</html>
<?php /**PATH C:\Users\Maryam Alalawi\inspower\resources\views/writers/index.blade.php ENDPATH**/ ?>