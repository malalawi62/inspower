<html lang="<?php echo e(__('text.lang')); ?>" dir="<?php echo e(__('text.dir')); ?>">

<?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<body>

<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app form" id="main">

        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe" style="min-height: 95%">
            <div class="container">
                <div class="cta-inner">
                    <div class="form wow fadeInUp" data-wow-delay="0.3s">
                        <div class="row top-padding-center" style="padding-bottom: 20px">
                            <div class="row" style="padding-bottom: 20px">
                                <h1 style="color: #fdca00; font-weight: 700; margin-top: 15px; margin-bottom: 0; text-align: center; float: none;">
                                    <?php echo e(__('text.verifyTitle')); ?></h1>
                            </div>

                            <?php if(session('resent')): ?>
                                <div class="alert alert-success" role="alert">
                                    <?php echo e(__('text.freshLink')); ?>

                                </div>
                            <?php endif; ?>

                            <div class="row">
                                <h1 style="color: #F0E299; font-weight: 700; margin-top: 15px; margin-bottom: 0; text-align: center; float: none;"> <?php echo e(__('text.before')); ?> </h1>
                                <h1 style="color: #F0E299; font-weight: 700; margin-top: 15px; margin-bottom: 0; text-align: center; float: none;"> <?php echo e(__('text.sure')); ?> </h1>
                            <form class="d-inline" method="POST" style="padding-top: 20px" action="<?php echo e(route('verification.resend')); ?>">
                                <?php echo csrf_field(); ?>
                                <button type="submit" class="btn btn-link p-0 m-0 align-baseline"> <span style="font-weight: 700; font-size: 28px;margin-top: 15px; margin-bottom: 0; text-align: center; float: none;color: #8c0000"> <?php echo e(__('text.click')); ?> </span></button>.
                            </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        <?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
<?php echo $__env->make('includes.footerScripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>
</html>

<?php /**PATH C:\Users\Maryam Alalawi\inspower\resources\views/auth/verify.blade.php ENDPATH**/ ?>