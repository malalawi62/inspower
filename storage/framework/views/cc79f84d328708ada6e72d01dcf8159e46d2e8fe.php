<div class="footer fixed-bottom" id="footer">
    <div class="container">
        <p style="text-align: center">©2020 Inspower. All Rights Reserved</p>
        <p style="text-align: center">Developed by <a href="http://www.maryam-alalawi.com" style="color: #FFF">MARYAM AL-ALAWI</a></p>
    </div>
</div>
<?php /**PATH /home2/inspower/inspower/resources/views/layout/footer.blade.php ENDPATH**/ ?>