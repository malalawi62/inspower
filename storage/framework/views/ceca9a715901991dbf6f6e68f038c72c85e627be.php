<script type="text/javascript" src="<?php echo e(asset('js/jquery-2.1.1.js')); ?>"></script>
<script src="<?php echo e(asset('js/jquery.subscribe.js')); ?>"></script>
<script src="<?php echo e(asset('js/app.js')); ?>" defer></script>
<script type="text/javascript" src="<?php echo e(asset('js/bootstrap.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('js/plugins.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('js/menu.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('js/custom.js')); ?>"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAJ5BZftoKtBStvTZ_6NZy0eIqDuR6h5UM&callback=initMap"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="js.js"></script>
<script src="https://unpkg.com/boxicons@latest/dist/boxicons.js"></script>
<script>
    <?php if(Session::has('message')): ?>
        var type="<?php echo e(Session::get('alert-type','info')); ?>"

        toastr.options.positionClass = "toast-top-center";
        toastr.options.hideMethod = 'slideUp';

        <?php if(App::isLocale('ar')): ?>
            toastr.options.rtl = true;
            switch(type){
                case 'info':
                    toastr.info("<?php echo e(Session::get('messageAr')); ?>");
                    break;
                case 'success':
                    toastr.success("<?php echo e(Session::get('messageAr')); ?>");
                    break;
                case 'warning':
                    toastr.warning("<?php echo e(Session::get('messageAr')); ?>");
                    break;
                case 'error':
                    toastr.error("<?php echo e(Session::get('messageAr')); ?>");
                    break;
            }
        <?php else: ?>
            switch(type){
                case 'info':
                    toastr.info("<?php echo e(Session::get('message')); ?>");
                    break;
                case 'success':
                    toastr.success("<?php echo e(Session::get('message')); ?>");
                    break;
                case 'warning':
                    toastr.warning("<?php echo e(Session::get('message')); ?>");
                    break;
                case 'error':
                    toastr.error("<?php echo e(Session::get('message')); ?>");
                    break;
            }
        <?php endif; ?>
    <?php endif; ?>
</script>
<script>
    var counter_m = 2;
    var wrapper_m = $('.modules'); //Input field wrapper
    function addModule(){
        var moduleNew='<?php echo e(__('text.moduleNew')); ?>';
        var moduleTitle='<?php echo e(__('text.moduleTitle')); ?>';
        var moduleDescription ='<?php echo e(__('text.moduleDescription')); ?>';
        var moduleVideoURL='<?php echo e(__('text.moduleVideoURL')); ?>';
        var ModuleContent = '' +
            '<div class="module module_wrapper_'+counter_m+'">' +
            '<hr style="width: 100%; border-width:1px">' +
            '<div class="row">'+
                    '<h1 style="text-align: center; float: none; padding-top: 0" id="module_title_'+counter_m+'"> ' + moduleNew +
                        '<input class="add_module_button" type="button" onclick="addModule()" value="+" style="color: #8c0000; background: transparent; border-color: transparent; border-width: 0">' +
                        '<input class="delete_module_button" type="button" onclick="deleteModule('+counter_m+')" value="-" style="color: #8c0000; background: transparent; border-color: transparent; border-width: 0">' +
                    '</h1>' +
                '</div>'+
                '<input class="mail" type="text" name="m_title[]" placeholder="' + moduleTitle + '" autocomplete="title" value="">' +
                '<p>' + moduleDescription + '</p>' +
                '<textarea class="mail" rows="10" name="m_description[]" style="height:auto;" value="value"></textarea>' +
                '<div class="video_wrapper">'+
                    '<div class="row">'+
                        '<input class="mail" type="text" name="m_videoUrl['+counter_m+'][]" placeholder="' + moduleVideoURL + '" autocomplete="off" value="" style="width:50%">'+
                        '<input class="add_video_course_button" type="button" onclick="addCourseVideo('+counter_m+')" value="+" style="color: #8c0000; background: transparent; border-color: transparent; border-width: 0">' +
                    '</div>'+
                '</div>'+
            '</div>'; //New input field html

            $(".modules:last").append(ModuleContent);

        counter_m ++;
    }

 function deleteModule(x){
     $('.module_wrapper_'+x).remove();
     counter_m --;
 }
</script>
<script>
    var videoCounterCourse = 2;
    function addCourseVideo(x){
        var wrapper1 = $('.module_wrapper_'+x); //Input field wrapper
        var moduleVideoURL='<?php echo e(__('text.moduleVideoURL')); ?>';
        var VideoURL = '' +
            '<div class="video_wrapper video_'+videoCounterCourse+'">' +
                '<div class="row">'+
                    '<input class="mail" type="text" name="m_videoUrl['+x+'][]" placeholder="' + moduleVideoURL + '" autocomplete="off" value="" style="width: 50%">' +
                    '<input class="add_video_course_button" type="button" onclick="addCourseVideo('+x+')" value="+" style="color: #8c0000; background: transparent; border-color: transparent; border-width: 0">'+
                    '<input class="delete_video_course_button" type="button" onclick="deleteCourseVideo('+videoCounterCourse+')" value="-" style="color: #8c0000; background: transparent; border-color: transparent; border-width: 0">' +
                '</div>'+
            '</div>';//New input field html

        $(VideoURL).clone().appendTo(wrapper1);
        videoCounterCourse ++;
    }

    function deleteCourseVideo(y){
        $('.video_'+y).remove();
    }
</script>
<script>
    var counter = 2;
    var wrapper = $('.phases'); //Input field wrapper
    function addPhase(){
        var phaseNew='<?php echo e(__('text.phaseNew')); ?>';
        var phaseTitle='<?php echo e(__('text.phaseTitle')); ?>';
        var phaseDescription ='<?php echo e(__('text.phaseDescription')); ?>';
        var phasePdf='<?php echo e(__('text.phasePdf')); ?>';
        var phaseVideoURL='<?php echo e(__('text.phaseVideoURL')); ?>';
        var phaseBriefDescription = '<?php echo e(__('text.phaseBriefDescription')); ?>';
        var PhaseContent = '' +
            '<div class="phase phase_wrapper_'+counter+'">' +
                '<hr style="width: 100%; border-width:1px">' +
                '<div class="row">'+
                    '<h1 style="text-align: center; float: none; padding-top: 0" id="phase_title_'+counter+'"> ' + phaseNew +
                        '<input class="add_phase_button" type="button" onclick="addPhase()" value="+" style="color: #8c0000; background: transparent; border-color: transparent; border-width: 0">' +
                        '<input class="delete_phase_button" type="button" onclick="deletePhase('+counter+')" value="-" style="color: #8c0000; background: transparent; border-color: transparent; border-width: 0">' +
                    '</h1>'+
                '</div>'+
                '<input class="mail" type="text" name="p_title[]" placeholder="' + phaseTitle + '" autocomplete="title" value="">' +
                '<p>' + phaseDescription + '</p>' +
                '<textarea class="mail" rows="10" name="p_description[]" style="height:auto;" value="value"></textarea>' +
                '<p>'+ phaseBriefDescription +'</p>' +
                '<textarea class="mail" rows="10" name="p_overview[]" style="height: auto; width: 60%;" value=""></textarea>'+
            '<input class="mail" type="text" name="p_videoUrl[]" placeholder="pl"' +
            'autocomplete="off" value="">' +
                '<label class="mail" style="border: 1.5px solid #babdc2; padding-top: 10px">' +
                    '' + phasePdf + ''+
                '<input type="file" name="p_Pdf[]" id="bodyPdf" style="display: inline-block"/>' +
                '</label>'+
                '<div class="videos">' +
                    '<div class="video_wrapper">'+
                        '<div class="row">'+
                            '<input class="mail" type="text" name="ps_videoUrl['+counter+'][]" placeholder="' + phaseVideoURL + '" autocomplete="off" value="">'+
                            '<p>'+ phaseBriefDescription +'</p>' +
                            '<textarea class="mail" rows="10" name="ps_description['+counter+'][]" style="height: auto;" value=""></textarea>'+
                            '<div class="row">'+
                                '<input class="add_video_button" type="button" onclick="addVideo('+counter+')" value="+" style="color: #8c0000; background: transparent; border-color: transparent; border-width: 0">' +
                            '</div>' +
                        '</div>'+
                    '</div>'+
                '</div>' +
            '</div>'; //New input field html

        $(".phases:last").append(PhaseContent);
        counter ++;
    }

    function deletePhase(x){
        $('.phase_wrapper_'+x).remove();
        counter--;
    }
</script>
<script>
    var videoCounter = 2;
    function addVideo(x){
        var wrapper1 = $('.phase_wrapper_'+x); //Input field wrapper
        var phaseVideoURL='<?php echo e(__('text.phaseVideoURL')); ?>';
        var phaseBriefDescription = '<?php echo e(__('text.phaseBriefDescription')); ?>';
        var VideoURL = '' +
            '<div class="video_wrapper video_'+videoCounter+'">' +
                '<div class="row">'+
                    '<input class="mail" type="text" name="ps_videoUrl['+x+'][]" placeholder="' + phaseVideoURL + '" autocomplete="off" value="">' +
                    '<p>'+ phaseBriefDescription +'</p>' +
                    '<textarea class="mail" rows="10" name="ps_description['+x+'][]" style="height: auto; width: 60%;" value=""></textarea>'+
                    '<div class="row">'+
                        '<input class="add_video_button" type="button" onclick="addVideo('+x+')" value="+" style="color: #8c0000; background: transparent; border-color: transparent; border-width: 0">'+
                        '<input class="delete_video_button" type="button" onclick="deleteVideo('+videoCounter+')" value="-" style="color: #8c0000; background: transparent; border-color: transparent; border-width: 0">' +
                    '</div>' +
                '</div>'+
            '</div>';//New input field html

        $(VideoURL).clone().appendTo(wrapper1);
        videoCounter ++;
    }

    function deleteVideo(y){
        $('.video_'+y).remove();
    }
</script>
<script>
    function showPdf() {
        var x = document.getElementById("pdf");
        var r= document.getElementById("read");
        x.style.display = "block";
        r.style.display="none";
    }
</script>
<?php /**PATH /Users/maryam/inspower/resources/views/includes/footerScripts.blade.php ENDPATH**/ ?>