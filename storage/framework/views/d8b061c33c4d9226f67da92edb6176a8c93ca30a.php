<html lang="<?php echo e(__('text.lang')); ?>" dir="<?php echo e(__('text.dir')); ?>">

<?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<body>

<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app form" id="main">

        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe" style="min-height: 100%">
            <div class="container">
                <div class="cta-inner">
                    <div class="form wow fadeInUp" data-wow-delay="0.3s">
                        <div class="row banner-padding" style="padding-bottom: 20px">
                            <?php if($user->gender == 'f'): ?>
                                <img class="img-responsive wow fadeIn" src="<?php echo e(asset('storage/female_avatar.png')); ?>"
                                     alt="Talal Nabeel" style="max-height: 300px; display: block"/>
                            <?php else: ?>
                                <img class="img-responsive wow fadeIn" src="<?php echo e(asset('storage/male_avatar.png')); ?>"
                                     alt="Talal Nabeel" style="max-height: 500px; display: block"/>
                            <?php endif; ?>
                        </div>
                    </div>

                    <hr style="width: 100%; border-width:1px">

                    <form class="forms-form center-form wow zoomIn" method="POST" action="/profile/<?php echo e($user->id); ?>">
                        <?php echo method_field('PATCH'); ?>

                        <?php $__errorArgs = ['name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> <h5 style="color: red;"><?php echo e($message); ?></h5> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                        <input class="mail" type="text" name="name" placeholder="<?php echo e(__('text.registerName')); ?>"
                               required autocomplete="name" autofocus value="<?php echo e($user->name); ?>">

                        <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> <h5 style="color: red;"><?php echo e($message); ?></h5> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                        <input class="mail" type="email" name="email" placeholder="<?php echo e(__('text.registerEmail')); ?>"
                               required autocomplete="email" value="<?php echo e($user->email); ?>">

                        <?php $__errorArgs = ['DOB'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> <h5 style="color: red;"><?php echo e($message); ?></h5> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                        <input class="mail" type="date" name="DOB" id="DOB"
                               required value="<?php echo e($user->DOB); ?>">

                        <div>
                            <button class="btn btn-primary"
                                    style="background-color: #EC3642; border:0; padding: 10px 40px 10px 40px; width:60%;">
                                <?php echo e(__('text.editProfile')); ?>

                            </button>
                        </div>
                        <?php echo csrf_field(); ?>
                    </form>

                </div>
            </div>
        </div>

        <!-- Footer Section -->
        <?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
<?php echo $__env->make('includes.footerScripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>
</html>
<?php /**PATH C:\Users\Maryam Alalawi\inspower\resources\views/users/editProfile.blade.php ENDPATH**/ ?>