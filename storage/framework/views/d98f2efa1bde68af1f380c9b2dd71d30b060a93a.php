<!DOCTYPE html>
<html lang="<?php echo e(__('text.lang')); ?>" dir="<?php echo e(__('text.dir')); ?>">

<?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<body>

<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<div class="wrapper">
    <!-- Main Section-->
    <div class="main app" id="main">
        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe" style="height: 100%">
            <div class="container">
                <div class="cta-inner">
                    <h1> <?php echo e(__('text.addYoutubeTitle')); ?> </h1>
                    <div class="wow fadeInUp" data-wow-delay="0.3s">
                        <form class="forms-form center-form wow zoomIn" action="/videos" method="post">

                            <?php $__errorArgs = ['videoId'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> <h5 style="color: red;"><?php echo e($message); ?></h5> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            <input class="mail" type="text" name="videoId" placeholder="Video URL"
                                   autocomplete="off" id="videoId" value="<?php echo e(old('videoId')); ?>">

                            <?php $__errorArgs = ['title'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> <h5 style="color: red;"><?php echo e($message); ?></h5> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            <input class="mail" type="text" name="title" placeholder="Video Title"
                                   autocomplete="off" id="title" value="<?php echo e(old('title')); ?>">

<<<<<<< HEAD
                            <?php $__errorArgs = ['titleAr'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> <h5 style="color: red;"><?php echo e($message); ?></h5> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            <input class="mail" type="text" name="titleAr"
                                   autocomplete="off" id="titleAr" placeholder="عنوان الفيديو باللغة العربية"
                                   value="<?php echo e(old('titleAr')); ?>">

=======
>>>>>>> 68063f6772344442635537c26f6420f83f85c557
                            <?php $__errorArgs = ['category_id'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> <h5 style="color: red"><?php echo e($message); ?></h5> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            <select class="mail" name="category_id" id="category_id" style="color: #FFF">
                                <option hidden value="" selected><?php echo e(__('text.chooseCategory')); ?></option>
                                <?php $__empty_1 = true; $__currentLoopData = $data['categories']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                    <option value="<?php echo e($category->id); ?>"
                                            style="background-color: #2D322F">
                                        <?php if($data['lang'] == 'en'): ?>
                                            <?php echo e($category->englishName); ?>

                                        <?php else: ?>
                                            <?php echo e($category->arabicName); ?>

                                        <?php endif; ?>
                                    </option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                    <option>No Categories Found</option>
                                <?php endif; ?>
                            </select>

                            <p>Video Description in english</p>
                            <textarea class="mail" rows="5" name="description" id="description"
                                      style="height: auto;"></textarea>

<<<<<<< HEAD
                            <p >وصف الفيديو باللغة العربية</p>
                            <textarea class="mail" rows="5" name="descriptionAr" id="descriptionAr"
                                      style="height: auto;"></textarea>

                            <?php $__errorArgs = ['date'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> <p style="color: red"><?php echo e($message); ?></p> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            <input type="hidden" name="date" id="date" value="02-02-2020">
=======
                            <input type="hidden" name="type" id="type" value="youtube">

                            <?php if($data['lang'] == 'en'): ?>
                                <input type="hidden" name="language" id="language" value="Eng">
                            <?php else: ?>
                                <input type="hidden" name="language" id="language" value="Ar">
                            <?php endif; ?>
>>>>>>> 68063f6772344442635537c26f6420f83f85c557

                            <?php echo csrf_field(); ?>
                            <div>
                                <button class="btn btn-primary"
                                        style="background-color: #EC3642; border:0; padding: 10px 40px 10px 40px; width:60%;">
                                    <?php echo e(__('text.addYoutubeBtn')); ?>

                                </button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        <?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
<?php echo $__env->make('includes.footerScripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

</body>
</html>
<?php /**PATH C:\Users\Maryam\inspower\resources\views/videos/create.blade.php ENDPATH**/ ?>