<html lang="<?php echo e(__('text.lang')); ?>" dir="<?php echo e(__('text.dir')); ?>">

<?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<body>
<script>
    var msg = '<?php echo e(Session::get('alert')); ?>';
    var exist = '<?php echo e(Session::has('alert')); ?>';
    if (exist) {
        alert(msg);
    }
</script>

<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="wrapper" style="background-color: #2D322F">
    <!-- Main Section-->
    <div class="main app form" id="main">
        <div class="pricing-section text-center" style="min-height: 100%">
            <div class="container">
                <div style="width: 100%">
                    <div class="pricing-intro banner-padding">
                        <h1 class="wow fadeInUp" data-wow-delay="0s"> <?php echo e(__('text.allProgramsTitle')); ?> </h1>
                        <?php if((App::isLocale('en'))): ?>
                            <h2>For programs in Arabic language <a href="setlocale/ar">press here</a></h2>
                        <?php endif; ?>
                        <?php if((App::isLocale('ar'))): ?>
                            <h2>للبرامج التدريبية باللغة الإنجليزية <a href="setlocale/en">اضغط هنا</a></h2>
                        <?php endif; ?>
                    </div>
                    <?php $__currentLoopData = $programs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $program): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php $flag = false; ?>
                            <?php if(auth()->guard()->guest()): ?>
                            <?php else: ?>
                            <?php if(auth()->user()->type == 'admin'): ?>
                                <?php $flag = true; ?>
                            <?php elseif($orders != null): ?>
                                <?php for($i=0; $i<count($orders); $i++): ?>
                                    <?php if($orders[$i]->program_id == $program->id): ?>
                                        <?php $flag = true; ?>
                                        <?php break; ?>
                                    <?php endif; ?>
                                <?php endfor; ?>
                            <?php endif; ?>
                            <?php endif; ?>
                        <?php if($program->status == 'inactive'): ?>
                            <?php if(auth()->guard()->guest()): ?>
                            <?php else: ?>
                                <?php if(auth()->user()->type == 'admin' || $flag == true): ?>
                                    <div class="table-left wow fadeInUp" data-wow-delay="0.4s" style="overflow: auto;">
                                        <div class="pricing-details">
                                            <div style="display: block">
                                                <div class="col-md-4">
                                                    <img class="show-img wow fadeIn"
                                                         src="<?php echo e(asset('storage/'.$program->image)); ?>"
                                                         style="padding-bottom: 10px"/>
                                                </div>
                                                <br>
                                                <div class="col-md-8">
                                                    <div class="row">
                                                <span class="video-title">
                                                    <a style="color: #ee2724; font-size:20px"
                                                       href="/program/<?php echo e($program->id); ?>_<?php echo e(str_replace(' ', '_', $program->title)); ?>">
                                                        <?php echo e($program->title); ?> <span style="color: #2D322F; font-weight: 700;">*<?php echo e(__('text.inactiveProgram')); ?>*</span>
                                                    </a>
                                                </span>
                                                        <?php if(auth()->guard()->guest()): ?>
                                                        <?php else: ?>
                                                            <?php if(auth()->user()->type == 'admin'): ?>
                                                                <form action="/program/<?php echo e($program->id); ?>" method="post">
                                                        <span class="editDeleteIcons">
                                                            <a href="/program/<?php echo e($program->id); ?>/edit"
                                                               style="color: #364f87">
                                                                <i class="ion-edit"></i>
                                                            </a>
                                                            <a href="/sendProgram/<?php echo e($program->id); ?>"
                                                               style="color: #364f87">
                                                                <i class="ion-android-send"></i>
                                                            </a>
                                                            <?php if($program->status == 'active'): ?>
                                                                <button
                                                                    onclick="return confirm('Are you sure you want to INACTIVE this program?')"
                                                                    style="color: #364f87; background-color: transparent; border-width: 0; font-size: 24px">
                                                                    <i class="fas fa-toggle-off"></i>
                                                                </button>
                                                            <?php else: ?>
                                                                <a href="/programActive/<?php echo e($program->id); ?>"
                                                                   style="color: #364f87">
                                                                    <i class="fas fa-toggle-on"></i>
                                                                </a>
                                                            <?php endif; ?>
                                                        </span>
                                                                    <?php echo csrf_field(); ?>
                                                                </form>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </div>
                                                    <h2 class="articleBody" style="color: #2D322F">
                                                        <?php echo e(__("text.price")); ?>

                                                        <span style="color: #ee2724"><?php echo e($program->price); ?> <?php echo e(__("text.BD")); ?></span>
                                                    </h2>
                                                    <h2 class="articleBody" style="color: #969696">
                                                        <?php echo nl2br($program->overview); ?>

                                                    </h2>
                                                    <form action="/cart" method="post">
                                                        <input type="hidden" name="program_id" value="<?php echo e($program->id); ?>">
                                                        <?php if(auth()->guard()->guest()): ?>
                                                        <?php else: ?>
                                                            <input type="hidden" name="user_id" value="<?php echo e(auth()->user()->id); ?>">
                                                        <?php endif; ?>
                                                        <button class="submit-button"
                                                                style="<?php echo e(__('text.floating')); ?>; padding-left: 15px; padding-right: 15px" formaction="/program/<?php echo e($program->id); ?>" formmethod="get">
                                                            <?php echo e(__('text.learnMoreBtn')); ?>

                                                        </button>
                                                        <?php if($flag == false): ?>
                                                            <button class="cart-button" type="submit"
                                                                    style="<?php echo e(__('text.opFloating')); ?>; padding-left: 15px; padding-right: 15px">
                                                                <?php echo e(__('text.addToCartBtn')); ?>

                                                            </button>
                                                        <?php endif; ?>
                                                        <?php echo csrf_field(); ?>
                                                    </form>
                                                </div>
                                                <br>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            <?php endif; ?>
                        <?php else: ?>
                            <div class="table-left wow fadeInUp" data-wow-delay="0.4s" style="overflow: auto;">
                                <div class="pricing-details">
                                    <div style="display: block">
                                        <div class="col-md-4">
                                            <img class="show-img wow fadeIn"
                                                 src="<?php echo e(asset('storage/'.$program->image)); ?>"
                                                 style="padding-bottom: 10px"/>
                                        </div>
                                        <br>
                                        <div class="col-md-8">
                                            <div class="row">
                                                <span class="video-title">
                                                    <a style="color: #ee2724; font-size:20px; font-weight: 700"
                                                       href="/program/<?php echo e($program->id); ?>_<?php echo e(str_replace(' ', '_', $program->title)); ?>">
                                                        <?php echo e($program->title); ?>

                                                    </a>
                                                </span>
                                                <?php if(auth()->guard()->guest()): ?>
                                                <?php else: ?>
                                                    <?php if(auth()->user()->type == 'admin'): ?>
                                                        <form action="/program/<?php echo e($program->id); ?>" method="post">
                                                        <span class="editDeleteIcons">
                                                            <a href="/program/<?php echo e($program->id); ?>/edit"
                                                               style="color: #364f87">
                                                                <i class="ion-edit"></i>
                                                            </a>
                                                            <a href="/sendProgram/<?php echo e($program->id); ?>"
                                                               style="color: #364f87">
                                                                <i class="ion-android-send"></i>
                                                            </a>
                                                            <button
                                                                onclick="return confirm('Are you sure you want to INACTIVE this program?')"
                                                                style="color: #364f87; background-color: transparent; border-width: 0; font-size: 24px">
                                                                <i class="fas fa-toggle-off"></i>
                                                            </button>
                                                        </span>
                                                            <?php echo csrf_field(); ?>
                                                        </form>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </div>
                                            <h2 class="articleBody" style="color: #2D322F">
                                                <?php echo e(__("text.price")); ?>

                                                <span style="color: #ee2724"><?php echo e($program->price); ?> <?php echo e(__("text.BD")); ?></span>
                                            </h2>
                                            <h2 class="articleBody" style="color: #969696">
                                                <?php echo nl2br($program->overview); ?>

                                            </h2>
                                            <form action="/cart" method="post">
                                                <input type="hidden" name="program_id" value="<?php echo e($program->id); ?>">
                                                <?php if(auth()->guard()->guest()): ?>
                                                <?php else: ?>
                                                    <input type="hidden" name="user_id" value="<?php echo e(auth()->user()->id); ?>">
                                                <?php endif; ?>
                                                <button class="submit-button"
                                                        style="<?php echo e(__('text.floating')); ?>; padding-left: 15px; padding-right: 15px" formaction="/program/<?php echo e($program->id); ?>" formmethod="get">
                                                    <?php echo e(__('text.learnMoreBtn')); ?>

                                                </button>
                                                <?php if($flag == false): ?>
                                                    <button class="cart-button" type="submit"
                                                            style="<?php echo e(__('text.opFloating')); ?>; padding-left: 15px; padding-right: 15px">
                                                        <?php echo e(__('text.addToCartBtn')); ?>

                                                    </button>
                                                <?php endif; ?>
                                                <?php echo csrf_field(); ?>
                                            </form>
                                        </div>
                                        <br>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
        </div>
        <!-- Footer Section -->
        <?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
<?php echo $__env->make('includes.footerScripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>
</html>

<?php /**PATH C:\Users\Maryam Alalawi\inspower\resources\views/programs/index.blade.php ENDPATH**/ ?>