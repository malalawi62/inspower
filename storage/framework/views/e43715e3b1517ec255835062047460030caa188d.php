<html lang="<?php echo e(__('text.lang')); ?>" dir="<?php echo e(__('text.dir')); ?>">

<?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<body>

<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app" id="main">

        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe" style="min-height: 90%">
            <div class="container">
                <div class="cta-inner">
                    <div class="row">
                        <h1 style="float: none; text-align: center"> <?php echo e(__('text.addImageTitle')); ?> </h1>
                    </div>
                    <div class="wow fadeInUp" data-wow-delay="0.3s">
                        <form class="forms-form center-form wow zoomIn" action="/slideShow" method="post" enctype="multipart/form-data">

                            <?php $__errorArgs = ['imageEng'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> <h5 style="color: red"><?php echo e($message); ?></h5> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            <h3 style="padding-bottom: 10px"><?php echo e(__('text.slideShowEng')); ?></h3>
                            <input class="mail" type="file" name="imageEng" id="imageEng"
                                   style="display: inline-block;padding-top: 10px">

                            <?php $__errorArgs = ['imageAr'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> <h5 style="color: red"><?php echo e($message); ?></h5> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            <h3 style="padding-bottom: 10px"><?php echo e(__('text.slideShowAr')); ?></h3>
                            <input class="mail" type="file" name="imageAr" id="imageAr"
                                   style="display: inline-block;padding-top: 10px">

                            <?php $__errorArgs = ['show'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> <h5 style="color: red"><?php echo e($message); ?></h5> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            <select class="mail" name="show" id="show">
                                <option value="Yes" selected><?php echo e(__('text.showImage')); ?></option>
                                <option value="No"><?php echo e(__('text.hideImage')); ?></option>
                            </select>

                            <?php echo csrf_field(); ?>
                            <div>
                                <button class="submit-button" style="width: 60%;">
                                    <?php echo e(__('text.addImage')); ?>

                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        <?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
<?php echo $__env->make('includes.footerScripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>
</html>
<?php /**PATH C:\Users\ZAINAB ALMOOT\inspower\resources\views/slideShow/create.blade.php ENDPATH**/ ?>