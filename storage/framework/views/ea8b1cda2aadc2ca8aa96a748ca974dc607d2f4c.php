<html lang="<?php echo e(__('text.lang')); ?>" dir="<?php echo e(__('text.dir')); ?>">
<?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<body>


<div class="wrapper">
    <!-- Main Section-->
    <div class="main app" id="main">
        <!-- Subscribe Form -->
        <div class="forms-section" id="subscribe" style="min-height: 100%">
            <div class="container">
                <div class="cta-inner">
                    <div class="row">
                        <h1 style="text-align: center; float: none; margin-top: 10px"> <?php echo e(__('text.upgrade')); ?> </h1>
                    </div>
                    <div class="wow fadeInUp" data-wow-delay="0.3s">
                        <form class="forms-form center-form wow zoomIn" method="POST" action="<?php echo e(route('register')); ?>">
                            <div class="col-sm-12">
                                <div class="col-sm-6" style="padding: 15px">
                                    <div class="pricing-col">
                                        <h4><input type="radio" name="type" value="month" style="height: 20px"> <?php echo nl2br(__('text.monthlyPlan')); ?></h4>
                                        <h2><i class="ion-checkmark" style="color: green"></i> <?php echo nl2br(__('text.free1')); ?></h2>
                                        <h2><i class="ion-checkmark" style="color: green"></i> <?php echo nl2br(__('text.monthly1')); ?></h2>
                                        <h2><i class="ion-checkmark" style="color: green"></i> <?php echo nl2br(__('text.monthly2')); ?></h2>
                                        <h2><i class="ion-checkmark" style="color: green"></i> <?php echo nl2br(__('text.monthly3')); ?></h2>
                                        <h2><i class="ion-checkmark" style="color: green"></i> <?php echo nl2br(__('text.monthly4')); ?></h2>
                                        <h3><i class="ion-social-usd" style="color: #ee2724"></i> <?php echo nl2br(__('text.monthlyPlanPricing')); ?></h3>
                                    </div>
                                </div>

                                <div class="col-sm-6" style="padding: 15px">
                                    <div class="pricing-col">
                                        <h4><input type="radio" name="type" value="year" style="height: 20px"> <?php echo nl2br(__('text.yearlyPlan')); ?></h4>
                                        <h2><i class="ion-checkmark" style="color: green"></i> <?php echo nl2br(__('text.free1')); ?></h2>
                                        <h2><i class="ion-checkmark" style="color: green"></i> <?php echo nl2br(__('text.monthly1')); ?></h2>
                                        <h2><i class="ion-checkmark" style="color: green"></i> <?php echo nl2br(__('text.monthly2')); ?></h2>
                                        <h2><i class="ion-checkmark" style="color: green"></i> <?php echo nl2br(__('text.monthly3')); ?></h2>
                                        <h2><i class="ion-checkmark" style="color: green"></i> <?php echo nl2br(__('text.monthly4')); ?></h2>
                                        <h3><i class="ion-social-usd" style="color: #ee2724"></i> <?php echo nl2br(__('text.yearlyPlanPricing')); ?></h3>
                                    </div>
                                </div>
                            </div>

                            <div>
                                <button class="submit-button"
                                        style="width:60%; margin-top: 30px">
                                    <?php echo e(__('text.paymentBtn')); ?>

                                </button>
                            </div>
                            <?php echo csrf_field(); ?>
                        </form>

                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->


<!-- Jquery and Js Plugins -->
<?php echo $__env->make('includes.footerScripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

</body>
</html>
<?php /**PATH C:\Users\ZAINAB ALMOOT\inspower\resources\views/auth/registerUpgrade.blade.php ENDPATH**/ ?>