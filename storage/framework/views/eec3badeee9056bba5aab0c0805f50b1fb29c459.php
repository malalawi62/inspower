<?php
    $images = \Illuminate\Support\Facades\DB::table('slide_shows')->where('show', 'Yes')->get();
?>

<!DOCTYPE html>
<html lang="<?php echo e(__('text.lang')); ?>" dir="<?php echo e(__('text.dir')); ?>">

<?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<body>
<script>
    var msg = '<?php echo e(Session::get('alert')); ?>';
    var exist = '<?php echo e(Session::has('alert')); ?>';
    if (exist) {
        alert(msg);
    }
</script>
<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="wrapper">
    <!-- Main Section-->
    <div class="main app form" id="main">
        <?php if($images != null): ?>
            <div id="slide">
                <div class="slideshow-container">
                    <?php $__currentLoopData = $images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="mySlides fadeSlideShow"> <img <?php if((App::isLocale('ar'))): ?> src="storage/<?php echo e($image->imageAr); ?>" <?php else: ?> src="storage/<?php echo e($image->imageEng); ?>" <?php endif; ?> style="width:100%"> </div>
                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
                <br>
                <div style="text-align:center">
                    <?php for($i=0; $i<count($images); $i++): ?>
                        <span class="dot" onclick="currentSlide(<?php echo e($i+1); ?>)"></span>
                    <?php endfor; ?>
                </div>
            </div>
        <?php endif; ?>
        <!-- About Section -->
        <div class="split-features" id="about">
            <div class="col-md-6 nopadding">
                <div class="split-image">
                    <img class="img-responsive wow fadeIn" src="images/about.jpg" alt="about Us"/>
                </div>
            </div>
            <div class="col-md-6 nopadding">
                <div class="split-content">
                    <h1 class="wow fadeInUp"> <?php echo e(__('text.aboutTitle')); ?> </h1>
                    <p class="wow fadeInUp">
                        <?php echo nl2br(__('text.aboutText')); ?>

                    </p>
                </div>
            </div>
        </div>

        <div class="line" style="margin-top: 5px"></div>

        <!-- Teams Section -->
        <div class="pricing-section text-center" id="team">
            <div class="container">
                <div class="col-md-12 col-sm-12 nopadding">
                    <div class="pricing-intro">
                        <h1 class="wow fadeInUp" data-wow-delay="0s"><?php echo e(__('text.teamTitle')); ?></h1>
                    </div>
                    <div class="">
                        <div class="table-left wow fadeInUp" data-wow-delay="0.4s">
                            <div class="pricing-details">
                                <div style="display: inline-block">
                                    <span class="span"><?php echo e(__('text.talalName')); ?></span>
                                    <div class="col-sm-4">
                                        <img class="img-team wow fadeIn img-circle" src="images/Talal_Nabeel.jpeg"
                                             style="width: 90%"/>
                                    </div>
                                    <div class="col-sm-8">
                                        <h2><?php echo nl2br(__('text.talalInfo')); ?></h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="">
                        <div class="table-left wow fadeInUp" data-wow-delay="0.4s">
                            <div class="pricing-details">
                                <div style="display: inline-block">
                                    <span class="span"><?php echo e(__('text.monaName')); ?></span>
                                    <div class="col-sm-4">
                                        <img class="img-team wow fadeIn img-circle" src="images/Mona_Faraj.jpeg"
                                             style="width: 90%"/>
                                    </div>
                                    <div class="col-sm-8">
                                        <h2><?php echo nl2br(__('text.monaInfo')); ?></h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="">
                        <div class="table-left wow fadeInUp" data-wow-delay="0.4s">
                            <div class="pricing-details">
                                <div style="display: inline-block">
                                    <span class="span"><?php echo e(__('text.hasanName')); ?></span>
                                    <div class="col-sm-4">
                                        <img class="img-team wow fadeIn img-circle" src="images/Hasan_Aradi.jpeg"
                                             style="width: 90%"/>
                                    </div>
                                    <div class="col-sm-8">
                                        <h2><?php echo nl2br(__('text.hasanInfo')); ?></h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Services Section -->
        <div class="split-features" id="services"
             style="box-shadow: 0 3px 40px 6px rgba(0, 0, 0, 0.2); background: linear-gradient(to right, rgba(45, 50, 47, 0.9), rgba(45, 50, 47, 0.3)), url(<?php echo e(asset('images/services.jpg')); ?>) no-repeat center center;
                 background-size: cover;">
            <div class="col-md-12 nopadding">
                <div class="split-content">
                    <div class="split-content-item">
                        <h1 class="wow fadeInUp" data-wow-delay="0s" style="color: #FFF"><?php echo e(__('text.servicesTitle')); ?></h1>
                        <h2 class="wow fadeInUp" data-wow-delay="0s"><?php echo e(__('text.personalTitle')); ?></h2>
                        <h3 class="wow fadeInUp" data-wow-delay="0.2s"><?php echo e(__('text.personal1')); ?></h3>
                        <h3 class="wow fadeInUp" data-wow-delay="0.2s"><?php echo e(__('text.personal2')); ?></h3>
                        <h3 class="wow fadeInUp" data-wow-delay="0.2s"><?php echo e(__('text.personal3')); ?></h3>
                        <div class="white-line"></div>
                        <h2 class="wow fadeInUp" data-wow-delay="0s"><?php echo e(__('text.otherTitle')); ?></h2>
                        <h3 class="wow fadeInUp" data-wow-delay="0.2s"><?php echo e(__('text.other1')); ?></h3>
                        <h3 class="wow fadeInUp" data-wow-delay="0.2s"><?php echo e(__('text.other2')); ?></h3>
                        <h3 class="wow fadeInUp" data-wow-delay="0.2s"><?php echo e(__('text.other3')); ?></h3>
                    </div>
                </div>
            </div>
        </div>

        <div class="line"></div>

        <!-- Contact Section -->
        <div class="container" style="background-color: #FFF; width: 100%;" id="contact">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-md-6" style="float: right;">

                        <form class="subscribe-form wow zoomIn" action="/subscribe" method="post">
                            <div class="pricing-intro" style="text-align: center; float: none">
                                <h1 class="wow fadeInUp" data-wow-delay="0s" ><?php echo e(__('text.contactMsgTitle')); ?></h1>
                            </div>
                            <input class="smallInput" type="text" name="name" placeholder="<?php echo e(__('text.inputNamePlaceholder')); ?>"
                                   autocomplete="off" id="name">
                            <input class="smallInput" type="text" name="email" placeholder="<?php echo e(__('text.inputPlaceholder')); ?>"
                                   autocomplete="off" id="email">
                            <select class="mail" name="writer_id" id="category" style="width: 90.5%; margin-bottom: 10px">
                                <option value="" hidden> <?php echo e(__('text.chooseTopic')); ?> </option>
                                <option value="<?php echo e(__('text.topic1')); ?>"><?php echo e(__('text.topic1')); ?></option>
                                <option value="<?php echo e(__('text.topic2')); ?>"><?php echo e(__('text.topic2')); ?></option>
                                <option value="<?php echo e(__('text.topic3')); ?>"><?php echo e(__('text.topic3')); ?></option>
                                <option value="<?php echo e(__('text.topic4')); ?>"><?php echo e(__('text.topic4')); ?></option>
                                <option value="<?php echo e(__('text.topic5')); ?>"><?php echo e(__('text.topic5')); ?></option>
                                <option value="<?php echo e(__('text.topic6')); ?>"><?php echo e(__('text.topic6')); ?></option>
                                <option value="<?php echo e(__('text.otherTopic')); ?>"><?php echo e(__('text.otherTopic')); ?></option>
                            </select>
                            <input class="mail" type="text" name="subject" placeholder="<?php echo e(__('text.subject')); ?>"
                                   autocomplete="off" id="email" style="width: 90.5%; margin-bottom: 10px">
                            <textarea class="mail" style="width: 90.5%; color: #2D322F; height: 120px"><?php echo e(__('text.enterMessage')); ?></textarea>
                            <?php echo csrf_field(); ?>
                            <button class="submit-button" style="width: 96%; margin-top: 30px; margin-bottom: 15px">
                                <?php echo e(__('text.sendMessage')); ?>

                            </button>
                        </form>
                    </div>
                    <div class="col-md-6 subscribe-form" style="float: left">
                        <div class="pricing-intro hide-small-devices" style="text-align: center; float: none">
                            <h1 class="wow fadeInUp" data-wow-delay="0s" ><?php echo e(__('text.contactTitle')); ?></h1>
                        </div>
                        <div class="row social-item">
                            <div class="social-icons">
                                <a ><i class="ion-location"></i></a>
                            </div>
                            <div class="social-text">
                                <h2> Manama, Kingdom of Bahrain</h2>
                            </div>
                        </div>

                        <div class="row social-item">
                            <div class="social-icons">
                                <a href="https://wa.me/97339188159"><i class="ion-social-whatsapp"></i></a>
                            </div>
                            <div class="social-text">
                                <h2> <a href="https://wa.me/97339188159" class="link">(+973) 39188159</a></h2>
                            </div>
                        </div>

                        <div class="row social-item">
                            <div class="social-icons">
                                <a href="https://www.instagram.com/inspower_pt"><i class="ion-social-instagram"></i></a>
                            </div>
                            <div class="social-text" style="text-align: left">
                                <h2> <a href="https://www.instagram.com/inspower_pt" class="link"> @inspower_pt  </a></h2>
                            </div>
                        </div>

                        <div class="row social-item">
                            <div class="social-icons">
                                <a ><i class="ion-email"></i></a>
                            </div>
                            <div class="social-text" style="text-align: left">
                                <h2> inspowerpt@gmail.com</h2>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>

        <div class="line"></div>

        <!-- Subscribe Form -->
        <div class="subscribe-section no-color" id="subscribe">
            <div class="container">
                <div class="cta-inner">
                    <div class="form wow fadeInUp" data-wow-delay="0.3s">
                        <h2 style="margin-bottom: 20px; color: #2D322F; font-size: 36px; font-weight: 900; line-height: 1.2;;"> <?php echo e(__('text.subscribeTitle')); ?> </h2>
                        <form class="subscribe-form center-form wow zoomIn" action="/subscribe" method="post">
                            <input class="smallInput" type="text" name="email" placeholder="<?php echo e(__('text.inputPlaceholder')); ?>"
                                   autocomplete="off" id="email">
                            <input class="smallInput" type="text" name="name" placeholder="<?php echo e(__('text.inputNamePlaceholder')); ?>"
                                   autocomplete="off" id="name">
                            <h2 style="margin-bottom: 10px; font-size: 18px"> <?php echo e(__('text.chooseLanguage')); ?> </h2>
                            <div class="col-md-12">
                                <input type="checkbox" name="ar" value="yes"
                                       autocomplete="off" id="ar" style="color: #8c0000; margin-bottom: 10px">  استلام المحتوى باللغة العربية
                            </div>
                            <div class="col-md-12">
                                <input type="checkbox" name="eng" value="yes"
                                       autocomplete="off" id="ar" style="color: #8c0000; margin-bottom: 10px"> Receive content in English
                            </div>
                            <?php echo csrf_field(); ?>
                            <button class="submit-button" style="width: 96%; margin-top: 30px">
                                <?php echo e(__('text.button')); ?>

                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
        <?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
<?php echo $__env->make('includes.footerScripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>
</html>
<?php /**PATH C:\Users\Maryam Alalawi\inspower\resources\views/welcome.blade.php ENDPATH**/ ?>