<html lang="<?php echo e(__('text.lang')); ?>" dir="<?php echo e(__('text.dir')); ?>">

<?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<body>
<script>
    var msg = '<?php echo e(Session::get('alert')); ?>';
    var exist = '<?php echo e(Session::has('alert')); ?>';
    if (exist) {
        alert(msg);
    }
</script>
<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="wrapper" style="background-color: #2D322F">
    <!-- Main Section-->
    <div class="main app form" id="main">
        <div class="pricing-section text-center" style="min-height: 100%">
            <div class="container">
                <div class="col-md-8 col-md-offset-2 col-sm-12 nopadding text-center" style="float: left">
                    <div class="pricing-intro">
                        <h1 class="wow fadeInUp" data-wow-delay="0s"> <?php echo e(__('text.allProgramsTitle')); ?> </h1>
                        <?php if((App::isLocale('en'))): ?>
                            <h2>For programs in Arabic language <a href="setlocale/ar">press here</a></h2>
                        <?php endif; ?>
                        <?php if((App::isLocale('ar'))): ?>
                            <h2>للبرامج التدريبية باللغة الإنجليزية <a href="setlocale/en">اضغط هنا</a></h2>
                        <?php endif; ?>
                    </div>
                    <?php $__empty_1 = true; $__currentLoopData = $programs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $program): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                        <div class="table-left wow fadeInUp" data-wow-delay="0.4s" style="overflow: auto;">
                            <div class="pricing-details">
                                <div style="display: block">
                                    <div class="col-md-4">
                                        <img class="show-img wow fadeIn"
                                             src="<?php echo e(asset('storage/male_avatar.png')); ?>"
                                             style="padding-bottom: 10px"/>
                                    </div>
                                    <br>
                                    <div class="col-md-8">
                                        <div class="row">
                                                <span class="video-title">
                                                    <a style="color: #fdca00; font-size:20px"
                                                       href="/program/<?php echo e($program->id); ?>_<?php echo e(str_replace(' ', '_', $program->title)); ?>">
                                                        <?php echo e($program->title); ?>

                                                    </a>
                                                </span>
                                            <?php if(auth()->guard()->guest()): ?>
                                            <?php else: ?>
                                                <form action="/program/<?php echo e($program->id); ?>" method="post">
                                                    <?php echo method_field('DELETE'); ?>
                                                    <span class="editDeleteIcons">
                                                        <a href="/program/<?php echo e($program->id); ?>/edit"
                                                           style="color: #fdca00">
                                                            <i class="ion-edit"></i>
                                                        </a>
                                                        <a href="/sendProgram/<?php echo e($program->id); ?>"
                                                           style="color: #fdca00">
                                                            <i class="ion-android-send"></i>
                                                        </a>
                                                        <button
                                                            onclick="return confirm('Are you sure you want to delete the program?')"
                                                            style="color: #fdca00; background-color: transparent; border-width: 0; font-size: 24px">
                                                            <i class="ion-android-delete"></i>
                                                        </button>
                                                    </span>
                                                    <?php echo csrf_field(); ?>
                                                </form>
                                            <?php endif; ?>
                                        </div>
                                        <h2 class="articleBody">
                                            <?php echo nl2br($program->overview); ?>

                                        </h2>
                                        <form action="/cart" method="post">
                                            <input type="hidden" name="program_id" value="<?php echo e($program->id); ?>">
                                            <input type="hidden" name="user_id" value="<?php echo e(auth()->user()->id); ?>">
                                            <button class="btn btn-primary"
                                                    style="background-color: #EC3642; border:0; padding: 5px 20px 5px 20px; <?php echo e(__('text.floating')); ?>" formaction="/program/<?php echo e($program->id); ?>" formmethod="get">
                                                <?php echo e(__('text.learnMoreBtn')); ?>

                                            </button>
                                            <button class="btn btn-primary" type="submit"
                                                    style="background-color: #EC3642; border:0; padding: 5px 20px 5px 20px; <?php echo e(__('text.opFloating')); ?>">
                                                <?php echo e(__('text.addToCartBtn')); ?>

                                            </button>
                                            <?php echo csrf_field(); ?>
                                        </form>
                                    </div>
                                    <br>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                        <h2>No Programs Found</h2>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <!-- Footer Section -->
        <?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
<?php echo $__env->make('includes.footerScripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>
</html>

<?php /**PATH /Users/maryam/inspower/resources/views/programs/index.blade.php ENDPATH**/ ?>