<script type="text/javascript" src="<?php echo e(asset('js/jquery-2.1.1.js')); ?>"></script>
<script src="<?php echo e(asset('js/jquery.subscribe.js')); ?>"></script>
<script src="<?php echo e(asset('js/app.js')); ?>" defer></script>
<script type="text/javascript" src="<?php echo e(asset('js/bootstrap.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('js/plugins.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('js/menu.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('js/custom.js')); ?>"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAJ5BZftoKtBStvTZ_6NZy0eIqDuR6h5UM&callback=initMap"></script>
<?php /**PATH /home2/inspower/inspower/resources/views/includes/footerScripts.blade.php ENDPATH**/ ?>