@if ($paginator->hasPages())
    <nav dir='rtl'>
        <ul class="pagination">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <li style="float: right;" class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.next')">
                    <span class="page-link" aria-hidden="true" style="color: #8c0000 ;border-color: #8c0000">&lsaquo;</span>
                </li>
            @else
                <li style="float: right;" class="page-item">
                    <a class="page-link" href="{{ $paginator->previousPageUrl() }}" rel="next" aria-label="@lang('pagination.next')"style="color: #8c0000 ;border-color: #8c0000">&lsaquo;</a>
                </li>
            @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <li style="float: right;" class="page-item disabled" aria-disabled="true"><span class="page-link" style="color: #8c0000 ;border-color: #8c0000">{{ $element }}</span></li>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li style="float: right;" class="page-item active" aria-current="page"><span class="page-link" style="color: #8c0000 ;border-color: #8c0000; background-color: #B2C6BC;">{{ $page }}</span></li>
                        @else
                            <li style="float: right;"class="page-item"><a class="page-link" href="{{ $url }}" style="color: #8c0000 ;border-color: #8c0000">{{ $page }}</a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li style="float: right;"class="page-item">
                    <a class="page-link" href="{{ $paginator->nextPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')" style="color: #8c0000 ;border-color: #8c0000">&rsaquo;</a>
                </li>
            @else
                <li style="float: right;"class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.previous')">
                    <span class="page-link" aria-hidden="true" style="color: #8c0000 ;border-color: #8c0000">&rsaquo;</span>
                </li>
            @endif
        </ul>
    </nav>
@endif
